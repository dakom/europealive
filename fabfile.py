"""
Fabric file to manage deployment.
NOTE: This uses python 2.7, cannot be installed in project virtualenv.
"""
# Fabric is python 2.7, so we disable the normal pylint
#pylint: skip-file
import os
import time
from contextlib import contextmanager as _contextmanager

from fabric import api
from fabric.api import abort, env, local, run, settings, sudo
from fabric.context_managers import cd, prefix
from fabric.contrib.console import confirm

api.env.hosts = ['46.101.162.210']   # master server

# NB! No trailing slashes
ENV_DIR =      '/var/www/europealive/.virtualenv'
PROJECT_DIR =  '/var/www/europealive'
BACKEND_DIR =  '/var/www/europealive/backend'
FRONTEND_DIR = '/var/www/europealive/frontend'

# Use the local .ssh/config
env.use_ssh_config = True

env.virtualenv = ENV_DIR
env.activate = 'source %(virtualenv)s/bin/activate' % env
env.code_dir = BACKEND_DIR
env.user = 'deploy'


@_contextmanager
def backend_env():
    with cd(env.virtualenv), prefix(env.activate), cd(env.code_dir):
        yield

@_contextmanager
def frontend_env():
    with cd(FRONTEND_DIR):
        yield


def frontend_install():
    """
    Update node_modules according to package.json
    """
    with frontend_env():
        run('npm install')

def frontend_build():
    """ Build Reason code and collect with Webpack. """
    with frontend_env():
        run("npm run clean")
        run("npm run build")
        run("npx webpack")


def pip_install(upgrade=False):
    """
    Pulls the latest master branch
    """
    with backend_env():
        if upgrade:
            run('pip install -r requirements.txt --upgrade')
        else:
            run('pip install -r requirements.txt')


def git_pull():
    """
    Pulls the latest master branch
    """
    with cd(PROJECT_DIR):
        run('git pull origin master')


def migrate():
    """
    Migrates the project
    """
    with backend_env():
        run('./manage.py migrate')


def debug():
    """ Put commands here to diagnose the server environment """
    with backend_env():
        run('python --version')
        run('which python')
        run('locale')


def collectstatic():
    """
    Runs collectstatic on the remote project
    """
    with backend_env():
        run('./manage.py collectstatic --noinput')

def compilemessages():
    """
    Runs django compilemessages on the remote project
    """
    with backend_env():
        run('./manage.py compilemessages')


def generate_graphql_schema():
    """ Generate Graphql schema from Graphene """
    with backend_env():
        run("./manage.py graphql_schema")


def reload_uwsgi(chain=True):
    """ Reload uwsgi to restart Django server """
    # Use chain reloading to minimize impact on active users
    command = "c" if chain else "r"
    api.run("echo %s | sudo -u www-data tee /tmp/europealive_uwsgi_fifo" % command)


def test():
    with settings(warn_only=True):
        result = local("./manage.py test", capture=False)
    if result.failed and not confirm("Test failed, continue anyway?"):
        abort("Aborting...")


def deploy():
    """
    Runs all commands necessary to deploy either front- or backend changes
    """
    git_pull()
    pip_install()
    migrate()
    compilemessages()
    generate_graphql_schema()

    frontend_install()
    frontend_build()

    collectstatic()

    reload_uwsgi(chain=False)
