# EuropeAlive Game source code

Repo for europealive.org is [here](https://bitbucket.org/dakom/europealive-webpage)

### Setup
Assuming for each section that the current directory is the project root.

##### Frontend
Install dependencies
```
cd frontend
npm install
```

##### Backend
```
cd backend
```
Setup a virtual environment:
```
python -m venv venv
source venv/bin/activate
```

Install dependencies:
```
pip install -r requirements.txt
```

##### Git
Create a `pre-commit` hook setup to run pylint.

Add the following to `.git/hooks/pre-commit`:
```
#!/usr/bin/env bash
git-pylint-commit-hook
```
Then make sure `pre-commit` is executable:
```
chmod +x .git/hooks/pre-commit
```


### Development
This section describes startup of processes for development.

##### Backend
```
cd backend
source venv/bin/activate
./manage.py runserver
```

The project uses `pylint` via `git-pylint-commit-hook`.
Add the following to a pre commit hook:
```
#!/usr/bin/env bash
git-pylint-commit-hook --ignore [0-9]{4}_.*py
```

The configuration of `pylint` is defined in `.pylintrc` in the project root
directory.

##### Frontend
In two separate terminals:
```
cd frontend
npm start
```
and
```
cd frontend
npm run webpack
```


## Internationalization
All translation is done through Crowdin using the
[gettext](https://www.gnu.org/software/gettext/) method. Gathering all
translation work into one system requires several different systems to work together.

### Backend
Static strings are extracted using Django's built-in system. A lot of text is stored
in database model fields. We have chosen to route these into django using
[django-vinaigrette](https://github.com/ecometrica/django-vinaigrette).
This means that you MUST have the right database, usually downloaded from the server,
before running `./manage.py makemessages`.

### Frontend
[See here](frontend/translation.md) for explanation of frontend translation choice.

Extraction is done by running the `/backend/makemessages_frontend.sh` script.

This extracts strings using Django `makemessages -d djangojs`. They are then converted by Django to javascript, and translations are looked up clientside by Gettext.re, which is a binding to Django's JavascriptCatalog.

### Synchronizing with Crowdin
Documentation for using Crowdin from the CLI can be found [here](https://support.crowdin.com/cli-tool/).
The main commands are 
```
crowdin upload sources
```
to upload new translation strings, and
```
crowdin download
```
to download new translations produced in Crowdin.
The  `--dryrun` argument is useful in both cases to preview the effect of running the command. Note that a path to the Crowdin credentials must be passed to the above commandes with `--identity <path_to_credentials>`.

## Compiling translations
Translations are compiled by running `./manage.py compilemessages`

### Automatic Game server creation

Game creation is handled by the *master* server, which is IP 46.101.162.210, serving domain www.europatwork.org.
This server cannot host a game, it only serves to create games, and perform any changes to database models.
When a new game server is needed, the master server image is copied as a basis for the new server.


A game server is automatically created from each GameLauncher, when its create_at date arrives. What happens is:

- The game_launcher script runs on the master server:
    - A snapshot of the master server is created.
    - A new droplet is started
    - A config file is written to the slave at `backend/core/settings/slave_env.py`.
    - A DNS record is added to point the new subdomain to the new server.
- The game_creater script runs on the slave server, which sets up the Game and Round models.

In order for the game_creater script to run, there **must not** be a Game object in the database.
This means the master server should never have a Game or Rounds, since they will be copied to slaves. Therefore, these models are hidden in the admin on the master.

The DigitalOcean api is used to facilitate this. The key is in `/var/www/europealive/.secrets/` and is from the Dansk Kommunikation account. The key is also used by certbot to automatically renew the Let's Encrypt wildcard certificate.
