Require.css("./style/style.scss");

Sentry.init(
  Sentry.sentry,
  Sentry.config(
    ~dsn="https://a4cc533ecc144818b74b62ee0d55ada5@sentry.io/1548800",
  ),
);

ReactDOMRe.renderToElementWithId(<App />, "index");
