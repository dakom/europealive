type createResponse('a) =
  | Created('a)
  | BadRequest(string);

type updateResponse('a) =
  | Updated('a)
  | BadRequest(string);

type destroyResponse =
  | Destroyed
  | NotFound;

let retrieveCurrentRound = callback =>
  Js.Promise.(
    Fetch.fetch("/data/current_round/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.round(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listDirective = callback =>
  Js.Promise.(
    Fetch.fetch("/data/directive/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.directive, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveDirective = (id: GenData.id(GenData.directive), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/directive/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.directive(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listDirectiveArticle = callback =>
  Js.Promise.(
    Fetch.fetch("/data/directive_template/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.directiveArticle, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveDirectiveArticle =
    (id: GenData.id(GenData.directiveArticle), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/directive_template/"
      ++ string_of_int(GenData.idToInt(id))
      ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.directiveArticle(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listGame = callback =>
  Js.Promise.(
    Fetch.fetch("/data/games/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Json.Decode.array(GenData.Decode.game, json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveGame = (id: GenData.id(GenData.game), callback) =>
  Js.Promise.(
    Fetch.fetch("/data/games/" ++ string_of_int(GenData.idToInt(id)) ++ "/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.game(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listTeam = callback =>
  Js.Promise.(
    Fetch.fetch("/data/ip_recipient_teams/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Json.Decode.array(GenData.Decode.team, json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveTeam = (id: GenData.id(GenData.team), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/ip_recipient_teams/"
      ++ string_of_int(GenData.idToInt(id))
      ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.team(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listMediaHouse = callback =>
  Js.Promise.(
    Fetch.fetch("/data/media_houses/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.mediaHouse, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveMediaHouse = (id: GenData.id(GenData.mediaHouse), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/media_houses/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.mediaHouse(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listNewsArticle = callback =>
  Js.Promise.(
    Fetch.fetch("/data/news_articles/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.newsArticle, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveNewsArticle = (id: GenData.id(GenData.newsArticle), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/news_articles/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.newsArticle(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listNewsTelegram = callback =>
  Js.Promise.(
    Fetch.fetch("/data/news_telegrams/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.newsTelegram, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveNewsTelegram = (id: GenData.id(GenData.newsTelegram), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/news_telegrams/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.newsTelegram(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listNewsfeed = callback =>
  Js.Promise.(
    Fetch.fetch("/data/newsfeed/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.newsItemProxy, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listPointsAssigned = callback =>
  Js.Promise.(
    Fetch.fetch("/data/points_awarded/")
    |> then_(Fetch.Response.json)
    |> then_(json =>
         resolve(Json.Decode.array(GenData.Decode.pointsAssigned, json))
       )
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrievePointsAssigned =
    (id: GenData.id(GenData.pointsAssigned), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/points_awarded/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.pointsAssigned(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listRound = callback =>
  Js.Promise.(
    Fetch.fetch("/data/rounds/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Json.Decode.array(GenData.Decode.round, json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveRound = (id: GenData.id(GenData.round), callback) =>
  Js.Promise.(
    Fetch.fetch(
      "/data/rounds/" ++ string_of_int(GenData.idToInt(id)) ++ "/",
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.round(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let listUser = callback =>
  Js.Promise.(
    Fetch.fetch("/data/users/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Json.Decode.array(GenData.Decode.user, json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );

let retrieveUser = (id: GenData.id(GenData.user), callback) =>
  Js.Promise.(
    Fetch.fetch("/data/users/" ++ string_of_int(GenData.idToInt(id)) ++ "/")
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(GenData.Decode.user(json)))
    |> then_(data => resolve(callback(data)))
    |> catch(err => resolve(Js.Console.warn(err)))
    |> ignore
  );
