/* Type for database ID wrapping int. Using a phantom type for safety. */
type id('a) =
  | Id(int);

let idToInt = (Id(inner)) => inner;
type round = {
  active: bool,
  directive: option(id(directive)),
  game: id(game),
  id: id(round),
  length: int,
}
and articleSelection = {
  article: id(directiveArticle),
  id: id(articleSelection),
  itemsSelected: int,
}
and directive = {
  id: id(directive),
  selections: array(articleSelection),
}
and directiveItem = {
  id: id(directiveItem),
  number: int,
  text: string,
}
and directiveArticle = {
  description: string,
  id: id(directiveArticle),
  items: array(directiveItem),
  number: int,
  shortDescription: string,
}
and game = {
  id: id(game),
  name: string,
  url: string,
}
and student = {
  email: string,
  firstName: string,
  id: id(student),
  lastName: string,
  team: id(team),
}
and team = {
  id: id(team),
  influencePoints: int,
  logo: string,
  name: string,
  startingRoom: string,
  students: array(student),
}
and mediaHouse = {
  id: id(mediaHouse),
  influencePoints: int,
  logo: string,
  name: string,
  startingRoom: string,
}
and newsArticle = {
  bodyText: string,
  headline: string,
  id: id(newsArticle),
  isBreakingNews: bool,
  isDraft: bool,
  isOpinionPiece: bool,
  leadText: string,
  logo: string,
  publishingTime: option(MomentRe.Moment.t),
}
and newsTelegram = {
  bodyText: string,
  headline: string,
  id: id(newsTelegram),
  isBreakingNews: bool,
  isDraft: bool,
  logo: string,
  publishingTime: option(MomentRe.Moment.t),
}
and newsItemProxy = {
  bodyText: string,
  headline: string,
  id: id(newsItemProxy),
  isBreakingNews: bool,
  kind: string,
  mediahouseLogo: string,
}
and pointsAssigned = {
  amount: int,
  article: id(directiveArticle),
  id: id(pointsAssigned),
  round: id(round),
  team: id(team),
}
and user = {
  email: string,
  firstName: string,
  id: id(user),
  isStaff: bool,
  lastName: string,
};
/* Matches Django model behaviour of empty string meaning lack of data */
let nonEmptyString =
  Json.(Decode.map(str => str == "" ? None : Some(str), Decode.string));

/* Optional decoder which only returns None if json is null. */
let optionalStrict = (decoder, json) =>
  Js.testAny(json) ? None : Some(decoder(json));
module Decode = {
  open Json;
  let round = json => {
    active: json |> Decode.field("active", Decode.bool),
    directive:
      json
      |> Decode.field(
           "directive",
           optionalStrict(Decode.map(i => Id(i), Decode.int)),
         ),
    game: json |> Decode.field("game", Decode.map(i => Id(i), Decode.int)),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    length: json |> Decode.field("length", Decode.int),
  };
  let articleSelection = json => {
    article:
      json |> Decode.field("article", Decode.map(i => Id(i), Decode.int)),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    itemsSelected: json |> Decode.field("items_selected", Decode.int),
  };
  let directive = json => {
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    selections:
      json |> Decode.field("selections", Decode.array(articleSelection)),
  };
  let directiveItem = json => {
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    number: json |> Decode.field("number", Decode.int),
    text: json |> Decode.field("text", Decode.string),
  };
  let directiveArticle = json => {
    description: json |> Decode.field("description", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    items: json |> Decode.field("items", Decode.array(directiveItem)),
    number: json |> Decode.field("number", Decode.int),
    shortDescription:
      json |> Decode.field("short_description", Decode.string),
  };
  let game = json => {
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    name: json |> Decode.field("name", Decode.string),
    url: json |> Decode.field("url", Decode.string),
  };
  let student = json => {
    email: json |> Decode.field("email", Decode.string),
    firstName: json |> Decode.field("first_name", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    lastName: json |> Decode.field("last_name", Decode.string),
    team: json |> Decode.field("team", Decode.map(i => Id(i), Decode.int)),
  };
  let team = json => {
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    influencePoints: json |> Decode.field("influence_points", Decode.int),
    logo: json |> Decode.field("logo", Decode.string),
    name: json |> Decode.field("name", Decode.string),
    startingRoom: json |> Decode.field("starting_room", Decode.string),
    students: json |> Decode.field("students", Decode.array(student)),
  };
  let mediaHouse = json => {
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    influencePoints: json |> Decode.field("influence_points", Decode.int),
    logo: json |> Decode.field("logo", Decode.string),
    name: json |> Decode.field("name", Decode.string),
    startingRoom: json |> Decode.field("starting_room", Decode.string),
  };
  let newsArticle = json => {
    bodyText: json |> Decode.field("body_text", Decode.string),
    headline: json |> Decode.field("headline", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    isBreakingNews: json |> Decode.field("is_breaking_news", Decode.bool),
    isDraft: json |> Decode.field("is_draft", Decode.bool),
    isOpinionPiece: json |> Decode.field("is_opinion_piece", Decode.bool),
    leadText: json |> Decode.field("lead_text", Decode.string),
    logo: json |> Decode.field("logo", Decode.string),
    publishingTime:
      json
      |> Decode.field(
           "publishing_time",
           optionalStrict(
             Decode.map(s => MomentRe.moment(s), Decode.string),
           ),
         ),
  };
  let newsTelegram = json => {
    bodyText: json |> Decode.field("body_text", Decode.string),
    headline: json |> Decode.field("headline", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    isBreakingNews: json |> Decode.field("is_breaking_news", Decode.bool),
    isDraft: json |> Decode.field("is_draft", Decode.bool),
    logo: json |> Decode.field("logo", Decode.string),
    publishingTime:
      json
      |> Decode.field(
           "publishing_time",
           optionalStrict(
             Decode.map(s => MomentRe.moment(s), Decode.string),
           ),
         ),
  };
  let newsItemProxy = json => {
    bodyText: json |> Decode.field("body_text", Decode.string),
    headline: json |> Decode.field("headline", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    isBreakingNews: json |> Decode.field("is_breaking_news", Decode.bool),
    kind: json |> Decode.field("kind", Decode.string),
    mediahouseLogo: json |> Decode.field("mediahouse_logo", Decode.string),
  };
  let pointsAssigned = json => {
    amount: json |> Decode.field("amount", Decode.int),
    article:
      json |> Decode.field("article", Decode.map(i => Id(i), Decode.int)),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    round:
      json |> Decode.field("round", Decode.map(i => Id(i), Decode.int)),
    team: json |> Decode.field("team", Decode.map(i => Id(i), Decode.int)),
  };
  let user = json => {
    email: json |> Decode.field("email", Decode.string),
    firstName: json |> Decode.field("first_name", Decode.string),
    id: json |> Decode.field("id", Decode.map(i => Id(i), Decode.int)),
    isStaff: json |> Decode.field("is_staff", Decode.bool),
    lastName: json |> Decode.field("last_name", Decode.string),
  };
};
