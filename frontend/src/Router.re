open Belt;

type teacherRoute =
  | TeamInfo(int)
  | FinalVote
  | Calendar
  | WritePopup
  | ManageJobs
  | Signup
  | TeamScore;

type route =
  | Home
  | Teacher(teacherRoute)
  | NewsReader(NewsReader.kind, int)
  | TransferInfluence
  | MyArticlesOverview
  | WriteArticle(option(Api.Types.id(Api.Types.newsArticle)))
  | TeamInfo
  | IndicateDirective
  | CalendarEntry(int)
  | DirectiveIndications
  | InvestIP
  | Research
  | MajorityCalculator
  | JobDescription
  | MeetingAgenda
  | MyStaff
  | WriteMemo
  | TeamMemoSubmissions
  | PopupOverview
  | NotImplemented
  | PublishDirective
  | Page404;

/* Alias to avoid cyclical reference in Config */
type routeAlias = route;

/* This will become a builtin function once Buclescript updates compiler version */
let stringOfIntOpt = Int.fromString;

module Config = {
  type route = routeAlias;

  let prefix = "app";

  let routeFromUrl = (url: ReasonReact.Router.url) =>
    switch (url.path) {
    | ["app", ...path] =>
      switch (path) {
      | ["teacher", ...teacherPath] =>
        switch (teacherPath) {
        | ["team", id] =>
          switch (stringOfIntOpt(id)) {
          | Some(id) => Teacher(TeamInfo(id))
          | None => Page404
          }
        | ["calendar"] => Teacher(Calendar)
        | ["team-score"] => Teacher(TeamScore)
        | ["write-popup"] => Teacher(WritePopup)
        | ["final-vote"] => Teacher(FinalVote)
        | ["manage-jobs"] => Teacher(ManageJobs)
        | ["secret-signup-page"] => Teacher(Signup)
        | _ => Page404
        }
      | [] => Home
      | ["news-reader", kind, id] =>
        switch (kind, stringOfIntOpt(id)) {
        | ("article", Some(id)) => NewsReader(`ARTICLE, id)
        | ("telegram", Some(id)) => NewsReader(`TELEGRAM, id)
        | _ => Page404
        }
      | ["write-article"] => WriteArticle(None)
      | ["write-article", id] =>
        switch (Int.fromString(id)) {
        | Some(id) => WriteArticle(Some(Api.Types.Id(id)))
        | None => Page404
        }
      | ["transfer-influence"] => TransferInfluence
      | ["my-articles-overview"] => MyArticlesOverview
      | ["team-info"] => TeamInfo
      | ["calendar-item", id] =>
          switch (stringOfIntOpt(id)) {
          | Some(id) => CalendarEntry(id)
          | None => Page404
          }
      | ["indicate-directive"] => IndicateDirective
      | ["directive-indications"] => DirectiveIndications
      | ["invest-ip"] => InvestIP
      | ["research"] => Research
      | ["job-description"] => JobDescription
      | ["majority-calculator"] => MajorityCalculator
      | ["meeting-agenda"] => MeetingAgenda
      | ["my-staff"] => MyStaff
      | ["write-memo"] => WriteMemo
      | ["team-memo-submissions"] => TeamMemoSubmissions
      | ["popup-overview"] => PopupOverview
      | ["not-implemented"] => NotImplemented
      | ["publish-directive"] => PublishDirective
      | _other => Page404
      }
    | _ => Page404
    };

  let routeToUrl = (route: route) =>
    "/"
    ++ prefix
    ++ (
      switch (route) {
      | Home => ""
      | Teacher(teacherRoute) =>
        "/teacher/"
        ++ (
          switch (teacherRoute) {
          | TeamScore => "team-score"
          | Calendar => "calendar"
          | FinalVote => "final-vote"
          | WritePopup => "write-popup"
          | ManageJobs => "manage-jobs"
          | Signup => "secret-signup-page"
          | TeamInfo(id) => "team/" ++ Int.toString(id)
          }
        )
      | NewsReader(kind, id) =>
        "/news-reader/"
        ++ (
          switch (kind) {
          | `ARTICLE => "article/"
          | `TELEGRAM => "telegram/"
          }
        )
        ++ string_of_int(id)
      | TransferInfluence => "/transfer-influence"
      | WriteArticle(None) => "/write-article"
      | WriteArticle(Some(id)) =>
        "/write-article/" ++ Api.Types.idToString(id)
      | MyArticlesOverview => "/my-articles-overview"
      | TeamInfo => "/team-info"
      | CalendarEntry(id) => "/calendar-item/" ++ Int.toString(id)
      | IndicateDirective => "/indicate-directive"
      | DirectiveIndications => "/directive-indications"
      | InvestIP => "/invest-ip"
      | Research => "/research"
      | JobDescription => "/job-description"
      | MajorityCalculator => "/majority-calculator"
      | MeetingAgenda => "/meeting-agenda"
      | MyStaff => "/my-staff"
      | WriteMemo => "/write-memo"
      | TeamMemoSubmissions => "/team-memo-submissions"
      | PopupOverview => "/popup-overview"
      | NotImplemented => "/not-implemented"
      | PublishDirective => "/publish-directive"
      | Page404 => "/page-not-found"
      }
    );
};

include ReRoute.CreateRouter(Config);

let pushRoute = route => route |> Config.routeToUrl |> ReasonReact.Router.push;
