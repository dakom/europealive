open Gettext;
open Belt;
open BsReactstrap;

// This works around ReactDOMRe.objToDOMProps which seems to be where the method_="post" bug is
let mkDOMProps: string => ReactDOMRe.props = [%bs.raw
  {|
  function(className) {
    return {method: "post", action: "/i18n/setlang/", a: "b", className: className}
  }
  |}
];

[@react.component]
let make = (~className=?) =>
  // There is a bug with form method_="post" not generating the right html,
  // we use the createElementVariadic escape hatch.
  ReactDOMRe.createElementVariadic(
    "form",
    ~props=mkDOMProps(Option.getWithDefault(className, "")),
    [|
      <input
        type_="hidden"
        name="csrfmiddlewaretoken"
        value=?{JustgageReasonCookie.Cookie.getAsString("csrftoken")}
      />,
      <Input
        name="language"
        _type="select"
        bsSize="sm"
        onChange={e => Js.log(ReactEvent.Form.target(e)##form##submit())}
        value=currentLanguage>
        {availableLanguages
         ->Js.Dict.entries
         ->Array.map(((languageCode, name)) =>
             <option key=languageCode value=languageCode>
               name->React.string
             </option>
           )
         ->React.array}
      </Input>,
    |],
  );
