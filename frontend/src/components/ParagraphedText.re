open Belt;
// Taken from https://github.com/janl/mustache.js/blob/master/mustache.js#L69
let escapeHtml: string => string = [%bs.raw
  {|
function escapeHtml (string) {
    var entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;'
    };
    return String(string).replace(/[&<>"'\/]/g, function fromEntityMap (s) {
        return entityMap[s];
    });
}
|}
];
// "'\/ This comment counteracts wrong syntax highlighting in vim cascading from the regex above

/* This component HTML-escapes the given text, and wraps all newline-separated
   parts in paragraph tags */
[@react.component]
let make = (~safe=false, ~text) => {
  let paragraphs =
    Js.String.split("\n", text)
    ->Array.mapWithIndex((idx, par) =>
        <p
          key={Int.toString(idx)}
          dangerouslySetInnerHTML={
            "__html":
              if (safe) {
                par;
              } else {
                escapeHtml(par);
              },
          }
        />
      )
    ->React.array;
  <span> paragraphs </span>;
};
