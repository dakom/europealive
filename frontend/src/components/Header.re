open Gettext;
open BsReactstrap;
open Belt;

let logoSrc = Require.file("../img/EuropeAtWork_logo.svg");

[@react.component]
let make = (~user: option(AppState.anyUser), ~team: option(AppState.team)) =>
  <div className="header-wrapper">
    <Container>
      <Row>
        <Col xs=3 className="header-logo">
          <img src=logoSrc className="img-fluid" />
        </Col>
        <Col xs=5 lg=6 className="header-job">
          {JSX.ifSome(team, team =>
             <div className="team-logo"> <img src={team.logo} /> </div>
           )}
          <div className="self-text">
            {JSX.ifSome(team, team =>
               <h3 className="team-name"> {team.name->React.string} </h3>
             )}
            <h3 className="job-name">
              {React.string(
                 switch (user) {
                 | None => s_("No user")
                 | Some(Teacher(_)) => s_("Teacher")
                 | Some(AnyStudent(student)) =>
                   "- " ++ AppState.studentJobName(student)
                 },
               )}
            </h3>
          </div>
        </Col>
        <Col xs=4 lg=3 className="header-info">
          {Option.mapWithDefault(user, rs_("Not logged in"), user =>
             <div>
               <b>
                 AppState.(user->firstName ++ " " ++ user->lastName)
                 ->React.string
               </b>
               <a href="/logout/">
                 {s_(" (log out)") -> React.string}
               </a>
             </div>
           )}
          {JSX.ifSome(team, team =>
             <>
               <div>
                 <b> {(s_("Room:") ++ " ")->React.string} </b>
                 team.startingRoom->React.string
               </div>
               <div>
                 <b> {(s_("InfluencePoints:") ++ " ")->React.string} </b>
                 {team.influencePoints->Int.toString->React.string}
               </div>
             </>
           )}
          <ChooseLanguage />
        </Col>
      </Row>
    </Container>
  </div>;
