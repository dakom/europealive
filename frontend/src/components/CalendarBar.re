open Belt;


module EntriesQuery = [%graphql
  {|{
  calendarEntries {
    id
    text
    time @bsDecoder(fn: "Utils.jsonToMoment")
    duration
    roomName
    playerDescription
  }
  }|}
];

type status =
  | Past
  | Current
  | Future;

[@react.component]
let make = () => {
  let%Epitath entryData =
    Fetcher.whenReady1(Fetcher.pollQL(~interval=30000, EntriesQuery.make()));
  let timeToString = MomentRe.Moment.format("H:mm");
  let now = MomentRe.momentNow();
  let addMinutes = (time, minutes) =>
    MomentRe.Moment.add(
      ~duration=MomentRe.duration(minutes->Int.toFloat, `minutes),
      time,
    );

  <div className="calendar-bar mt-3">
    {Array.map(
       entryData##calendarEntries,
       entry => {
         let endTime =
           Option.map(entry##duration, duration =>
             entry##time->addMinutes(duration)
           );
         let isOver = Option.getWithDefault(endTime, entry##time) < now;

         <p className={"entry" ++ (isOver ? " past" : "")} key=entry##id>
           <b>
             {entry##time->timeToString->React.string}
             {JSX.ifSome(endTime, endTime =>
                ("-" ++ endTime->timeToString)->React.string
              )}
           </b>

           <br />
          <b>
             <Router.Link route={Router.CalendarEntry(entry##id->int_of_string)}>
               {entry##text->React.string}
             </Router.Link>
           </b>
           <br />
           {JSX.ifSome(
            entry##playerDescription, playerDescription => 
              <span className="description-slice">
                {switch(Js.String.match([%re "/(\s*[^\s]+?\s+){1,10}/"], playerDescription)) {
                  | Some(array) => Js.String.make(array[0]) ++ " ..." 
                  | None => ""
                }}->React.string
              </span>
           )}
           <br />
           <em>
               {JSX.ifSome(entry##roomName, roomName =>
                    (roomName) -> React.string
               )}
           </em>
         </p>;
       },
     )
     ->React.array}
  </div>;
};
