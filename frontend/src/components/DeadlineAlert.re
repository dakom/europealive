open Gettext;
open BsReactstrap;
open Belt;

let useDeadline =
    (
      ~text=s_("until deadline"),
      ~className=?,
      ~color=?,
      ~time: MomentRe.Moment.t,
      (),
    ) => {
  let (currentTime, setTime) = React.useState(() => MomentRe.momentNow());

  // Effect forces update of time every second
  React.useEffect1(
    () => {
      let intervalID =
        Js.Global.setInterval(() => setTime(_ => MomentRe.momentNow()), 1000);
      Some(() => Js.Global.clearInterval(intervalID));
    },
    [|Js.String.make(time)|] // Stringify to avoid referential comparison
  );

  let timeLeftSeconds =
    MomentRe.diff(time, currentTime, `milliseconds)
    ->Float.toInt
    ->(millis => millis / 1000)
    ->max(0);
  let timeLeft =
    Format.sprintf("%i:%02i", timeLeftSeconds / 60, timeLeftSeconds mod 60);

  (
    timeLeftSeconds > 0,
    () =>
      <Alert color={Option.getWithDefault(color, "danger")} ?className>
        <b> timeLeft->React.string </b>
        {(" " ++ s_("minutes") ++ " ")->React.string}
        text->React.string
      </Alert>,
  );
};

/* Component wrapping Hook in case only element is needed */
[@react.component]
let make = (~text=?, ~className=?, ~color=?, ~time: MomentRe.Moment.t) => {
  let (_hasTimeLeft, render) =
    useDeadline(~text?, ~className?, ~color?, ~time, ());
  render();
};
