open BsReactstrap;
open Gettext;
open Belt;

type link = (string, Router.route);
type navItem =
  | Link(link)
  | Submenu(string, array(link));

let baseNav = (~overview=?, ~workspace=?, ~directive=?, ()) => [|
  Option.getWithDefault(
    overview,
    Submenu(
      s_("Overview"),
      [|
        (s_("Job Description"), Router.JobDescription),
        (s_("How To Win"), Router.TeamInfo),
        (s_("All Popups"), Router.PopupOverview),
        (s_("Majority Calculator"), Router.MajorityCalculator),
      |],
    ),
  ),
  Submenu(
    s_("Workspace"),
    Option.getWithDefault(
      workspace,
      [|
        (s_("Briefs"), Router.TeamMemoSubmissions),
        (s_("My Staff"), Router.MyStaff),
      |],
    ),
  ),
  Option.getWithDefault(
    directive,
    Link((s_("Directive"), Router.IndicateDirective)),
  ),
|];

let commissionSecretaryNav =
  baseNav(
    ~workspace=[|
      (s_("Place InfluencePoints"), Router.InvestIP),
      (s_("Transfer InfluencePoints"), Router.TransferInfluence),
      (s_("Publish Directive"), Router.PublishDirective),
      (s_("Directive Indications"), Router.DirectiveIndications),
    |],
    (),
  );

let secretaryNav =
  baseNav(
    ~workspace=[|
      (s_("Place InfluencePoints"), Router.InvestIP),
      (s_("Transfer InfluencePoints"), Router.TransferInfluence),
    |],
    (),
  );

let journalistNav =
  baseNav(
    ~workspace=[|
      (s_("Write Articles"), Router.MyArticlesOverview),
      (s_("Research"), Router.Research),
    |],
    (),
  );

let advisorNav =
  baseNav(
    ~workspace=[|
      (s_("Research"), Router.Research),
      (s_("Briefs"), Router.WriteMemo),
    |],
    (),
  );

let commissionAdvisorNav =
  baseNav(
    ~workspace=[|
      (s_("Research"), Router.Research),
      (s_("Briefs"), Router.WriteMemo),
      (s_("Directive Indications"), Router.DirectiveIndications),
    |],
    (),
  );

let strategistNav =
  baseNav(~workspace=[|(s_("Briefs"), Router.TeamMemoSubmissions)|], ());

let commissionStrategistNav =
  baseNav(
    ~workspace=[|
      (s_("Briefs"), Router.TeamMemoSubmissions),
      (s_("Directive Indications"), Router.DirectiveIndications),
    |],
    ()
  );

let chiefNav =
  baseNav(
    ~workspace=[|
      (s_("Briefs"), Router.TeamMemoSubmissions),
      (s_("My Staff"), Router.MyStaff),
      (s_("Hold Meeting"), Router.MeetingAgenda),
    |],
    (),
  );

let commissionChiefNav =
  baseNav(
    ~workspace=[|
      (s_("Briefs"), Router.TeamMemoSubmissions),
      (s_("My Staff"), Router.MyStaff),
      (s_("Hold Meeting"), Router.MeetingAgenda),
      (s_("Directive Indications"), Router.DirectiveIndications),
    |],
    (),
  );

let teacherNav = [|
  Submenu(
    s_("The Game"),
    [|
      (s_("Teams"), Teacher(TeamScore)),
      (s_("Calendar"), Teacher(Calendar)),
      (s_("Final Vote"), Router.Teacher(FinalVote)),
    |],
  ),
  Submenu(
    s_("Research"),
    [|
      (s_("Research"), Router.Research),
      (s_("Directive"), Router.IndicateDirective),
      (s_("Majority Calculator"), Router.MajorityCalculator),
    |],
  ),
  Submenu(
    s_("Gamemaster"),
    [|
      (s_("Manage Students"), Teacher(ManageJobs)),
      (s_("Send Message"), Teacher(WritePopup)),
    |],
  ),
|];

let navItemsFor =
  fun
  | AppState.AnyStudent(anyStudent) =>
    switch (anyStudent) {
    | Secretary(secretary) =>
      switch (secretary.student.team.kind) {
      | Commission => commissionSecretaryNav
      | _ => secretaryNav
      }
    | Journalist(_) => journalistNav
    | Advisor(advisor) =>
      switch (advisor.student.team.kind) {
      | Commission => commissionAdvisorNav
      | _ => advisorNav
      }
    | Chief(chief) =>
      switch (chief.student.team.kind) {
      | Commission => commissionChiefNav
      | _ => chiefNav
      }
    | Strategist(strategist) =>
      switch (strategist.student.team.kind) {
      | Commission => commissionStrategistNav
      | _ => strategistNav
      }
    }
  | Teacher(_) => teacherNav;

module DropdownToggleable = {
  /* Wraps Reactstrap Dropdown to add toggle state */
  [@react.component]
  let make = (~children: React.element, ~className=?) => {
    let (isOpen, setIsOpen) = React.useState(() => false);
    <Dropdown
      nav=true
      isOpen
      toggle={_ => setIsOpen(previous => !previous)}
      ?className>
      children
    </Dropdown>;
  };
};

[@react.component]
let make = (~loggedIn: AppState.anyUser, ~currentRoute) =>
  <Nav pills=true justified=true className="main-nav sticky-top">
    {Belt.Array.map(navItemsFor(loggedIn), navItem =>
       switch (navItem) {
       | Link((label, route)) =>
         <NavItem key=label>
           <Router.Link
             route
             className={"nav-link" ++ (currentRoute == route ? " active" : "")}>
             label->React.string
           </Router.Link>
         </NavItem>
       | Submenu(menuLabel, links) =>
         <DropdownToggleable
           key=menuLabel
           className={
             Belt.Array.some(links, ((_, route)) => route == currentRoute)
               ? "active" : ""
           }>
           <DropdownToggle nav=true caret=true>
             menuLabel->React.string
           </DropdownToggle>
           <DropdownMenu>
             {Array.map(links, ((label, route)) =>
                <Router.Link
                  key=label
                  route
                  className={
                    "dropdown-item" ++ (currentRoute == route ? " active" : "")
                  }>
                  label->React.string
                </Router.Link>
              )
              ->React.array}
           </DropdownMenu>
         </DropdownToggleable>
       }
     )
     |> React.array}
  </Nav>;
