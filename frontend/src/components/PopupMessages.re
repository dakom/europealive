open BsReactstrap;
open Belt;

module PopupQuery = [%graphql
  {| query($since: DateTime) {
	myPopups(since: $since) {
    id
    message
    title
    important
    sender {
      fullName
    }
    time @bsDecoder(fn: "Utils.jsonToMoment")
  }
  }|}
];

/* Fetch all popups sent out since last time popupQueryTime cookie was set.
   Continually updates this cookie, so a popup should only be seen once ever.
   If cookie is not set, it is initiated to the current time, so no previous
   cookies will be shown. */
[@react.component]
let make = () => {
  let (lastQueryTime, setQueryTime) =
    ReactStorage.useString(
      ~storage=ReactStorage.localStorage, "popupQueryTime", () =>
      Js.Date.make()->Js.Date.toJSONUnsafe
    );

  /* We cannot use Fetcher.pollQL here, so polling code is duplicated.
     This is needed in order for the argument to the query to change. */
  let (loadState, setLoadState) = React.useState(() => Fetcher.Loading);
  Utils.useInterval(() => {
    Api.sendQuery(PopupQuery.make(~since=lastQueryTime->Js.Json.string, ()))
    |> Js.Promise.then_(data =>
         setLoadState(oldState =>
           Ready(
             switch (oldState) {
             | Fetcher.Ready(popups) => Array.concat(data##myPopups, popups)
             | Error(_)
             | Loading => data##myPopups
             },
           )
         )
         ->Js.Promise.resolve
       )
    |> ignore;
    setQueryTime(_old => Js.Date.make()->Js.Date.toJSONUnsafe);
  });

  let%Epitath popups = Fetcher.whenReady1(loadState);

  let closePopup = popupId =>
    setLoadState(oldState =>
      switch (oldState) {
      | Fetcher.Ready(popups) =>
        Ready(popups->Array.keep(pop => pop##id != popupId))
      | _ => oldState
      }
    );

  <div className="popup-wrapper">
    {Array.map(popups, popup =>
       <Card key=popup##id className={popup##important ? " important" : ""}>
         <CardBody>
           <button
             type_="button"
             onClick={_event => closePopup(popup##id)}
             className="close"
             ariaLabel="Close">
             <span ariaHidden=true> {js|×|js}->React.string </span>
           </button>
           {JSX.if_(
              popup##title != "",
              <CardTitle> {popup##title->React.string} </CardTitle>,
            )}
           <CardText> {popup##message->React.string} </CardText>
           <span className="footer">
             {JSX.ifSome(popup##sender, sender =>
                (sender##fullName ++ " - ")->React.string
              )}
             {MomentRe.Moment.format("H:mm", popup##time)->React.string}
           </span>
         </CardBody>
       </Card>
     )
     ->React.array}
  </div>;
};
