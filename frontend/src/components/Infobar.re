open Gettext;
open BsReactstrap;
open Belt;
open Utils;

module TeamChooser = {
  [@react.component]
  let make = (~teams, ~className="", ~optionId, ~chooseId) =>
    <Row className={"choose-team " ++ className}>
      {Array.map(teams, anyTeam =>
         <Col sm=4 key=anyTeam##id>
           <img
             onClick={_ => chooseId(anyTeam##id)}
             alt=anyTeam##team##name
             className={
               "img-fluid clickable"
               ++ activeOrEmpty(Some(anyTeam##id) == optionId)
             }
             src=anyTeam##team##logo
           />
         </Col>
       )
       ->React.array}
    </Row>;
};

module InfoLines = {
  /* When given Some(team) uses toLines to get info to render */
  [@react.component]
  let make =
      (
        ~name,
        ~team: option('a),
        ~toLines: 'a => array((string, array(string))),
      ) =>
    JSX.ifSome(team, team =>
      <div>
        <h4 className="text-primary"> {team->name->React.string} </h4>
        {Array.map(toLines(team), ((label, contents)) =>
           <p key=label>
             <b> {(label ++ ": ")->React.string} </b>
             {if (Array.length(contents) == 1) {
                <span> {contents->(Array.getUnsafe(0))->React.string} </span>;
              } else {
                Array.map(contents, subLine =>
                  <span className="d-block" key=subLine>
                    subLine->React.string
                  </span>
                )
                ->React.array;
              }}
           </p>
         )
         ->React.array}
      </div>
    );
};

module MakeTeamQuery = [%graphql
  {|
  fragment teamFields on TeamType {
    name
    logo
    startingRoom
    priorities {
      article {
        number
        shortDescription
      }
    }
  }
  query {
    countries {
      id
      static {
        population
        votingWeight
      }
      team {
        ...TeamFields
      }
    }
    parliamentGroups {
      id
      static {
        memberCount
        votingWeight
      }
      team {
        ...TeamFields
      }
    }
    commission {
      team {
        ...TeamFields
      }
    }
    lobbyOrganizations {
      id
      team {
        ...TeamFields
      }
    }
    mediahouses {
      id
      static  {
        editorialLine
        countries
      }
      team {
        name
        logo
        startingRoom
      }
    }
  }
  |}
];

type topic =
  | Countries
  | Groups
  | Commission
  | Lobbyists
  | Media;

let allTopics = [|Countries, Groups, Commission, Lobbyists, Media|];

let topicToText =
  fun
  | Countries => s_("Countries")
  | Groups => s_("EP Groups")
  | Commission => s_("The Commission")
  | Lobbyists => s_("Lobbyists")
  | Media => s_("MediaHouses");

let prioritiesToLine = priorities => (
  s_("Priorities"),
  Array.map(priorities, priority =>
    Int.toString(priority##article##number)
    ++ ". "
    ++
    priority##article##shortDescription
  ),
);

type action =
  | Topic(topic)
  | Id(topic, string);

[@react.component]
let make = () => {
  let ((selectedTopic, optionId), setContent) =
    React.useReducer(
      (_old, action) =>
        switch (action) {
        | Topic(topic) => (Some(topic), None)
        | Id(topic, id) => (Some(topic), Some(id))
        },
      (None, None),
    );

  let%Epitath teamData =
    Fetcher.whenReady1(Fetcher.onceQL(MakeTeamQuery.make()));
  let getChosenTeam = teams =>
    Option.flatMap(optionId, id => Array.getBy(teams, t => t##id == id));

  <div className="infobar">
    <ListGroup className="topic-list">
        {Array.map(allTopics, topic =>
           <button
             key={topicToText(topic)}
             onClick={_ => setContent(Topic(topic))}
             className={
               "list-group-item list-group-item-action"
               ++ (selectedTopic == Some(topic) ? " active" : "")
             }>
             {topic->topicToText->React.string}
           </button>
         )
         ->React.array}
      </ListGroup>
      /*<button disabled=true className="list-group-item list-group-item-action">*/
      /*{rs_("Dictionary")}*/
      /*</button>*/
    {JSX.ifSome(
       selectedTopic,
       fun
       | Commission =>
         <Row>
           <Col sm=4 className="mb-3">
             <img src=teamData##commission##team##logo />
           </Col>
           <Col sm=12>
             <InfoLines
               name={comm => comm##team##name}
               team={Some(teamData##commission)}
               toLines={comm =>
                 [|
                   (s_("Room"), [|comm##team##startingRoom|]),
                   prioritiesToLine(comm##team##priorities),
                 |]
               }
             />
           </Col>
         </Row>
       | Media =>
         <>
           <TeamChooser
             className="media"
             teams=teamData##mediahouses
             optionId
             chooseId={id => setContent(Id(Media, id))}
           />
           <InfoLines
             name={media => media##team##name}
             team={getChosenTeam(teamData##mediahouses)}
             toLines={media =>
               [|
                 (s_("Room"), [|media##team##startingRoom|]),
                 (s_("Editorial line"), [|media##static##editorialLine|]),
                 (s_("Countries"), [|media##static##countries|]),
               |]
             }
           />
         </>
       | Lobbyists =>
         <>
           <TeamChooser
             className="lobbyists"
             teams=teamData##lobbyOrganizations
             optionId
             chooseId={id => setContent(Id(Lobbyists, id))}
           />
           <InfoLines
             name={org => org##team##name}
             team={getChosenTeam(teamData##lobbyOrganizations)}
             toLines={org =>
               [|
                 (s_("Room"), [|org##team##startingRoom|]),
                 prioritiesToLine(org##team##priorities),
               |]
             }
           />
         </>
       | Groups =>
         <>
           <TeamChooser
             className="parliament-groups"
             teams=teamData##parliamentGroups
             optionId
             chooseId={id => setContent(Id(Groups, id))}
           />
           <InfoLines
             name={group => group##team##name}
             team={Option.flatMap(optionId, id =>
               Array.getBy(teamData##parliamentGroups, l => l##id == id)
             )}
             toLines={group =>
               [|
                 (s_("Room"), [|group##team##startingRoom|]),
                 prioritiesToLine(group##team##priorities),
                 (
                   s_("Members / Voting weight"),
                   [|
                     Printf.sprintf(
                       "%i / %.0f %%",
                       group##static##memberCount,
                       group##static##votingWeight *. 100.,
                     ),
                   |],
                 ),
               |]
             }
           />
         </>
       | Countries =>
         <>
           <TeamChooser
             className="countries"
             teams=teamData##countries
             optionId
             chooseId={id => setContent(Id(Countries, id))}
           />
           <InfoLines
             name={country => country##team##name}
             team={getChosenTeam(teamData##countries)}
             toLines={country =>
               [|
                 (s_("Room"), [|country##team##startingRoom|]),
                 prioritiesToLine(country##team##priorities),
                 (
                   s_("Population / Voting weight"),
                   [|
                     Printf.sprintf(
                       "%i / %.1f %%",
                       country##static##population,
                       country##static##votingWeight *. 100.,
                     ),
                   |],
                 ),
               |]
             }
           />
         </>,
     )}
  </div>;
};
