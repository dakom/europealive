open Gettext;
open Belt;

let opt_int_of_string = value => Option.map(value, int_of_string);

module NewsItemsQuery = [%graphql
    {|
    {
        newsItems {
            articleId @bsDecoder(fn: "opt_int_of_string")
            telegramId @bsDecoder(fn: "opt_int_of_string")
            headline
            leadText
            bodyText
            publishingTime
            isDraft
            isBreakingNews
            writer {
                mediahouse {
                    logo
                }
            }
        }
    }
    |}
];

let newsItemsQuery = NewsItemsQuery.make();

let newShowItem = newsItem => 
  <div
    key={Js.String.make((newsItem##articleId, newsItem##telegramId))} className="news-item">
    <img src={newsItem##writer##mediahouse##logo} />
    <b>
      <Router.Link
        route={
            switch (newsItem##articleId, newsItem##telegramId) {
                | (Some(articleId), None) => Router.NewsReader(`ARTICLE, articleId)
                | (None, Some(telegramId)) => Router.NewsReader(`TELEGRAM, telegramId)
                | _ =>
                  Js.Console.error("NewsItem of unrecognized type!!");
                  Router.Page404;
            }
        }>
        {JSX.if_(
           newsItem##isBreakingNews,
           <span className="breaking" key={newsItem##headline}>
             {rs_("Breaking")}
           </span>,
         )}
        {React.string(" ")}
        {React.string(newsItem##headline)}
      </Router.Link>
    </b>
    {JSX.ifSome(newsItem##leadText, leadText =>
            <p>{React.string(leadText ++ "...")}</p>
    )}
  </div>;


[@react.component]
let make = () => {
  let%Epitath newsItems =
  Fetcher.whenReady1(Fetcher.pollQL(~interval=10000, newsItemsQuery));
  <div className="newsfeed">
    {switch (Array.length(newsItems##newsItems)) {
     | 0 =>
       <div className="text-center mt-3">
         {rs_("No journalists have published anything")}
       </div>
     | _ => Array.map(newsItems##newsItems, newShowItem) |> React.array
     }}
  </div>;
};
