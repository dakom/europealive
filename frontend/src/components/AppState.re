open Gettext;

type id('a) =
  | Id(string);

let idFromString = (s: string): id('a) => Id(s);
let idToString = (Id(s)) => s;

let idToGenId = (Id(s)) => Api.Types.Id(int_of_string(s));

type user = {
  id: id(user),
  email: option(string),
  firstName: string,
  lastName: string,
};

type teacher = {
  user,
  id: id(teacher),
  schoolclass: string,
};

type teamKind =
  | Commission
  | ParliamentGroup
  | Country
  | LobbyOrganization
  | MediaHouse;

let teamKindFromPolymorphic =
  fun
  | `Commission => Commission
  | `Country => Country
  | `LobbyOrganization => LobbyOrganization
  | `MediaHouse => MediaHouse
  | `ParliamentGroup => ParliamentGroup;

type team = {
  id: id(team),
  influencePoints: int,
  logo: string,
  name: string,
  startingRoom: string,
  videoEmbedUrl: string,
  kind: teamKind,
};

type student = {
  id: id(student),
  jobDescription: string,
  user,
  team,
};

type mediahouse;

type journalist = {
  id: id(journalist),
  student,
  mediahouse: id(mediahouse),
};

type secretary = {
  id: id(secretary),
  student,
};

type advisorKind =
  | Technical
  | Political
  | Media
  | Attache;

let toNonPolymorphicAdvisor =
  fun
  | `TECHNICAL => Technical
  | `POLITICAL => Political
  | `MEDIA => Media
  | `ATTACHE => Attache;

type advisor = {
  id: id(advisor),
  kind: advisorKind,
  student,
};

type chief = {
  id: id(chief),
  student,
};

type strategist = {
  id: id(strategist),
  student,
};

type anyStudent =
  | Secretary(secretary)
  | Journalist(journalist)
  | Advisor(advisor)
  | Chief(chief)
  | Strategist(strategist);

let advisorTitle =
  fun
  | Technical => sp_("advisor kind", "Technical")
  | Political => sp_("advisor kind", "Political")
  | Media => sp_("advisor kind", "Media")
  | Attache => sp_("advisor kind", "Relation");

// this is pretty damn hacky, but I was in a hurry. A better solution would be for the StudentJobTemplate to 
// have a field for the job title and load that onto the student. Maybe at some point when I have loads of time!

let advisorTitleTeam = (advisorKind: advisorKind, teamKind: teamKind) =>
  switch (advisorKind) {
  | Technical => 
    switch (teamKind) {
      | Commission => sp_("advisor kind", "Technical advisor")
      | Country => sp_("advisor kind", "Technical advisor")
      | ParliamentGroup => sp_("advisor kind", "Technical advisor")
      | LobbyOrganization => sp_("advisor kind", "Technical lobbyist")
      | MediaHouse => sp_("advisor kind", "Journalist"); // not used
    }
  | Political => 
    switch (teamKind) {
      | Commission => sp_("advisor kind", "Political") // not used
      | Country => sp_("advisor kind", "Political advisor")
      | ParliamentGroup => sp_("advisor kind", "Political advisor")
      | LobbyOrganization => sp_("advisor kind", "Political") // not used
      | MediaHouse => sp_("advisor kind", "Political"); // not used
    }
  | Media => 
    switch (teamKind) {
      | Commission => sp_("advisor kind", "Media advisor")
      | Country => sp_("advisor kind", "Media advisor")
      | ParliamentGroup => sp_("advisor kind", "Media advisor")
      | LobbyOrganization => sp_("advisor kind", "Media advisor")
      | MediaHouse => sp_("advisor kind", "Media advisor"); //not used
    }
  | Attache => 
        switch (teamKind) {
      | Commission => sp_("advisor kind", "Relations advisor")
      | Country => sp_("advisor kind", "Relations advisor")
      | ParliamentGroup => sp_("advisor kind", "Relations advisor")
      | LobbyOrganization => sp_("advisor kind", "Lobbyist envoy")
      | MediaHouse => sp_("advisor kind", "Relations") // not used
    };
  }

let studentJobName =
  fun
  | Secretary(secretary) =>
    switch (secretary.student.team.kind) {
    | ParliamentGroup => sp_("parliament", "Group Secretary")
    | Commission => sp_("commission", "Secretary general")
    | Country => sp_("country", "Chief Secretary")
    | MediaHouse // not used, since journalist is student subtype
    | LobbyOrganization => sp_("lobbyist", "Chief Secretary")
    }
  | Journalist(_) => s_("Journalist")
  | Chief(chief) =>
    switch (chief.student.team.kind) {
    | Commission  => sp_("commission", "Chief of Cabinet")
    | ParliamentGroup => sp_("parliament", "Chief of Staff")
    | Country => sp_("country", "Chief of Staff")
    | MediaHouse => "chief of staff" // Not used, mediahouse has no chief
    | LobbyOrganization => sp_("lobbyist", "Chief of Staff")
    }
  | Strategist(strategist) =>
    switch (strategist.student.team.kind) {
    | Commission => s_("EU-Commissioner")
    | ParliamentGroup => s_("Spokesperson")
    | Country => s_("Minister")
    | LobbyOrganization => s_("Chief Lobbyist")
    | MediaHouse => s_("Editor") // not used, since journalist is student subtype
    }
  // | Advisor({kind}) => advisorTitle(kind);
  | Advisor(advisor) => advisorTitleTeam(advisor.kind, advisor.student.team.kind);

type anyUser =
  | AnyStudent(anyStudent)
  | Teacher(teacher);

/* Boilerplate functions to interact with these generic user types */
let getUser =
  fun
  | AnyStudent(student) =>
    switch (student) {
    | Secretary(s) => s.student.user
    | Journalist(j) => j.student.user
    | Advisor(a) => a.student.user
    | Chief(c) => c.student.user
    | Strategist(s) => s.student.user
    }
  | Teacher(teacher) => teacher.user;

let getStudent =
  fun
  | Secretary(s) => s.student
  | Journalist(j) => j.student
  | Advisor(a) => a.student
  | Chief(c) => c.student
  | Strategist(s) => s.student;

let firstName = anyUser => anyUser->getUser.firstName;

let lastName = anyUser => anyUser->getUser.lastName;

let team =
  fun
  | Secretary(s) => s.student.team
  | Journalist(j) => j.student.team
  | Advisor(a) => a.student.team
  | Chief(c) => c.student.team
  | Strategist(s) => s.student.team;

type t = {
  loggedIn: anyUser,
  team: option(team), /* Duplicates team field on student */
  currentDirective: id(Api.Types.directive),
  setModal: ((unit => unit) => SingletonModal.content) => unit,
  currentRound: {
    .
    "id": string,
    "length": int,
    "active": bool,
    "isFinal": bool,
    "startTime": MomentRe.Moment.t,
    "ipInvestmentTime": MomentRe.Moment.t,
    "directive": {. "id": string},
  },
};

module GameStateQuery = [%graphql
  {|
  fragment UserFields on UserType {
    id
    email
    firstName
    lastName
  }
  fragment StudentFields on StudentType {
    id
    jobDescription
    user {
      ...UserFields

    }
    team @bsRecord {
      id @bsDecoder(fn: "idFromString")
      name
      startingRoom
      logo
      influencePoints
      videoEmbedUrl
      kind @bsDecoder(fn: "teamKindFromPolymorphic")
    }
  }
  query {
    gamestate {
      ... on PreGame {
        startingTime @bsDecoder(fn: "Utils.jsonToMoment")
        loggedIn {
            # This should be UserFields, but graphql_ppx bugs out
            id
            email
            firstName
            lastName
        }
      }
      ... on PlatformReady {
        loggedIn {
          ... on TeacherType {
            id
            user {
              # This should be UserFields, but graphql_ppx bugs out
              id
              email
              firstName
              lastName
            }
          }
          ... on SecretaryType {
            id
            student {
              ...StudentFields
            }
          }
          ... on JournalistType {
            id
            mediahouse {
              id
            }
            student {
              ...StudentFields
            }
          }
          ... on AdvisorType {
            id
            kind
            student {
              ...StudentFields
            }
          }
          ... on ChiefType {
            id
            student {
              ...StudentFields
            }
          }
          ... on StrategistType {
            id
            student {
              ...StudentFields
            }
          }
        }
        team @bsRecord {
          id @bsDecoder(fn: "idFromString")
          influencePoints
          name
          logo
          startingRoom
          videoEmbedUrl
          kind @bsDecoder(fn: "teamKindFromPolymorphic")
        }
        currentRound {
            id
            length
            active
            isFinal
            startTime @bsDecoder(fn: "Utils.jsonToMoment")
            ipInvestmentTime @bsDecoder(fn: "Utils.jsonToMoment")
            directive {
              id
            }
        }
    }
  }
  }|}
];

let decodeUser = userObj => {
  id: userObj##id->Id,
  email: userObj##email,
  firstName: userObj##firstName,
  lastName: userObj##lastName,
};

let decodeStudent = studentObj => {
  id: studentObj##id->Id,
  jobDescription: studentObj##jobDescription,
  team: studentObj##team,
  user: decodeUser(studentObj##user),

}

// This function duplicates most of decodeAnyUser. Could be refactored nicer.
let decodeAnyStudent =
  fun
  | `SecretaryType(secretary) =>
    AnyStudent(
      Secretary({
        id: secretary##id->Id,
        student: decodeStudent(secretary##student),
      }),
    )
  | `AdvisorType(advisor) =>
    AnyStudent(
      Advisor({
        id: advisor##id->Id,
        kind: advisor##kind->toNonPolymorphicAdvisor,
        student: decodeStudent(advisor##student),
      }),
    )
  | `ChiefType(chief) =>
    AnyStudent(
      Chief({id: chief##id->Id, student: decodeStudent(chief##student)}),
    )
  | `StrategistType(strategist) =>
    AnyStudent(
      Strategist({
        id: strategist##id->Id,
        student: decodeStudent(strategist##student),
      }),
    )
  | `JournalistType(journalist) =>
    AnyStudent(
      Journalist({
        id: journalist##id->Id,
        mediahouse: journalist##mediahouse##id->Id,
        student: decodeStudent(journalist##student),
      }),
    );

let decodeAnyUser =
  fun
  | `TeacherType(teacher) =>
    Teacher({
      id: teacher##id->Id,
      user: decodeUser(teacher##user),
      schoolclass: "",
    })
  | `SecretaryType(secretary) =>
    AnyStudent(
      Secretary({
        id: secretary##id->Id,
        student: decodeStudent(secretary##student),
      }),
    )
  | `AdvisorType(advisor) =>
    AnyStudent(
      Advisor({
        id: advisor##id->Id,
        kind: advisor##kind->toNonPolymorphicAdvisor,
        student: decodeStudent(advisor##student),
      }),
    )
  | `ChiefType(chief) =>
    AnyStudent(
      Chief({id: chief##id->Id, student: decodeStudent(chief##student)}),
    )
  | `StrategistType(strategist) =>
    AnyStudent(
      Strategist({
        id: strategist##id->Id,
        student: decodeStudent(strategist##student),
      }),
    )
  | `JournalistType(journalist) =>
    AnyStudent(
      Journalist({
        id: journalist##id->Id,
        mediahouse: journalist##mediahouse##id->Id,
        student: decodeStudent(journalist##student),
      }),
    );
