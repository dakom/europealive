open Gettext;
open BsReactstrap;

[@react.component]
let make = (~startingTime=?, ~className="", ~children: React.element) => {
  <Container className={"pre-game-box" ++ className}>
    <Row className="justify-content-md-center mt-5">
      <Col sm=5>
        <img src=Header.logoSrc className="mb-3 mt-3" />
        {JSX.ifSome(startingTime, time =>
           <DeadlineAlert
             className="countdown"
             time
             text={sp_("time until", "until game starts!")}
             color="secondary"
           />
         )}
        children
      </Col>
    </Row>
  </Container>;
};
