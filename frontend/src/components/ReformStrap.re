[@bs.config {jsx: 3}];
open BsReactstrap;
open BsReform;

module InputWithMessage = {
  [@react.component]
  let make =
      (
        ~id=?,
        ~_type=?,
        ~className=?,
        ~size=?,
        ~bsSize=?,
        ~state=?,
        ~tag=?,
        ~innerRef=?,
        ~static=?,
        ~plaintext=?,
        ~addon=?,
        ~onBlur=?,
        ~onFocus=?,
        ~placeholder=?,
        ~min=?,
        ~max=?,
        ~rows=?,
        ~name=?,
        ~cssModule=?,
        ~readOnly=?,
        ~children=?,
        ~showError=?,
        ~value,
        ~fieldState: ReForm.fieldState,
        ~handleChange,
      ) =>
    <>
      <BsReactstrap.Input
        ?id
        ?_type
        ?className
        ?size
        ?bsSize
        ?state
        ?tag
        ?innerRef
        ?static
        ?plaintext
        ?addon
        ?onBlur
        ?onFocus
        ?placeholder
        ?min
        ?max
        ?rows
        ?name
        ?cssModule
        ?readOnly
        ?children
        onChange={Helpers.handleChange(handleChange)}
        valid={fieldState == Valid}
        invalid={
          switch (fieldState) {
          | Error(_) => true
          | _ => false
          }
        }
        value
      />
      {switch (fieldState) {
       | Error(message) =>
         Belt.Option.mapWithDefault(
           showError,
           <Alert color="danger"> message->React.string </Alert>,
           shower =>
           shower(message)
         )
       | _ => React.null
       }}
    </>;
};

module SubmitButtonWithMessage = {
  [@react.component]
  let make = (~disabled=?, ~text) =>
    <Button ?disabled color="danger"> text->React.string </Button>;
};
