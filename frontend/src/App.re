open Gettext;
open BsReactstrap;
/*open Belt;*/

module FetchRound =
  Fetcher.Poll({
    type data = Api.Types.round;
  });

// Wrapper around AppState.t handling cases when AppState is not available
type gameState =
  // Getting AJAX data
  | Loading
  // Use is not logged in. Show game start time if game not yet started
  | NoLogin(option(MomentRe.Moment.t))
  // User is logged in, waiting for game to start
  | WaitingForStart(MomentRe.Moment.t, AppState.user)
  // User is logged in, game is started
  | Ready(AppState.t);

[@react.component]
let make = () => {
  let (gameState: gameState, setAppState) = React.useState(() => Loading);
  let (modalElement, setModalContent) = SingletonModal.useModal();
  let update = () =>
    AppState.GameStateQuery.make()
    |> Api.sendQuery
    |> Js.Promise.then_(data => {
         setAppState(_old =>
           switch (data##gamestate) {
           | `PreGame(preGame) =>
             switch (preGame##loggedIn) {
             | None => NoLogin(Some(preGame##startingTime))
             | Some(user) =>
               WaitingForStart(
                 preGame##startingTime,
                 AppState.decodeUser(user),
               )
             }

           | `PlatformReady(activeGame) =>
             switch (activeGame##loggedIn) {
             | None => NoLogin(None)
             | Some(dataAnyUser) =>
               let anyUser = AppState.decodeAnyUser(dataAnyUser);

               /* Configure user info for error logging */
               Sentry.setLoggedInUser(anyUser);
               Ready({
                 loggedIn: anyUser,
                 team: activeGame##team,
                 currentDirective:
                   AppState.Id(activeGame##currentRound##directive##id),
                 currentRound: activeGame##currentRound,
                 setModal: setModalContent,
               });
             }
           }
         );
         Js.Promise.resolve();
       })
    |> ignore;
  React.useEffect1(
    () => {
      update();
      let timerId = Js.Global.setInterval(update, 5000);
      Some(() => Js.Global.clearInterval(timerId));
    },
    [||],
  );
  let currentRoute = Router.useRouter();

  switch (gameState) {
  | Loading => <div> {s_("Loading...")->React.string} </div>
  // Teachers can signup no matter the game state
  | _ when currentRoute == Teacher(Signup) =>
    <TeacherCreateUser updateGameState=update />
  | NoLogin(startingTime) => <Login startingTime updateGameState=update />
  | WaitingForStart(time, _user) => <WaitingForStart time />
  | Ready(appState) =>
    <>
      modalElement
      <PopupMessages />
      <Header user={Some(appState.loggedIn)} team={appState.team} />
      <Container className="main-wrapper">
        <Row>
          <Col sm=3 className="sidebar-wrapper sticky-top"> <Sidebar /> </Col>
          <Col sm=9 className="main">
            <MainNav loggedIn={appState.loggedIn} currentRoute />
            {switch (currentRoute) {
             | Router.Home => {switch(appState.loggedIn) {
               | AnyStudent(_) => Router.pushRoute(JobDescription)
               | Teacher(_) => Router.pushRoute(Teacher(Calendar))
               };
              React.null;
             }
             | Teacher(teacherRoute) =>
               UserIs.teacher(appState.loggedIn, _teacher =>
                 switch (teacherRoute) {
                 | TeamScore => <TeacherTeamScore />
                 | Calendar => <TeacherCalendar />
                 | FinalVote => <TeacherFinalVote />
                 | WritePopup => <TeacherWritePopup />
                 | ManageJobs => <TeacherManageJobs />
                 | Signup => <TeacherCreateUser updateGameState=update />
                 | TeamInfo(teamId) => <TeacherTeamInfo teamId />
                 }
               )
             | NewsReader(kind, id) =>
               <NewsReader kind id key={string_of_int(id)} />
             | TransferInfluence => <TransferInfluenceReform appState />
             | WriteArticle(id) => <WriteArticle appState initialId=?id />
             | MyArticlesOverview => <MyArticlesOverview />
             | TeamInfo => <TeamInfo appState />
             | CalendarEntry(entryId) => <CalendarEntry entryId key={string_of_int(entryId)} user={appState.loggedIn}/>
             | IndicateDirective => <IndicateDirective appState />
             | DirectiveIndications => <DirectiveIndications appState />
             | InvestIP => <InvestIP appState />
             | Research => <Research />
             | JobDescription => <JobDescription appState />
             | MajorityCalculator => <MajorityCalculator />
             | MeetingAgenda => <MeetingAgenda appState />
             | MyStaff => <MyStaff appState />
             | WriteMemo => <WriteMemo appState />
             | TeamMemoSubmissions => <TeamMemoSubmissions appState />
             | PopupOverview => <PopupOverview />
             | NotImplemented => rs_("Not Implemented Yet")
             | PublishDirective => <PublishDirective appState />
             | Page404 => <Page404 />
             }}
          </Col>
        </Row>
      </Container>
    </>
  };
};
