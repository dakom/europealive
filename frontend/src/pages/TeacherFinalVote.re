open BsReactstrap;
open Gettext;
open Belt;


type finalVote =
  | Yes
  | No
  | Abstain;

let monomorphizeTeamVote =
  fun
  | `YES => Yes
  | `NO => No
  | `ABSTAIN => Abstain;

let description =
  s_(
    {js|
    Here the final results of the vote are shown. There must be a majority in both the Council of Ministers and the European Parliament before the directive is passed.
  |js},
  );

module VotesQuery = [%graphql
  {| query {
  detailedVoteResults {
    parliament
    council
    overall
  }
  countries {
    id
    static {
      name
      votingWeight
      population
    }
    team {
      finalteamvote {
        option @bsDecoder(fn: "monomorphizeTeamVote")
      }
    }
  }
  parliamentGroups {
    id
    name
    static {
      name
      memberCount
      votingWeight
    }
    team {
      finalteamvote {
        option @bsDecoder(fn: "monomorphizeTeamVote")
      }
    }
  }
  }|}
];

module VotesBar = {
  
  let voterList =
    fun
    | [||] => rs_("None")
    | voters =>
      <ul>
        {Array.map(voters, voter =>
           <li key=voter##id>
             {voter##static##name->React.string}
             {(
                " ("
                ++ interpolate(
                     "%s %",
                     [|Utils.fractionToPercent(voter##static##votingWeight)|],
                   )
                ++ ")"
              )
              ->React.string}
           </li>
         )
         ->React.array}
      </ul>;

  [@react.component]
  let make = (~voters) => {
    let sumWeight = countries =>
      Array.reduce(countries, 0.0, (sum, country) =>
        sum +. country##static##votingWeight
      );
    let max = sumWeight(voters);
    let keepIfVoted = voted => {
      let voters =
        Array.keep(voters, voter =>
          Option.mapWithDefault(voter##team##finalteamvote, false, teamVote =>
            teamVote##option == voted
          )
        );
      (voters, sumWeight(voters));
    };
    let (votedYes, yesTotal) = keepIfVoted(Yes);
    let (votedNo, noTotal) = keepIfVoted(No);
    let (votedAbstain, abstainTotal) = keepIfVoted(Abstain);

    let didntVote =
      Array.keep(voters, voter => voter##team##finalteamvote == None);
    let didntVoteTotal = sumWeight(didntVote);

    <>
      <Progress multi=true className="mb-3">
        <Progress bar=true max value=yesTotal color="secondary">
          {rs_("Yes")}
        </Progress>
        <Progress bar=true max value=noTotal color="danger">
          {rs_("No")}
        </Progress>
        <Progress bar=true max value=abstainTotal color="warning">
          {rs_("Abstain")}
        </Progress>
        <Progress bar=true max value=didntVoteTotal color="default">
          {rs_("Undecided")}
        </Progress>
      </Progress>
      <Row>
        {Array.map(
           [|
             (s_("Voted Yes (%s %):"), yesTotal, votedYes),
             (s_("Voted No (%s %):"), noTotal, votedNo),
             (s_("Decided to abstain (%s %):"), abstainTotal, votedAbstain),
             (s_("Did not yet vote (%s %):"), didntVoteTotal, didntVote),
           |],
           ((translatedString, weight, voterSubset)) =>
           <Col xs=6 key=translatedString>
             <h5>
               {rinterpolate(
                  translatedString,
                  [|Float.toInt(weight *. 100.0)|],
                )}
             </h5>
             {voterList(voterSubset)}
           </Col>
         )
         ->React.array}
      </Row>
    </>;
  };
};

let showVoteResult =
  fun
  | `PASSED => s_("Passed")
  | `REJECTED => s_("Rejected")
  | `UNDECIDED => s_("Undecided");

[@react.component]
let make = () => {
  let%Epitath response =
    Fetcher.whenReady1(Fetcher.pollQL(VotesQuery.make()));

  <div className="teacher-final-vote">
    <h1> {rs_("Final vote")} </h1>
    <p dangerouslySetInnerHTML={"__html": description} />
    <Alert color="primary" className="bg-primary-lighter">
      <p className="text-black font-weight-bold">
        {rinterpolate(
           s_("Overall result: %s"),
           [|response##detailedVoteResults##overall->showVoteResult|],
         )}
      </p>
      <p className="text-info">
        {rinterpolate(
           s_("Council of Ministers: %s"),
           [|response##detailedVoteResults##council->showVoteResult|],
         )}
      </p>
      <p className="text-info mb-0">
        {rinterpolate(
           s_("European Parliament: %s"),
           [|response##detailedVoteResults##parliament->showVoteResult|],
         )}
      </p>
    </Alert>
    <h2> {rs_("Council of Ministers")} </h2>
    <ParagraphedText
      text={s_(
        {js|See how the Council of Ministers chooses to vote.

In the Council, the rules of qualified majority apply. That means that 55 % of the states must vote “yes”, and they must at the same time represent at least 65 % of all EU citizens.

If a team abstains from voting, or forgets to vote, that is de facto the same as a technical “no”.

|js},
      )}
    />
    <VotesBar voters=response##countries />
    <h2> {rs_("The European Parliament")} </h2>
    <ParagraphedText
      text={s_(
        {js|See how the European Parliament chooses to vote.

In the Parliament, the rules of simple majority apply, meaning that there must be more “yes” than “no” votes in order to pass the directive.
|js},
      )}
    />
    <VotesBar voters=response##parliamentGroups />
  </div>;
};
