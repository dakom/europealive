/*
    This module implementes three pages:
    - Showing the current directive
    - Secretaries' page for sending directive indications to Commission
    - Final team vote on the directive

    Additionally, ShowDirectiveArticle is used from PublishDirective.

    This module is in dire need of refactoring. The different pages should be
    moved to separate files, and common functionality properly abstracted.
    Also, reliance on the old Api module should be moved to GraphQL
 */
open Gettext;
open Belt;
open BsReactstrap;
open BsReform;
module Element = Webapi.Dom.Element;
module HtmlCollection = Webapi.Dom.HtmlCollection;

let description =
  s_(
    {js|
The Commission has sent out a draft of the directive. Under each of the 8 articles, the Commission has suggested a point, which is <b>highlighted in purple</b>.
  |js},
  );

let canIndicateDescription =
  s_(
    {js|
<br/>Decide in your team if you want <b>More</b> or <b>Less</b> extensive rules adopted within each article, or if you think the level is <b>OK</b>.
  |js},
  );

module MakeDirectiveIndicationQuery = [%graphql
  {|
  mutation($indications: [Indication!]!, $directive: ID!) {
            makeDirectiveIndication(
                input: {
                    targetDirective: $directive,
                    indications: $indications
                }
            ) {
                directiveIndication {
                    id
                    indications
                }
                errors {
                    field
                    messages
                }
            }
  }
  |}
];

module ArticlesQuery = [%graphql
{| query {
    directiveArticles {
        id
        number
        description
        shortDescription
        items {
            text
            number
        }
    }
 }
|}
];

module GetIndicationQuery = [%graphql
{| query {
    myIndicationFor {
        id
        indications
    }
  }
|}
];

module DirectiveQuery = [%graphql
{| query {
    directive {
        id
        selections {
            itemsSelected
        }
    }
 }
|}
];

[@bs.val]
external printOnClick: unit => unit = 
    "window.print";

/* BsReactstrap.ListGroupItem does not expose style prop, so we emulate it here */
module ListGroupItemShift = {
  [@react.component]
  let make = (~onClick, ~isSelected, ~shift, ~children) =>
    <li
      style={ReactDOMRe.Style.make(
        ~marginLeft=?
          shift != 0
            ? Some(
                Float.toString(Int.toFloat(shift) *. (-33.33333)) ++ "%",
              )
            : None,
        (),
      )}
      onClick
      className={
        "list-group-item list-group-item-action"
        ++ (isSelected ? " selected" : "")
      }>
      children
    </li>;
};

type indication = [ | `LESS | `OK | `MORE];

module ShowDirectiveArticle = {
  type scrollButton =
    | Left
    | Right;

  [@react.component]
  let make =
      (
        ~isLocked,
        ~indication,
        ~showIndicateButtons,
        ~handleChange,
        ~fieldState: ReForm.fieldState,
        ~selected: int,
        ~article,
        ~mkItemElement: option('a => React.element)=?,
      ) => {
    /* State of which item is centered */
    let (viewing, move) =
      React.useReducer(
        (oldViewing, action) =>
          Utils.clamp(
            (1, 5),
            switch (action) {
            | Left => oldViewing - 1
            | Right => oldViewing + 1
            },
          ),
        selected,
      );

    /* Calculate height required for currently viewed items */
    let listRef = React.useRef(Js.Nullable.null);
    let (listHeight, setListHeight) = React.useState(() => "auto");
    React.useEffect1(
      () => {
        listRef
        ->React.Ref.current
        ->Js.Nullable.toOption
        ->Option.map(listEl => {
            let liElements = listEl->Element.children;
            let visibleLiElements =
              HtmlCollection.[
                /* Compensate for 1-indexing */
                item(viewing - 2, liElements),
                item(viewing - 1, liElements),
                item(viewing, liElements),
              ];
            let requiredHeight =
              visibleLiElements
              ->List.keepMap(Fn.id)
              ->List.map(divEl =>
                  divEl
                  ->Element.children
                  ->HtmlCollection.item(0, _)
                  ->Option.mapWithDefault(0, Element.scrollHeight)
                )
              ->List.reduce(0, max);
            setListHeight(_ =>
              Int.toString(requiredHeight + /* padding */ 24) ++ "px"
            );
          })
        ->ignore;
        None;
      },
      [|viewing|],
    );

    let indicate = newIndication =>
      handleChange(
        indication == Some(newIndication) ? None : Some(newIndication),
      );

    let mkIndicateButton = (value, text) =>
      <button
        onClick={_ => indicate(value)}
        disabled=isLocked
        type_="button"
        className={indication == Some(value) ? "selected" : ""}>
        text->React.string
      </button>;

    /* Presenter */
    <div className="directive-article">
      <h3>
        {(Int.toString(article##number) ++ ". " ++ article##shortDescription)
         ->React.string}
      </h3>
      <p className={showIndicateButtons ? "" : "mb-0"}>
        article##description->React.string
      </p>
      {JSX.if_(
         showIndicateButtons,
         <div className="indicate-chooser">
           {mkIndicateButton(`LESS, s_("Less"))}
           {mkIndicateButton(`OK, s_("OK"))}
           {mkIndicateButton(`MORE, s_("More"))}
         </div>,
       )}
      <div className="move-horizontal-wrapper">
        <button
          className="move-horizontal"
          onClick={_ => move(Left)}
          type_="button"
          disabled={viewing == 1}>
          <div />
        </button>
        <button
          className="move-horizontal"
          onClick={_ => move(Right)}
          type_="button"
          disabled={viewing == 5}>
          <div />
        </button>
      </div>
      <div className="hide-overflow">
        <ul
          style={ReactDOMRe.Style.make(~height=listHeight, ())}
          className="list-group list-group-horizontal article-items" 
          ref={ReactDOMRe.Ref.domRef(listRef)}>
          {article##items
           ->(
               Array.map(item => {
                 let shift = item##number == 1 ? viewing - 2 : 0;
                 <ListGroupItemShift
                   key={Int.toString(item##number)}
                   shift
                   onClick={_ =>
                     switch (Fn.compare(item##number, viewing)) {
                     | Lt => move(Left)
                     | Eq => ()
                     | Gt => move(Right)
                     }
                   }
                   isSelected={item##number == selected}>
                   <div>
                     <p>
                       {(item##number->Int.toString ++ ". ")->React.string}
                       item##text->React.string
                     </p>
                     {JSX.ifSome(mkItemElement, mk => mk(item))}
                   </div>
                 </ListGroupItemShift>;
               })
             )
           ->React.array}
        </ul>
        {JSX.whenFormError(fieldState, errorMessage =>
           <Alert color="danger"> errorMessage->React.string </Alert>
         )}
      </div>
    </div>;
  };
};

let mkModal = (onClose, submit, close): SingletonModal.content => {
  element:
    <>
      <ModalHeader> {rs_("Are you sure?")} </ModalHeader>
      <ModalBody>
        {rs_("Once you make your indications, they cannot be changed.")}
      </ModalBody>
    </>,
  buttons:
    Some(
      <>
        <Button
          color="primary"
          onClick={_event => {
            submit();
            close();
          }}>
          {rs_("Submit")}
        </Button>
        <Button color="secondary" onClick={_ => close()}>
          {rs_("Cancel")}
        </Button>
      </>,
    ),
  onClose,
};

/* State is specified manually instead of with lenses-ppx in order to
   support index on Indications field. */
module IndicateStateLenses = {
  type state = {indications: array(option(indication))};
  type field(_) =
    | Indications(int): field(option(indication));
  /*let get: (state, field('value)) => 'value;*/
  let get: type value. (state, field(value)) => value =
    (state, field) =>
      switch (field) {
      | Indications(index) => state.indications[index]->Option.flatMap(Fn.id)
      };
  let set: type value. (state, field(value), value) => state =
    (state, field, value) =>
      switch (field) {
      | Indications(index) =>
        let indications = Array.copy(state.indications);
        Array.setUnsafe(indications, index, value);
        {indications: indications};
      };
};

module IndicateForm = ReForm.Make(IndicateStateLenses);

module Main = {
  [@react.component]
  let make = (~appState: AppState.t, ~existingIndication) => {
    /* Disable indication buttons if user is not secretary */
    let canIndicate =
      switch (appState.loggedIn) {
      | AnyStudent(Secretary(secretary)) =>
        switch (secretary.student.team.kind) {
        | ParliamentGroup
        | Country => true
        | _ => false
        }
      | _ => false
      };

    let (isLocked, lock) =
      React.useState(() => Option.isSome(existingIndication) || !canIndicate);
    let (submitSuccessful, setSubmitSuccessful) = React.useState(() => false);

    let {state, submit, getFieldState, handleChange}: IndicateForm.api =
      IndicateForm.use(
        ~schema=
          IndicateForm.Validation.Schema(
            Array.makeBy(8, index =>
              IndicateForm.Validation.Custom(
                Indications(index),
                values =>
                  switch (values.indications[index]->Option.flatMap(Fn.id)) {
                  | Some(_) => Valid
                  | _ => Error(s_("You must indicate on all articles"))
                  },
              )
            ),
          ),
        ~initialState={
          indications:
            existingIndication
            ->Option.map(data =>
                Array.map(data##indications, ind => Some(ind))
              )
            ->Option.getWithDefault(Array.make(8, None)),
        },
        ~onSubmit=
          ({state, send}) => {
            appState.setModal(
              mkModal(
                () => send(ValidateForm(false)),
                () =>
                  MakeDirectiveIndicationQuery.make(
                    ~directive=AppState.idToString(appState.currentDirective),
                    ~indications=
                      state.values.indications->Array.keepMap(Fn.id),
                    (),
                  )
                  ->Api.sendQuery
                  |> Js.Promise.then_(_data => {
                       setSubmitSuccessful(Fn.const(true));
                       lock(Fn.const(true));
                       Js.Promise.resolve();
                     })
                  |> ignore,
              ),
            );
            None;
          },
        (),
      );

    let%Epitath (artResponse, dirResp) = Fetcher.whenReady2((
        Fetcher.pollQL(ArticlesQuery.make()),
        Fetcher.pollQL(DirectiveQuery.make()),
    ));

    let (selections, articles) = (
        dirResp##directive##selections,
        artResponse##directiveArticles
    );

    <div className={"indicate-directive" ++ (isLocked ? " locked" : "")}>
      <h1>
        {canIndicate ? s_("Indicate on directive") : s_("The Directive")}
        ->React.string
      </h1>
      <Button onClick={_=>printOnClick()} className="print-button">
        {rs_("Print directive")}
      </Button>
      <p
        className="description"
        dangerouslySetInnerHTML={
          "__html": description ++ (canIndicate ? canIndicateDescription : ""),
        }
      />
      <Form onSubmit={submit->Helpers.handleSubmit}>
        {Array.zip(selections, articles)
         ->(
             Array.mapWithIndex((index, (selection, article)) =>
               <ShowDirectiveArticle
                 showIndicateButtons=canIndicate
                 isLocked
                 key={Js.String.make(article##id)}
                 indication={
                   state.values.indications[index]->Option.flatMap(Fn.id)
                 }
                 selected={selection##itemsSelected}
                 handleChange={handleChange(Indications(index))}
                 fieldState={getFieldState(Field(Indications(index)))}
                 article
               />
             )
           )
         ->React.array}
        {JSX.if_(
           canIndicate,
           <Button
             color="danger"
             className="btn-decorated submit"
             disabled={state.formState == Submitting || isLocked}
             onClick=submit>
             {rs_("Send Indication")}
           </Button>,
         )}
        {JSX.if_(
           submitSuccessful,
           <Alert color="success" className="text-center">
             {rs_("Submitted successfully!")}
           </Alert>,
         )}
      </Form>
    </div>;
  };
};

module FinalVoteMain = {
  module ExistingVote = [%graphql
    {|
    query {
      myFinalVote {
        id
        option
      }
    }
    |}
  ];
  module FinalVoteMutation = [%graphql
    {|
    mutation($option: VoteOption!) {
      finalVote(option: $option) {
        didVote
      }
    }
    |}
  ];

  [@react.component]
  let make = (~appState: AppState.t) => {
    let submitState = SubmitState.use();

    let%Epitath (artResp, dirResp, responseQL) = Fetcher.whenReady3((
        Fetcher.pollQL(ArticlesQuery.make()),
        Fetcher.pollQL(DirectiveQuery.make()),
        Fetcher.pollQL(ExistingVote.make()),
    ));

    let (directive, articles) = (
        dirResp##directive,
        artResp##directiveArticles
    );

    let isVotingJob =
      switch (appState.loggedIn) {
      | AnyStudent(Secretary(secretary)) =>
        switch (secretary.student.team.kind) {
        | ParliamentGroup
        | Country => true
        | _ => false
        }
      | _ => false
      };

    let votingDisabled =
      !isVotingJob
      || submitState.state == Success
      || Option.isSome(responseQL##myFinalVote);

    let finalVoteQuery = voteOption =>
      FinalVoteMutation.make(~option=voteOption, ())
      |> Api.sendQuery
      |> Js.Promise.then_(_response => {
           submitState.set(Success);
           Js.Promise.resolve();
         })
      |> ignore;

    <div className="indicate-directive final-directive-vote">
      <h1> {rs_("The Final Vote")} </h1>
      <p className="description">
        {rs_(
           "The Commission has released the final proposal for the directive. Vote Yes, No or Abstain from voting.",
         )}
      </p>
      <Button onClick={_=>printOnClick()} className="print-button">
        {rs_("Print directive")}
      </Button>
      {Array.zip(directive##selections, articles)
       ->(
           Array.map(((selection, article)) =>
             <ShowDirectiveArticle
               showIndicateButtons=false
               isLocked=true
               key={Js.String.make(article##id)}
               indication=None
               selected={selection##itemsSelected}
               handleChange={_ => ()}
               fieldState=Pristine
               article
             />
           )
         )
       ->React.array}
      <div className="vote-buttons mb-3">
        {JSX.if_(
           isVotingJob,
           <>
             <h3> {rs_("Do you vote for passing the directive?")} </h3>
             <p>
               <b> {rs_("WARNING: Your choice cannot be changed.")} </b>
             </p>
             <Button
               disabled=votingDisabled
               onClick={_ => finalVoteQuery(`NO)}
               color="danger"
               className="btn-decorated">
               {rs_("No")}
             </Button>
             <Button
               disabled=votingDisabled
               color="info"
               onClick={_ => finalVoteQuery(`ABSTAIN)}
               className="btn-decorated">
               {rs_("Abstain")}
             </Button>
             <Button
               disabled=votingDisabled
               color="primary"
               onClick={_ => finalVoteQuery(`YES)}
               className="btn-decorated">
               "Ja"->React.string
             </Button>
           </>,
         )}
      </div>
      {submitState.present()}
      {JSX.ifSome(responseQL##myFinalVote, vote =>
         <Alert color="secondary">
           {interpolate(
              s_("Your team has voted %s"),
              [
                switch (vote##option) {
                | `ABSTAIN => s_("Abstain")
                | `YES => s_("Yes")
                | `NO => s_("No")
                },
              ],
            )
            ->React.string}
         </Alert>
       )}
    </div>;
  };
};

/* Wrapper to fetch possible existing indication, so initialization of the main
   component is straightforward. */
[@react.component]
let make = (~appState: AppState.t) =>
if (appState.currentRound##isFinal) {
    <FinalVoteMain appState />;

} else {

    let%Epitath indicationData = Fetcher.whenReady1(
        Fetcher.pollQL(GetIndicationQuery.make()),
    );

    <Main
        appState
        existingIndication=indicationData##myIndicationFor
    />;
};
