open Gettext;
open Belt;
open BsReactstrap;

let description = s_(
    {js|
        In this chart, you can see whether a State or Group has indicated that they want an article to be more extensive →, less exensive ← or whether they are neutral --. 

The 7 Member States in the Council of Ministers and the 4 EP-Groups in the Labour Market Committee have all indicated their opinions on each of the 8 articles in the Directive. 

Use the chart to make your next Directive draft one that gathers more support. 
    |js},
);


module DirectiveIndicationsQuery = [%graphql
    {| query
        {
            directiveIndications {
                indications
                by {
                    name
                }
            }
        }
    |}
]

module DirectiveIndicationsSumQuery = [%graphql
    {| query
        {
            directiveIndicationsSum {
                more
                less
                ok
            }
        }
    |}
]

type indication = [ | `LESS | `OK | `MORE];

let moreIcon = Require.file("../img/right-arrow.svg");
let lessIcon = Require.file("../img/left-arrow.svg");
let fineIcon = Require.file("../img/dash.svg");

[@react.component]
let make = (~appState: AppState.t) => {
    let%Epitath _commission = UserIs.commissionMember(appState.team);
    let%Epitath (sets, sums) = Fetcher.whenReady2(
        (
            Fetcher.pollQL(DirectiveIndicationsQuery.make()),
            Fetcher.pollQL(DirectiveIndicationsSumQuery.make())
        )
    );
    <div>
        <h1>{rs_("Directive indications")}</h1>
        <p>description -> React.string</p>
        <Table striped=true>
            <thead className="thead-light">
                <tr>
                    <th rowSpan={ 2 }> {rs_("Voters")} </th>
                    <th colSpan={ 8 }> {rs_("Directive articles")} </th>
                </tr>
                <tr>
                    <th>{"1" -> React.string}</th>
                    <th>{"2" -> React.string}</th>
                    <th>{"3" -> React.string}</th>
                    <th>{"4" -> React.string}</th>
                    <th>{"5" -> React.string}</th>
                    <th>{"6" -> React.string}</th>
                    <th>{"7" -> React.string}</th>
                    <th>{"8" -> React.string}</th>
                </tr>
            </thead>
            <tbody>
                {sets##directiveIndications -> Array.map(el =>
                    <tr>
                        <td>{el##by##name -> React.string}</td>
                        {el##indications -> Array.map(indication =>
                            <td>{
                                (switch (indication) {
                                    | `LESS => <img src=lessIcon />
                                    | `MORE => <img src=moreIcon />
                                    | `OK => <img src=fineIcon />
                                })
                            }</td>
                        ) -> React.array}
                    </tr>
                ) -> React.array}
            </tbody>
            <tfoot>
                <tr>
                    <th>{"Total -->" -> React.string}</th>
                    {switch(sums##directiveIndicationsSum) {
                        | Some(s) =>
                            {s##more -> Array.map(el =>
                                <th>{string_of_int(el) -> React.string}</th>
                            ) -> React.array}
                        | _ => <th colSpan={ 8 }>{"No data" -> React.string}</th>
                    };}
                </tr>
                <tr>
                    <th>{"Total <--" -> React.string}</th>
                    {switch(sums##directiveIndicationsSum) {
                        | Some(s) =>
                            {s##less -> Array.map(el =>
                                <th>{string_of_int(el) -> React.string}</th>
                            ) -> React.array}
                        | _ => <th colSpan={ 8 }>{"No data" -> React.string}</th>
                    };}
                </tr>
            </tfoot>
        </Table>
    </div>
}
