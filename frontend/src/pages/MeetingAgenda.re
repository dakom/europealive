open Gettext;

let commissionDescription =
  s_(
    {js|
<p>As chief of cabinet, it is your job to call the group to meetings and moderate them. There are several meetings throughout the day, starting with the team meeting where everyone should introduce themselves and their responsibilities, and you should learn more about your priorities as a group. You can see the rest of the meeting agenda by clicking through to its calendar page on the left.</p>

<p>For the rest of the day, you should make sure that the group is brought together before the important influence point and directive deadlines that you can see in the calendar. Again, you are the moderator and you want to get through all the points on the agenda.</p>

<ol class="line-height-lg">
  <li>The directive: What do you think of the newest draft? Decide how you want to signify to the directive. </li>
  <li>Influence points: Look at your IP account. How should you place your IP behind your priorities for the next round? </li>
  <li>Make sure the secretary general starts their tasks and completes them all in time!</li>
  <li>Decide what you should accomplish before you meet again. Make sure everyone knows where to begin. </li>
</ol>
|js},
  );

let countryDescription =
  s_(
    {js|
<p>As chief of staff, it is your job to call the group to meetings and moderate them. There are several meetings throughout the day, starting with the team meeting where everyone should introduce themselves and their responsibilities, and you should learn more about your priorities as a group. You can see the rest of the meeting agenda by clicking through to its calendar page on the left.</p>

<p>For the rest of the day, you should make sure that the group is brought together before the important influence point and directive deadlines that you can see in the calendar. Again, you are the moderator and you want to get through all the points on the agenda.</p>

<ol class="line-height-lg">
  <li>The directive: What do you think of the newest draft? Decide how you want to signify to the directive. </li>
  <li>Influence points: Look at your IP account. How should you place your IP behind your priorities for the next round? </li>
  <li>Make sure the chief secretary starts their tasks and completes them all in time!</li>
  <li>Decide what you should accomplish before you meet again. Make sure everyone knows where to begin. </li>
</ol>
|js},
  );

let epGroupDescription =
  s_(
    {js|
<p>As chief of staff, it is your job to call the team to meetings and moderate them. There are several meetings throughout the day, starting with the team meeting where everyone should introduce themselves and their responsibilities, and you should learn more about your priorities as a team. You can see the rest of the meeting agenda by clicking through to its calendar page on the left.</p>

<p>For the rest of the day, you should make sure that the team is brought together before the important influence points and directive deadlines that you can see in the calendar. Again, you are the moderator and you want to get through all the points on the agenda.</p>

<ol class="line-height-lg">
  <li>The directive: What do you think of the newest draft? Decide how you want to signify to the directive. </li>
  <li>Influence points: Look at your IP account. How should you place your IP behind your priorities for the next round? </li>
  <li>Make sure the group secretary starts their tasks and completes them all in time!</li>
  <li>Decide what you should accomplish before you meet again. Make sure everyone knows where to begin. </li>
</ol>
|js},
  );

let lobbyistDescription =
  s_(
    {js|
<p>As chief of staff, it is your job to call the team to meetings and moderate them. There are several meetings throughout the day, starting with the team meeting where everyone should introduce themselves and their responsibilities, and you should learn more about your priorities as a team. You can see the rest of the meeting agenda by clicking through to its calendar page on the left.</p>

<p>For the rest of the day, you should make sure that the team is brought together before the important influence point deadlines that you can see in the calendar. Again, you are the moderator and you want to get through all the points on the agenda.</p>

<ol class="line-height-lg">
  <li>The directive: What do you think of the newest draft?</li>
  <li>Influence points: Look at your IP account. How should you place your IP behind your priorities for the next round? </li>
  <li>Make sure the chief secretary starts their tasks and completes them all in time!</li>
  <li>Decide what you should accomplish before you meet again. Make sure everyone knows where to begin. </li>
</ol>
|js},
  );

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath chief = UserIs.chief(appState.loggedIn);
  let description =
    switch (chief.student.team.kind) {
    | Commission => commissionDescription
    | ParliamentGroup => epGroupDescription
    | Country => countryDescription
    | LobbyOrganization => lobbyistDescription
    | MediaHouse => s_("This page is not available to Media House teams.")
    };
  <div>
    <h1> {rs_("Meeting Agenda")} </h1>
    <div dangerouslySetInnerHTML={"__html": description} />
  </div>;
};
