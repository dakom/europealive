open BsReactstrap;
open Gettext;
open Belt;

let studentInput = (~allStudents, ~value, ~handleFieldChange, ~fieldState) => {
  <ReformStrap.InputWithMessage
    _type="select" value handleChange=handleFieldChange fieldState>
    <option value=""> {rs_("Choose a student")} </option>
    {Array.map(allStudents, student =>
       <option value=student##id key=student##id>
         {Js.String.concatMany(
                 {[|
                    student##fullName, " (",
                    student##team##name, " - ",
                    student##jobName,
                    ")"
                 |]}, " ")->React.string}
       </option>
     )
     ->React.array}
  </ReformStrap.InputWithMessage>;
};

module SwapStudents = {
  open BsReform;

  let description = s_({js|If a <b>Secretary (all teams), Minister, Spokesperson or Commissioner</b> leaves the game, they must be replaced. Choose an <b>Advisor</b> from the team in question - or another team - and swap the two students' jobs. Remember to let the students know. |js});

  module SwapQuery = [%graphql
    {| mutation($studentId1: ID!, $studentId2: ID!) {
      swapStudents(studentId1: $studentId1, studentId2: $studentId2) @bsVariant {
        didSwap
        errors {
          field
          messages
        }
      }
    } |}
  ];

  module SwapStateLenses = [%lenses
    type state = {
      studentId1: string,
      studentId2: string,
    }
  ];
  module SwapForm = ReForm.Make(SwapStateLenses);

  [@react.component]
  let make = (~allStudents) => {
    let submitState = SubmitState.use();

    let {state, submit, getFieldState, handleChange}: SwapForm.api =
      SwapForm.use(
        ~initialState={studentId1: "", studentId2: ""},
        ~schema=
          SwapForm.Validation.Schema([|
            Custom(
              StudentId2,
              values =>
                if (values.studentId1 == values.studentId2
                    && values.studentId1 != "") {
                  Error(s_("You cannot swap a student with themself"));
                } else {
                  Valid;
                },
            ),
          |]),
        ~onSubmit=
          ({state, send}) => {
            SwapQuery.make(
              ~studentId1=state.values.studentId1,
              ~studentId2=state.values.studentId2,
              (),
            )
            |> Api.sendQuery
            |> Js.Promise.then_(response => {
                 switch (response##swapStudents) {
                 | `DidSwap(_) => submitState.set(Success)
                 | `Errors(_) as error =>
                   submitState.handleError(Some(error))
                 };
                 send(ResetForm);
                 Js.Promise.resolve();
               })
            |> ignore;
            None;
          },
        (),
      );

    <div>
      <h2> {rs_("Swap Student jobs")} </h2>
      <p dangerouslySetInnerHTML={"__html": description} />
      <Form className="clearfix" onSubmit={submit->Helpers.handleSubmit}>
        <FormGroup row=true>
          <Col sm=6>
            {studentInput(
               ~allStudents,
               ~value=state.values.studentId1,
               ~handleFieldChange=handleChange(StudentId1),
               ~fieldState=getFieldState(Field(StudentId1)),
             )}
          </Col>
          <Col sm=6>
            {studentInput(
               ~allStudents,
               ~value=state.values.studentId2,
               ~handleFieldChange=handleChange(StudentId2),
               ~fieldState=getFieldState(Field(StudentId2)),
             )}
          </Col>
        </FormGroup>
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={
            state.formState == Submitting
            || state.values.studentId1 == ""
            || state.values.studentId2 == ""
          }>
          {rs_("Swap")}
        </Button>
      </Form>
      {submitState.present()}
    </div>;
  };
};

module DeleteStudent = {
  open BsReform;

  let deleteDescription = s_({js|If an <b>Advisor</b> leaves the game, you can delete them here.|js});

  module SwapQuery = [%graphql
    {| mutation($studentId: ID!) {
      deleteStudent(studentId: $studentId) @bsVariant {
        didDelete
        errors {
          field
          messages
        }
      }
    } |}
  ];

  module DeleteStateLenses = [%lenses type state = {studentId: string}];

  module DeleteForm = ReForm.Make(DeleteStateLenses);

  [@react.component]
  let make = (~allStudents) => {
    let deletableStudents =
      Array.keep(allStudents, student => Option.isSome(student##advisor));
    let submitState = SubmitState.use();

    let {state, submit, getFieldState, handleChange}: DeleteForm.api =
      DeleteForm.use(
        ~initialState={studentId: ""},
        ~schema=DeleteForm.Validation.Schema([||]),
        ~onSubmit=
          ({state, send}) => {
            SwapQuery.make(~studentId=state.values.studentId, ())
            |> Api.sendQuery
            |> Js.Promise.then_(response => {
                 switch (response##deleteStudent) {
                 | `DidDelete(_) => submitState.set(Success)
                 | `Errors(_) as error =>
                   submitState.handleError(Some(error))
                 };
                 send(ResetForm);
                 Js.Promise.resolve();
               })
            |> ignore;
            None;
          },
        (),
      );
    <div>
      <h2> {rs_("Delete student")} </h2>
      <p dangerouslySetInnerHTML={"__html": deleteDescription} />
      <Form className="clearfix" onSubmit={submit->Helpers.handleSubmit}>
        <FormGroup>
          <Col sm=6>
            {studentInput(
               ~allStudents=deletableStudents,
               ~value=state.values.studentId,
               ~handleFieldChange=handleChange(StudentId),
               ~fieldState=getFieldState(Field(StudentId)),
             )}
          </Col>
        </FormGroup>
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={
            state.formState == Submitting || state.values.studentId == ""
          }>
          {rs_("Delete")}
        </Button>
      </Form>
      {submitState.present()}
    </div>;
  };
};

module StudentsQuery = [%graphql
  {| query {
    allStudents {
      id
      fullName
      jobName
      secretary {id}
      chief {id}
      advisor {id}
      chief {id}
      strategist {id}
      journalist {id}
      team {
        name
      }
    }
  } |}
];

[@react.component]
let make = () => {
  let%Epitath response =
    Fetcher.whenReady1(Fetcher.pollQL(StudentsQuery.make()));
  <div>
    <h1> {rs_("Manage Students")} </h1>
    <SwapStudents allStudents=response##allStudents />
    <DeleteStudent allStudents=response##allStudents />
  </div>;
};
