open Gettext;
open BsReactstrap;
open Belt;

let opt_int_of_string = value => Option.map(value, int_of_string);

module MyNewsItemsQuery = [%graphql
  {|
  {
    myNewsItems {
      articleId @bsDecoder(fn: "opt_int_of_string")
      telegramId @bsDecoder(fn: "opt_int_of_string")
      headline
      publishingTime
      isDraft
    }
  }
  |}
];

let myNewsItemsQuery = MyNewsItemsQuery.make();

let showNewsItems = (items, mkRoute) =>
  if (List.length(items) > 0) {
    <ListGroup>
      {Array.map(List.toArray(items), newsItem =>
         <Router.Link
           route={mkRoute(newsItem)}
           key={Js.String.make((newsItem##articleId, newsItem##telegramId))}>
           (
             Js.String.length(newsItem##headline) > 0
               ? newsItem##headline : s_("[Headline Missing]")
           )
           ->React.string
         </Router.Link>
       )
       |> React.array}
    </ListGroup>;
  } else {
    rs_("No articles");
  };

let routeToReader = newsItem =>
  switch (newsItem##articleId, newsItem##telegramId) {
  | (Some(articleId), None) => Router.NewsReader(`ARTICLE, articleId)
  | (None, Some(telegramId)) => Router.NewsReader(`TELEGRAM, telegramId)
  | _ =>
    Js.Console.error("NewsItem of unrecognized type!!");
    Router.Page404;
  };

let routeToWriter = newsItem =>
  switch (newsItem##articleId, newsItem##telegramId) {
  | (Some(articleId), None) =>
    Router.WriteArticle(Some(Api.Types.Id(articleId)))
  | (None, Some(telegramId)) => Router.NewsReader(`TELEGRAM, telegramId)
  | _ =>
    Js.Console.error("NewsItem of unrecognized type!!");
    Router.Page404;
  };

[@react.component]
let make = () =>
  Fetcher.whenReady1(
    Fetcher.onceQL(myNewsItemsQuery),
    myNewsItems => {
      let (publishedItems, sentItems, draftItems) =
        Array.reduce(
          myNewsItems##myNewsItems,
          ([], [], []),
          ((published, sent, drafts), item) =>
          switch (item##publishingTime, item##isDraft) {
          | (Some(_), _) => ([item, ...published], sent, drafts)
          | (None, true) => (published, sent, [item, ...drafts])
          | (None, false) => (published, [item, ...sent], drafts)
          }
        );
      <div>
        <h1> {rs_("Your articles")} </h1>
        <p>
          <Router.Link
            route={Router.WriteArticle(None)}
            className="btn btn-primary btn-decorated">
            {rs_("Write new article")}
          </Router.Link>
        </p>
        <h2> {rs_("Drafts")} </h2>
        {showNewsItems(draftItems, routeToWriter)}
        {JSX.if_(
           List.length(sentItems) > 0,
           <>
             <h2> {rs_("Waiting for your editor")} </h2>
             {showNewsItems(sentItems, routeToReader)}
           </>,
         )}
        <h2> {rs_("Published articles")} </h2>
        {showNewsItems(publishedItems, routeToReader)}
      </div>;
    },
  );
