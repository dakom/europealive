open Gettext;
open Belt;
open BsReactstrap;
open Utils;

module ResearchCategoriesQuery = [%graphql
  {| query  {
    researchCategories  {
      id
      name
      researchItems {
        id
        name
        order
        content
      }
    }
  }|}
];

[@react.component]
let make = () => {
  let (selectedItem, setItem) =
    React.useReducer((_old, item) => Some(item), None);
  let (activeTab, setTab) = React.useReducer((_old, tab) => tab, "0");
  let%Epitath catData =
    Fetcher.whenReady1(Fetcher.onceQL(ResearchCategoriesQuery.make()));
  <div className="research">
    <h1> {rs_("Research")} </h1>
    <Nav tabs=true>
      {Array.mapWithIndex(catData##researchCategories, (index, category) =>
         <NavItem key={index->Int.toString}>
           <NavLink
             onClick={_ => setTab(Int.toString(index))}
             className={activeOrEmpty(Int.toString(index) == activeTab)}>
             {category##name->React.string}
           </NavLink>
         </NavItem>
       )
       ->React.array}
    </Nav>
    <TabContent activeTab>
      {Array.mapWithIndex(catData##researchCategories, (index, category) =>
         <TabPane tabId={index->Int.toString} key={index->Int.toString}>
           <Row>
             {Array.map(category##researchItems, item =>
                <Col sm=4 key=item##id>
                  <Button
                    onClick={_ => setItem(item)}
                    className={activeOrEmpty(
                      Option.mapWithDefault(selectedItem, false, selected =>
                        selected##id == item##id
                      ),
                    )}>
                    {item##name->Js.String.make->React.string}
                  </Button>
                </Col>
              )
              ->React.array}
           </Row>
         </TabPane>
       )
       ->React.array}
    </TabContent>
    {Option.mapWithDefault(
       selectedItem,
       <p> {rs_("Choose a subject to research above")} </p>,
       item =>
       <div>
         <h2> {item##name->React.string} </h2>
         <ParagraphedText text=item##content safe=true />
       </div>
     )}
  </div>;
};
