open Gettext;

[@react.component]
let make = () => <h3> {rs_("The page was not found!")} </h3>;
