open BsReactstrap;
open BsReform;
open Gettext;

module LoginQuery = [%graphql
  {|
  query($username: String!, $password: String!) {
    login(username: $username, password: $password)
  }|}
];

module LoginState = [%lenses
  type state = {
    username: string,
    password: string,
  }
];

module LoginForm = ReForm.Make(LoginState);

type submitState =
  | NotSubmitted
  | Invalid
  | Success;

[@react.component]
let make = (~startingTime, ~updateGameState) => {
  let (submitState, setSubmitState) = React.useState(() => NotSubmitted);
  let {state, submit, getFieldState, handleChange}: LoginForm.api =
    LoginForm.use(
      ~schema=
        LoginForm.Validation.Schema([|
          Custom(
            Password,
            values =>
              Js.String.length(values.password) > 0
                ? Valid : Error(s_("Write your password here")),
          ),
        |]),
      ~initialState={password: "", username: ""},
      ~onSubmit=
        ({state}) => {
          Js.log("Submitting");
          LoginQuery.make(
            ~username=state.values.username,
            ~password=state.values.password,
            (),
          )
          |> Api.sendQuery
          |> Js.Promise.then_(data => {
               setSubmitState(_ => data##login ? Success : Invalid);
               updateGameState();
               Js.Promise.resolve();
             })
          |> ignore;
          None;
        },
      (),
    );

  <PreGameBox ?startingTime>
    <JSX.Toggler
      first={toggle =>
        <Form onSubmit={submit->Helpers.handleSubmit}>
          <h1>
            {rs_("Log in")}
            <Button color="default" className="float-right" onClick=toggle>
              {rs_("Create new user")}
            </Button>
          </h1>
          <FormGroup>
            <Label> {rs_("Username")} </Label>
            <ReformStrap.InputWithMessage
              id="username-input"
              _type="username"
              value={state.values.username}
              fieldState={getFieldState(Field(Username))}
              handleChange={handleChange(Username)}
            />
          </FormGroup>
          <FormGroup>
            <Label> {rs_("Password")} </Label>
            <ReformStrap.InputWithMessage
              id="password-input"
              _type="password"
              value={state.values.password}
              fieldState={getFieldState(Field(Password))}
              handleChange={handleChange(Password)}
            />
          </FormGroup>
          <Button
            disabled={state.formState == Submitting || submitState == Success}
            color="primary"
            className="btn-decorated mb-3">
            {rs_("Log in")}
          </Button>
          <ChooseLanguage className="float-right" />
          {JSX.if_(
             submitState == Success,
             <Alert color="success">
               {rs_("Logged in! Loading page...")}
             </Alert>,
           )}
          {JSX.if_(
             submitState == Invalid,
             <Alert color="danger">
               {rs_("Wrong password or username")}
             </Alert>,
          )}
          <Alert color="secondary-lighter">
          <p>{rs_(
            "Europe at Work uses the technical cookies necessary for the platform to work - nothing else." 
          )}</p>
          <p>{rs_(
            "We process your login information (your name and password) while the game is being played and for no more than 72 hours. All text written on the platform by you, as well as all actions will also be deleted within a maximum of 72 hours."
          )} </p>
          <p>{rs_(
            "If you have a Vimeo account and you are logged in to this, we recommend logging out to ensure that Vimeo does not gather data about you."
          )}</p>
          </Alert>
        </Form>
      }
      second={toggle =>
        <>
          <h1>
            {rs_("Create User")}
            <Button color="default" className="float-right" onClick=toggle>
              {rs_("Existing User")}
            </Button>
          </h1>
          <CreateUser onSuccess=updateGameState userToCreate=Student />
        </>
      }
    />
  </PreGameBox>;
};
