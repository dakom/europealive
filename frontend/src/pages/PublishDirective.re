open Belt;
open Gettext;
open BsReactstrap;
open BsReform;

module SelectArticleField = {
  [@react.component]
  let make = (~article, ~value, ~handleChange, ~locked) =>
    <IndicateDirective.ShowDirectiveArticle
      showIndicateButtons=false
      isLocked=true
      indication=None
      selected=value
      handleChange={_ => ()}
      fieldState=BsReform.ReForm.Pristine
      mkItemElement={item => {
        let selected = item##number == value;
        <Button
          disabled={selected || locked}
          onClick={_ => handleChange(item##number)}>
          {if (selected) {
             rs_("Chosen");
           } else {
             rs_("Choose");
           }}
        </Button>;
      }}
      article
    />;
};

module DataQuery = [%graphql
  {| query {
    nextRound {
      id
      isFinal
      startTime @bsDecoder(fn: "Utils.jsonToMoment")
      directive {
        id
        selections {
          itemsSelected
        }
      }
    }
    directive {
        id
        selections {
            itemsSelected
        }
    }
    directiveArticles {
        id
        number
        description
        shortDescription
        items {
            text
            number
        }
    }
  } |}
];

module PublishDirectiveQuery = [%graphql
  {| mutation($items: [Int!]!, $roundId: ID!) {
    publishDirective(items: $items, roundId: $roundId) @bsVariant {
      didPublish
      errors {
        field
        messages
      }
    }
  } |}
];

/* State is specified manually instead of with lenses-ppx in order to
   support index on Items field. */
module ArticlesStateLenses = {
  type state = {items: array(int)};
  type field(_) =
    | Items(int): field(int);

  /*let get: (state, field('value)) => 'value;*/
  let get: type value. (state, field(value)) => value =
    (state, field) =>
      switch (field) {
      | Items(index) => Array.getExn(state.items, index)
      };
  let set: type value. (state, field(value), value) => state =
    (state, field, value) =>
      switch (field) {
      | Items(index) =>
        let items = Array.copy(state.items);
        Array.setUnsafe(items, index, value);
        {items: items};
      };
};

module DirectiveForm = ReForm.Make(ArticlesStateLenses);

module Main = {
  let description =
    rs_(
      "Choose a point under each Directive clause, that you in the Commission
      agree on proposing to the other teams. Point 1 is the least extensive,
      and point 5 is the most extensive.",
    );
  [@react.component]
  let make = (~articles, ~currentDirective, ~nextRound) => {

    let selections = {
        switch(nextRound##directive) {
            | Some(directive) => directive##selections
            | None => currentDirective##selections
        }
    }
    let submitState = SubmitState.use();
    let {state, submit, handleChange}: DirectiveForm.api =
      DirectiveForm.use(
        ~schema=DirectiveForm.Validation.Schema([||]),
        ~initialState={
          items: Array.map(selections, s => s##itemsSelected),
        },
        ~onSubmit=
          ({state}) => {
            Js.log("submit");
            PublishDirectiveQuery.make(
              ~items=state.values.items,
              ~roundId=nextRound##id,
              (),
            )
            ->Api.sendQuery
            |> Js.Promise.then_(response => {
                 switch (response##publishDirective) {
                 | `Errors(_) as error =>
                   submitState.handleError(Some(error))
                 | _ => submitState.set(Success)
                 };
                 Js.Promise.resolve();
               })
            |> ignore;
            None;
          },
        (),
      );
    <div className="indicate-directive">
      <h1> {rs_("Publish Directive")} </h1>
      {switch (nextRound##directive) {
       | Some(_) =>
         <Alert color="info">
           {rs_(
              "You have finished this directive. It will be available
           to everyone when the next round starts.",
            )}
         </Alert>
       | None => <DeadlineAlert time=nextRound##startTime />
       }}
      <p> description </p>
      <Form onSubmit={submit->Helpers.handleSubmit}>
        {Array.zip(articles, state.values.items)
         -> { Array.mapWithIndex((index, (article, chosenItem)) => {
             <SelectArticleField
               article
               key={article##id->Js.String.make}
               handleChange={handleChange(Items(index))}
               value=chosenItem
               locked={Option.isSome(nextRound##directive)}
             />
         })}
         ->React.array}
      {switch (nextRound##directive) {
        | Some (_) =>
            <Button
                color="info"
                className="btn-decorated mb-3"
                disabled=true>
                {rs_("Waiting for next round...")}
            </Button>
        | None =>
            <Button
              color="danger"
              className="btn-decorated mb-3"
              disabled={state.formState == Submitting}>
              {rs_("Publish directive")}
            </Button>
        }}
        {submitState.present()}
      </Form>
    </div>;
  };
};

// Wrapper compoment around Main to fetch data for form initialization
[@react.component]
let make = (~appState: AppState.t) => {
    let%Epitath (response) = Fetcher.whenReady1(
        Fetcher.pollQL(DataQuery.make())
    );

    let cannotPublishMsg =
        <>
          <h1> {rs_("Publish Directive")} </h1>
          {rs_(
             "You are in the final round, and cannot send out any more proposals.",
           )}
        </>;

    // unpack data (cannot be done in Main constructor)
    let (articles, currentDirective, nextRoundOption) = (
        response##directiveArticles,
        response##directive,
        response##nextRound
    )

    switch (nextRoundOption) {
        | None => cannotPublishMsg
        | Some(nextRound) =>
            <Main
              articles
              currentDirective
              nextRound
              key={appState.currentDirective->Js.String.make}
            />
    };
};
