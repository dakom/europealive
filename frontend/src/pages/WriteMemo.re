open Gettext;
open Belt;
open BsReform;
open BsReactstrap;

let description =
  fun
  | AppState.Technical =>
    s_(
      {js|
When the representatives of the teams meet to negotiate it’s important that they know what the articles in the directive mean.
Write a brief to your leader where you describe which article in the directive is most important to you right now - and why.
Use what you know about your priorities and the directive.
|js},
    )
  | Political =>
    s_(
      {js|
When the Ministers and Spokepersons meet to negotiate it’s important that they know who they represent.
Write a brief to your leader where you describe why your priorities are especially important for your voters. Which compromises can you live with, and which are unacceptable?
Use what you know about your team’s background and political relations.
|js},
    )
  | Media =>
    s_(
      {js|
It is important to look good in the media in order to succeed.
Write a brief to your leader where you describe how best to make statements to the media.
Which stories would you like to see on NEWstream? How can you make your statements interesting for the Media Houses?
Use what you know about the Media Houses’ interests and your team’s strategy.
|js},
    )
  | Attache =>
    s_(
      {js|
When the representatives of the teams meet to negotiate it’s important that they know, who they are negotiating with.
Write a brief to your leader where you describe which other teams you think that your team should cooperate with. What can you collaborate about? What’s in it for you?
Use what you know about the other teams’ strategies and priorities.
|js},
    );

let jsonToMomentOpt = json => Option.map(json, Utils.jsonToMoment);

module MemoQuery = [%graphql
  {| query {
    myMemoDraft {
      id
      subject
      content
    }
    nextMemoDeadline {
      id
      deadlineTime @bsDecoder(fn: "Utils.jsonToMoment")
    }
    mySentMemos {
      writer {
        kind
        student {
          user {
            fullName
          }
        }
      }
      id
      subject
      content
      sent @bsDecoder(fn: "jsonToMomentOpt")
    }
  }|}
];

module Writer = {
  module WriteMemoStateLenses = [%lenses
    type state = {
      subject: string,
      content: string,
    }
  ];

  module UpdateMemoQuery = [%graphql
    {|
    mutation($id: ID!, $subject: String!, $content: String!,
             $sendMemo: Boolean!) {
      updateMemo(id: $id, subject: $subject, content: $content,
                 sendMemo: $sendMemo) {
        advisorMemo {
          id
        }
      }
    }
  |}
  ];

  let updateMemoQuery = (~id, ~subject, ~content, ~send, ~handleResponse) =>
    UpdateMemoQuery.make(~id, ~subject, ~content, ~sendMemo=send, ())
    |> Api.sendQuery
    |> Js.Promise.then_(data => {
         handleResponse(data);
         Js.Promise.resolve();
       })
    |> ignore;

  let autosaveMemoQueryDebounced =
    Debouncer.make(~wait=1000, ((id, subject, content)) =>
      updateMemoQuery(
        ~id,
        ~subject,
        ~content,
        ~send=false,
        ~handleResponse=Fn.const(),
      )
    );

  module MemoForm = ReForm.Make(WriteMemoStateLenses);

  let showXOutOfY = (x, y) =>
    interpolate(s_("%s/%s characters"), (x, y))->React.string;

  [@react.component]
  let make = (~existingDraft) => {
    let submitState = SubmitState.use();
    let {state, submit, getFieldState, handleChange}: MemoForm.api =
      MemoForm.use(
        ~schema=
          MemoForm.Validation.Schema([|
            MemoForm.Validation.Custom(
              Subject,
              values =>
                switch (Js.String.length(values.subject)) {
                | 0 => Error(s_("You must write a subject"))
                | num when num > 50 => Error(s_("The subject is too long"))
                | _ => Valid
                },
            ),
            MemoForm.Validation.Custom(
              Content,
              values =>
                switch (Js.String.length(values.content)) {
                | 0 => Error(s_("Write the brief here"))
                | num when num > 2000 => Error(s_("The brief is too long"))
                | _ => Valid
                },
            ),
          |]),
        ~initialState={
          subject: existingDraft##subject,
          content: existingDraft##content,
        },
        ~onSubmit=
          ({state}) => {
            updateMemoQuery(
              ~id=existingDraft##id,
              ~subject=state.values.subject,
              ~content=state.values.content,
              ~send=true,
              ~handleResponse=response =>
              switch (response##updateMemo) {
              /* We simply show success message, and wait for next GQL poll
                 to re-initialize Writer  */
              | Some(_memo) => submitState.set(Success)
              | None => submitState.handleError(None)
              }
            );
            None;
          },
        (),
      );

    /* Intercept field change to auto-save */
    let handleChange = (a, b) => {
      autosaveMemoQueryDebounced((
        existingDraft##id,
        state.values.subject,
        state.values.content,
      ));
      handleChange(a, b);
    };

    <div>
      <Form
        className="blocky-form clearfix"
        onSubmit={submit->Helpers.handleSubmit}>
        <FormGroup>
          <Label>
            {rs_("Subject")}
            <span className="float-right">
              {showXOutOfY(Js.String.length(state.values.subject), 50)}
            </span>
          </Label>
          <ReformStrap.InputWithMessage
            _type="text"
            placeholder={s_("Write your subject here")}
            value={state.values.subject}
            fieldState={getFieldState(Field(Subject))}
            handleChange={handleChange(Subject)}
          />
        </FormGroup>
        <FormGroup>
          <Label>
            {rs_("Content")}
            <span className="float-right">
              {showXOutOfY(Js.String.length(state.values.content), 2000)}
            </span>
          </Label>
          <ReformStrap.InputWithMessage
            _type="textarea"
            placeholder={s_("Write the brief here")}
            value={state.values.content}
            fieldState={getFieldState(Field(Content))}
            handleChange={handleChange(Content)}
          />
        </FormGroup>
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={state.formState == Submitting}>
          {rs_("Send")}
        </Button>
        {submitState.present()}
      </Form>
    </div>;
  };
};

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath advisor = UserIs.advisor(appState.loggedIn);
  let%Epitath response =
    Fetcher.whenReady1(Fetcher.pollQL(MemoQuery.make()));
  <div>
    <h1> {rs_("Write Brief")} </h1>
    {JSX.ifSome(response##nextMemoDeadline, deadline =>
       <DeadlineAlert
         text={sp_("Minutes until", "until next note must be handed en")}
         time=deadline##deadlineTime
       />
     )}
    <ParagraphedText text={description(advisor.kind)} />
    <Writer
      existingDraft={response##myMemoDraft}
      // The key ensures writer is re-initialized when new draft is started
      key=response##myMemoDraft##id
    />
    {JSX.if_(
       Array.length(response##mySentMemos) > 0,
       <>
         <h2> {rs_("Delivered Notes")} </h2>
         <TeamMemoSubmissions.SentMemos memos=response##mySentMemos />
       </>,
     )}
  </div>;
};
