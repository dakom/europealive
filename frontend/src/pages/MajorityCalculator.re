/* possibly a starting point for creating a MajorityCalculator.
   One way could be to load all teams eligible for voting and then do the
   calculations here, in the frontend. That, however, might lead to some
   annoying duplication of the logic performed in the backend regarding
   qualified and simply majority. But... yeah, this is one way.
*/
open Gettext;
open Belt;
open Utils;
open BsReactstrap;

let description =
  s_(
    {js|
<p>Use this calculator to find out whether you can gather a majority for your preferred directive. <br> 
Click the groups or countries voting "yes" to see if they compose a majority – if they do not, you have more work to do!</p>

<p>In the Council, the rules of qualified majority apply. That means that at least 4 states (55 %) must vote “yes”, and they must at the same time represent at least 65 % of all EU citizens.</p>

<p>In the Parliament, the rules of simple majority apply, meaning that there must be more “yes” than “no” votes in order to pass the directive.</p>

<p>You will need a majority in both the Council and the Parliament for the directive to pass.</p>
|js},
  );

module VotesQuery = [%graphql
{| query {
    countries {
        name
        static {
            votingWeight
            population
        }
    }
    parliamentGroups {
        name
        static {
            votingWeight
            memberCount
        }
    }
}|}
];

// first, we define the voter type as a record, that makes it easy to work with
type voterType = {
    teamName: string,
    votingWeight: float,
    vote: bool
};

// this module will handle displaying a voting situation, that is, either
// the councils voting or the parliaments voting. It needs an array of voters
// as argument, as well as boolean telling it if it is the council (false by
// default
module VotingModule = {
    [@react.component]
    let make = (~voters, ~council=false) => {

        // here, we define two variables: `state` and `toggleVote`. `state` is
        // the state of the module, and `toggleVote` is the function that can
        // update the state
        let (state, toggleVote) =

            // we use `React.useReducer` which is a function that takes two
            // arguments: a function of type (a', b') => a', and an initial
            // state of type a'. That is, the first argument is a function, that
            // can take a state and something of type b' and return and altered
            // state (still of type a'). Here, the type a' is an array of
            // voterType elements and b' is the voterType
            React.useReducer(
                (state, voter) => {
                    // we map over each member of our state
                    Array.map(state, member =>
                        // if the member is the same as the `voter` argument..
                        voter == member ?
                        // ..return a voterType that votes differently...
                        {...member, vote: !member.vote } :
                        // ..else, just return the original member
                        member
                    )
                },

                // this is simply a mapping over the argument `voters` that was
                // given to the module when it was build, and we create a state
                // that is an array of voterType records
                Array.map(voters, voter =>

                    // note that here we use ## to acces fields. This is
                    // because `voter` right now is a GraphQL type of sorts
                    // (but we create records of type voterType wich we can
                    // access with classical dot-notation)
                    {
                        teamName: voter##name,
                        votingWeight: voter##static##votingWeight,
                        vote: false
                    }
                )
            );

        // this function takes a voter (of type voterType) and returns a button
        let mkVoteButton = voter => {
            <Col sm=4>
                <button
                    // we call our `toggleVote` function when the buttono is
                    // clicked which then updates the state
                    onClick={_ => toggleVote(voter)}
                    className={activeOrEmpty(voter.vote)}
                    key={voter.teamName}
                    >
                    { interpolate(
                        "%s (%s %)",
                        (voter.teamName, fractionToPercent(voter.votingWeight))
                    ) -> React.string }
                </button>
            </Col>
        };

        let votingResult = {
            Array.reduce(
                state, 0, (c, voter) => {
                    voter.vote ? c + fractionToPercent(voter.votingWeight) : c
                }
            )
        };

        let voterCount = {
            Array.reduce(state, 0, (c, voter) => voter.vote ? c + 1 : c)
        };
        
        let countryMajority = {voterCount >= 4};

        let voteMajority = {
            council ? {votingResult >= 65} : {votingResult >= 50};
        }
        let votePassed = {
            council ? { countryMajority && voteMajority } :
            voteMajority 
        };
          
        <div>
            <div className="voteButtons">
                <Row>
                    {
                        Array.map(state, voter => mkVoteButton(voter))
                    } -> React.array
                </Row>
            </div>

            <Table >
                <tr>
                    <td>
                        {
                            council ?
                            <span>
                                <b>{rs_("Majority population: ")}</b>{rs_("at least 65 %")}
                            </span>:
                            <span>
                                <b>{rs_("Majority population: ")}</b>{rs_("at least 50 %")}
                            </span>
                        }
                    </td>
                    <td className= {voteMajority ? "count majority" : "count minority"} >
                        {
                            Int.toString(votingResult) -> React.string 
                        } 
                        {rs_(" %")}
                    </td>
                    <td className = {votePassed ? "result majority" : "result minority"} rowSpan={ 2 }>
                        {votePassed ? {rs_("Majority")} : {rs_("Minority")}}
                    </td>
                </tr>
                {JSX.if_(
                    council,
                    <tr>
                        <td><span>
                                <b>{rs_("Majority countries: ")}</b>{rs_("at least 4")}
                            </span></td>
                        <td className= {countryMajority ? "count majority" : "count minority"}>
                            {
                                Int.toString(voterCount) -> React.string
                            }
                        </td>
                    </tr>
                )}
            </Table>   
        </div>
    };
};

[@react.component]
let make = () => {
    let%Epitath (response) = Fetcher.whenReady1(
        (
            Fetcher.pollQL(VotesQuery.make())   
        )
    );

    <div className="majority-calculator">
        <h1> {rs_("Majority calculator")} </h1>
        <p dangerouslySetInnerHTML={"__html": description} />
        
        <h4><span>{rs_("Council of Ministers")}</span></h4>
        <VotingModule voters=response##countries council=true />

        <h4><span>{rs_("European Parliament")}</span></h4>
        <VotingModule voters=response##parliamentGroups council=false />
    </div>
};




