open Gettext;
open Belt;

let description =
  s_(
    {js| Here you find the briefs that your advisors deliver to you during the day. The briefs provide the knowledge you need in order to negotiate with the Commission, EP-Groups and Member States. They contain good arguments for your priorities or prepare you for giving interviews to the media.|js},
  );

let jsonToMomentOpt = json => Option.map(json, Utils.jsonToMoment);

module MemoQuery = [%graphql
  {| query {
    myTeamMemos {
      writer {
        kind
        student {
          user {
            fullName
          }
        }
      }
      id
      subject
      content
      sent @bsDecoder(fn: "jsonToMomentOpt")
    }
  }|}
];

module SentMemos = {
  [@react.component]
  let make = (~memos) => {
    let (selectedId, setId) =
      React.useReducer(
        (oldId, id) => oldId == Some(id) ? None : Some(id),
        None,
      );

    <div className="write-memo">
      {Array.map(memos, memo =>
         <div
           className={
             "blocky-card"
             ++ Utils.activeOrEmpty(Some(memo##id) == selectedId)
           }
           key=memo##id>
           <div className="info" onClick={_ => setId(memo##id)}>
             {JSX.ifSome(memo##sent, time =>
                <span className="time">
                  {MomentRe.Moment.format("H:mm", time)->React.string}
                </span>
              )}
             {rs_("From:")}
             {Printf.sprintf(
                " %s, %s",
                memo##writer##student##user##fullName,
                AppState.(
                  memo##writer##kind->toNonPolymorphicAdvisor->advisorTitle
                ),
              )
              ->React.string}
           </div>
           <h5 onClick={_ => setId(memo##id)}>
             {(s_("Subject: ") ++ memo##subject)->React.string}
           </h5>
           <div className="content">
             <ParagraphedText text=memo##content />
           </div>
         </div>
       )
       ->React.array}
    </div>;
  };
};

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath _user = UserIs.strategistOrChief(appState.loggedIn);
  let%Epitath response =
    Fetcher.whenReady1(Fetcher.pollQL(MemoQuery.make()));
  <div>
    <h1> {rs_("Briefs from my team")} </h1>
    <p> description->React.string </p>
    {if (Array.length(response##myTeamMemos) > 0) {
       <SentMemos memos=response##myTeamMemos />;
     } else {
       <b className="text-secondary">
         {rs_("Your advisors have not yet sent you any notes")}
       </b>;
     }}
  </div>;
};
