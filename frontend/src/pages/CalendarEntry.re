open Gettext;
open Belt;
open BsReactstrap;

module EntriesQuery = [%graphql
  {| query($entryId: Int!) {
    calendarEntry(entryId: $entryId) {
        id
        text
        playerDescription
        facilitatorDescription
        time @bsDecoder(fn: "Utils.jsonToMoment")
        duration
        roomName
        agenda
        penalty
  }
  }|}
];


[@react.component]
let make = (~entryId: int, ~user: AppState.anyUser) => {
    let timeToString = MomentRe.Moment.format("H:mm");
    let addMinutes = (time, minutes) =>
        MomentRe.Moment.add(
        ~duration=MomentRe.duration(minutes->Int.toFloat, `minutes),
        time,
    );
    let%Epitath entryData =
        Fetcher.(
            whenReady1(
                onceQL(
                    EntriesQuery.make(~entryId=entryId, ()),
                    ~changeWith=[|entryId|]
                )
            )
        );
    let isTeacher = (anyUser: AppState.anyUser) =>
        switch (anyUser) {
        | Teacher(_) => true
        | _ => false
        };
    <div className="calendar-entry">
        <h1> {entryData##calendarEntry##text -> React.string} </h1> 
        <Table>
            <tbody>
            <tr>
                <td>
                    <h2>{rs_("Time:")}</h2>
                </td>
                <td className="td-right">
                    <h2>
                        {timeToString(entryData##calendarEntry##time) 
                            -> React.string}
                        {JSX.ifSome(entryData##calendarEntry##duration, duration =>
                            ("-" ++ addMinutes(
                                entryData##calendarEntry##time, duration)->timeToString)
                                    -> React.string
                            )
                        }
                        
                    </h2>
                </td>
            </tr>
            {JSX.ifSome(
                entryData##calendarEntry##roomName, roomName =>
                    <tr>
                        <td>
                            <h2>{rs_("Room:")}</h2>
                        </td>
                        <td className="td-right">
                            <h2>{roomName -> React.string}</h2>
                        </td>
                    </tr>
            )}
            </tbody>
        </Table>
        
       
        {JSX.ifSome(
            entryData##calendarEntry##playerDescription, playerDescription => 
                <div>
                    <h2>{rs_("What to do")} </h2>
                    <p> {playerDescription -> React.string} </p> 
                </div>
            )
        }
        
        {JSX.ifSome(
            entryData##calendarEntry##penalty, penalty => 
                <div>
                    <h2>{rs_("Points penalty: -")}{string_of_int(penalty) -> React.string} </h2>
                    <p> {rs_("If your team does not submit to this deadline on time, you will be deducted ")} {string_of_int(penalty) -> React.string} {rs_(" influence points")} </p> 
                </div>
            )
        }
        
        {JSX.ifSome(
            entryData##calendarEntry##agenda, agenda => 
                <div className="agenda">
                    <h2>{rs_("Meeting agenda")} </h2>
                    <p> {agenda -> React.string} </p> 
                </div>
            )
        }
        
        {JSX.if_(
            isTeacher(user), 
                {JSX.ifSome(
                    entryData##calendarEntry##facilitatorDescription, facilitatorDescription => 
                        <div>
                            <h2>{rs_("Teacher hints")}</h2>
                            <p> {facilitatorDescription -> React.string} </p> 
                        </div>
                    )
                }
            )
        }
    </div>
};
