open Gettext;
open BsReform;
open BsReactstrap;

let description =
  s_(
    {js| Write a long article with room for quotes from interviews, descriptions of the mood in the negotiations, providing the reader with necessary background knowledge or analysis.
Remember to give your article a catchy headline and a byline which states what the article is about and makes the reader want to read on.

It’s the Editors job to make the article ready for publication and decide when to publish it on NEWstream.  |js},
  );

/* Server interaction */

module GetArticleQuery = [%graphql
  {|
  query($id: Int!) {
    article(id: $id) {
      headline
      leadText
      bodyText
    }
  }
|}
];

module MutateArticleQuery = [%graphql
  {|
  mutation($id: ID, $headline: String!, $isOpinionPiece: Boolean!,
           $isDraft: Boolean!  $isBreakingNews: Boolean!, $bodyText: String!,
           $leadText: String!) {
    updateArticle(input: { id: $id, headline: $headline,
        isOpinionPiece: $isOpinionPiece, isDraft: $isDraft,
        isBreakingNews: $isBreakingNews, bodyText: $bodyText,
        leadText: $leadText}) @bsVariant {
      newsArticle {
        id @bsDecoder(fn: "int_of_string")
      }
      errors {
        field
        messages
      }
    }
  }
  |}
];

type idOrCreating =
  | NotCreated
  | Creating
  | Exists(Api.Types.id(Api.Types.newsArticle));

/* Article form setup */

module ArticleStateLenses = [%lenses
  type state = {
    id: idOrCreating,
    headline: string,
    leadText: string,
    bodyText: string,
    isOpinionPiece: bool,
  }
];

let updateArticleQuery =
    (
      ~isDraft,
      id: Api.Types.id(Api.Types.newsArticle),
      values: ArticleStateLenses.state,
      handleSuccess,
      handleError: Js.Promise.error => unit,
    ) =>
  MutateArticleQuery.make(
    ~id=Api.Types.idToString(id),
    ~headline=values.headline,
    ~isOpinionPiece=values.isOpinionPiece,
    ~isBreakingNews=false,
    ~isDraft,
    ~bodyText=values.bodyText,
    ~leadText=values.leadText,
    (),
  )
  |> Api.sendQuery
  |> Js.Promise.then_(data =>
       switch (data##updateArticle) {
       | Some(`NewsArticle(article)) =>
         Js.Promise.resolve(handleSuccess(article))
       | Some(`Errors(errors)) =>
         Js.Promise.reject(
           Api.Graphql_error(
             "Query error: " ++ Js.String.make(Js.Json.stringifyAny(errors)),
           ),
         )
       | None => Js.Promise.reject(Api.Graphql_error("Unknown query error"))
       }
     )
  |> Js.Promise.catch(error => {
       handleError(error);
       Js.Promise.resolve();
     })
  |> ignore;

let updateArticleQueryDebounced =
  Debouncer.make(~wait=1000, ((id, values, handleError)) =>
    updateArticleQuery(~isDraft=true, id, values, _ => (), handleError)
  );

let finishArticleQuery = (id, values, handleError) =>
  updateArticleQuery(
    ~isDraft=false,
    id,
    values,
    _article => {
      Js.log("Article sent to editor");
      Router.pushRoute(Router.MyArticlesOverview);
    },
    handleError,
  );

module ArticleForm = ReForm.Make(ArticleStateLenses);

let maxLengthHeadline = 50;
let maxLengthBody = 2000;
let maxLengthLead = 300;

let initialState = (id): ArticleStateLenses.state => {
  id,
  headline: "",
  leadText: "",
  bodyText: "",
  isOpinionPiece: false,
};

/* Component */

let showXOutOfY = (x, y) =>
  interpolate(s_("%s/%s characters"), (x, y))->React.string;

[@react.component]
let make =
    (
      ~appState: AppState.t,
      ~initialId: option(Api.Types.id(Api.Types.newsArticle))=?,
    ) => {
  let%Epitath _journalist = UserIs.journalist(appState.loggedIn);
  let (isReady, setIsReady) = React.useState(() => false);
  let (unexpectedResponse, setUnexpectedResponse) =
    React.useState(() => None);
  let {state, submit, getFieldState, handleChange}: ArticleForm.api =
    ArticleForm.use(
      ~schema=
        ArticleForm.Validation.Schema([|
          Custom(
            Id,
            values =>
              switch (values.id) {
              | Exists(_) => Valid
              | _ => Error(s_("The page is not ready!"))
              },
          ),
          Custom(
            Headline,
            values =>
              switch (Js.String.length(values.headline)) {
              | 0 => Error(s_("You must write a headline"))
              | num when num > maxLengthHeadline =>
                Error(s_("Headline is too long"))
              | _ => Valid
              },
          ),
          Custom(
            LeadText,
            values =>
              switch (Js.String.length(values.leadText)) {
              | 0 => Error(s_("You must write a lead paragraph"))
              | num when num > maxLengthLead =>
                Error(s_("The lead paragraph is too long"))
              | _ => Valid
              },
          ),
          Custom(
            BodyText,
            values =>
              switch (Js.String.length(values.bodyText)) {
              | 0 => Error(s_("You must write a body text"))
              | num when num > maxLengthBody =>
                Error(s_("The body text is too long"))
              | _ => Valid
              },
          ),
        |]),
      ~initialState=
        initialState(
          switch (initialId) {
          | Some(id) => Exists(id)
          | None => NotCreated
          },
        ),
      ~onSubmit=
        ({state}) => {
          switch (state.values.id) {
          | Exists(id) =>
            finishArticleQuery(id, state.values, error =>
              setUnexpectedResponse(_ => Some(error))
            )
          | _ => Js.Console.error("The impossible happened") /* Impossible */
          };
          None;
        },
      (),
    );

  /* Initializer */
  React.useEffect1(
    () => {
      switch (state.values.id) {
      | NotCreated =>
        /* Create new article on server, get id */
        setIsReady(_ => true);
        MutateArticleQuery.make(
          ~headline=state.values.headline,
          ~isOpinionPiece=state.values.isOpinionPiece,
          ~isBreakingNews=false,
          ~isDraft=true,
          ~bodyText=state.values.bodyText,
          ~leadText=state.values.leadText,
          (),
        )
        |> Api.sendQuery
        |> Js.Promise.then_(data =>
             switch (data##updateArticle) {
             | Some(`NewsArticle(a)) =>
               handleChange(Id, Exists(Api.Types.Id(a##id)));
               Js.Promise.resolve();
             | Some(`Errors(errors)) =>
               Js.Promise.reject(
                 Api.Graphql_error(
                   "Query error: "
                   ++ Js.String.make(Js.Json.stringifyAny(errors)),
                 ),
               )
             | None =>
               Js.Promise.reject(Api.Graphql_error("Unknown query error"))
             }
           )
        |> Js.Promise.catch(error => {
             setUnexpectedResponse(_old => Some(error));
             Js.Promise.resolve();
           })
        |> ignore;
      | Creating => () /* Impossible */
      | Exists(id) =>
        GetArticleQuery.make(~id=Api.Types.idToInt(id), ())
        |> Api.sendQuery
        |> Js.Promise.then_(data => {
             handleChange(Headline, data##article##headline);
             handleChange(LeadText, data##article##leadText);
             handleChange(BodyText, data##article##bodyText);
             setIsReady(_ => true);
             Js.Promise.resolve();
           })
        |> ignore
      };
      None;
    },
    [||] /* Empty dependency array means only run on first render! */
  );

  /* Intercept field handleChange calls to provide auto-saving */
  let handleChange = (a, b) => {
    switch (state.values.id) {
    | Creating
    | NotCreated => ()
    | Exists(id) =>
      updateArticleQueryDebounced((
        id,
        state.values,
        error => setUnexpectedResponse(_ => Some(error)),
      ))
    };
    handleChange(a, b);
  };

  switch (isReady) {
  | false => <div> {rs_("Loading...")} </div>
  | true =>
    <div>
      <h1> {rs_("Write Article")} </h1>
      <ParagraphedText text=description />
      {JSX.ifSome(unexpectedResponse, response =>
         <Alert color="danger">
           <h2>
             {rs_(
                "An unexpected error happened on the server. Try reloading the page.",
              )}
           </h2>
           {React.string(Js.String.make(response))}
         </Alert>
       )}
      <Form
        className="blocky-form"
        onSubmit={submit->Helpers.handleSubmit}>
        <FormGroup>
          <Label>
            {rs_("Headline")}
            <span className="float-right">
              {showXOutOfY(
                 state.values.headline |> Js.String.length,
                 maxLengthHeadline,
               )}
            </span>
          </Label>
          <ReformStrap.InputWithMessage
            _type="text"
            placeholder={s_("Write the headline here...")}
            value={state.values.headline}
            handleChange={handleChange(Headline)}
            fieldState={getFieldState(Field(Headline))}
          />
        </FormGroup>
        <FormGroup>
          <Label>
            {rs_("Lead paragraph")}
            <span className="float-right">
              {showXOutOfY(
                 state.values.leadText |> Js.String.length,
                 maxLengthLead,
               )}
            </span>
          </Label>
          <ReformStrap.InputWithMessage
            _type="textarea"
            placeholder={s_("Write the lead paragraph here...")}
            value={state.values.leadText}
            handleChange={handleChange(LeadText)}
            fieldState={getFieldState(Field(LeadText))}
          />
        </FormGroup>
        <FormGroup>
          <Label>
            {rs_("Body text")}
            <span className="float-right">
              {showXOutOfY(
                 state.values.bodyText |> Js.String.length,
                 maxLengthBody,
               )}
            </span>
          </Label>
          <ReformStrap.InputWithMessage
            _type="textarea"
            placeholder={s_("Write your body text here...")}
            value={state.values.bodyText}
            handleChange={handleChange(BodyText)}
            fieldState={getFieldState(Field(BodyText))}
          />
        </FormGroup>
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={state.formState == Submitting}>
          (state.formState == Submitting ? s_("Sending...") : s_("Publish"))
          ->React.string
        </Button>
      </Form>
    </div>
  };
};
