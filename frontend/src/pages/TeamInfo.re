open Gettext;
open BsReactstrap;
open Belt;

let negotiatorWin =
  s_(
    {js|
      <p>Today is about negotiating a directive on the labour market. <br>
      The winner is the team that achieves the most InfluencePoints. You get IP by investing in your priorities and by influencing the directive, but also by negotiating your priorities to a better place in the final directive.  
      <br> Remember, it’s best to come home with results - the directive should ideally be adopted.  
      </p>
    |js},
  );

let journalistWin =
  s_(
    {js|
      <p> Today is about negotiating a directive on the labour market, and you are covering the negotiations along with the other media houses. 
      <br>The winner is the media house that achieves the most InfluencePoints. You get IP by writing articles that throw light on the negotiations. Every time another player answers that an article of yours has made them smarter, you get a point. If they answer that this is not the case, however, you will have a point deducted. </p>
    |js},
  );

let negotiatorPoints =
  s_(
    {js|
      <p>Here you can see how much influence your team has on the directive. The more influence you have in the negotiations, the more IP you will get!</p>
      <p>If you want to know more about IP, look at your secretary’s Workspace. </p>
    |js},
  );

let journalistPoints =
  s_(
    {js|
      <p>Here you can see how may points have been earned by your articles. The better articles you write, the more points you will receive!</p>
    |js},
  );


module GetIPTransactions = [%graphql
  {|{
  myTeamIpLog {
    transactions{
      id
      description
      amount
      recipient {
        id
        name
      }
      sender {
        id
        name
      }
      timeSent @bsDecoder(fn: "Utils.jsonToMoment")
    }
    currentBalance
  }
  myTeam {
    specialSuccessCriteria
    priorities {
      article {
        number
        shortDescription
      }
    }
  }
}|}
];

type transactionDirection =
  | Received
  | Sent;

let showTransaction = (myTeam, (transaction, balanceAfter: int)) => {
  let amount =
    transaction##amount * (transaction##recipient##id == myTeam ? 1 : (-1));
  <tr key=transaction##id>
    <td>
      {React.string(MomentRe.Moment.format("HH:mm", transaction##timeSent))}
    </td>
    <td> {transaction##description->React.string} </td>
    <td className="text-right">
      (amount > 0 ? "+ " : "- ")->React.string
      {amount->abs->Int.toString->React.string}
    </td>
    <td className="text-right">
      {balanceAfter->Int.toString->React.string}
    </td>
  </tr>;
};

let transactionsToPreview = 8;

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath anyStudent = UserIs.student(appState.loggedIn);

  let (winningDescription, pointsDescription) = 
    switch (anyStudent) {
      | Journalist(_) => (journalistWin,journalistPoints)
      | _ => (negotiatorWin, negotiatorPoints)
    };

  let (showAllTransactions, setShowAllTransactions) =
    React.useState(() => false);
  let%Epitath transactionData =
    Fetcher.whenReady1(Fetcher.pollQL(GetIPTransactions.make()));
  let transactions = transactionData##myTeamIpLog##transactions;
  let myTeamId = AppState.team(anyStudent).id |> AppState.idToString;

  /* Calculate what the IP balance was before each transaction */
  let amounts =
    Belt.Array.reduce(
      transactions,
      [transactionData##myTeamIpLog##currentBalance],
      (amounts, transaction) =>
      switch (amounts) {
      | [] => [] /* Unreachable due to reduce initial value */
      | [a, ...previous_amounts] =>
        if (transaction##recipient##id == myTeamId) {
          [a - transaction##amount, a, ...previous_amounts];
        } else {
          [a + transaction##amount, a, ...previous_amounts];
        }
      }
    )
    |> List.toArray;
  Array.reverseInPlace(amounts); /* Un-reverses amounts */

  let sliceTransactions =
    if (showAllTransactions) {
      Fn.id;
    } else {
      transactions =>
        Array.slice(transactions, ~offset=0, ~len=transactionsToPreview);
    };

  <div className="team-info">
    <h1> (rs_("How To Win"))</h1>
    <p dangerouslySetInnerHTML={"__html": winningDescription} />
    {JSX.if_(
      Array.length(transactionData##myTeam##priorities) > 0,
      <>
        <h4> (rs_("Priorities")) </h4>
        <ul className="team-priority-list">
          {Array.map(transactionData##myTeam##priorities, priority =>
              <li key={Int.toString(priority##article##number)}>
                {React.string(
                  Utils.concat([|
                    s_("Article "),
                    priority##article##number->Int.toString,
                    ". ",
                    priority##article##shortDescription,
                  |]),
                )}
              </li>
            )
            ->React.array}
        </ul>
      </>,
    )} 
    <h4> (rs_("Special Success Criteria")) </h4>
    <p> {transactionData##myTeam##specialSuccessCriteria->React.string} </p>
    <h4> (rs_("InfluencePoint log"))</h4>
    <p dangerouslySetInnerHTML={"__html": pointsDescription} />
    <Table striped=true className="transaction-table">
      <thead className="thead-light">
        <tr>
          <th> (rs_("Time"))</th>
          <th> (rs_("Note")) </th>
          <th> (rs_("Amount"))</th>
          <th> (rs_("New balance")) </th>
        </tr>
      </thead>
      <tbody>
        {Array.zip(transactions, amounts)
        ->sliceTransactions
        ->Array.map(showTransaction(myTeamId))
        ->React.array}
        {JSX.if_(
          transactionsToPreview < Array.length(transactions)
          && !showAllTransactions,
          <tr
            onClick={_event => setShowAllTransactions(Fn.const(true))}
            className="show-all">
            <td />
            <td> (rs_("Show more"))</td>
            <td />
            <td />
          </tr>,
        )}
      </tbody>
    </Table>
  </div>;
}
