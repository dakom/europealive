open Gettext;
open Belt;
open BsReactstrap;


module EntriesQuery = [%graphql
  {|{
  extendedCalendarEntries {

    name
    time @bsDecoder(fn: "Utils.jsonToMoment")
    duration
    roomName
    playerDescription
    facilitatorDescription
    agenda
  }
  }|}
];

[@bs.val]
external printOnClick: unit => unit = 
    "window.print";
    
[@react.component]
let make = () => {    
    let timeToString = MomentRe.Moment.format("H:mm");
    let addMinutes = (time, minutes) =>
        MomentRe.Moment.add(
        ~duration=MomentRe.duration(minutes->Int.toFloat, `minutes),
        time,
    );

    let%Epitath entryData =
        Fetcher.whenReady1(Fetcher.pollQL(~interval=30000, EntriesQuery.make()));
        <div className = "teacher-calendar">
            <h1>{rs_("Calendar")}</h1>
            
            <Button onClick={_=>printOnClick()}>
                {rs_("Print calendar")}
            </Button>

            {entryData##extendedCalendarEntries -> Array.map(entry =>
                    <section className="calendar-entry">
                        <h3 className="time">
                            {timeToString(entry##time) 
                                -> React.string}
                            {JSX.ifSome(entry##duration, duration =>
                                ("-" ++ addMinutes(
                                    entry##time, duration)->timeToString)
                                        -> React.string
                                )
                            }
                        </h3>

                        {JSX.ifSome(
                            entry##roomName, roomName => 
                                <h3 className="room-name">
                                    {roomName -> React.string}
                                </h3>
                            )
                        }

                        <h3 className="meeting-title">
                            {entry##name -> React.string}
                        </h3>
                        
                        
                        
                        {JSX.ifSome(entry##agenda, agenda =>
                            <div>
                                <h4> {rs_("Meeting agenda")} </h4>
                                <p>{agenda -> React.string}</p>
                            </div>
                            )
                        }
                        
                        /* A bit wordy, maybe? But the only model I couldn think of that didn't make assumptions on the database*/
                        {switch (entry##playerDescription, entry##facilitatorDescription) {
                            | (Some(playerDescription), Some(facilitatorDescription)) => 
                                <div className="description-wrapper">
                                    <div className="description">
                                        <h4> {rs_("What to do")} </h4>
                                        <p>{playerDescription -> React.string}</p>
                                    </div>
                                    <div className="description">
                                        <h4> {rs_("Teacher hints")} </h4>
                                        <p>{facilitatorDescription -> React.string}</p>
                                    </div>
                                </div>
                                        
                            | (Some(playerDescription), None) => 
                                <div>
                                    <h4> {rs_("What to do")} </h4>
                                    <p>{playerDescription -> React.string}</p>
                                </div>

                            |  (None, Some(facilitatorDescription)) =>
                                    <div> 
                                        <h4> {rs_("Teacher hints")} </h4>
                                        <p>{facilitatorDescription -> React.string}</p>
                                    </div>

                            | (None, None) =>
                                {ReasonReact.null}
                            }
                        }   
                    </section>
                
                )->React.array
            }
    </div>
};