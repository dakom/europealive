open Gettext;
open BsReactstrap;
open Belt;

module MyStaffQuery = [%graphql
    {| query
        {
            myStaffPage {
                strategist {
                    headline
                    subline
                    content
                    times
                }
                chief {
                    headline
                    subline
                    content
                    times
                }
                secretary {
                    headline
                    subline
                    content
                    times
                }
                relationBuilder {
                    headline
                    subline
                    content
                    times
                }
                technicalAdvisor {
                    headline
                    subline
                    content
                    times
                }
                mediaAdvisor {
                    headline
                    subline
                    content
                    times
                }
                politicalAdvisor {
                    headline
                    subline
                    content
                    times
                }
            }
        }
    |}
]

let staffRole = role =>
  <div className="staff-role">
    <div className="header">
      <h5> role##headline->React.string </h5>
      role##subline->React.string
    </div>
    <div className="content">
      {JSX.ifSome(role##content, content => content->React.string)}
         <table>
           <tbody>
            {role##times -> Array.map(tuple => {
                (switch (tuple) {
                    | [|Some(left), Some(right)|] =>
                        <tr key=right>
                          <td> left->React.string </td>
                          <td className="text-right"> right->React.string </td>
                        </tr>
                    | _ => <tr></tr>
                })
            }) -> React.array}
           </tbody>
         </table>
    </div>
  </div>;

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath _ = UserIs.chief(appState.loggedIn);
  let%Epitath staff = Fetcher.whenReady1(
      Fetcher.pollQL(MyStaffQuery.make())
  );
  <div className="my-staff">
    <h1> {rs_("My Staff")} </h1>
    {switch (staff##myStaffPage) {
     | Some(staff) =>
       <>
         <Row className="justify-content-md-center">
            {JSX.ifSome(staff##strategist, role =>
                <Col sm=5> {staffRole(role)} </Col>
            )}
         </Row>
         <Row className="justify-content-md-center">
            {JSX.ifSome(staff##chief, role =>
                <Col sm=5 className="primary"> {staffRole(role)} </Col>
            )}
            {JSX.ifSome(staff##secretary, role =>
                <Col sm=5> {staffRole(role)} </Col>
            )}
         </Row>
         <Row className="justify-content-md-center">
            {JSX.ifSome(staff##technicalAdvisor, role =>
                <Col sm=3> {staffRole(role)} </Col>
            )}
            {JSX.ifSome(staff##politicalAdvisor, role =>
              <Col sm=3> {staffRole(role)} </Col>
            )}
            {JSX.ifSome(staff##relationBuilder, role =>
              <Col sm=3> {staffRole(role)} </Col>
            )}
            {JSX.ifSome(staff##mediaAdvisor, role =>
              <Col sm=3> {staffRole(role)} </Col>
            )}
         </Row>
       </>
     | None => rs_("Your team does not have access to the My Staff page.")
     }}
  </div>;
};
