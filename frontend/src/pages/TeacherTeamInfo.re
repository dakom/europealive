open Gettext;
open Belt;
open BsReactstrap;

let jsonToMomentOpt = json => Option.map(json, Utils.jsonToMoment);

module TeamQuery = [%graphql
  {| query($teamId: Int!) {
  team(teamId: $teamId) {
    id
    name
    kind
    startingRoom
    priorities { 
      article {
        number
        shortDescription
      }
    }
    specialSuccessCriteria
    students {
      id
      fullName
      jobName
      jobTitle
    }
  }
  teamMemos(teamId: $teamId) {
    writer {
      kind
      student {
        user {
          fullName
        }
      }
    }
    id
    subject
    content
    sent @bsDecoder(fn: "jsonToMomentOpt")
  }
  }|}
];

// TODO: decide wether to keep (ie reimplement) the option to change
// a teams room from the the teacher platform

// module StartingRoomField = {
//   open BsReform;
// 
//   module UpdateRoomQuery = [%graphql
//     {|
//       mutation($startingRoom: String! $teamId: ID!) {
//         updateStartingRoom(startingRoom: $startingRoom, teamId: $teamId) {
//           didUpdate
//         }
//       }
//     |}
//   ];
// 
//   module RoomStateLenses = [%lenses type state = {startingRoom: string}];
//   module RoomForm = ReForm.Make(RoomStateLenses);
// 
//   [@react.component]
//   let make = (~currentRoom: string, ~teamId: string, ~close: unit => unit) => {
//     let {state, submit, getFieldState, handleChange}: RoomForm.api =
//       RoomForm.use(
//         ~schema=
//           RoomForm.Validation.Schema([|
//             RoomForm.Validation.Custom(
//               StartingRoom,
//               values =>
//                 switch (String.length(values.startingRoom)) {
//                 | 0 => Error(s_("The team must have a room."))
//                 | num when num > 100 =>
//                   Error(s_("The room name must be less than 100 characters"))
//                 | _ => Valid
//                 },
//             ),
//           |]),
//         ~initialState={startingRoom: currentRoom},
//         ~onSubmit=
//           ({state}) => {
//             Js.log2("submit", state);
//             let _response =
//               UpdateRoomQuery.make(
//                 ~startingRoom=state.values.startingRoom,
//                 ~teamId,
//                 (),
//               )
//               ->Api.sendQuery;
//             close();
//             None;
//           },
//         (),
//       );
//     <Form onSubmit={submit->Helpers.handleSubmit}>
//       <ReformStrap.InputWithMessage
//         className="col-sm-6"
//         value={state.values.startingRoom}
//         fieldState={getFieldState(Field(StartingRoom))}
//         handleChange={handleChange(StartingRoom)}
//       />
//       <Button color="primary"> {rs_("Save")} </Button>
//       <Button color="default" onClick=close> {rs_("Cancel")} </Button>
//     </Form>;
//   };
// };

[@react.component]
let make = (~teamId: int) => {
  
  let%Epitath response =
    Fetcher.(whenReady1(pollQL(TeamQuery.make(~teamId, ()))));
  let team = response##team;
  <div className="teacher-team-info">
    <h1> {rs_(team##name)} </h1>
    <Table>
      <thead className="thead-light">
        <td> {rs_("Priorities")} </td>
        <td> {rs_("Special success criteria")} </td>
      </thead>
      <tr>
        <td>
        {Array.map(team##priorities, priority =>
          <span>
            {React.string(
               Utils.concat([|
                 priority##article##number->Int.toString,
                 ". ",
                 priority##article##shortDescription,
               |]),
             )}
             <br/>
          </span>    
        )
        ->React.array}
        </td>
        <td> {team##specialSuccessCriteria->React.string} </td>
      </tr>
    </Table>
    // <h2> {rs_("Starting Room")} </h2>
    // <p>
    //   <JSX.Toggler
    //     first={toggle =>
    //       <>
    //         <span className="mr-1"> {team##startingRoom->React.string} </span>
    //         <Button size="sm" onClick=toggle> {rs_("Edit")} </Button>
    //       </>
    //     }
    //     second={toggle =>
    //       <StartingRoomField
    //         key=team##startingRoom
    //         currentRoom=team##startingRoom
    //         teamId={team##id}
    //         close=toggle
    //       />
    //     }
    //   />
    // </p>
    
    <h2> {rs_("Students")} </h2>
    <ul className="student-list">
      {team##students
       ->Array.map(student =>
          <li key=student##id> 
            {interpolate(
              "%s, %s", (student##fullName, student##jobTitle))->React.string} 
          </li>
         )
       ->React.array}
    </ul>
    <h2> {rs_("Briefs sent by team")} </h2>
    <TeamMemoSubmissions.SentMemos memos=response##teamMemos />
  </div>;
};
