open Gettext;
open Belt;
open BsReactstrap;

module NegotiatingTeamsQuery = [%graphql
  {| query { 
    allNegotiatingTeams {
      id
      name
      startingRoom
      influencePoints
  }
}|}
];

module MediaHouseQuery = [%graphql
  {| query {
    mediahouses {
      id
      name
      startingRoom
      influencePoints
     }
  }|}
];

let description =
  s_(
    {js|
    This overview shows the room of each team as well as their current InfluencePoint (IP) status. IP are updated each time a directive is sent out.
<br><br>
You can see the briefs that the advisors on a team have written if you click on the name of the team.
  |js},
  );

let pointsTable = (response) =>
    <Table striped=true>
      <thead className="thead-light">
            <tr>
              <th> {rs_("Team")} </th>
              <th> {rs_("Room")} </th>
              <th> {rs_("InfluencePoints")} </th>
            </tr>
      </thead>
      <tbody>
        {response
          ->Array.map(team =>
              <tr key=team##id>
                <td>
                  <Router.Link
                    route={Router.Teacher(TeamInfo(team##id->int_of_string))}>
                    {team##name->React.string}
                  </Router.Link>
                </td>
                <td> {team##startingRoom->React.string} </td>
                <td> {team##influencePoints->Int.toString->React.string} </td>
              </tr>
          )
          ->React.array}
      </tbody>
    </Table>
    ;

[@react.component]
let make = () => {
  let%Epitath (responseNegotiatingTeams, responseMediaHouses) = Fetcher.whenReady2(
        (
            Fetcher.pollQL(NegotiatingTeamsQuery.make()),
            Fetcher.pollQL(MediaHouseQuery.make())
        )
    );
  
  <div>
    <h1> {rs_("Team InfluencePoints")} </h1>
    <p dangerouslySetInnerHTML={"__html": description} />

    <h3> {rs_("Negotiating Teams")} </h3>
    {pointsTable(responseNegotiatingTeams##allNegotiatingTeams)}
    
    <h3> {rs_("Media Houses")} </h3>
    {pointsTable(responseMediaHouses##mediahouses)}
    
  </div>;
};
