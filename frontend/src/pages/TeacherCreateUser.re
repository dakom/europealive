open Gettext;

[@react.component]
let make = (~updateGameState) => {
  <PreGameBox>
    <h1> {rs_("Sign up as Teacher")} </h1>
    <CreateUser onSuccess={_ => {
      updateGameState()
      Router.pushRoute(Teacher(TeamScore))
    }} userToCreate=Teacher />
  </PreGameBox>;
};
