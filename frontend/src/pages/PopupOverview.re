open BsReactstrap;
open Gettext;
open Belt;
module PopupList = {
  [@react.component]
  let make = (~popups) => {
    Array.map(popups, popup =>
      <Card
        key=popup##id
        className={"mb-3 " ++ (popup##important ? " important" : "")}>
        <CardBody>
          {JSX.if_(
             popup##title != "",
             <CardTitle> {popup##title->React.string} </CardTitle>,
           )}
          <CardText> {popup##message->React.string} </CardText>
          <span className="footer">
            {JSX.ifSome(popup##sender, sender =>
               (sender##fullName ++ " - ")->React.string
             )}
            {MomentRe.Moment.format("H:mm", popup##time)->React.string}
          </span>
        </CardBody>
      </Card>
    )
    ->React.array;
  };
};

[@react.component]
let make = () => {
  let%Epitath response =
    Fetcher.whenReady1(
      Fetcher.pollQL(~interval=20000, PopupMessages.PopupQuery.make()),
    );
  <div>
    <h1> {rs_("All Popup Messages")} </h1>
    <PopupList popups=response##myPopups />
    {JSX.if_(
       Array.length(response##myPopups) == 0,
       <p>
         {rs_(
            "You have not yet received any messages. "
            ++ "They will pop up on the right side of your screen.",
          )}
       </p>,
     )}
  </div>;
};
