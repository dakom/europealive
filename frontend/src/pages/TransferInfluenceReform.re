open Gettext;
open BsReform;
open BsReactstrap;

let description =
  s_(
    {js|
<p>Transfer InfluencePoints to another team here.</p>
<p>It’s important to have an agreement with the team you are transferring to, because you cannot withdraw IPs! </p>
|js},
  );

module TransferStateLenses = [%lenses
  type state = {
    team: option(Api.Types.id(Api.Types.team)),
    amount: string,
    description: string,
  }
];

module TransferForm = ReForm.Make(TransferStateLenses);

let initialState: TransferStateLenses.state = {
  team: None,
  amount: "0",
  description: "",
};

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath student = UserIs.student(appState.loggedIn);
  let loggedInTeam = student->AppState.team;
  let (result, setResult) = React.useState(() => None);

  let {state, submit, getFieldState, handleChange}: TransferForm.api =
    TransferForm.use(
      ~schema=
        TransferForm.Validation.Schema([|
          Custom(
            Description,
            values =>
              Js.String.length(values.description) > 0
                ? Valid : Error(s_("A message is required")),
          ),
          Custom(
            Amount,
            values =>
              switch (Router.stringOfIntOpt(values.amount)) {
              | None => Error(s_("The amount must be a number"))
              | Some(num) when num < 1 =>
                Error(s_("The amount must be positive"))
              | Some(num) when num > loggedInTeam.influencePoints =>
                Error(
                  s_("You cannot transfer more than what is in your account"),
                )
              | _ => Valid
              },
          ),
          Custom(
            Team,
            values =>
              switch (values.team) {
              | None => Error(s_("You must choose a recipient"))
              | _ => Valid
              },
          ),
        |]),
      ~initialState,
      ~onSubmit=
        ({state}) => {
          switch (state.values) {
          | {team: Some(teamId), amount, description} =>
            Api.transferPoints(
              teamId, int_of_string(amount), description, _response =>
              setResult(_old => Some("Hurra"))
            )
          | _ => Js.log("Noget gik galt i onSubmit")
          };
          None;
        },
      (),
    );

  let%Epitath (teams, _round) =
    Fetcher.whenReady2((
      Fetcher.once(Api.listTeam),
      Fetcher.once(Api.retrieveCurrentRound),
    ));
  <div className="transfer-influence">
    <h1> {rs_("Transfer InfluencePoints")} </h1>
    <p dangerouslySetInnerHTML={"__html": description} />
    <Form
      onSubmit={submit->Helpers.handleSubmit}
      className="transfer-ip-form">
      <Row>
        {Belt.Array.keep(teams, team =>
           team.id != AppState.idToGenId(loggedInTeam.id)
         )
         ->Belt.Array.map(team =>
             <Col xs=4 className="team-button" key={Js.String.make(team.id)}>
               <Button
                 color={
                   Some(team.id) == state.values.team
                     ? "primary" : "secondary"
                 }
                 onClick={() => handleChange(Team, Some(team.id))}>
                 {React.string(team.name)}
               </Button>
             </Col>
           )
         |> React.array}
        {switch (getFieldState(Field(Team))) {
         | Error(message) =>
           <Col xs=12>
             <Alert color="danger"> message->React.string </Alert>
           </Col>
         | _ => React.null
         }}
        <Col xs=12>
          <Row className="chooser">
            <Label className="col-6 col-form-label">
              {rs_("Number of IP's (")}
              <b>
                {loggedInTeam.influencePoints->string_of_int->React.string}
              </b>
              {rs_(" in your account):")}
            </Label>
            <Col xs=6>
              <ReformStrap.InputWithMessage
                id="amount-input"
                _type="number"
                min=0.0
                value={state.values.amount}
                fieldState={getFieldState(Field(Amount))}
                handleChange={handleChange(Amount)}
              />
            </Col>
          </Row>
          <Row className="chooser">
            <Label className="col-6 col-form-label">
              {rs_("Besked til modtageren:")}
            </Label>
            <Col xs=6>
              <ReformStrap.InputWithMessage
                id="description-input"
                _type="text"
                value={state.values.description}
                fieldState={getFieldState(Field(Description))}
                handleChange={handleChange(Description)}
                placeholder={s_("Skriv din besked her...")}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <div className="mt-3">
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={state.formState == Submitting}>
          (
            state.formState == Submitting
              ? s_("Transferring...") : s_("Transfer")
          )
          ->React.string
        </Button>
        {switch (result) {
         | Some(_) =>
           <Alert color="success">
             {rs_("InfluencePoints were transferred")}
           </Alert>
         | _ => React.null
         }}
      </div>
    </Form>
  </div>;
};
