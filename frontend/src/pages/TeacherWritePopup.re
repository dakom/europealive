open Gettext;
open BsReactstrap;

module PopupForm = {
  open BsReform;

  module CreatePopupQuery = [%graphql
    {| mutation($title: String!, $message: String!) {
      updatePopupMessage(input: {title: $title, message: $message}) @bsVariant {
        popupMessage {
          id
        }
        errors {
          field
          messages
        }
      }
    }|}
  ];

  module PopupStateLenses = [%lenses
    type state = {
      title: string,
      message: string,
    }
  ];

  module PopupForm = ReForm.Make(PopupStateLenses);

  let initialState: PopupStateLenses.state = {title: "", message: ""};

  [@react.component]
  let make = () => {
    let submitState = SubmitState.use();

    let {state, submit, getFieldState, handleChange}: PopupForm.api =
      PopupForm.use(
        ~initialState,
        ~schema=
          PopupForm.Validation.Schema([|
            PopupForm.Validation.Custom(
              Title,
              values =>
                switch (Js.String.length(values.title)) {
                | 0 => Error(s_("You must write a title"))
                | num when num > 50 =>
                  Error(s_("The title must be shorter than 50 characters"))
                | _ => Valid
                },
            ),
            PopupForm.Validation.Custom(
              Message,
              values =>
                switch (Js.String.length(values.message)) {
                | 0 => Error(s_("You must write a message"))
                | num when num > 1000 =>
                  Error(s_("The message must be shorter than 1000 characters"))
                | _ => Valid
                },
            ),
          |]),
        ~onSubmit=
          ({state, send}) => {
            CreatePopupQuery.make(
              ~title=state.values.title,
              ~message=state.values.message,
              (),
            )
            |> Api.sendQuery
            |> Js.Promise.then_(response => {
                 switch (response##updatePopupMessage) {
                 | Some(`PopupMessage(_)) => submitState.set(Success)
                 | Some(`Errors(_)) as error =>
                   submitState.handleError(error)
                 | None as error => submitState.handleError(error)
                 };
                 send(PopupForm.ResetForm);
                 Js.Promise.resolve();
               })
            |> ignore;

            None;
          },
        (),
      );
    <div>
      <Form
        className="blocky-form clearfix mb-3"
        onSubmit={submit->Helpers.handleSubmit}>
        <FormGroup>
          <Label> {rs_("Title")} </Label>
          <ReformStrap.InputWithMessage
            _type="text"
            value={state.values.title}
            fieldState={getFieldState(Field(Title))}
            handleChange={handleChange(Title)}
          />
        </FormGroup>
        <FormGroup>
          <Label> {rs_("Message")} </Label>
          <ReformStrap.InputWithMessage
            _type="textarea"
            value={state.values.message}
            fieldState={getFieldState(Field(Message))}
            handleChange={handleChange(Message)}
          />
        </FormGroup>
        <Button
          color="danger"
          className="float-right btn-decorated"
          disabled={state.formState == Submitting}>
          {rs_("Send")}
        </Button>
      </Form>
      {submitState.present()}
    </div>;
  };
};

module PopupQuery = [%graphql
  {| query {
	popupsSentByMe {
    id
    message
    title
    important
    sender {
      fullName
    }
    time @bsDecoder(fn: "Utils.jsonToMoment")
  }
  }|}
];

let description =
  rs_(
    {js|Send a message to all students - the message will appear as a pop-up.|js},
  );

[@react.component]
let make = () => {
  let%Epitath response =
    Fetcher.whenReady1(Fetcher.pollQL(PopupQuery.make()));
  <div>
    <h1> {rs_("Send Message To Everyone")} </h1>
    <p> description </p>
    <PopupForm />
    <PopupOverview.PopupList popups=response##popupsSentByMe />
  </div>;
};
