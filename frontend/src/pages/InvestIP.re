open Gettext;
open BsReform;
open Belt;
open BsReactstrap;

let description =
  s_(
    "
<p>InfluencePoints show how much influence your team has on the directive. You start with 100 points, and you get more points by investing them in the articles of the directive. </p>

<p>When investing your IP, there are two things to consider: the priorities of your team, but also which articles you can actually get through negotiations.   
The more the final directive reflects your investment, the more points you will get. You won't lose any IP by investing. </p>

<p>When one of your chosen articles is moved one step up, you will receive a bonus of 20 % on your investment. That means that if you invest 100 IP and the article reaches point 2, you will get a return of 120 IP. If it reaches point 3, you will get a return of 140 IP, and so on. </p>
",
  );

module MakeDirectiveInvestmentQuery = [%graphql
  {|
  mutation($priority: ID!, $amount: Int!) {
    makeDirectiveInvestment(
        input: {priority: $priority, amount: $amount}
    ) {
      directiveInvestment {
        id
      }
    }
  }
  |}
];

module TeamArticlesQuery = [%graphql
  {|
  query {
    myTeam {
      priorities {
        id
        article {
          shortDescription
          number
        }
      }
    }
    myCurrentInvestments {
      priority {
        id
      }
      amount
    }
  }
  |}
];

module InvestmentStateLenses = [%lenses
  type state = {
    investment0: string,
    investment1: string,
    investment2: string,
    /* Meta-field to validate that total investments does not exceed owned IP */
    totalIPAvailable: bool,
  }
];
let allInvestmentFields: array(InvestmentStateLenses.field(_)) = [|
  Investment0,
  Investment1,
  Investment2,
|];

module InvestmentForm = ReForm.Make(InvestmentStateLenses);

let investedIn = (values, field) =>
  InvestmentStateLenses.get(values, field)->Utils.intFromString;

module Main = {
  [@react.component]
  let make = (~appState: AppState.t, ~myTeam, ~existingInvestments) => {
    let%Epitath _secretary = UserIs.secretary(appState.loggedIn);
    let currentIP =
      Option.mapWithDefault(appState.team, 0, team => team.influencePoints);

    let priorities = myTeam##priorities;

    let (submitSuccessful, setSubmitSuccessful) = React.useState(() => false);

    let {state, submit, getFieldState, handleChange}: InvestmentForm.api =
      InvestmentForm.use(
        ~schema=
          InvestmentForm.Validation.Schema(
            Array.map(allInvestmentFields, investmentField =>
              InvestmentForm.Validation.Custom(
                investmentField,
                values =>
                  switch (investedIn(values, investmentField)) {
                  | Some(num) when num < 0 =>
                    Error(s_("Investment cannot be negative"))
                  | Some(_num) => Valid
                  | None =>
                    Error(s_("Investment must be a whole number"))
                  },
              )
            )
            ->Array.concat([|
                InvestmentForm.Validation.Custom(
                  TotalIPAvailable,
                  values =>
                    if (values.totalIPAvailable) {
                      Valid;
                    } else {
                      Error(
                        s_(
                          "You cannot invest more in total than your team currently has",
                        ),
                      );
                    },
                ),
              |]),
          ),
        ~initialState={
          let getExistingInvestment = index =>
            Belt.Option.mapWithDefault(
              existingInvestments[index], "0", investment =>
              investment##amount->Int.toString
            );
          {
            investment0: getExistingInvestment(0),
            investment1: getExistingInvestment(1),
            investment2: getExistingInvestment(2),
            totalIPAvailable: true,
          };
        },
        ~onSubmit=
          ({state, send}) => {
            setSubmitSuccessful(Fn.const(false));
            Array.zip(allInvestmentFields, priorities)
            ->Array.map(((field, priority)) =>
                MakeDirectiveInvestmentQuery.make(
                  ~priority=priority##id,
                  ~amount=
                    InvestmentStateLenses.get(state.values, field)
                    ->int_of_string, /* Should be safe, as value passed validation */
                  (),
                )
                ->Api.sendQuery
              )
            ->Js.Promise.all
            |> Js.Promise.(
                 then_(_data => {
                   setSubmitSuccessful(Fn.const(true));
                   send(SetFormState(Valid));
                   Js.log("Submitted");
                   resolve();
                 })
               )
            |> ignore;

            Js.log2("Submitted", state.values);
            None;
          },
        (),
      );
    /* Intercept handleChange to update totalInvestments */
    let handleChange = (field, value) => {
      handleChange(field, value);
      let totalInvestments =
        allInvestmentFields
        ->Array.map(investmentField =>
            if (field == investmentField) {
              /* This comparison is suuuper not typesafe */
              Utils.intFromString(
                value,
              );
            } else {
              investedIn(state.values, investmentField);
            }
          )
        ->Array.keepMap(Fn.id)
        ->Array.reduce(0, (a, b) => a + b);
      handleChange(TotalIPAvailable, totalInvestments <= currentIP);
    };

    let (hasTimeLeft, renderDeadline) =
      DeadlineAlert.useDeadline(
        ~time=appState.currentRound##ipInvestmentTime,
        (),
      );

    <div className="invest-ip">
      <h1> {rs_("Place InfluencePoints")} </h1>
      {renderDeadline()}
      <p dangerouslySetInnerHTML={"__html": description} />
      <h3> {rs_("Your Priorities")} </h3>
      {JSX.if_(
         !hasTimeLeft,
         <Alert color="danger">
           <b> {rs_("Time is up")} </b>
           {rs_("Invest again next time the Commission sends out a draft")}
         </Alert>,
       )}
      <Form onSubmit={submit->Helpers.handleSubmit}>
        // NOTE: We assume both are ordered by article number

          {Array.zip(priorities, allInvestmentFields)
           ->Array.map(((priority, investmentField)) =>
               <FormGroup key=priority##id className="form-row">
                 <Label className="col-sm-6">
                   <b>
                     {(
                        s_("Priority ")
                        ++ priority##article##number->Int.toString
                        ++ ". "
                      )
                      ->React.string}
                   </b>
                   {priority##article##shortDescription->React.string}
                 </Label>
                 <ReformStrap.InputWithMessage
                   className="col-sm-6"
                   _type="number"
                   value={InvestmentStateLenses.get(
                     state.values,
                     investmentField,
                   )}
                   readOnly={
                       submitSuccessful ||
                       !hasTimeLeft ||
                       Array.length(existingInvestments) > 0
                   }
                   fieldState={getFieldState(Field(investmentField))}
                   handleChange={handleChange(investmentField)}
                 />
               </FormGroup>
             )
           ->React.array}
          {switch (getFieldState(Field(TotalIPAvailable))) {
           | Error(message) =>
             <Alert className="mt-3" color="danger">
               message->React.string
             </Alert>
           | _ => React.null
           }}
          <div className="clearfix mb-3">
            <Button
              color="danger"
              className="float-right btn-decorated"
              disabled={
                  submitSuccessful ||
                  state.formState == Submitting ||
                  !hasTimeLeft ||
                  Array.length(existingInvestments) > 0
              }>
              (state.formState == Submitting ? s_("Sending...") : s_("Send"))
              ->React.string
            </Button>
          </div>
          {JSX.if_(
             submitSuccessful,
             <Alert color="success" className="text-center">
               {rs_("Submitted successfully!")}
             </Alert>,
           )}
        </Form>
    </div>;
  };
};

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath data =
    Fetcher.whenReady1(Fetcher.pollQL(TeamArticlesQuery.make()));
  <Main
    key=appState.currentRound##id
    appState
    myTeam=data##myTeam
    existingInvestments=data##myCurrentInvestments
  />;
};
