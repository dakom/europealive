open Gettext;

[@react.component]
let make = (~appState: AppState.t) => {
  let%Epitath anyStudent = UserIs.student(appState.loggedIn);
  let student = AppState.getStudent(anyStudent);
  <div className="job-description">
    <h1> {rs_("Job Description")} </h1>
    <iframe
      className="job-video"
      src={student.team.videoEmbedUrl}
      allowFullScreen=true
    />
    <ParagraphedText text={student.jobDescription} />
  </div>;
};
