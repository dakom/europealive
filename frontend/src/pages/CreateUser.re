open Gettext;
open BsReform;
open BsReactstrap;

module CreateTeacherQuery = [%graphql
  {|
  mutation($firstName: String!, $lastName: String!, $username: String!,
           $password: String!) {
    createTeacher(input: {firstName: $firstName, lastName: $lastName,
        username: $username, password: $password}) @bsVariant {
      user {
        id
      }
      errors {
        field
        messages
      }
    }
  }|}
];

module CreateStudentQuery = [%graphql
  {|
  mutation($firstName: String!, $lastName: String!, $username: String!,
           $password: String!) {
    createStudent(input: {firstName: $firstName, lastName: $lastName,
        username: $username, password: $password}) @bsVariant {
      user {
        id
      }
      errors {
        field
        messages
      }
    }
  }|}
];

type userToCreate =
  | Teacher
  | Student;

module UserCreationState = [%lenses
  type state = {
    firstName: string,
    lastName: string,
    username: string,
    password: string,
    repeatPassword: string,
  }
];

module UserCreationForm = ReForm.Make(UserCreationState);

[@react.component]
let make = (~onSuccess, ~userToCreate: userToCreate) => {
  let submitState = SubmitState.use();

  let {state, submit, getFieldState, handleChange}: UserCreationForm.api =
    UserCreationForm.use(
      ~schema=
        UserCreationForm.Validation.Schema([|
          Custom(
            FirstName,
            values =>
              Js.String.length(values.firstName) > 0
                ? Valid : Error(s_("Please enter your first name")),
          ),
          Custom(
            LastName,
            values =>
              Js.String.length(values.lastName) > 0
                ? Valid : Error(s_("Please enter your last name")),
          ),
          Custom(
              Username,
              values =>
              switch (Js.String.length(values.username)) {
                  | 0 => Error(s_("Please choose a username"))
                  | num when num > 35 =>
                    Error(s_("Username must be less than 35 characters"))
                  | _ => Valid
                  },
          ),
          Custom(
            Password,
            values =>
              switch (Js.String.length(values.password)) {
              | 0 => Error(s_("Please choose a password"))
              | num when num < 10 =>
                Error(s_("Password must be at least 10 characters"))
              | _ => Valid
              },
          ),
          Custom(
            RepeatPassword,
            values =>
              values.password == values.repeatPassword
                ? Valid : Error(s_("Entered passwords do not match")),
          ),
        |]),
      ~initialState={
        firstName: "",
        lastName: "",
        username: "",
        password: "",
        repeatPassword: "",
      },
      ~onSubmit=
        ({state}) => {
          Js.log("Submitting");
          (
            // GQL queries are awkward to abstract over, so there is some
            // duplication here.
            switch (userToCreate) {
            | Student =>
              CreateStudentQuery.make(
                ~firstName=state.values.firstName,
                ~lastName=state.values.lastName,
                ~username=state.values.username,
                ~password=state.values.password,
                (),
              )
              |> Api.sendQuery
              |> Js.Promise.then_(r => r##createStudent->Js.Promise.resolve)
            | Teacher =>
              CreateTeacherQuery.make(
                ~firstName=state.values.firstName,
                ~lastName=state.values.lastName,
                ~username=state.values.username,
                ~password=state.values.password,
                (),
              )
              |> Api.sendQuery
              |> Js.Promise.then_(r => r##createTeacher->Js.Promise.resolve)
            }
          )
          |> Js.Promise.then_(createUser => {
               switch (createUser) {
               | Some(`Errors(_)) as err
               | None as err => submitState.handleError(err)
               | Some(`User(_)) =>
                 submitState.set(Success);
                 onSuccess();
               };
               Js.Promise.resolve();
             })
          |> ignore;
          None;
        },
      (),
    );
  <Form onSubmit={submit->Helpers.handleSubmit}>
    <FormGroup>
      <Label> {rs_("First name")} </Label>
      <ReformStrap.InputWithMessage
        id="first-name-input"
        _type="text"
        value={state.values.firstName}
        fieldState={getFieldState(Field(FirstName))}
        handleChange={handleChange(FirstName)}
      />
    </FormGroup>
    <FormGroup>
      <Label> {rs_("Last name")} </Label>
      <ReformStrap.InputWithMessage
        id="last-name-input"
        _type="text"
        value={state.values.lastName}
        fieldState={getFieldState(Field(LastName))}
        handleChange={handleChange(LastName)}
      />
    </FormGroup>
    <FormGroup>
      <Label> {rs_("Username")} </Label>
      <ReformStrap.InputWithMessage
        id="username-input"
        _type="username"
        value={state.values.username}
        fieldState={getFieldState(Field(Username))}
        handleChange={handleChange(Username)}
      />
    </FormGroup>
    <FormGroup>
      <Label> {rs_("Password")} </Label>
      <ReformStrap.InputWithMessage
        id="password-input"
        _type="password"
        value={state.values.password}
        fieldState={getFieldState(Field(Password))}
        handleChange={handleChange(Password)}
      />
    </FormGroup>
    <FormGroup>
      <Label> {rs_("Repeat password")} </Label>
      <ReformStrap.InputWithMessage
        id="repeat-password-input"
        _type="password"
        value={state.values.repeatPassword}
        fieldState={getFieldState(Field(RepeatPassword))}
        handleChange={handleChange(RepeatPassword)}
      />
    </FormGroup>
    <Button
      disabled={state.formState == Submitting || submitState.state == Success}
      color="primary"
      className="btn-decorated mb-3">
      {rs_("Create new user")}
    </Button>
    <ChooseLanguage className="float-right" />
    {submitState.present()}
  </Form>;
};
