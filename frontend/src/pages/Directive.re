open Gettext;

module FetchDirTemplate =
  Fetcher.Once({
    type data = array(Api.Types.directiveArticle);
  });

module FetchDirective =
  Fetcher.Poll({
    type data = Api.Types.directive;
  });

let showItem = (articleNum: int, item: Api.Types.directiveItem) =>
  <li key={Js.String.make(item.id)}>
    <b>
      {Printf.sprintf("%i.%i ", articleNum, item.number) |> ReasonReact.string}
    </b>
    {ReasonReact.string(item.text)}
  </li>;

/* Maps the given directive onto the given template of articles. Both must be in sorted order. */
let showDirective =
    (
      directive: Api.Types.directive,
      articles: array(Api.Types.directiveArticle),
    ) =>
  <div>
    <h2> (rs_("Directive"))</h2>
    <Router.Link route=Router.Home>
      (rs_("To frontpage"))
    </Router.Link>
    Belt.(
      Array.zip(directive.selections, articles)
      |> Array.map(_, ((selection, article)) =>
           <div key={Js.String.make(article.id)}>
             <b>
               {ReasonReact.string(string_of_int(article.number) ++ ". ")}
             </b>
             {ReasonReact.string(article.description)}
             <ul>
               {Array.slice(
                  article.items,
                  ~offset=0,
                  ~len=selection.itemsSelected,
                )
                |> Array.map(_, showItem(article.number))
                |> React.array}
             </ul>
           </div>
         )
      |> ReasonReact.array
    )
  </div>;

[@react.component]
let make = (~directiveId) => {
  let%Hook articles = FetchDirTemplate.make(~fetch=Api.listDirectiveArticle);
  let%Hook directive =
    FetchDirective.make(~fetch=Api.retrieveDirective(directiveId));
  <div>
    <h3> (rs_("Directive:"))</h3>
    {showDirective(directive, articles)}
  </div>;
};
