open BsReactstrap;
open Router;

type action =
  | ShowTeam
  | ShowRounds
  | Rounds(array(GenData.round));

type state = {
  showTeam: bool,
  showRounds: bool,
  rounds: array(GenData.round),
};

[@react.component]
let make = () => {
  let (state, send) =
    React.useReducer(
      (oldState: state, action: action) =>
        switch (action) {
        | ShowRounds => {...oldState, showRounds: !oldState.showRounds}
        | Rounds(rounds) => {...oldState, rounds}
        | ShowTeam => {...oldState, showTeam: !oldState.showTeam}
        },
      {showTeam: false, showRounds: false, rounds: [||]},
    );
  React.useEffect1(
    () => {
      Api.listRound(rounds => send(Rounds(rounds)));
      None;
    },
    [||],
  );
  <div>
    <div>
      <h1> {js|Første overskrift h1|js}->React.string </h1>
      <h2> "Anden overskrift h2"->React.string </h2>
      <h3> "Tredje overskrift h3"->React.string </h3>
      <h4> "Fjerde overskrift h4"->React.string </h4>
      <Link className="btn btn-primary" route=TransferInfluence>
        {React.string("Transfer influence")}
      </Link>
      <Team teamId={GenData.Id(5)} />
      <Button color="secondary" onClick={_e => send(ShowRounds)}>
        {if (state.showRounds) {
           React.string("Skjul runder");
         } else {
           React.string("Vis runder");
         }}
      </Button>
      <Button color="primary"> {React.string("Primary")} </Button>
      <Button color="secondary"> {React.string("Secondary")} </Button>
      <Button color="danger"> {React.string("Danger")} </Button>
      <Button color="info"> {React.string("Info")} </Button>
      <ol>
        {if (state.showRounds) {
           state.rounds
           |> Array.mapi((index, round) =>
                <li key={string_of_int(index)}>
                  {React.string({js| Runde længde: |js})}
                  {React.string(string_of_int(round.GenData.length))}
                </li>
              )
           |> React.array;
         } else {
           React.null;
         }}
      </ol>
      <AssignPoints />
    </div>
  </div>;
};
