open Gettext;
open BsReactstrap;
open Belt;

module GetNewsItem = [%graphql
  {|
  query($itemId: ID!, $kind: NewsItemKind!) {
    newsItem(itemId: $itemId, kind: $kind) {
      publishingTime
      headline
      bodyText
      leadText
      isBreakingNews
      writer {
        id
        mediahouse {
          logo
        }
        student {
          user {
            fullName
          }
        }
      }
    }
  }
  |}
];

module UpdateNewsReaction = [%graphql
  {|
  mutation($articleId: ID!, $isPositive: Boolean!) {
    reactToNews(articleId: $articleId, isPositive: $isPositive) {
      didReact
    }
  }
  |}
];

type kind = [ | `ARTICLE | `TELEGRAM];

let kindOfString = str =>
  switch (str) {
  | "article" => `ARTICLE
  | "telegram" => `TELEGRAM
  | _other => raise(Not_found)
  };

[@react.component]
let make = (~kind: kind, ~id: int) => {
  let submitState = SubmitState.use();

  let%Epitath itemData =
    Fetcher.whenReady1(
      Fetcher.onceQL(
        GetNewsItem.make(~itemId=Int.toString(id), ~kind, ()),
        ~changeWith=[|Int.toString(id) ++ Js.String.make(kind)|],
      ),
    );
  let newsReactionQuery = (~isPositive) =>
    UpdateNewsReaction.make(~isPositive, ~articleId=string_of_int(id), ())
    |> Api.sendQuery
    |> Js.Promise.then_(response => {
         switch (response##reactToNews) {
         | Some(didReact) =>
           didReact##didReact
             ? submitState.set(Success) : submitState.handleError(None)
         | None => submitState.handleError(None)
         };
         Js.Promise.resolve();
       })
    |> ignore;
  let newsItem = itemData##newsItem;
  <div className="news-reader">
    <h1> {newsItem##headline->React.string} </h1>
    {JSX.if_(
       newsItem##isBreakingNews,
       <div className="breaking"> {rs_("Breaking News")} </div>,
     )}
    {JSX.ifSome(newsItem##leadText, leadText =>
       <div className="lead-text"> {React.string(leadText)} </div>
     )}
    <Media className="byline">
      <img src=newsItem##writer##mediahouse##logo />
      <Media body=true>
        <div className="writer">
          (rs_("Writer"))
          <br />
          {React.string(newsItem##writer##student##user##fullName)}
        </div>
        {JSX.ifSome(newsItem##publishingTime, time =>
           <div className="time">
             (rs_("Published"))
             <br />
             {(time->Utils.jsonToMoment |> MomentRe.Moment.format("H:mm"))
              ->React.string}
           </div>
         )}
      </Media>
    </Media>
    <ParagraphedText text=newsItem##bodyText />
    <div className="news-reaction">
      (rs_("Did the article make you smarter?"))
      <Button
        color="secondary"
        className="btn-decorated-inverse"
        onClick={_ => newsReactionQuery(~isPositive=true)}>
        (rs_("Yes"))
      </Button>
      <Button
        color="secondary"
        className="btn-decorated-inverse"
        onClick={_ => newsReactionQuery(~isPositive=false)}>
        (rs_("No"))
      </Button>
    </div>
    {submitState.present()}
  </div>;
};
