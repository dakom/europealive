open Gettext;

[@react.component]
let make = (~time) =>
  <PreGameBox startingTime=time>
    <p>
      {rs_(
         "You are logged in and ready. You will be assigned a job when the game starts.",
       )}
    </p>
    <p> {rs_("Waiting for game to start..")} </p>
  </PreGameBox>;
