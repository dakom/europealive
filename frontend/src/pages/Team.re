open Gettext;

let showParticipant = (student: GenData.student) =>
  <li key={Js.String.make(student.id)}>
    <div> {React.string(s_("Name: ") ++ student.firstName)} </div>
  </li>;

[@react.component]
let make = (~teamId) =>
  Fetcher.whenReady1(Fetcher.once(Api.retrieveTeam(teamId)), team =>
    <div>
      <h4> {React.string(s_("Teams: ") ++ team.name)} </h4>
      <div>
        {React.string(
           s_("InfluencePoints: ") ++ string_of_int(team.influencePoints),
         )}
      </div>
      <div> {React.string(s_("Nummer: ") ++ Js.String.make(team.id))} </div>
      <div> {React.string(s_("Medlemmer:"))} </div>
      <ul> {Array.map(showParticipant, team.students) |> React.array} </ul>
    </div>
  );
