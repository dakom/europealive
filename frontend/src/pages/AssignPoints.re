open Gettext;

let presentArticle = (article: Api.Types.directiveArticle) =>
  <div key={Js.String.make(article.id)}>
    <input type_="number" />
    {ReasonReact.string(article.shortDescription)}
  </div>;

[@react.component]
let make = () =>
  Fetcher.whenReady2(
    (
      Fetcher.once(Api.listPointsAssigned),
      Fetcher.poll(Api.listDirectiveArticle),
    ),
    ((_pointsAssigned, articles)) =>
    <div>
      <h3> (rs_("Assign Points")) </h3>
      {Belt.Array.map(articles, presentArticle) |> ReasonReact.array}
      <button className="btn"> (rs_("Save points")) </button>
    </div>
  );
