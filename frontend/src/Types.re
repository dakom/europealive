type username = string;

type password = string;

type token = string;

type user = {
  username,
  loggedIn: bool,
  token,
};

/* State handed to all subpages */
type appState = {
  currentRound: Api.Types.round,
  /*participant: Api.Types.participant,*/
};
