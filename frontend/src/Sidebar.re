open Belt;

let newsIcon = Require.file("./img/News.svg");
let calendarIcon = Require.file("./img/Kalender.svg");
let infoIcon = Require.file("./img/Information.svg");

type subcomponent =
  | Calendar
  | Infobar
  | Newsfeed;

let componentsWithInfo = [|
  (Calendar, calendarIcon),
  (Infobar, infoIcon),
  (Newsfeed, newsIcon),
|];

[@react.component]
let make = () => {
  let (activeComponent, setComponent) =
    React.useReducer((_oldState, action) => action, Newsfeed);
  <div className="sidebar">
    <div className="nav-circles sticky-top">
      {Array.map(componentsWithInfo, ((component, src)) =>
         <div
           key={component->Js.String.make}
           onClick={_ => setComponent(component)}
           className={
             "circle" ++ (component == activeComponent ? " active" : "")
           }>
           <img src />
         </div>
       )
       ->React.array}
    </div>
    {switch (activeComponent) {
     | Newsfeed => <Newsfeed />
     | Infobar => <Infobar />
     | Calendar => <CalendarBar />
     }}
  </div>;
};
