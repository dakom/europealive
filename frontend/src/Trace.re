/* Wrapper around Js.log to insert into dataflow instead of returning unit */
let trace = value => {
  Js.log(value);
  value;
};

let traceTag = (tag, value) => {
  Js.log2(tag, value);
  value;
};
