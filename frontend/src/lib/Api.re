include GenApi;
open Belt;

module Types = {
  include GenData;
  let idToString = id => Belt.Int.toString(idToInt(id));

  module BaseUser = {
    /* Utilities for handling all user types generically. */
    type t =
      | Student(GenData.student);

    let firstName =
      fun
      | Student(student) => student.firstName;
    let lastName =
      fun
      | Student(student) => student.lastName;
    let email =
      fun
      | Student(student) => student.email;
  };
};

let pointTransactionJsonString = (~recipientId, ~amount, ~description) => {
  open Js.Json;
  let payload = Js.Dict.empty();
  let set = Js.Dict.set(payload);
  set("amount", number(float_of_int(amount)));
  set("recipient", number(float_of_int(GenData.idToInt(recipientId))));
  set("description", string(description));
  set("description", string(description));
  stringify(object_(payload));
};

let transferPoints = (recipientId, amount, description, callback) =>
  Js.Promise.(
    Fetch.fetchWithInit(
      "/data/point_transaction/",
      Fetch.RequestInit.make(
        ~method_=Post,
        ~headers=
          Fetch.HeadersInit.make({
            "Content-Type": "application/json",
            "X-CSRFToken":
              JustgageReasonCookie.Cookie.getAsString("csrftoken"),
          }),
        ~body=
          Fetch.BodyInit.make(
            pointTransactionJsonString(~recipientId, ~amount, ~description),
          ),
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(data => resolve(callback(data)))
    |> ignore
  );

exception Graphql_error(string);

/* Helper for sending GraphQL queries to the server */
let sendQuery = q =>
  Bs_fetch.(
    fetchWithInit(
      "/graphql",
      RequestInit.make(
        ~method_=Post,
        ~body=
          Js.Dict.fromList([
            ("query", Js.Json.string(q##query)),
            ("variables", q##variables),
          ])
          |> Js.Json.object_
          |> Js.Json.stringify
          |> BodyInit.make,
        ~credentials=Include,
        ~headers=
          HeadersInit.makeWithArray([|
            ("content-type", "application/json"),
            (
              "X-CSRFToken",
              Belt.Option.getWithDefault(
                JustgageReasonCookie.Cookie.getAsString("csrftoken"),
                "",
              ),
            ),
          |]),
        (),
      ),
    )
    |> Js.Promise.then_(resp =>
         if (Response.ok(resp)) {
           Response.json(resp)
           |> Js.Promise.then_(data =>
                switch (Js.Json.decodeObject(data)) {
                | Some(obj) =>
                  Js.Dict.get(obj, "errors")
                  ->Option.map(Js.Console.error2("Errors in GQL query"))
                  /* TODO: Report error properly!!! */
                  ->ignore;

                  Js.Dict.unsafeGet(obj, "data")
                  |>
                  q##parse
                  |> Js.Promise.resolve;
                | None =>
                  Js.Promise.reject(
                    Graphql_error("Response is not an object"),
                  )
                }
              );
         } else {
           Js.Promise.reject(
             Graphql_error("Request failed: " ++ Response.statusText(resp)),
           );
         }
       )
  );
