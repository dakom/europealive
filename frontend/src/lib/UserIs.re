open Gettext;
/* Functions to extract specific user type, and show login-required page otherwise */

let loginRequired = loginType =>
  <div>
    <h1> {rs_("Access denied")} </h1>
    {rinterpolate(
       s_(
         "You are not allowed to access this page. A %s account is required.",
       ),
       [|loginType|],
     )}
  </div>;


let commissionMember = (team: option(AppState.team), component) =>
    switch (team) {
        | Some(t) =>
            switch(t.kind) {
            | Commission => component(t)
            | _ => loginRequired(s_("European Commission"))
        };
        | None => loginRequired(s_("European Commission"))
    };

let teacher = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | Teacher(t) => component(t)
  | _ => loginRequired(s_("teacher"))
  };

let student = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(s) => component(s)
  | _ => loginRequired(s_("student"))
  };

let secretary = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Secretary(s)) => component(s)
  | _ => loginRequired(s_("secretary"))
  };

let chief = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Chief(s)) => component(s)
  | _ => loginRequired(s_("chief of staff"))
  };

let advisor = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Advisor(s)) => component(s)
  | _ => loginRequired(s_("advisor"))
  };

let strategist = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Strategist(s)) => component(s)
  | _ => loginRequired(s_("strategist"))
  };

let journalist = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Journalist(s)) => component(s)
  | _ => loginRequired(s_("journalist"))
  };

let strategistOrChief = (anyUser: AppState.anyUser, component) =>
  switch (anyUser) {
  | AnyStudent(Strategist(s)) => component(`Strategist(s))
  | AnyStudent(Chief(s)) => component(`Chief(s))
  | _ => loginRequired(s_("strategist or chief"))
  };
