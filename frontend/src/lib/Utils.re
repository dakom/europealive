/* Unsafe function to convert json to Moment.
   The Graphql schema guarantees that this is safe to do for DateTime values,
   but graphql_ppx does not explot this.
   Do not use on arbitrary Json, it will throw exceptions! */
let jsonToMoment = (json: Js.Json.t) => json->Js.String.make->MomentRe.moment;

let clamp = ((min, max), num) =>
  switch (num < min, num > max) {
  | (true, _) => min
  | (_, true) => max
  | _ => num
  };

let intFromString = str =>
  switch (int_of_string(str)) {
  | number => Some(number)
  | exception (Failure(_msg)) => None
  };

/* Wrapper around Js.String.concatMany to not require an extra string argument */
let concat = strings => Js.String.concatMany(strings, "");

/* Useful for conditionally adding "active" css class */
let activeOrEmpty = condition => condition ? " active" : "";

/* converts a floating point fraction to an integer in the range of [0,100] */
let fractionToPercent = (number) => {
    int_of_float(Js.Math.round(number *. 100.))
}

let round = (flt: float): int => int_of_float(flt +. 0.5);


/* Reason conversion of "Making setInterval Declarative with React Hooks".
   https://overreacted.io/making-setinterval-declarative-with-react-hooks/
   Basically setting setInterval in a useEffect, but picking up changes to
   the callback.  */
let useInterval = (~interval=5000, callback) => {
  let savedCallback = React.useRef(callback);

  // Remember the latest function.
  let _ = React.useEffect1(() => {
    React.Ref.(savedCallback->setCurrent(callback));
    None;
  }, [|callback|]);

  // Set up the interval.
  let _ = React.useEffect1(() => {
    let tick = () => {
      React.Ref.current(savedCallback)();
    }
    let timerId = Js.Global.setInterval(tick, interval);
    Some(() => Js.Global.clearInterval(timerId));
  }, [|interval|]);
}
