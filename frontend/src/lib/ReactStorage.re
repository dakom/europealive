/*
   This acts like React.useState, except it persists the state in a cookie.
 */
open Belt;

type storage = {
  get: string => option(string),
  set: (string, string) => unit,
};

let cookieStorage = {
  get: JustgageReasonCookie.Cookie.getAsString,
  set: JustgageReasonCookie.Cookie.setString,
};

let localStorage =
  Dom.Storage.{
    get: name => getItem(name, localStorage),
    set: (name, value) => setItem(name, value, localStorage),
  };

type serializer('a) = {
  serialize: 'a => string,
  deserialize: string => 'a,
};

let useBase =
    (
      ~storage: storage=cookieStorage, /* Storage backend to use */
      ~serializer: serializer('state), /* Convert to/from storage */
      key: string, /* Key under which to store the value */
      onEmpty: unit => 'state,
    ) /* Function to initialize state if storage is empty */
    : ('state, ('state => 'state) => unit) => {
  let setStorage = value => storage.set(key, serializer.serialize(value));

  let (state, setState) =
    React.useState(() =>
      switch (storage.get(key)) {
      | Some(value) => serializer.deserialize(value)
      | None =>
        let value = onEmpty();
        setStorage(value);
        value;
      }
    );

  let setStateAndSave = getNewState => {
    let newState = getNewState(state);
    setState(_oldState => newState);
    setStorage(newState);
  };

  (state, setStateAndSave);
};

let useString = useBase(~serializer={serialize: Fn.id, deserialize: Fn.id});

let useInt =
  useBase(~serializer={serialize: Int.toString, deserialize: int_of_string});

let useMoment =
  useBase(
    ~serializer={
      deserialize: MomentRe.moment,
      serialize: t => MomentRe.Moment.format("", t),
    },
  );
