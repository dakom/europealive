/* Functional composition helpers
   Inspired by Jane Street:
   https://ocaml.janestreet.com/ocaml-core/latest/doc/base/Base/Fn/
   And Haskell */

let const = (a, _) => a;

let flip = (fn, snd, fst) => fn(fst, snd);

let id = a => a;

let compose = (f, g, x) => f(g(x));

let curry = (uncurriedFn, a, b) => uncurriedFn((a, b));

let uncurry = (curriedFn, (a, b)) => curriedFn(a, b);

type ordering =
  | Lt
  | Eq
  | Gt;

exception Bad_comparison_result(int);

/* Comparison returning nice variant instead of int */
let compare = (~fn=compare, a, b) =>
  switch (fn(a, b)) {
  | (-1) => Lt
  | 0 => Eq
  | 1 => Gt
  | n => failwith("Bad comparison result: " ++ Belt.Int.toString(n))
  };
