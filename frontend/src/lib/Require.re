/* Require a file, and get its content */
[@bs.val] external file: string => string = "require";

/* Require a file to get it bundled, ignore content */
[@bs.val] external css: string => unit = "require";

/* Dangerously require anything, arbitrarily define its type at call site */
[@bs.val] external any: string => 'a = "require";
