[@bs.deriving abstract]
type config = {dsn: string};

type sentry;
[@bs.send] external init: (sentry, config) => unit = "init";

let sentry: sentry = Require.any("@sentry/browser");

type scope;

[@bs.deriving abstract]
type sentryUser = {
  id: string,
  email: string,
  fullName: string,
  kind: string,
};

type userSetResult;
[@bs.send] external setUser: (scope, sentryUser) => userSetResult = "setUser";

[@bs.send]
external configureScope: (sentry, scope => userSetResult) => unit = "init";

let setLoggedInUser = anyUser => {
  let user = AppState.getUser(anyUser);
  configureScope(sentry, scope =>
    setUser(
      scope,
      sentryUser(
        ~id=user.id->AppState.idToString,
        ~email=
          switch (user.email) {
          | Some(email) => email
          | None => "no@mail.com"
          },
        ~fullName=user.firstName ++ " " ++ user.lastName,
        ~kind=
          switch (anyUser) {
          | Teacher(_) => "teacher"
          | AnyStudent(_) => "student"
          },
      ),
    )
  );
};
