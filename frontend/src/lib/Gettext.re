/*
  Bindings to Django's javscript gettext
  https://docs.djangoproject.com/en/2.2/topics/i18n/translation/#using-the-javascript-translation-catalog

  WARNING: DO NOT ABSTRACT OVER THESE FUNCTIONS!
  The string extraction requires the translation functions to be present in
  the output javascript with direct string literal arguments.

  'r' prefix is the same as non-r, but typed as returning React element.
  'p' postfix indicates that the first argument is a translation context hint.
 */

// These two variables are defined in the Django template app_base.html
[@bs.val] external currentLanguage: string = "document.currentLanguage";
[@bs.val]
external availableLanguages: Js.Dict.t(string) =
  "document.availableLanguages";

[@bs.val] external s_: string => string = "gettext";
[@bs.val] external rs_: string => React.element = "gettext";

[@bs.val] external sp_: (string, string) => string = "pgettext";
[@bs.val] external rsp_: (string, string) => React.element = "pgettext";

// NON-TYPESAFE string interpolation from Django.
// Pass array/tuple/JS.Dict.t with variables to interpolate as second argument
[@bs.val] external interpolate: (string, 'a) => string = "interpolate";
[@bs.val] external rinterpolate: (string, 'a) => React.element = "interpolate";

[@bs.val] external sn_: (string, string, int) => string = "ngettext";
[@bs.val] external rsn_: (string, string, int) => React.element = "ngettext";

// Are these bindings correct!?
/*[@bs.val] external snp_: (string, string, string, int) => string = "npgettext";*/
/*[@bs.val] external rsnp_: (string, string, string, int) => React.element = "npgettext";*/

[@bs.val] external getFormat: string => string = "get_format";

let getDateFormat = () => getFormat("DATE_FORMAT");

let getTimeFormat = () => getFormat("TIME_FORMAT");
