open BsReactstrap;
open Belt;

type content = {
  element: React.element,
  buttons: option(React.element),
  onClose: unit => unit,
};

type action =
  | SetContent(content)
  | Close;

let useModal = () => {
  let (maybeContent: option(content), send) =
    React.useReducer(
      (oldContent, action) =>
        switch (action) {
        | SetContent(content) => Some(content)
        | Close =>
          Option.map(oldContent, content => content.onClose())->ignore;
          None;
        },
      None,
    );
  let close = () => send(Close);
  let element =
    JSX.ifSome(maybeContent, content =>
      <Modal
        className="singleton-modal"
        isOpen={Option.isSome(maybeContent)}
        toggle=close>
        {content.element}
        {JSX.ifSome(content.buttons, buttons =>
           <ModalFooter className="modal-buttons">
             <div className="button-wrapper"> buttons </div>
           </ModalFooter>
         )}
      </Modal>
    );
  (
    element,
    mkContent => {
      let content = mkContent(close);
      send(SetContent(content));
    },
  );
};
