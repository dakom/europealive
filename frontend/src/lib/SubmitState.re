open Gettext;
open Belt;
open BsReactstrap;

type gqlErrors =
  array(
    option({
      .
      "field": string,
      "messages": array(string),
    }),
  );

type submitState =
  | NotSubmitted
  | Invalid(gqlErrors)
  | Success;

/*type response = option([> `Errors(gqlErrors) ]);*/
type response = option([ | `Errors(gqlErrors)]);

type submitHook = {
  present: unit => React.element,
  set: submitState => unit,
  state: submitState,
  handleError: response => unit,
};

let use = () => {
  let (submitState, setSubmitState) =
    React.useReducer((_old, state) => state, NotSubmitted);

  let presenter = () =>
    switch (submitState) {
    | NotSubmitted => React.null
    | Success => <Alert color="success"> {rs_("Success!")} </Alert>
    | Invalid([||]) =>
      <Alert color="danger"> {rs_("An unknown error happened")} </Alert>
    | Invalid(errors) =>
      errors
      ->Array.keepMap(Fn.id)
      ->Array.map(error =>
          <Alert color="danger">
            {error##messages->Js.String.make->React.string}
          </Alert>
        )
      ->React.array
    };

  let handleError =
    fun
    | Some(`Errors(errors)) => setSubmitState(Invalid(errors))
    | None => setSubmitState(Invalid([||]));

  {present: presenter, set: setSubmitState, state: submitState, handleError};
};
