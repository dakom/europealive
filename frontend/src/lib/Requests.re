/***
 * This is meant as a library file for making requests
 ***/
let getCookies = c => {
  let restOf = cs => String.sub(cs, 1, String.length(cs) - 1);
  let updateDict = (dict, key, value) => {
    Js.Dict.set(dict, key, value);
    dict;
  };
  let rec tokens = (cs, value, key, dict) =>
    if (String.length(cs) == 0) {
      /* The last key value pair, does not seem to be concluded with a ';'
            therefore the last pair is added here:
         */
      updateDict(
        dict,
        key,
        value,
      );
    } else {
      switch (cs.[0]) {
      | ' ' => tokens(restOf(cs), value, key, dict)
      | '=' => tokens(restOf(cs), "", value, dict)
      | ';' => tokens(restOf(cs), "", "", updateDict(dict, key, value))
      | next => tokens(restOf(cs), value ++ Char.escaped(next), key, dict)
      };
    };
  tokens(c, "", "", Js.Dict.empty());
};

let getCSRFToken = (~cookie_string=?, ()) => {
  /* Optionally provide `cookie_string` otherwise
        the "document.cookie" will be used
     */
  let str =
    switch (cookie_string) {
    | None =>
      %raw
      "document.cookie"
    | Some(c) => c
    };
  let cookieDict = getCookies(str);
  switch (Js.Dict.get(cookieDict, "csrftoken")) {
  | None => ""
  | Some(t) => t
  };
};

let getHeaders = (~token) =>
  [|
    (
      "Authorization",
      "Token "
      ++ (
        switch (token) {
        | Some(t) => t
        | None => ""
        }
      ),
    ),
    ("Content-Type", "Application/json"),
    ("X-CSRFToken", getCSRFToken()),
  |]
  |> Fetch.HeadersInit.makeWithArray;

let get = (~headers, ~endpoint) =>
  Fetch.fetchWithInit(
    endpoint,
    Fetch.RequestInit.make(~method_=Fetch.Get, ~headers, ()),
  );

let request = (~method_, ~endpoint, ~token=?, ~body=?, ()) => {
  /* Sends a requests to the server with the
        provided params.
        method_ is of type Fetch.requestMethod, i.e.:
        ```
             Fetch.Get | Fetch.Post
        ```
        If token is defined, it will be includeded
        in the headers.
        Params should be a list of tuples.
        Returns Js.Promise
     */
  let response =
    /* Since Get does not support a body: */
    switch (method_) {
    | Fetch.Get => get(~headers=getHeaders(~token), ~endpoint)
    | _ =>
      Fetch.fetchWithInit(
        endpoint,
        Fetch.RequestInit.make(
          ~method_,
          ~headers=getHeaders(~token),
          ~body=
            switch (body) {
            | Some(b) => b |> Js.Json.stringify |> Fetch.BodyInit.make
            | None => "" |> Fetch.BodyInit.make
            },
          (),
        ),
      )
    };
  response
  |> Js.Promise.then_(res =>
       if (Fetch.Response.ok(res)) {
         /* Assumes all endpoints can be parsed as JSON: */
         Fetch.Response.json(
           res,
         );
       } else {
         /* Some generic error handling could go here... */
         Js.Exn.raiseError(
           "Error: requesting " ++ endpoint ++ "!",
         );
       }
     );
};
