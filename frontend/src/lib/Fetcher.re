open Gettext;
type loadState('a) =
  | Loading
  | Error(string)
  | Ready('a);

module Once = (Config: {type data;}) => {
  /*type state = loadState(Config.data);*/
  type state =
    | Loading
    | Ready(Config.data);
  type retainedProps = {key: option(string)};

  let component =
    ReasonReact.reducerComponentWithRetainedProps("FetcherComponent");

  let make = (~fetch, ~key: option(string)=?, children) => {
    ...component,
    retainedProps: {
      key: key,
    },
    /* This replicates React "key" behaviour, allowing it to work with Epitath.Hook. */
    willReceiveProps: self =>
      if (self.retainedProps.key !== key) {
        fetch(data => self.send(Ready(data)));
        Loading;
      } else {
        self.state;
      },
    initialState: () => Loading,
    didMount: self => fetch(data => self.send(Ready(data))),
    reducer: (newState, _state) => Update(newState),
    render: ({state}) =>
      switch (state) {
      | Loading => <div> {rs_("Loading...")} </div>
      | Ready(data) => children(data)
      },
  };
};

/* Load data and automatically update every 'interval' milliseconds */
module Poll = (Config: {type data;}) => {
  module OnceBase = Once(Config);
  include OnceBase;

  let make = (~fetch, ~interval=5000, ~key=?, children) => {
    /* Extend didMount behaviour from Once module. */
    let makeOnce = OnceBase.make(~fetch, ~key?, children);
    {
      ...makeOnce,
      didMount: self => {
        makeOnce.didMount(self);

        let intervalId =
          Js.Global.setInterval(
            () => fetch(data => self.send(Ready(data))),
            interval,
          );
        self.onUnmount(() => Js.Global.clearInterval(intervalId));
      },
    };
  };
};

/* Hook implementation of loading data once */
let once = (~changeWith=[||], fetch) => {
  let (state, setState) = React.useState(() => Loading);
  React.useEffect1(
    _ => {
      fetch(data => setState(_oldState => Ready(data)));
      None;
    },
    changeWith,
  );
  state;
};

/* Hook implementation of loading and updating periodically */
let poll = (~interval=5000, ~changeWith=[||], fetch) => {
  let (state, setState) = React.useState(() => Loading);
  React.useEffect1(
    _ => {
      let update = () => fetch(data => setState(_oldState => Ready(data)));
      update();
      let timerId = Js.Global.setInterval(update, interval);
      Some(() => Js.Global.clearInterval(timerId));
    },
    changeWith,
  );
  state;
};

let onceQL = (~changeWith=[||], query) => {
  let (state, setState) = React.useState(() => Loading);
  React.useEffect1(
    _ => {
      Js.Promise.(
        Api.sendQuery(query)
        |> then_(data => resolve(setState(_old => Ready(data))))
      )
      ->ignore;
      None;
    },
    changeWith,
  );
  state;
};

let pollQL = (~interval=5000, ~changeWith=[||], query) => {
  let (state, setState) = React.useState(() => Loading);
  React.useEffect1(
    _ => {
      let update = () =>
        Js.Promise.(
          Api.sendQuery(query)
          |> then_(data => resolve(setState(_old => Ready(data))))
        )
        ->ignore;
      update();
      let timerId = Js.Global.setInterval(update, interval);
      Some(() => Js.Global.clearInterval(timerId));
    },
    changeWith,
  );
  state;
};

/*
    -- Boilerplate to generically handle rendering when not ready --
 */

let loading = <div> {rs_("Loading...")} </div>;
let error = <div> {rs_("Error loading data!")} </div>;

/* whenReady functions call a given render function as soon as data is available.
     IMPORTANT: Hooks cannot be used in render function. Since the render is only
     called once data is loaded, this would violate the React Hook invariant
     that the hook configuration must be static between function calls.
   */
let whenReady1 = (prop, render) =>
  switch (prop) {
  | Loading => loading
  | Error(_msg) => error
  | Ready(data) => render(data)
  };

let whenReady2 = (props, render) =>
  switch (props) {
  | (Error(_), _) => error
  | (_, Error(_)) => error
  | (Ready(d1), Ready(d2)) => render((d1, d2))
  | _ => loading
  };

let whenReady3 = (props, render) =>
  switch (props) {
  | (Error(_), _, _) => error
  | (_, Error(_), _) => error
  | (_, _, Error(_)) => error
  | (Ready(d1), Ready(d2), Ready(d3)) => render((d1, d2, d3))
  | _ => loading
  };

let whenReady4 = (props, render) =>
  switch (props) {
  | (Error(_), _, _, _) => error
  | (_, Error(_), _, _) => error
  | (_, _, Error(_), _) => error
  | (_, _, _, Error(_)) => error
  | (Ready(d1), Ready(d2), Ready(d3), Ready(d4)) => render((d1, d2, d3, d4))
  | _ => loading
  };

let whenReady5 = (props, render) =>
  switch (props) {
  | (Error(_), _, _, _, _) => error
  | (_, Error(_), _, _, _) => error
  | (_, _, Error(_), _, _) => error
  | (_, _, _, Error(_), _) => error
  | (_, _, _, _, Error(_)) => error
  | (Ready(d1), Ready(d2), Ready(d3), Ready(d4), Ready(d5)) =>
    render((d1, d2, d3, d4, d5))
  | _ => loading
  };

let whenReady6 = (props, render) =>
  switch (props) {
  | (Error(_), _, _, _, _, _) => error
  | (_, Error(_), _, _, _, _) => error
  | (_, _, Error(_), _, _, _) => error
  | (_, _, _, Error(_), _, _) => error
  | (_, _, _, _, Error(_), _) => error
  | (_, _, _, _, _, Error(_)) => error
  | (Ready(d1), Ready(d2), Ready(d3), Ready(d4), Ready(d5), Ready(d6)) =>
    render((d1, d2, d3, d4, d5, d6))
  | _ => loading
  };
