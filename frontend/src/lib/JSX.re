/* Helper functions for interacting with React JSX */
open BsReform;

let if_ = (condition, element) =>
  if (condition) {element} else {ReasonReact.null};

let ifSome = (maybeValue, toElement) =>
  switch (maybeValue) {
  | Some(value) => toElement(value)
  | None => ReasonReact.null
  };

let whenFormError = (fieldState: ReForm.fieldState, toElement) =>
  switch (fieldState) {
  | Error(message) => toElement(message)
  | _ => React.null
  };

let whenFormValid = (fieldState: ReForm.fieldState, element) =>
  switch (fieldState) {
  | Valid => element
  | _ => React.null
  };

module Toggler = {
  [@react.component]
  let make = (~first, ~second) => {
    let (isFirst, toggle) = React.useReducer((old, ()) => !old, true);
    if (isFirst) {
      first(toggle);
    } else {
      second(toggle);
    };
  };
};
