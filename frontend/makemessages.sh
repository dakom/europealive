#!/bin/bash

#find src -type f -name "*.re" |
    #while read filename; do
        #name=`basename $filename`
        #echo "locale/tmp/${name}"
        #~/kode/bs-platform-7/node_modules/bs-platform/lib/bsrefmt --parse re --print ml $filename > "locale/tmp/${name%.re}.ml" ;
    #done

for LANG in da en nl de
do
    echo $LANG
    tmp_file="locale/${LANG}_tmp.po"
    ocaml-gettext --action extract --extract-pot $tmp_file `find src -type f -name "*.ml"`
    ocaml-gettext --action merge --merge-pot $tmp_file "locale/$LANG.po"

done
