# Choosing a frontend translation system
This file documents my (Rasmus Klett) thought process in implementing translation on the frontend.
Due to ReasonML's relative immaturity at the time of writing, the best long-term option does not seem obvious.

## Runtime
We will write a binding to Django's JSONCatalog for looking up localized strings at runtime.
This is good because it draws on existing experience with Django, and is quite simple.

It leaves us needing a way to extract strings from the client code.


## String extraction
There are  several options with quite distinct advantages and disadvantages:

### Django's Javascript string extraction 
Django can extract strings from .js files using `makemessages -d djangojs`. 

* \+ No new dependencies
* \+ Simple implementation
* \+ Very well supported, apart from the simple bindings
* \- Extracts from .js files
* \- Dependent on Bucklescript compiler not doing anything funky


### Ocaml-gettext
[Here](https://github.com/gildor478/ocaml-gettext). Does not support Reason natively.
I experimented with extracting to ml using `refmt`, and then using gettext. This did not work,
since `refmt`'s ml output at this time has a variety of bugs, and is generally not well-supported.

The alternative is to fork it and switch the parsing to parse .re files directly.

* \+ Native and traditional gettext extraction
* \+ Extraction directly from .re
* \- Requires knowledge of dune/opam/ocaml to maintain
* \- Will only use a tiny part of the library


## Reason-i18n-extractor
[Here](https://github.com/bloodyowl/reason-i18n-extractor)

* \+ Very simple
* \+ Extraction directly from .re
* \+ Probably easy to maintain and extend
* \- Unlikely to be supported
* \- Only supports simple lookup translation
* \- Extracts to simple json file


## bs-react-intl
[Here](https://github.com/reasonml-community/bs-react-intl)

* \+ Seems reasonably supported
* \+ Extraction directly from .re
* \- Does not natively use gettext .po format
* \- Cannot integrate with Django translation
* \- Seems quite complex
