""" App """
# pylint: disable=unused-import, import-outside-toplevel
import vinaigrette
from django.apps import AppConfig


class GameConfig(AppConfig):
    """ Game config """
    name = 'game'

    def ready(self):
        import game.signals
        from .models import (
            CountryStatic, ParliamentGroupStatic, ResearchCategory, ResearchItem,
            CalendarEntry, MyStaffRoleContent, MyStaffTime,
        )
        vinaigrette.register(CountryStatic, ['name', 'special_success_criteria',
                                             'video_embed_url'])
        vinaigrette.register(ParliamentGroupStatic, ['name', 'special_success_criteria',
                                                     'video_embed_url'])

        vinaigrette.register(ResearchCategory, ['name'])
        vinaigrette.register(ResearchItem, ['name', 'content'])

        vinaigrette.register(CalendarEntry, ['text'])

        vinaigrette.register(MyStaffRoleContent, ['headline', 'subline', 'content'])
        vinaigrette.register(MyStaffTime, ['description'])
