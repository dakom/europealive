"""
Import models here so that they are accessible through game.models

Note that event_models must be loaded first to avoid circular imports
"""

from .all_models import *
from .team_models import *
