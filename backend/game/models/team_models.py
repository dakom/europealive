"""
These models should be moved to `core` or at least somewhere else.
Or otherwise, the `Team` and `TeamStatic` should be moved to `game`.
"""

from django.db import models
from django.db.models import Sum

from core.models import Team, TeamStaticBase


class ParliamentGroup(Team):
    """ A group in the European Parliament """
    static = models.OneToOneField("ParliamentGroupStatic", on_delete=models.PROTECT)

    def __str__(self):
        return f'ParliamentGroup {self.name}'

    class Meta:
        verbose_name = "parliament group"
        verbose_name_plural = "parliament groups"


class ParliamentGroupStatic(TeamStaticBase):
    """ Static information about a parliament group """
    team_class = ParliamentGroup
    member_count = models.PositiveSmallIntegerField()

    @classmethod
    def total_member_count(cls):
        """ Calculate total member count in the game """
        return cls.objects.aggregate(
            members=Sum("member_count")
        )["members"]

    @property
    def voting_weight(self):
        """ Calculate voting weight based on total count """
        total_members = self.total_member_count()
        return self.member_count / total_members

    class Meta:
        verbose_name = "parliament group static"
        verbose_name_plural = "parliament group statics"


class Country(Team):
    """ A country team is a member country of the EU """
    static = models.OneToOneField("CountryStatic", on_delete=models.PROTECT)

    def __str__(self):
        return f'Country {self.name}'

    class Meta:
        verbose_name = "country"
        verbose_name_plural = "countries"


class CountryStatic(TeamStaticBase):
    """ Static information about a country team """
    team_class = Country

    population = models.PositiveIntegerField()

    @classmethod
    def total_population_count(cls):
        """ Calculate total population in the game """
        return cls.objects.aggregate(
            population=Sum("population")
        )["population"]

    @property
    def voting_weight(self):
        """ Calculate voting weight based on total count """
        return self.population / self.total_population_count()

    class Meta:
        verbose_name = "country static"
        verbose_name_plural = "country statics"
