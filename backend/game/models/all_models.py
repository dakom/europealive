""" Models """
import csv
import enum
import re
from collections import defaultdict
from datetime import datetime, timedelta
from random import shuffle

from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django.db.models import Sum
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from core.models import Commission, LobbyOrganization
from events.models import BaseEvent, DeadlineEvent, MeetingEvent, Submittable
from game.models.team_models import Country, ParliamentGroup
from news.models import MediaHouse


class Game(models.Model):
    """ Game model """
    name = models.TextField()
    starting_time = models.DateTimeField()
    configuration = models.ForeignKey(
        "meta.GameConfiguration",
        on_delete=models.PROTECT
    )

    last_job_ordering = models.PositiveSmallIntegerField(
        help_text="Keeps track of order value of last student job assigned."
    )
    job_ordering_first_pass = models.BooleanField(
        default=True,
        help_text="Initially true, set false when game configuration jobs have\
                been exhaused and only repeatable jobs should be considered."
    )
    teams_created = models.BooleanField(default=False)
    game_started = models.BooleanField(default=False)

    @classmethod
    def get_current_game(cls):
        """ TODO: Implement properly to handle multiple games """
        return cls.objects.first()

    def create_teams(self):
        """
        Run once to create teams needed for selected GameConfiguration
        """
        assert not self.teams_created
        with transaction.atomic():
            team_statics = self.configuration.get_required_teams()
            for static in team_statics:
                static.create_team()
            self.teams_created = True
            self.save()

    def get_next_job_template(self):
        """
        Wraps self.configuration.get_next_job_template to manage
        self.last_job_ordering automatically.
        """
        job_in_conf = self.configuration.get_next_job_in_conf(
            self.last_job_ordering,
            first_pass=self.job_ordering_first_pass
        )

        if self.last_job_ordering > job_in_conf.order:
            # First pass of jobs is exhausted, switch to only repeatable jobs.
            self.job_ordering_first_pass = False

        self.last_job_ordering = job_in_conf.order
        self.save()
        return job_in_conf.job

    def assign_initial_students_jobs(self, pending_students):
        """
        Ran at game start. Differs from later student job assignment because
        all x pending students are randomly assigned the first x jobs,
        to avoid a predictable ordering.
        Later students are assigned the next available job.
        NOTE: Assumes teams have been created already.
        """

        # make sure `pending_students` is list (and not QuerySet) for shuffle
        # to work
        if not isinstance(pending_students, list):
            pending_students = list(pending_students)

        shuffle(pending_students)

        while len(pending_students) > 0:
            job_template = self.get_next_job_template()

            pending_student = pending_students.pop()
            job_template.create_any_student(
                pending_student.user,
                delete_pending_student=True
            )

        self.save()

    def create_tasks_for_students(self):
        """
        Creates a task for each job/deadline pair.
        Should be run when starting a game after assigning student jobs.
        """
        DeadlineEvent.create_all_tasks()

    def minutes_passed(self):
        """
        Return the number of minutes that has passed since game start

        TODO: there should be some enforcement to ensure, that
        `self.starting_time` is not changed after the game is started or else
        this function will give wrong results. Another idea is to log the
        ACTUAL starting time in a separate field and use that instead.
        """
        return int((timezone.now() - self.starting_time).total_seconds() / 60)


class Round(models.Model):
    """
    Defines a round in the game.
    The current round is the round with the highest order,
    which also has active=True.
    An active round must have the directive field set.

    Periodic round tasks are defined in game.tasks
    """
    active = models.BooleanField(verbose_name="aktiv")
    order = models.PositiveSmallIntegerField()

    # Fields keeping track of which tasks ran
    special_success_criteria_task_ran = models.BooleanField(default=False)

    # deadlines for round
    round_end = models.ForeignKey(
        DeadlineEvent,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='round_end',
        help_text='Deadline for when to submit Directive and end the round'
    )

    ip_deadline = models.ForeignKey(
        DeadlineEvent,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='round_ip_deadline',
        help_text='Deadline for when to submit IP investment'
    )

    # Directive as it has been decided for this round.
    directive = models.ForeignKey(
        'core.Directive',
        models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"Round {self.order}"

    class Meta:
        verbose_name = "round"
        verbose_name_plural = "rounds"

    @classmethod
    def get_current_round(cls):
        """
        The current round is the round with the highest order,
        which also has active=True.
        """
        latest = cls.objects.filter(active=True).order_by("-order")
        return latest[0] if latest else None

    def get_following_round(self):
        """
        Get the next round after this one. Returns None if this is last round.
        """
        return Round.objects.filter(
            order__gt=self.order
        ).order_by('order').first()

    def get_previous_round(self):
        """
        Get the previous round before this one. Returns None if this is the
        first round.
        """
        return Round.objects.filter(
            order__lt=self.order
        ).order_by('order').last()

    def get_start_time(self):
        """
        Calculate starting time based on game start time and lengths of
        previous rounds. Changes if rounds are rescheduled.
        """
        game = Game.get_current_game()
        if game is None:
            return None
        else:
            game_start = game.starting_time

        previous_rounds = Round.objects.filter(
            order__lt=self.order,
        )
        previous_lengths = sum([r.length for r in previous_rounds])

        return game_start + timedelta(minutes=previous_lengths)

    @property
    def length(self):
        """
        Returns the length of the Round in minutes
        """
        # the final round has no length but has to return an int (for now)
        if self.is_final:
            return 999

        prev_round = self.get_previous_round()

        # this is the first round, the length is simply the offset of the
        # round_end deadline
        if prev_round is None:
            return self.round_end.offset

        # something is fishy, so return a fishy result
        if self.round_end is None or prev_round.round_end is None:
            return -1

        # length is the difference between the end of this round and last round
        return self.round_end.offset - prev_round.round_end.offset

    @property
    def start_time(self):
        """ Shorthand for self.get_start_time() """
        return self.get_start_time()

    @property
    def ip_investment_time(self):
        """ The datetime at which ip investment should be stopped """
        if self.ip_deadline is None:
            return self.get_start_time()

        ip_offset = self.ip_deadline.offset
        gamestart = Game.get_current_game().starting_time
        return gamestart + timedelta(minutes=ip_offset)

    @property
    def game(self):
        """ Deprecated shortcut since game property was removed from Round. """
        return Game.get_current_game()

    @property
    def is_final(self):
        """ The final round will be active permanently """
        return self.get_following_round() is None


class Room(models.Model):
    """
    A Room is where something can happen
    """
    CLASS_ROOM = 1
    COMMON_ROOM = 2
    ROOM_KIND = (
        (CLASS_ROOM, _("Class room")),
        (COMMON_ROOM, _("Common room"))
    )

    kind = models.IntegerField(choices=ROOM_KIND, default=CLASS_ROOM)
    name = models.CharField(max_length=50, unique=True, blank=True, null=True)
    meetings = models.ManyToManyField(
        MeetingEvent,
        blank=True,
        help_text='Select meetings to take place in this room'
    )

    class Meta:
        ordering = ['-kind', 'pk']

    def __str__(self):
        if self.name:
            return f'{self.name}'
        return f'{self.get_kind_display()} {self.pk}'

    @classmethod
    def allocate_rooms(cls):
        """
        Allocate rooms to created Teams in a way that best distributes team
        types in different rooms and puts all MediaHouses and the Commission
        in the common room. If there are not class rooms, it does nothing.
        """

        rooms = cls.objects.filter(kind=cls.CLASS_ROOM)

        # nothing to do, come back later
        if rooms.count() == 0:
            return

        # keep track of current room
        i = 0

        # only these teams should be in a class room
        for team_c in [Country, ParliamentGroup, LobbyOrganization]:
            for team in team_c.objects.all():

                # start from room 1 if we are throug the list
                if not i < len(rooms):
                    i = 0

                # assign room
                team.room = rooms[i]
                team.save()

                # increase i
                i += 1

        # there should always only be one common room
        common_room = cls.objects.get(kind=cls.COMMON_ROOM)

        # put commission and mediahouses in common room
        commission = Commission.objects.get()
        commission.room = common_room
        commission.save()

        # put mediahouses in common room
        for team in MediaHouse.objects.all():
            team.room = common_room
            team.save()

        # set room_name on associated meetings
        for room in Room.objects.filter(meetings__gt=0).distinct():
            for meeting in room.meetings.all():
                meeting.room_name = room.name
                meeting.save()


class ResearchCategory(models.Model):
    """ A category contains a set of research articles select
    players can read """
    name = models.CharField(max_length=500)
    order = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "research category"
        verbose_name_plural = "research categories"
        ordering = ["order"]


class ResearchItem(models.Model):
    """ A research item for the players to read """
    name = models.CharField(max_length=500)
    content = models.TextField()
    category = models.ForeignKey(ResearchCategory, on_delete=models.PROTECT,
                                 related_name="research_items")
    order = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "research item"
        verbose_name_plural = "research items"
        unique_together = ("order", "category")
        ordering = ["order"]


class AdvisorMemo(Submittable):
    """
    A memo written by an Advisor.
    If self.sent is set, the memo has been sent to relevant team members.
    """
    subject = models.CharField(max_length=100)
    content = models.TextField()
    sent = models.DateTimeField(blank=True, null=True)
    writer = models.ForeignKey('core.Advisor', on_delete=models.CASCADE)

    def __str__(self):
        return f'Memo by {self.writer.first_name}: {self.subject[:20]} ...'

    def submitted_by(self):
        """ Must be overridden when inheriting from Submittable """
        return self.writer.get_subtype()

    class Meta:
        verbose_name = "advisor memo"
        verbose_name_plural = "advisor memos"
        ordering = ["-sent", "writer"]


VOTE_CHOICES = [
    ('yes', 'Yes'),
    ('no', 'No'),
    ('abstain', 'Abstain'),
]


class VoteResult(enum.Enum):
    """
    The possible results of the final team votes.
    A vote starts out undecided until it is either passed or rejected when
    enough teams have voted.
    """
    PASSED = 'passed'
    REJECTED = 'rejected'
    UNDECIDED = 'undecided'


class FinalTeamVote(Submittable):
    """
    The final vote a country or group makes on whether the final directive
    is accepted
    """
    by = models.OneToOneField('core.Team', on_delete=models.CASCADE)
    option = models.CharField(choices=VOTE_CHOICES, max_length=200)

    def __str__(self):
        return f'Final vote by {self.by.name}'

    class Meta:
        verbose_name = "final team vote"
        verbose_name_plural = "final team votes"

    def submitted_by(self):
        """
        Return the Team that has submitted this vote (overrides
        `Submittable.submitted_by`)
        """
        return self.by.get_subclass()

    @classmethod
    def voting_complete(cls):
        """
        Return `True` if all `Team`s able to vote has voted. `False` otherwise
        """

        group_count = ParliamentGroup.objects.filter(
            finalteamvote__isnull=True
        ).count()
        country_count = Country.objects.filter(
            finalteamvote__isnull=True
        ).count()

        return (group_count + country_count) == 0

    @classmethod
    def parliament_vote_percentages(cls):
        """
        Returns a tuple containing the percentages of yes and no votes from the
        Parliament groups that have voted.
        """

        from game.models.team_models import ParliamentGroup

        yes, no = 0, 0
        groups = ParliamentGroup.objects.filter(
            finalteamvote__isnull=False
        )

        for group in groups:
            if group.finalteamvote.option == 'yes':
                yes += group.static.member_count
            else:
                no += group.static.member_count

        total = yes + no

        if total == 0:
            return 0, 0

        yes_percentage = yes / total * 100
        no_percentage = 100 - yes_percentage

        return yes_percentage, no_percentage

    @classmethod
    def parliament_result(cls):
        """
        50% of members is required to pass. Abstaining groups' population
        has no effect.
        """

        yes, no = cls.parliament_vote_percentages()

        if yes + no < 100:
            return VoteResult.UNDECIDED

        return VoteResult.PASSED if yes >= 50 else VoteResult.REJECTED

    @classmethod
    def council_vote_percentages(cls):
        '''Returns a tuple containing the council percentages of yes and no,
        a boolean determining if the vote is undecided,
        and the number of countries voting yes'''

        yes_count, no_count, abstain_count, country_count = 0, 0, 0, 0
        groups = Country.objects.filter(
            finalteamvote__isnull=False
        )

        for group in groups:
            if group.finalteamvote.option == 'yes':
                yes_count += group.static.voting_weight
            elif group.finalteamvote.option == 'no':
                no_count += group.static.voting_weight
            else:
                abstain_count += group.static.voting_weight

        total = sum([yes_count, no_count, abstain_count])

        if total == 0:
            return (0, 0, 0)

        yes_percentage      = yes_count / total * 100
        no_percentage       = no_count / total * 100
        abstain_percentage  = abstain_count / total * 100

        return (
            yes_percentage,
            no_percentage,
            abstain_percentage
        )

    @classmethod
    def council_result(cls):
        """
        65 % of population must vote for. Therefore abstaining is effectively voting against.
        Additionally, at least 4 countries must vote for.
        """
        yes, no, abstain = cls.council_vote_percentages()

        yes_count = Country.objects.filter(
            finalteamvote__isnull=False,
            finalteamvote__option='yes'
        ).count()
        yes_share = yes_count / Country.objects.count()

        if yes >= 65 and yes_share >= 0.55:
            return VoteResult.PASSED
        elif yes + no + abstain == 0:
            return VoteResult.UNDECIDED
        else:
            return VoteResult.REJECTED

    @classmethod
    def directive_passed(cls):
        """
        Returns `True` if the Directive is passed in both the Parliament and
        in the Council, else `False`
        """
        parliament = cls.parliament_result() == VoteResult.PASSED
        council = cls.council_result() == VoteResult.PASSED
        return parliament and council


class CalendarEntry(models.Model):
    """ An entry in the sidebar calendar """
    hour_offset = models.PositiveSmallIntegerField(
        help_text="hours after game start. Should be 0-23")
    minute_offset = models.PositiveSmallIntegerField(
        help_text="Minutes after hour offset. Should be 0-59")
    duration = models.PositiveSmallIntegerField(blank=True, null=True)
    text = models.CharField(max_length=500)
    event = models.ForeignKey(
        BaseEvent,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.text

    @property
    def time(self):
        """ Calculates actual datetime based on game start """
        game = Game.get_current_game()
        if game is not None:
            game_start = Game.get_current_game().starting_time
        else:
            # if no current game in progress, just use a probable default value
            game_start = datetime.today().replace(
                hour=9, minute=0, second=0, microsecond=0
            )

        entry_time = game_start + timedelta(hours=self.hour_offset,
                                            minutes=self.minute_offset)
        entry_time = entry_time.astimezone(timezone.get_current_timezone())
        return entry_time

    class Meta:
        verbose_name = "calendar entry"
        verbose_name_plural = "calendar entries"
        ordering = ["hour_offset", "minute_offset"]


class MemoDeadline(models.Model):
    """
    A time at which Advisors must have delivered a memo to their team
    """

    # copied from core.models to avoid circular imports
    KIND_CHOICES = (
        ('technical', 'Technical'),
        ('political', 'Political'),
        ('attache', 'Attaché'),
        ('media', 'Media'),
    )

    offset = models.PositiveSmallIntegerField(
        help_text="minute offset from game start")
    end_task_ran = models.BooleanField(default=False)
    kind = models.CharField(max_length=100, choices=KIND_CHOICES)

    @property
    def deadline_time(self):
        """ Get datetime of deadline based on game starting time """
        game = Game.get_current_game()
        if game is not None:
            game_start = game.starting_time
        else:
            # if no current game in progress, just use a probable default value
            game_start = datetime.today().replace(
                hour=9, minute=0, second=0, microsecond=0
            )
        deadline = game_start + timedelta(minutes=self.offset)
        return deadline.astimezone(timezone.get_current_timezone())

    def __str__(self):
        return f'Deadline after {self.offset} minutes for {self.kind}'

    class Meta:
        verbose_name = "memo deadline"
        verbose_name_plural = "memo deadlines"


class MyStaffRoleContent(models.Model):
    name = models.CharField(
        max_length=50,
        help_text='Name of this content to make it easily recognisable in the' \
        + 'backend (eg. CommissionSecretary or CountryStrategist)'
    )
    headline = models.CharField(max_length=50)
    subline = models.CharField(max_length=50)
    content = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Role content for MyStaffPage'


class MyStaffTime(models.Model):
    role_content = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.CASCADE,
        related_name='times'
    )

    description = models.CharField(max_length=50)
    event = models.ForeignKey(
        BaseEvent,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text='Select event to display'
    )

    @property
    def time_text(self):
        """
        Return string representation of the time associated with this
        MyStaff entry
        """
        # event should not be None ever, but for now handle it
        if self.event is None:
            return ''

        start = self.event.display_time
        duration = self.event.duration

        if start is None:
            return f'Offset {self.event.offset}'

        start_str = start.time().strftime('%H:%M')
        if duration > 0:
            stop = start + timedelta(minutes=duration)
            stop_str = stop.time().strftime('%H:%M')
            return f'{start_str}-{stop_str}'
        return start_str

    def __str__(self):
        return f'{self.description}, {self.time_text}'


class MyStaffPage(models.Model):
    """
    A class defining the content of the MyStaff page visible to every Chief
    of Staff. There should be one entry for each type of Team. The relation to
    the respective Team is handled via TeamStatic.
    """
    name = models.CharField(
        max_length=50, help_text='Not displayed anywhere but in the backend'
    )
    strategist = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='strategist'
    )
    chief = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='chief'
    )
    secretary = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='secretary'
    )
    relation_builder = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='relation_builder'
    )
    technical_advisor = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='technical_advisor'
    )
    media_advisor = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True,
        related_name='media_advisor'
    )
    political_advisor = models.ForeignKey(
        MyStaffRoleContent,
        on_delete=models.SET_NULL,
        null=True, blank=True,
        related_name='political_advisor'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'MyStaffPage'
        verbose_name_plural = 'MyStaffPages'
