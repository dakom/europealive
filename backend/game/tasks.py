""" Defines tasks to be run periodically """
from datetime import timedelta

from django.db.models import F
from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop

from core.models import (Advisor, ArticleSelection, Commission, Directive,
                         PendingStudent, Student, Team)
from events.models import DeadlineEvent, DeadlineTask
from game.models import (AdvisorMemo, Country, FinalTeamVote, Game,
                         MemoDeadline, ParliamentGroup, Round, VoteResult)
from inf_points.models import DirectiveInvestment, PointTransaction
from news.models import MediaHouse


def start_game_task():
    """ Starts the game by assigning jobs to pending students and setting a flag. """
    game = Game.get_current_game()
    if game is None:
        return

    assert not game.game_started

    game.assign_initial_students_jobs(PendingStudent.objects.all())
    game.create_tasks_for_students()
    game.game_started = True
    game.save()

def start_round(gameround):
    if gameround.directive is None:
        prev_directive = gameround.get_previous_round().directive
        new_directive = Directive.create_random_mutation(prev_directive)
        gameround.directive = new_directive

    gameround.active = True
    gameround.save()

def end_round(gameround):
    next_round = gameround.get_following_round()

    if next_round is not None:
        investments = gameround.directive.directiveinvestment_set.all()
        for investment in investments:
            team = investment.by
            invested = investment.amount
            selection = ArticleSelection.objects.get(
                article=investment.priority.article,
                directive=next_round.directive
            )

            # The team is awarded 20% to 100% on top of their investment
            awarded = invested * (1 + ((selection.items_selected - 1) / 5))

            PointTransaction(
                description=gettext_noop(
                    ("Returns from the %(inv)i points "
                     "placed in priority %(num)s") % ({
                         'inv': invested,
                         'num': investment.priority.article.number
                     })
                ),
                amount=round(awarded),
                recipient=team,
                sender=None,
            ).save()

            team.influence_points = F('influence_points') + awarded
            team.save()

    gameround.active = False
    gameround.save()

def award_special_success_criteria_points(gameround):
    """
    Determine if teams achieved their special succes criteria, and award points if so.
    """

    parliament_yes = FinalTeamVote.parliament_vote_percentages()[0]
    council_yes = FinalTeamVote.council_vote_percentages()[0]
    directive_passed = FinalTeamVote.directive_passed()

    directive = gameround.directive
    articles = directive.selections.all().order_by("article__number")

    for team in Team.objects.all():
        award_points = False

        criteria = team.special_success_criteria_value

        # team is commission and should be treated specially
        if criteria == "commission special":
            if parliament_yes >= 70 and council_yes >= 70:
                award_points = True

        # team has to have an article above 3
        elif not (criteria == 'none' or criteria == ''):
            article = directive.selections.filter(
                article__number=int(criteria[-1])
            ).first()

            if article is not None and article.items_selected <= 3:
                award_points = True

        # team does not have a special success criteria
        else:
            continue

        if award_points and directive_passed:
            PointTransaction.send(
                description=_("Your team achieved your special success criteria!"),
                amount=50,
                recipient=team,
            )
        else:
            PointTransaction.send(
                description=_("Your team did not achieve your special success criteria"),
                amount=0,
                recipient=team,
            )

    gameround.special_success_criteria_task_ran = True
    gameround.save()

def handle_tasks(deadline):
    """
    Give punishment or reward for all tasks associated with `deadline`
    """
    for task in deadline.tasks.all():

        # if task is completed, then give reward
        if task.completed_on_time():
            item = task.get_submittable()._meta.verbose_name
            msg = gettext_noop(f'Reward for submitting {item} on time')
            amount = deadline.reward

        # else give penalty
        else:
            item = deadline.action.model_class()._meta.verbose_name
            msg = gettext_noop(
                f'Punishment for not submitting {item} on time'
            )
            amount = -deadline.penalty

        if amount == 0:
            continue

        # actor is either Team or Student, but recipient must be Team
        recipient = task.get_actor()
        if isinstance(recipient, Student):
            recipient = recipient.team

        # create and send point transactoin
        PointTransaction.send(
            description=msg,
            amount=amount,
            recipient=recipient,
            task=task
        )

def handle_deadlines():
    """
    This function goes through all deadline events that are passed and
    unprocessed and then process them
    """

    game = Game.get_current_game()
    deadlines = DeadlineEvent.objects.filter(
        offset__lte=game.minutes_passed(),
        processed=False
    )

    for deadline in deadlines:
        handle_tasks(deadline)

        gameround = Round.objects.filter(
            round_end=deadline,
            active=True
        ).first()

        if gameround is not None:
            end_round(gameround)
            start_round(gameround.get_following_round())

        # mark deadline as processed
        deadline.processed = True
        deadline.save()
