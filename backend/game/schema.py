""" Game GraphQL schema """
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
from random import Random

import graphene
from django.db import transaction
from django.utils import timezone
from graphene_django.forms.types import ErrorType
from graphene_django.types import DjangoObjectType

from core.models import Student, Team, User
from core.shared_schema import (AdvisorType, ChiefType, DirectiveType,
                                SecretaryType, StrategistType, StudentType,
                                TeacherType, TeamType, TeamTypeMixin, UserType,
                                gql_strict_list, query_or_empty_list)
from events.models import BaseEvent, DeadlineEvent, MeetingEvent
from news.schema import JournalistType

from .models import (AdvisorMemo, CalendarEntry, Country, CountryStatic,
                     FinalTeamVote, Game, MemoDeadline, MyStaffPage,
                     MyStaffRoleContent, ParliamentGroup,
                     ParliamentGroupStatic, ResearchCategory, ResearchItem,
                     Round, VoteResult)


class MyStaffRoleContentType(DjangoObjectType):
    times = gql_strict_list(graphene.List(graphene.String))

    def resolve_times(self, _info):
        return [
            [t.description, t.time_text] for t in sorted(
                list(self.times.all()), key=lambda x: x.time_text
            )
        ]

    class Meta:
        model = MyStaffRoleContent


class MyStaffPageType(DjangoObjectType):
    class Meta:
        model = MyStaffPage


class CountryStaticType(DjangoObjectType):
    voting_weight = graphene.Field(graphene.Float, required=True)

    def resolve_voting_weight(self, _info):
        return self.voting_weight

    class Meta:
        model = CountryStatic


class CountryType(DjangoObjectType, TeamTypeMixin):
    class Meta:
        model = Country


class ParliamentGroupStaticType(DjangoObjectType):
    voting_weight = graphene.Field(graphene.Float, required=True)

    class Meta:
        model = ParliamentGroupStatic


class ParliamentGroupType(DjangoObjectType, TeamTypeMixin):
    class Meta:
        model = ParliamentGroup


class ResearchItemType(DjangoObjectType):
    class Meta:
        model = ResearchItem


class ResearchCategoryType(DjangoObjectType):
    research_items = gql_strict_list(ResearchItemType)

    def resolve_research_items(self, _info):
        return self.research_items.all() # pylint: disable=no-member

    class Meta:
        model = ResearchCategory


class CalendarEntryType(DjangoObjectType):
    time = graphene.Field(graphene.DateTime, required=True)

    class Meta:
        model = CalendarEntry


class EventType(DjangoObjectType):
    time = graphene.Field(graphene.DateTime, required=True)
    text = graphene.Field(graphene.String, required=True)
    duration = graphene.Field(graphene.Int)
    room_name = graphene.Field(graphene.String)
    agenda = graphene.Field(graphene.String)
    actor = graphene.Field(graphene.String)
    facilitator_description = graphene.Field(graphene.String)
    player_description = graphene.Field(graphene.String)
    penalty = graphene.Field(graphene.Int)

    class Meta:
        model = BaseEvent

    def resolve_time(self, _info):
        return self.display_time

    def resolve_duration(self, _info):
        return self.duration if self.duration > 0 else None

    def resolve_text(self, _info):
        return self.name if self.name else None
    
    def resolve_agenda(self, _info):
        event = MeetingEvent.objects.filter(pk=self.pk).first()
        if event is None:
            return None
        return event.agenda if event.agenda else None

    def resolve_room_name(self, _info):
        event = MeetingEvent.objects.filter(pk=self.pk).first()
        if event is None:
            return None
        return event.room_name
    
    def resolve_actor(self, _info):
        event = DeadlineEvent.objects.filter(pk=self.pk).first()
        if event is None:
            return None
        return event.actor
    
    def resolve_facilitator_description(self, _info):
        event = BaseEvent.objects.filter(pk=self.pk).first()
        return event.facilitator_description if event.facilitator_description else None
    
    def resolve_player_description(self, _info):
        event = BaseEvent.objects.filter(pk=self.pk).first()
        return event.player_description if event.player_description else None
    
    def resolve_penalty(self, _info):
        event = DeadlineEvent.objects.filter(pk=self.pk).first()
        return event.penalty if event.penalty else None

class GameType(DjangoObjectType):
    class Meta:
        model = Game


class RoundType(DjangoObjectType):
    """ A possibly not active round, so directive is not required. """
    start_time = graphene.Field(graphene.DateTime, required=True)
    ip_investment_time = graphene.Field(graphene.DateTime, required=True)
    is_final = graphene.Field(graphene.Boolean, required=True)
    length = graphene.Field(graphene.Int, required=True)

    class Meta:
        model = Round


class ActiveRoundType(DjangoObjectType):
    """ Active rounds always have directive set """
    directive = graphene.Field(DirectiveType, required=True)
    start_time = graphene.Field(graphene.DateTime, required=True)
    ip_investment_time = graphene.Field(graphene.DateTime, required=True)
    is_final = graphene.Field(graphene.Boolean, required=True)
    length = graphene.Field(graphene.Int, required=True)

    class Meta:
        model = Round


class GameStartTime(DjangoObjectType):
    """ This type wraps Game modelto make sure we only expose time """
    time = graphene.Field(graphene.DateTime, required=True)

    def resolve_time(self, _info):
        return self.starting_time # pylint: disable=no-member
    class Meta:
        model = Game
        only_fields = ["time"]


class PreGame(graphene.ObjectType):
    starting_time = graphene.Field(graphene.DateTime, required=True)
    logged_in = graphene.Field(UserType)


class AnyStudent(graphene.Union):
    class Meta:
        types = (JournalistType, SecretaryType, AdvisorType, ChiefType, StrategistType)


class AnyUser(graphene.Union):
    class Meta:
        types = (JournalistType, SecretaryType, AdvisorType, ChiefType,
                 StrategistType, TeacherType)


class PlatformReady(graphene.ObjectType):
    logged_in = graphene.Field(AnyUser)
    team = graphene.Field(TeamType) # Can this field be easily removed?
    current_round = graphene.Field(ActiveRoundType, required=True)


class GameState(graphene.Union):
    class Meta:
        types = (PreGame, PlatformReady)


class AdvisorMemoType(DjangoObjectType):
    class Meta:
        model = AdvisorMemo


class MemoDeadlineType(DjangoObjectType):
    deadline_time = graphene.Field(graphene.DateTime, required=True)

    class Meta:
        model = MemoDeadline
        only_fields = ["id", "deadline_time", "offset", "kind"]


class FinalTeamVoteType(DjangoObjectType):
    class Meta:
        model = FinalTeamVote


class UpdateMemoMutation(graphene.Mutation):
    """
    Create or update advisor memos.
    The send_memo argument makes using a ModelForm impractical so we use a
    direct graphene.Mutation instead.
    """
    advisorMemo = graphene.Field(AdvisorMemoType, required=True)

    class Arguments:
        id = graphene.ID(required=True)
        subject = graphene.String(required=True)
        content = graphene.String(required=True)
        send_memo = graphene.Boolean(required=True)

    def mutate(self, info, subject, content, send_memo, **kwargs):
        if "id" in kwargs:
            memo = AdvisorMemo.objects.get(id=kwargs["id"])
            memo.subject = subject
            memo.content = content
        else:
            memo = AdvisorMemo(
                subject=subject,
                content=content,
                writer=info.context.user.student.advisor,
            )
        if send_memo:
            memo.sent = timezone.now()
        memo.save(auto_submit=send_memo)
        return UpdateMemoMutation(advisorMemo=memo)


class VoteOption(graphene.Enum):
    YES = 1
    ABSTAIN = 2
    NO = 3

def option_to_choicefield(option):
    if option == VoteOption.YES:
        return "yes"
    if option == VoteOption.NO:
        return "no"
    if option == VoteOption.ABSTAIN:
        return "abstain"
    raise NotImplementedError


class FinalVoteMutation(graphene.Mutation):
    did_vote = graphene.Field(graphene.Boolean, required=True)

    class Arguments:
        option = VoteOption(required=True)

    def mutate(self, info, option):
        team = info.context.user.student.team
        vote, _ = FinalTeamVote.objects.update_or_create(
            by=team,
            defaults={
                "option": option_to_choicefield(option),
            }
        )
        vote.submit()
        return FinalVoteMutation(True)

VoteResultType = graphene.Enum.from_enum(VoteResult)

class DetailedVoteResults(graphene.ObjectType):
    council = graphene.Field(VoteResultType, required=True)
    parliament = graphene.Field(VoteResultType, required=True)
    overall = graphene.Field(VoteResultType, required=True)


class SwapStudentsMutation(graphene.Mutation):
    """
    WARNING: HACKY!!
    This mutation swaps the jobs between two students. This violates many base assumptions
    made in the database about uniqueness of ids and fields.

    WHY THE EASY SOLUTION DOESN'T WORK:
    The immediately obvious solution is to switch the implicit foreign key from Student to User
    between the two students. This is not possible, because in multi-table inheritance the
    foreign key to the superclass is also the primary key. This means changing it would require
    also changing all models with a foreign key into Student, apart from needing to work around
    uniqueness constraints.

    THE IMPLEMENTED SOLUTION:
    We simply copy all data between the two users. This temporarily violates some uniqueness
    constraints, so some dummy data is temporarily inserted.
    """
    did_swap = graphene.Field(graphene.Boolean)
    errors = graphene.List(ErrorType)

    class Arguments:
        student_id1 = graphene.ID(required=True)
        student_id2 = graphene.ID(required=True)

    def mutate(self, _info, student_id1, student_id2):
        with transaction.atomic():
            # We swap User instead of Student data to not transfer team and job
            # description as well
            student1 = User.objects.get(id=student_id1)
            student2 = User.objects.get(id=student_id2)

            student1_original = User.objects.get(id=student_id1)
            student2_original = Student.objects.get(id=student_id2)


            # Scramble student2 to avoid temporary uniqueness clash in db
            rand_gen = Random()
            tmp_string = "tmp" + str(rand_gen.randint(0, 1000))
            student2.username = tmp_string
            student2.email = tmp_string
            student2.save()

            student1.__dict__.update(student2_original.__dict__)
            student1.pk = student_id1
            student1.save()

            student2.__dict__.update(student1_original.__dict__)
            student2.pk = student_id2
            student2.save()

            # The students' login sessions seem to be automatically invalidated
            # by this, so we don't need to do that manually to log them out.

            return {"did_swap": True}


class DeleteStudentMutation(graphene.Mutation):
    """ Permanently deletes a student and their job from the game """
    did_delete = graphene.Field(graphene.Boolean)
    errors = graphene.List(ErrorType)

    class Arguments:
        student_id = graphene.ID(required=True)

    def mutate(self, _info, student_id):
        student = Student.objects.get(id=student_id)

        if hasattr(student, "advisor"):
            student.advisor.delete()
            return {"did_delete": True}

        raise ValueError("The student with id %s is not an advisor" % student.id)


class Query:
    parliament_groups = gql_strict_list(ParliamentGroupType)
    countries = gql_strict_list(CountryType)
    research_categories = gql_strict_list(ResearchCategoryType)
    research_item = graphene.Field(ResearchItemType, item_id=graphene.ID(), required=True)
    calendar_entries = gql_strict_list(EventType)
    calendar_entry = graphene.Field(EventType, required=True, entry_id=graphene.Int())
    extended_calendar_entries = gql_strict_list(EventType)
    gamestate = graphene.Field(GameState, required=True)

    my_sent_memos = gql_strict_list(AdvisorMemoType)
    my_memo_draft = graphene.Field(AdvisorMemoType, required=True)
    my_team_memos = gql_strict_list(AdvisorMemoType)
    team_memos = graphene.Field(gql_strict_list(AdvisorMemoType),
                                team_id=graphene.Int(required=True))

    next_memo_deadline = graphene.Field(MemoDeadlineType)
    my_final_vote = graphene.Field(FinalTeamVoteType)
    detailed_vote_results = graphene.Field(DetailedVoteResults, required=True)
    all_students = gql_strict_list(StudentType)
    next_round = graphene.Field(RoundType)
    my_staff_page = graphene.Field(MyStaffPageType)

    def resolve_next_round(self, _info):
        """ Returns the next round, if any exists. """
        current_round = Round.get_current_round()
        return current_round.get_following_round()

    def resolve_parliament_groups(self, _info):
        return query_or_empty_list(ParliamentGroup.objects.all())

    def resolve_countries(self, _info):
        return query_or_empty_list(Country.objects.all().order_by("name"))

    def resolve_research_categories(self, _info):
        return query_or_empty_list(ResearchCategory.objects.all())

    def resolve_research_item(self, _info, item_id):
        return ResearchItem.objects.get(id=item_id)

    def resolve_calendar_entries(self, _info):
        qs = BaseEvent.objects.filter(
            display_in_calendar=True
        ).order_by('offset', 'duration')
        return query_or_empty_list(qs)
        
    def resolve_calendar_entry(self, _info, entry_id):
        return BaseEvent.objects.get(id=entry_id)

    def resolve_extended_calendar_entries(self, _info):
        qs = BaseEvent.objects.filter(
            display_in_extended_calendar=True
            ).order_by('offset', 'duration')
        return query_or_empty_list(qs)

    def resolve_gamestate(self, info):
        current_round = Round.get_current_round()
        user = info.context.user if info.context.user.is_authenticated else None
        game = Game.get_current_game()

        if hasattr(info.context.user, "teacher"):
            if current_round is None:
                current_round = Round.objects.all().order_by("order")[0]
            # Short-cirquit if user is teacher, they can login any time.
            return PlatformReady(
                logged_in=info.context.user.teacher,
                team=None,
                current_round=current_round,
            )

        if not game.game_started:
            # Game is not started, returning PreGame type
            return PreGame(
                logged_in=user,
                starting_time=game.starting_time,
            )

        # Game is started, returning PlatformReady type
        if not info.context.user.is_authenticated:
            user = None
            team = None
        elif hasattr(info.context.user, "student"):
            student = info.context.user.student
            team = student.team
            if hasattr(student, "journalist"):
                user = student.journalist
            elif hasattr(student, "secretary"):
                user = student.secretary
            elif hasattr(student, "advisor"):
                user = student.advisor
            elif hasattr(student, "chief"):
                user = student.chief
            elif hasattr(student, "strategist"):
                user = student.strategist
        else:
            raise NotImplementedError("Encountered unknown user type %s" % str(user))

        return PlatformReady(
            logged_in=user,
            team=team,
            current_round=current_round,
        )

    def resolve_my_sent_memos(self, info):
        advisor = info.context.user.student.advisor
        memos = AdvisorMemo.objects.filter(
            writer=advisor,
            sent__isnull=False,
        )
        return query_or_empty_list(memos)

    def resolve_my_memo_draft(self, info):
        advisor = info.context.user.student.advisor
        try:
            return AdvisorMemo.objects.filter(
                writer=advisor,
                sent__isnull=True
            )[0]
        except IndexError:
            new_memo = AdvisorMemo(
                writer=advisor,
                content="",
                subject="",
            )
            new_memo.save()
            return new_memo

    def resolve_team_memos(self, _info, team_id):
        memos = AdvisorMemo.objects.filter(
            writer__team_id=team_id,
            sent__isnull=False,
        )
        return query_or_empty_list(memos)

    def resolve_my_team_memos(self, info):
        team = info.context.user.student.team
        memos = AdvisorMemo.objects.filter(
            writer__team=team,
            sent__isnull=False,
        )
        return query_or_empty_list(memos)

    def resolve_next_memo_deadline(self, info):
        advisor = info.context.user.student.advisor
        elapsed_delta = timezone.now() - Game.get_current_game().starting_time
        elapsed_minutes = elapsed_delta.total_seconds() / 60
        memo_deadlines = MemoDeadline.objects.filter(
            offset__gt=elapsed_minutes,
            kind=advisor.kind
        ).order_by("offset")
        try:
            return memo_deadlines[0]
        except IndexError:
            return None

    def resolve_my_final_vote(self, info):
        if hasattr(info.context.user, "student"):
            try:
                return FinalTeamVote.objects.get(
                    by=info.context.user.student.team,
                )
            except FinalTeamVote.DoesNotExist:
                return None
        return None

    def resolve_detailed_vote_results(self, _info):
        parliament = FinalTeamVote.parliament_result()
        council = FinalTeamVote.council_result()

        # pylint: disable=no-member
        overall = VoteResultType.UNDECIDED
        if parliament == VoteResultType.PASSED and council == VoteResultType.PASSED:
            overall = VoteResultType.PASSED
        elif parliament == VoteResultType.REJECTED or council == VoteResultType.REJECTED:
            overall = VoteResultType.REJECTED
        return {
            "parliament": parliament,
            "council": council,
            "overall": overall,
        }

    def resolve_all_students(self, _info):
        return Student.objects.all().select_related(
            "team"
        ).prefetch_related(
            "secretary", "chief", "advisor", "chief", "strategist", "journalist",
        ).order_by(
            "first_name", "last_name"
        )

    def resolve_my_staff_page(self, _info):
        team = _info.context.user.student.team
        page = Team.objects.get_subclass(id=team.id).static.staff_page
        return page


class Mutation:
    updateMemo = UpdateMemoMutation.Field()
    finalVote = FinalVoteMutation.Field(required=True)
    swapStudents = SwapStudentsMutation.Field(required=True)
    deleteStudent = DeleteStudentMutation.Field(required=True)
