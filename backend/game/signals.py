"""
Register signals here
"""
from django.db.models.signals import post_init, post_save, pre_save
from django.dispatch import receiver

from events.models import BaseEvent, DeadlineEvent, MeetingEvent
from game.models import Game, Room


@receiver(post_init, sender=Game)
def remember_starting_time_game(sender, **kwargs):
    """
    When instantiating a `Game` object, store the current value of
    `Game.starting_time` so it can be accessed if it is changed in a call to
    `Game.save`
    """
    game = kwargs['instance']
    game.old_starting_time = game.starting_time

@receiver(post_save, sender=Game)
def update_events_display_time_on_game_post_save(sender, **kwargs):
    """
    If `Game.starting_time` has changed, then call `.set_display_time` on all
    events
    """
    game = kwargs['instance']
    if hasattr(game, 'old_starting_time'):
        if game.old_starting_time != game.starting_time:
            for event in BaseEvent.objects.all():
                event.set_display_time(game.starting_time)


@receiver(post_save, sender=Room)
def update_room_name_on_post_save(sender, **kwargs):
    """
    After a `save()` is called on a `Room`, update the associated
    `MeetingEvents` (their `room_name` attributes should change)
    """
    instance = kwargs.get('instance')
    for meeting in instance.meetings.all():

        old_rooms = meeting.room_set.exclude(pk=instance.pk)
        for old_room in old_rooms:
            old_room.meetings.remove(meeting)

        meeting.room_name = instance.name
        meeting.save()


def set_display_time_on_save(sender, **kwargs):
    """
    Call `BaseEvent.set_display_time()` when a new event is created if a
    `Game` exists in database
    """
    event = kwargs['instance']
    if not event.pk:
        game = Game.get_current_game()
        if game is None:
            return

        if event.offset is None:
            return
        event.set_display_time(game.starting_time, save=False)

classes_to_connect = [BaseEvent, DeadlineEvent, MeetingEvent]
for cls in classes_to_connect:
    pre_save.connect(
        set_display_time_on_save,
        sender=cls,
        dispatch_uid='dpt_post_save' + cls.__name__
    )
