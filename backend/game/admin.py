""" Admin module """
#pylint: disable=missing-docstring

from django import forms
from django.conf import settings
from django.contrib import admin, messages
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import path
from django.utils.timezone import get_current_timezone

from core.models import Student, Team
from game.models import (AdvisorMemo, CalendarEntry, Country, CountryStatic,
                         FinalTeamVote, Game, MemoDeadline, MyStaffPage,
                         MyStaffRoleContent, MyStaffTime, ParliamentGroup,
                         ParliamentGroupStatic, ResearchCategory, ResearchItem,
                         Room, Round)

if not settings.IS_MASTER_SERVER:
    # The following models must never be present on the master server, because if they are,
    # they would be copied to all slave servers and mess with game creation process.
    @admin.register(Game)
    class GameAdmin(admin.ModelAdmin):
        list_display = ['name', 'starting_time', 'game_started']


@admin.register(Round)
class RoundAdmin(admin.ModelAdmin):
    """ Admin view of Round model """
    def start_time(self, obj):
        timezone = get_current_timezone()
        field = obj.start_time
        if field is not None:
            return field.astimezone(timezone).strftime('%H:%M:%S')
        else:
            return '-'

    def ip_investment_deadline(self, obj):
        timezone = get_current_timezone()
        field = obj.ip_investment_time
        return field.astimezone(timezone).strftime('%H:%M:%S')

    start_time.admin_order_field = 'start_time'
    ip_investment_deadline.admin_order_field = 'ip_investment_deadline'

    list_display = ["__str__", "start_time", "active", "directive"]
    readonly_fields = ["start_time", "ip_investment_deadline"]


class TeamInline(admin.TabularInline):
    """
    Read-only inline TeamAdmin to use for displaying teams under `RoomAdmin`
    """
    def starting_room(self, obj):
        """
        `Team.starting_room` is a property and therefore we need this method
        to display it as a field
        """
        return obj.starting_room

    model = Team
    can_delete = False
    fields = ['name', 'starting_room', 'influence_points']
    readonly_fields = [ 'name', 'starting_room', 'influence_points' ]

    def has_add_permission(self, request, obj=None):
        """
        Disables the ability to add a new `Team`
        """
        return False

    def has_change_permission(self, request, obj=None):
        """
        Disables the ability to change a `Team`
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disables the ability to delete a `Team`
        """
        return False


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    """
    Admin that affects the changelist view and the change view of `Room`
    """
    class Media:
        """
        Extra CSS to be loaded on the changelist view (styling for the
        allocate room button and stuff)
        """
        css = {
            "all": ("game/style/admin.css",)
        }

    def room(self, obj):
        """
        Custom str display of a Room
        """
        return f'Room {obj.pk} '

    def teams(self, obj):
        """
        Return the number of teams allocated to this room as a string
        """
        return str(obj.teams.count())

    list_editable = ['name']
    list_display = ['room', 'kind', 'name', 'teams']
    inlines = [ TeamInline, ]


admin.site.register(Country)
admin.site.register(CountryStatic)
admin.site.register(ParliamentGroup)
admin.site.register(ParliamentGroupStatic)
admin.site.register(ResearchItem)


@admin.register(AdvisorMemo)
class AdvisorMemoAdmin(admin.ModelAdmin):
    list_display = ["sent", "writer", "subject"]
    list_display_links = ["subject", "writer"]


def team_vote_is_country(team_vote):
    return hasattr(team_vote.by, "country")

def team_vote_is_epgroup(team_vote):
    return hasattr(team_vote.by, "parliamentgroup")

@admin.register(FinalTeamVote)
class FinalTeamVoteAdmin(admin.ModelAdmin):
    """ Admin view of FinalTeamVote model """
    list_display = ["by", "option", team_vote_is_country, team_vote_is_epgroup]


class ResearchItemInline(admin.TabularInline):
    model = ResearchItem
    extra = 1


@admin.register(ResearchCategory)
class ResearchCategoryAdmin(admin.ModelAdmin):
    """ Admin view of ResearchCategory model """
    inlines = [ResearchItemInline]


def display_time_offset(entry):
    return "%i:%02i" % (entry.hour_offset, entry.minute_offset)

@admin.register(CalendarEntry)
class CalendarEntryAdmin(admin.ModelAdmin):
    list_display = [display_time_offset, "text"]
    list_display_links = ["text"]


@admin.register(MemoDeadline)
class MemoDeadlineAdmin(admin.ModelAdmin):
    """ Admin view of MemoDeadline model """


@admin.register(MyStaffPage)
class MyStaffPageAdmin(admin.ModelAdmin):
    pass


class MyStaffTimeInline(admin.TabularInline):
    model = MyStaffTime


@admin.register(MyStaffRoleContent)
class MyStaffRoleContentAdmin(admin.ModelAdmin):
    inlines = [
        MyStaffTimeInline,
    ]
