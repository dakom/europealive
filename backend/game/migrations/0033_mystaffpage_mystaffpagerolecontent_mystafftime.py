# Generated by Django 2.2 on 2021-04-16 10:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0032_auto_20201029_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyStaffPageRoleContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=50)),
                ('subline', models.CharField(max_length=50)),
                ('content', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='MyStaffTime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=50)),
                ('calendar_entry', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='game.CalendarEntry')),
                ('memo_deadline', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='game.MemoDeadline')),
                ('role_content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='times', to='game.MyStaffPageRoleContent')),
            ],
        ),
        migrations.CreateModel(
            name='MyStaffPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Not displayed anywhere but in the backend', max_length=50)),
                ('chief', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='chief', to='game.MyStaffPageRoleContent')),
                ('media_advisor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='media_advisor', to='game.MyStaffPageRoleContent')),
                ('political_advisor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='political_advisor', to='game.MyStaffPageRoleContent')),
                ('relation_builder', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='relation_builder', to='game.MyStaffPageRoleContent')),
                ('secretary', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='secretary', to='game.MyStaffPageRoleContent')),
                ('strategist', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='strategist', to='game.MyStaffPageRoleContent')),
                ('technical_advisor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='technical_advisor', to='game.MyStaffPageRoleContent')),
            ],
        ),
    ]
