# Generated by Django 2.2 on 2019-10-24 16:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0020_auto_20191017_1714'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advisormemo',
            options={'ordering': ['-sent', 'writer'], 'verbose_name': 'advisor memo', 'verbose_name_plural': 'advisor memos'},
        ),
    ]
