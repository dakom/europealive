# Generated by Django 2.2 on 2020-10-29 19:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0030_game_game_started'),
    ]

    operations = [
        migrations.AddField(
            model_name='countrystatic',
            name='logo',
            field=models.FileField(default='', help_text='.svg file if possible, else image.', upload_to='team'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parliamentgroupstatic',
            name='logo',
            field=models.FileField(default='', help_text='.svg file if possible, else image.', upload_to='team'),
            preserve_default=False,
        ),
    ]
