""" View Module """
from django import forms
from django.contrib.auth.decorators import user_passes_test
from django.db import transaction
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from rest_framework import generics, serializers, viewsets
from rest_framework.permissions import IsAdminUser

from core.models import Team
from game.models import Game, Room, Round


class GameSerializer(serializers.ModelSerializer):
    """ Game serializer """
    class Meta:
        model = Game
        fields = ['id', 'name', 'url']

class RoundSerializer(serializers.ModelSerializer):
    """ Round serializer """

    game = serializers.SerializerMethodField()

    class Meta:
        model = Round
        fields = ('id', 'length', 'game', 'active', 'directive')

    # game = GameSerializer(many=False)
    def get_game(self, _round):
        return Game.get_current_game().id

class GameViewSet(viewsets.ModelViewSet):
    """ Game ViewSet """
    permission_classes = [IsAdminUser]
    queryset = Game.objects.all()
    serializer_class = GameSerializer

class RoundViewSet(viewsets.ModelViewSet):
    """ Round ViewSet """
    queryset = Round.objects.all()
    serializer_class = RoundSerializer


class AllocateRoomsForm(forms.Form):
    """
    Just a shallow form to pass to the `AllocateRooms` view
    """
    pass


@method_decorator(user_passes_test(lambda u: u.is_staff), name='dispatch')
class AllocateRooms(FormView):
    """
    A view that enables the allocation of rooms from the admin page
    """
    form_class = AllocateRoomsForm
    template_name = 'admin/game/allocate_rooms.html'
    success_url = '/admin/game/room/'

    def form_valid(self, form):
        """
        When form is valid, call `Room.allocate_rooms()`
        """
        Room.allocate_rooms()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Add some information to the context to help the user
        """
        context = super().get_context_data(**kwargs)
        context['opts'] = Room._meta
        context['room_count'] = Room.objects.count()
        context['team_count'] = Team.objects.count()
        context['allocation_exists'] = Team.objects.filter(
            room__isnull=False
        ).count() > 0
        return context


class CurrentRoundView(generics.RetrieveAPIView):
    """ Takes no parameters, returns currently active round """
    queryset = Round.objects.all()
    serializer_class = RoundSerializer
    reason_name = "currentRound"

    def get_object(self):
        return Round.get_current_round()


@transaction.atomic
def test_show_job_templates(request, count):
    game = Game.get_current_game()
    game_pristine = Game.get_current_game()

    templates = []
    for _ in range(count):
        templates.append(game.get_next_job_template())

    game_pristine.save() # Save to override changes made during iteration
    return render(request, 'game/test_show_job_templates.html', {
        "templates": templates,
    })
