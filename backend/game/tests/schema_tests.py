"""
Test functionality in `game.schema`
"""
import json

from django.contrib.contenttypes.models import ContentType
from django.utils.timezone import datetime
from graphene_django.utils.testing import GraphQLTestCase

from core.models import (Commission, CommissionStatic, Secretary,
                         TechnicalAdvisor)
from events.models import BaseEvent, DeadlineEvent
from game.models import (AdvisorMemo, Country, CountryStatic, FinalTeamVote,
                         ParliamentGroup, ParliamentGroupStatic)


class UpdateMemoMutationTestCase(GraphQLTestCase):
    """
    Tests functionality related to creating and updating AdvisorMemo via
    `game.schema.UpdateMemoMutation`
    """
    GRAPHQL_URL = '/graphql'

    def test_if_send_memo_is_true_memo_is_updated_and_submitted(self):
        """
        `UpdateMemoMutation` takes `send_memo` as argument indicating if the
        memo draft should be sent and submitted.
        """
        DeadlineEvent.objects.create(
            name='This shall result in a penalty of 15',
            actor=ContentType.objects.get_for_model(TechnicalAdvisor),
            action=ContentType.objects.get_for_model(AdvisorMemo),
            offset=99,
            penalty=15,
        )

        # create two tasks that shouldn't be processed
        DeadlineEvent.objects.create(
            name='Already processed',
            actor=ContentType.objects.get_for_model(TechnicalAdvisor),
            action=ContentType.objects.get_for_model(AdvisorMemo),
            offset=99,
            processed=True
        )

        DeadlineEvent.objects.create(
            name='In future',
            actor=ContentType.objects.get_for_model(TechnicalAdvisor),
            action=ContentType.objects.get_for_model(AdvisorMemo),
            offset=101,
        )

        team = Commission.objects.create(
            name='Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        advisor = TechnicalAdvisor.objects.create(
            first_name='Test',
            team=team
        )

        DeadlineEvent.create_all_tasks()

        # an AdvisorMemo is automatically created when accessing the WriteMemo
        # page, so we also need to create one here before our tests
        memo_draft = AdvisorMemo.objects.create(
            writer=advisor,
            content='',
            subject=''
        )

        # input variables to the query
        values = {
            'id': memo_draft.id,
            'subject': 'TestMemo',
            'content': 'TestContent',
            'sendMemo': True,
        }

        res = self.query(
            '''
            mutation(
                $id: ID!,
                $subject: String!,
                $content: String!,
                $sendMemo: Boolean!
                ) {
                  updateMemo(
                      id: $id,
                      subject: $subject,
                      content: $content,
                      sendMemo: $sendMemo
                  ) {
                      advisorMemo {
                        id
                      }
                    }
                }
            ''',
            variables=values
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        query = content['data']['updateMemo']
        memo = AdvisorMemo.objects.get(id=int(query['advisorMemo']['id']))

        # check that the returned id is the same as that of the draft
        self.assertEqual(memo.id, memo_draft.id)

        # check that the subject and content has been updated
        self.assertEqual(memo.subject, values['subject'])
        self.assertEqual(memo.content, values['content'])

        # chech that the memo has been submitted
        self.assertTrue(memo.is_submitted)

        # check that the memo now has a sent time
        self.assertIsInstance(memo.sent, datetime)

    def test_if_send_memo_is_false_memo_is_updated_and_not_submitted(self):
        """
        If `send_memo` is `False`, then the `AdvisorMemo` should be updated but
        neither sent nor submitted
        """
        team = Commission.objects.create(
            name='Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        advisor = TechnicalAdvisor.objects.create(
            first_name='Test',
            team=team
        )

        # an AdvisorMemo is automatically created when accessing the WriteMemo
        # page, so we also need to create one here before our tests
        memo_draft = AdvisorMemo.objects.create(
            writer=advisor,
            content='',
            subject=''
        )

        # get number of AdvisorMemos before (should be 1)
        num_before = AdvisorMemo.objects.count()
        self.assertEqual(num_before, 1)  # sanity

        # input variables for query
        values = {
            'id': memo_draft.id,
            'subject': 'TestMemo',
            'content': 'TestContent',
            'sendMemo': False,
        }

        # query
        res = self.query(
            '''
            mutation(
                $id: ID!,
                $subject: String!,
                $content: String!,
                $sendMemo: Boolean!
                ) {
                  updateMemo(
                      id: $id,
                      subject: $subject,
                      content: $content,
                      sendMemo: $sendMemo
                  ) {
                      advisorMemo {
                        id
                      }
                    }
                }
            ''',
            variables=values
        )

        self.assertResponseNoErrors(res)

        # since the memo was already created, the amount after making the
        # mutation should be the same
        self.assertEqual(AdvisorMemo.objects.count(), num_before)

        content = json.loads(res.content)
        query = content['data']['updateMemo']
        memo = AdvisorMemo.objects.get(id=int(query['advisorMemo']['id']))

        # check that the returned id is the same as that of the draft
        self.assertEqual(memo.id, memo_draft.id)

        # check that the subject and content has been updated
        self.assertEqual(memo.subject, values['subject'])
        self.assertEqual(memo.content, values['content'])

        # check that the memo hasn't been submitted
        self.assertFalse(memo.is_submitted)

        # check that the memo still doesn't have a sent time
        self.assertIsNone(memo.sent)


class CalendarEntriesQueryTestCase(GraphQLTestCase):
    """
    Test functionality related to querying events for calendar bar
    """
    GRAPHQL_URL = '/graphql'
    query_string = """
        query {
            calendarEntries {
                id
                text
                time
                duration
            }
        }
    """

    def test_only_events_with_display_in_calendar_is_returned(self):
        """
        Test that if `BaseEvent.display_in_calendar` is `False`, then the event
        is not returned in the queryset
        """
        BaseEvent.objects.create(
            name='True',
            offset=10,
            display_in_calendar=True
        )
        BaseEvent.objects.create(
            name='False',
            offset=10,
            display_in_calendar=False
        )

        gamestart = datetime(1991, 12, 27, 9, 0)
        for event in BaseEvent.objects.all():
            event.set_display_time(gamestart)

        res = self.query(self.query_string)
        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        query = content['data']['calendarEntries']

        self.assertEqual(len(query), 1)
        self.assertEqual(query[0]['text'], 'True')

    def test_queryset_is_ordered_correctly(self):
        """
        The returned queryset should be ordered by `duration` and then by
        reversed `duration`
        """
        # create events in some random order
        BaseEvent.objects.create(
            name='Third',
            display_in_calendar=True,
            offset=10,
            duration=0
        )
        BaseEvent.objects.create(
            name='Sixth',
            display_in_calendar=True,
            offset=11,
            duration=0
        )
        BaseEvent.objects.create(
            name='Fourth',
            display_in_calendar=True,
            offset=10,
            duration=30
        )
        BaseEvent.objects.create(
            name='Second',
            display_in_calendar=True,
            offset=5,
            duration=0
        )
        BaseEvent.objects.create(
            name='Fifth',
            display_in_calendar=True,
            offset=10,
            duration=31
        )
        BaseEvent.objects.create(
            name='First',
            display_in_calendar=True,
            offset=1,
            duration=30
        )

        gamestart = datetime(1991, 12, 27, 9, 0)
        for event in BaseEvent.objects.all():
            event.set_display_time(gamestart)

        res = self.query(self.query_string)

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        query = content['data']['calendarEntries']

        self.assertEqual(len(query), 6)
        self.assertEqual(query[0]['text'], 'First')
        self.assertEqual(query[1]['text'], 'Second')
        self.assertEqual(query[2]['text'], 'Third')
        self.assertEqual(query[3]['text'], 'Fourth')
        self.assertEqual(query[4]['text'], 'Fifth')
        self.assertEqual(query[5]['text'], 'Sixth')


class FinalVoteMutationTestCase(GraphQLTestCase):
    """
    Test `game.schema.FinalVoteMutation`
    """

    GRAPHQL_URL = '/graphql'
    QUERY_STR = """
        mutation($option: VoteOption!) {
            finalVote(option: $option) {
                didVote
            }
        }
    """

    def setUp(self):
        """
        Create a voter (student and team)
        """
        team = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                starting_ip=100,
                population=10
            )
        )

        user = Secretary.objects.create_user(
            username='whocares',
            password='whatever',
            team=team
        )

        self.client.force_login(user)

    def test_vote_is_submitted_when_successfull_mutation(self):
        """
        Test that the vote is marked as submitted for the purpose of deadlines
        and tasks
        """
        user = Secretary.objects.first()
        deadline = DeadlineEvent.objects.create(
            offset=10,
            actor=ContentType.objects.get_for_model(Country),
            action=ContentType.objects.get_for_model(FinalTeamVote)
        )
        task = deadline.create_task(user.team.get_subclass())

        res = self.query(
            self.QUERY_STR,
            variables={ 'option': 'YES' }
        )

        self.assertResponseNoErrors(res)

        vote = FinalTeamVote.objects.first()
        self.assertIsNotNone(vote)
        self.assertTrue(vote.is_submitted)

        task.refresh_from_db()
        self.assertTrue(task.completed())

    def test_multiple_votes_does_not_interfere_with_each_other(self):
        """
        Test that the system can handle two identical deadlines with only
        difference being actor as Country or ParliamentGroup
        """
        # create some additional teams
        team2 = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                starting_ip=100,
                population=10
            )
        )
        group = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                starting_ip=100,
                member_count=10
            )
        )

        # additional users
        user2 = Secretary.objects.create_user(
            username='whocares2',
            password='whatever',
            team=team2
        )
        Secretary.objects.create_user(
            username='whocares3',
            password='whatever',
            team=group
        )

        DeadlineEvent.objects.create(
            offset=10,
            actor=ContentType.objects.get_for_model(ParliamentGroup),
            action=ContentType.objects.get_for_model(FinalTeamVote)
        )
        DeadlineEvent.objects.create(
            offset=10,
            actor=ContentType.objects.get_for_model(Country),
            action=ContentType.objects.get_for_model(FinalTeamVote)
        )
        DeadlineEvent.create_all_tasks()

        # vote with default user
        res = self.query(
            self.QUERY_STR,
            variables={ 'option': 'YES' }
        )
        self.assertResponseNoErrors(res)

        # use next country secretary
        self.client.force_login(user2)

        res = self.query(
            self.QUERY_STR,
            variables={ 'option': 'YES' }
        )
        self.assertResponseNoErrors(res)

        for team in Country.objects.all():
            task = team.tasks.first()
            self.assertTrue(task.completed())

        team = ParliamentGroup.objects.first()
        task = team.tasks.first()
        self.assertFalse(task.completed())
