# pylint: disable=bad-continuation, invalid-name
"""
Tests for `game.tasks`
"""
import random
from datetime import timedelta
from unittest import skip
from unittest.mock import Mock, call, patch

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.utils import timezone

from core.models import (SUCCESS_CRITERIA_CHOICES, ArticleSelection,
                         Commission, CommissionStatic, Directive,
                         DirectiveArticle, Team, TechnicalAdvisor)
from events.models import DeadlineEvent, DeadlineTask
from game.models import (AdvisorMemo, Country, FinalTeamVote, Game,
                         ParliamentGroup, ParliamentGroupStatic, Round)
from game.tasks import (award_special_success_criteria_points,
                        handle_deadlines, start_game_task)
from inf_points.models import PointTransaction
from meta.models import GameConfiguration


class AwardSpecialSuccessCriteriaTestCase(TestCase):
    """
    Tests related to function
    `game.tasks.award_special_success_criteria_points()`
    """

    def setUp(self):
        """
        Create a Directive
        """
        directive = Directive.objects.create()
        for i in range(1, 9):
            DirectiveArticle.objects.create(number=i)

        # create teams and assign a success criteria
        for crit, _ in SUCCESS_CRITERIA_CHOICES:

            # using parliament groups arbitrarily for teams
            ParliamentGroup.objects.create(
                special_success_criteria_value=crit,
                influence_points=100,
                static=ParliamentGroupStatic.objects.create(
                    member_count=1,
                    starting_ip=100
                )
            )

        self.gameround = Round.objects.create(
            active=True,
            order=4,
            directive=directive,
        )

    @patch('game.tasks.FinalTeamVote')
    def test_commission_special_gets_points_when_70_percent_in_favor(
        self, mock_vote
    ):
        """Test that the special success criteria 'commission special' is
        resolved, when 70 percent of both the Parliament and the Council votes in favor"""
        team = Team.objects.get(
            special_success_criteria_value='commission special',
        )

        mock_vote.parliament_vote_percentages.return_value = 70, 30
        mock_vote.council_vote_percentages.return_value = 70, 30, 0
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        team.refresh_from_db()

        # team should have recieved a 10% IP reward
        self.assertEqual(team.influence_points, 150)

        # check that it is correct
        transaction = PointTransaction.objects.get(recipient=team)
        self.assertEqual(
            transaction.description,
            'Your team achieved your special success criteria!'
        )

        # check that the task is marked as ran
        self.assertTrue(
            self.gameround.special_success_criteria_task_ran
        )

    @patch('game.tasks.FinalTeamVote')
    def test_commission_special_is_not_resolved_when_less_than_70_percent_yes_in_parliament(
        self, mock_vote
    ):
        """Test that the special success criteria 'commission special' is
        not resolved, when less than 70 percent of the Parliament votes in
        favor"""
        team = Team.objects.get(
            special_success_criteria_value='commission special',
        )

        mock_vote.parliament_vote_percentages.return_value = 69, 31
        mock_vote.council_vote_percentages.return_value = 70, 30, 0
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        team.refresh_from_db()

        # team should have the same amount of IP
        self.assertEqual(team.influence_points, 100)

        transaction = PointTransaction.objects.get(recipient=team)

        # check that it is correct
        self.assertEqual(
            transaction.description,
            'Your team did not achieve your special success criteria'
        )

        # check that the task is marked as ran
        self.assertTrue(
            self.gameround.special_success_criteria_task_ran
        )

    @patch('game.tasks.FinalTeamVote')
    def test_commission_special_is_not_resolved_when_less_than_70_percent_yes_in_council(
        self, mock_vote
    ):
        """Test that the special success criteria 'commission special' is
        not resolved, when less than 70 percent of the Council votes in favor"""
        team = Team.objects.get(
            special_success_criteria_value='commission special',
        )

        mock_vote.parliament_vote_percentages.return_value = 70, 30
        mock_vote.council_vote_percentages.return_value = 69, 29, 1
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        team.refresh_from_db()

        # team should have the same amount of IP
        self.assertEqual(team.influence_points, 100)

        # check that it is correct
        transaction = PointTransaction.objects.get(recipient=team)
        self.assertEqual(
            transaction.description,
            'Your team did not achieve your special success criteria'
        )

        # check that the task is marked as ran
        self.assertTrue(
            self.gameround.special_success_criteria_task_ran
        )

    @patch('game.tasks.FinalTeamVote')
    def test_commission_special_is_not_resolved_when_directive_not_passed(
        self, mock_vote
    ):
        """Test that the special success criteria 'commission special' is
        not resolved, when the directive is rejected, even if 70 percent of
        both the Parliament and the Council votes in favor"""
        team = Team.objects.get(
            special_success_criteria_value='commission special',
        )

        mock_vote.parliament_vote_percentages.return_value = 70, 30
        mock_vote.council_vote_percentages.return_value = 70, 30, 0
        mock_vote.directive_passed.return_value = False

        award_special_success_criteria_points(self.gameround)

        team.refresh_from_db()

        # team should have the same amount of IP
        self.assertEqual(team.influence_points, 100)

        # check that it is correct
        transaction = PointTransaction.objects.get(recipient=team)
        self.assertEqual(
            transaction.description,
            'Your team did not achieve your special success criteria'
        )

        # check that the task is marked as ran
        self.assertTrue(
            self.gameround.special_success_criteria_task_ran
        )

    @patch('game.tasks.FinalTeamVote')
    def test_all_teams_except_commission_resolves_their_criteria(
        self, mock_vote
    ):
        """Test that all teams with 'regular' article based criterias can get
        points for resolving their criteria"""

        # create article selection and set all selections to something <= 3
        for article in DirectiveArticle.objects.all():
            ArticleSelection.objects.create(
                article=article,
                items_selected=random.randint(1, 3),
                directive=Directive.objects.get()
            )

        # set the vote result to something that prevents the Commission from
        # resolving their criteria, but let the directive pass
        mock_vote.parliament_vote_percentages.return_value = 65, 35
        mock_vote.council_vote_percentages.return_value = 65, 25, 10
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        for team in Team.objects.all():
            # there should be a PointTransaction for each team

            # if the team has an article based criteria, they should have
            # recieved a 10% reward and a corresponding transaction
            if 'article' in team.special_success_criteria_value:
                transaction = PointTransaction.objects.get(recipient=team)
                self.assertEqual(team.influence_points, 150)
                self.assertEqual(
                    transaction.description,
                    'Your team achieved your special success criteria!'
                )

            # if commission special, their IP should be the same, and the
            # transaction should reflect that they didn't meet the criteria
            elif 'commission' in team.special_success_criteria_value:
                transaction = PointTransaction.objects.get(recipient=team)
                self.assertEqual(team.influence_points, 100)
                self.assertEqual(
                    transaction.description,
                    'Your team did not achieve your special success criteria'
                )

            # if criteria 'none', then no transaction should exist
            else:
                self.assertIsNone(
                    PointTransaction.objects.filter(recipient=team).first()
                )

    @patch('game.tasks.FinalTeamVote')
    def test_three_teams_resolves_their_criteria(
        self, mock_vote
    ):
        """Test that the commission criteria and (arbitrarily chosen) the
        article 2 and 4 criterias are met, but the rest fails"""

        # create article selection and set all selections to something > 3
        for article in DirectiveArticle.objects.all():
            ArticleSelection.objects.create(
                article=article,
                items_selected=random.randint(4, 5),
                directive=Directive.objects.get()
            )

        # fix article 2 and 4
        arts = ArticleSelection.objects.filter(article__number__in=[2, 4])
        for art in arts:
            art.items_selected = random.randint(1, 3)
            art.save()

        # set the vote result so that the commission criteria is met
        mock_vote.parliament_vote_percentages.return_value = 70, 30
        mock_vote.council_vote_percentages.return_value = 70, 25, 5
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        exp_met = ['article 2', 'article 4', 'commission special']

        for team in Team.objects.all():

            # if the team has a criteria expected to be met, they should have
            # recieved a 10% reward and a corresponding transaction
            if team.special_success_criteria_value in exp_met:
                transaction = PointTransaction.objects.get(recipient=team)
                self.assertEqual(team.influence_points, 150)
                self.assertEqual(
                    transaction.description,
                    'Your team achieved your special success criteria!'
                )

            # no transaction if no criteria
            elif team.special_success_criteria_value == 'none':
                self.assertIsNone(
                    PointTransaction.objects.filter(recipient=team).first()
                )

            # otherwise, their IP should be the same, and the transaction
            # should reflect that they didn't meet the criteria
            else:
                transaction = PointTransaction.objects.get(recipient=team)
                self.assertEqual(team.influence_points, 100)
                self.assertEqual(
                    transaction.description,
                    'Your team did not achieve your special success criteria'
                )

    @patch('game.tasks.FinalTeamVote')
    def test_directive_passed_but_no_team_resolves_their_criteria(
        self, mock_vote
    ):
        """Test that the directive can pass without any team resolving their
        criteria"""

        # create article selection and set all selections to something < 3
        for article in DirectiveArticle.objects.all():
            ArticleSelection.objects.create(
                article=article,
                items_selected=random.randint(4, 5),
                directive=Directive.objects.get()
            )

        # set the vote result so that the commission criteria is met
        mock_vote.parliament_vote_percentages.return_value = 65, 35
        mock_vote.council_vote_percentages.return_value = 65, 25, 10
        mock_vote.directive_passed.return_value = True

        award_special_success_criteria_points(self.gameround)

        for team in Team.objects.all():
            # no transaction if no criteria
            if team.special_success_criteria_value == 'none':
                self.assertIsNone(
                    PointTransaction.objects.filter(recipient=team).first()
                )

            else:
                # there should be a PointTransaction for each team
                transaction = PointTransaction.objects.get(recipient=team)

                # but no one should have recieved a reward
                self.assertEqual(team.influence_points, 100)
                self.assertEqual(
                    transaction.description,
                    'Your team did not achieve your special success criteria'
                )


class StartGameTaskTestCase(TestCase):
    """
    Test the function `game.tasks.start_game()` which is responsible for
    starting a game and assigning jobs to all students
    """

    def setUp(self):
        """
        Create an empty GameConfiguration and a Game with startting_time now
        """
        config = GameConfiguration.objects.create(name='config', description='')
        self.game = Game.objects.create(
            name='test_game',
            starting_time=timezone.now(),
            configuration=config,
            last_job_ordering=0
        )

    def test_returns_none_when_no_game_is_created(self):
        """
        If `start_game_task` is called on server with no game in the database,
        it should just return None
        """
        Game.objects.all().delete()
        self.assertIsNone(start_game_task())

    @patch('game.models.Game.assign_initial_students_jobs')
    def test_assign_initial_student_job_called_once_and_game_started(
            self, mock_assign
    ):
        """
        When a game is started, all students should be assigned jobs by a call
        to `assign_initial_students_jobs()`
        """
        start_game_task()
        mock_assign.assert_called_once()
        self.game.refresh_from_db()
        self.assertTrue(self.game.game_started)

    def test_start_game_task_on_started_game_raises_error(self):
        """
        If `start_game()` is called when a Game already exists, raise an Error
        """
        game = self.game
        game.game_started = True
        game.save()

        self.assertRaises(AssertionError, start_game_task)


class HandleDeadlinesTestCase(TestCase):
    """
    Test the function `game.tasks.handle_deadlines()` which goes through all
    past DeadlineEvents and execute their effect
    """

    @patch('game.tasks.PointTransaction.send')
    @patch('game.tasks.Game')
    def test_pointtransactions_created_for_all_completed_tasks(
            self, mock_game_cls, mock_send
    ):
        """
        All tasks should generate a `PointTransaction`, either for a reward or
        for a point deduction
        """
        # setup mock objects
        mock_game = Mock()
        mock_game.minutes_passed.return_value = 100
        mock_game.tasks.all.return_value = []
        mock_game_cls.get_current_game.return_value = mock_game

        # create two deadline events that should be processed
        d1 = DeadlineEvent.objects.create(
            name='This shall result in a penalty of 15',
            actor=ContentType.objects.get_for_model(TechnicalAdvisor),
            action=ContentType.objects.get_for_model(AdvisorMemo),
            offset=99,
            penalty=15,
        )

        d2 = DeadlineEvent.objects.create(
            name='This shall result in a reward of 10',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=100,
            reward=10
        )

        # create two tasks that shouldn't be processed
        DeadlineEvent.objects.create(
            name='Already processed',
            actor=ContentType.objects.get_for_model(TechnicalAdvisor),
            action=ContentType.objects.get_for_model(AdvisorMemo),
            offset=99,
            processed=True
        )

        DeadlineEvent.objects.create(
            name='In future',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=101,
        )

        # create some actual actors
        commission = Commission.objects.create(
            name='Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        advisor = TechnicalAdvisor.objects.create(
            first_name='Test',
            team=commission
        )

        # create tasks
        DeadlineEvent.create_all_tasks()
        self.assertEqual(DeadlineTask.objects.count(), 4)

        # submit for the one deadline
        directive = Directive.create_first_directive()
        d2.set_display_time(timezone.now() - timedelta(minutes=90))
        d2.submit(commission, directive)

        # sanity check
        self.assertEqual(PointTransaction.objects.count(), 0)

        # function for testing
        handle_deadlines()

        # now two point transactions should have been created
        self.assertEqual(mock_send.call_count, 2)
        mock_send.assert_has_calls([
            call(
                description='Punishment for not submitting advisor memo on time',
                amount=-15,
                # since recipient is fetched from student.team it is not the
                # subtype (Commission) that is used for the point transaction
                # but that is ok
                recipient=Team.objects.get(id=commission.id),
                task=advisor.tasks.get(deadline=d1)
            ),
            call(
                description='Reward for submitting directive on time',
                amount=10,
                recipient=commission,
                task=commission.tasks.get(deadline=d2)
            )
        ], any_order=True)

    @patch('game.tasks.Game')
    def test_only_unprocessed_passed_deadlines_are_processed(
            self, mock_game_cls
    ):
        """
        Test that processed `DeadlineEvents` are not processed a second time
        """
        mock_game = Mock()
        mock_game.minutes_passed.return_value = 100
        mock_game.tasks.all.return_value = []
        mock_game_cls.get_current_game.return_value = mock_game

        actor = ContentType.objects.get_for_model(Commission)
        action = ContentType.objects.get_for_model(Directive)

        # create two deadline events that are already processed
        DeadlineEvent.objects.create(
            name='Passed and processed 1',
            actor=actor,
            action=action,
            offset=10,
            processed=True
        )

        DeadlineEvent.objects.create(
            name='Passed and processed 2',
            actor=actor,
            action=action,
            offset=99,
            processed=True
        )

        # create two deadline events that should be processed
        d1 = DeadlineEvent.objects.create(
            name='Check that this is marked as processed 1',
            actor=actor,
            action=action,
            offset=99,
        )

        d2 = DeadlineEvent.objects.create(
            name='Check that this is marked as processed 2',
            actor=actor,
            action=action,
            offset=100,
        )

        # create two deadline events that are in the future
        DeadlineEvent.objects.create(
            name='Should not be processed 1',
            actor=actor,
            action=action,
            offset=101,
        )

        DeadlineEvent.objects.create(
            name='Should not be processed 2',
            actor=actor,
            action=action,
            offset=200,
        )

        # sanity check
        self.assertEqual(
            DeadlineEvent.objects.filter(processed=True).count(), 2
        )

        # test this function
        handle_deadlines()

        # two extra deadline events should now be processed
        self.assertEqual(
            DeadlineEvent.objects.filter(processed=True).count(), 4
        )

        # and it should of course be the right ones
        d1.refresh_from_db()
        d2.refresh_from_db()

        self.assertTrue(d1.processed)
        self.assertTrue(d2.processed)

    def test_deadlines_with_no_reward_or_penalty_does_not_create_pointtransactoins(self):
        pass

    @patch('game.tasks.Game')
    @patch('game.tasks.end_round')
    @patch('game.tasks.start_round')
    def test_end_round_and_start_round_are_called_when_round_end_deadline_is_processed(
            self, mock_start_round, mock_end_round, mock_game_cls
    ):
        """
        Test that when the DeadlineEvent in the relation `Round.round_end` is
        processed, then both `game.tasks.end_round()` and
        `game.tasks.start_round()` are called
        """
        # setup mock objects
        mock_game = Mock()
        mock_game.minutes_passed.return_value = 100
        mock_game_cls.get_current_game.return_value = mock_game

        d1 = DeadlineEvent.objects.create(
            name='First round end',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=100,
            reward=10
        )

        d2 = DeadlineEvent.objects.create(
            name='Second round end',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=150,
            reward=10
        )

        r1 = Round.objects.create(
            round_end=d1,
            order=1,
            active=True,
            directive=Directive.create_first_directive()
        )

        r2 = Round.objects.create(
            round_end=d2,
            order=2,
            active=False,
            directive=None
        )

        # test this function
        handle_deadlines()

        mock_end_round.assert_called_once_with(r1)
        mock_start_round.assert_called_once_with(r2)

    def test_end_round_and_start_round_not_called_if_gameround_final(self):
        pass

    @patch('game.tasks.Game')
    def test_multiple_rounds(
            self, mock_game_cls
    ):
        """
        Test that `handle_deadlines()` changes the round also for multiple
        rounds
        """
        commission = Commission.objects.create(
            name='Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        d1 = DeadlineEvent.objects.create(
            name='First round end',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=100,
            reward=10,
            penalty=15
        )

        d2 = DeadlineEvent.objects.create(
            name='Second round end',
            actor=ContentType.objects.get_for_model(Commission),
            action=ContentType.objects.get_for_model(Directive),
            offset=150,
            reward=10,
            penalty=15
        )

        gamestart = timezone.now()
        for d in DeadlineEvent.objects.all():
            d.set_display_time(gamestart)
        DeadlineEvent.create_all_tasks()

        r1 = Round.objects.create(
            round_end=d1,
            order=1,
            active=True,
            directive=Directive.create_first_directive()
        )

        r2 = Round.objects.create(
            round_end=d2,
            order=2,
            active=False,
            directive=None
        )

        r3 = Round.objects.create(
            order=3,
            active=False,
            directive=None
        )

        # setup mock objects
        mock_game = Mock()
        mock_game.minutes_passed.return_value = 100
        mock_game_cls.get_current_game.return_value = mock_game

        # test this function first time
        handle_deadlines()

        r1.refresh_from_db()
        r2.refresh_from_db()
        r3.refresh_from_db()
        d1.refresh_from_db()
        d2.refresh_from_db()
        commission.refresh_from_db()

        # r1 and r3 should be inactive, r2 should be active
        self.assertFalse(r1.active)
        self.assertTrue(r2.active)
        self.assertFalse(r3.active)

        # get_current_round should now return r2
        self.assertEqual(Round.get_current_round(), r2)

        # since no Directive was submitted, r2 should have a randomly generated
        self.assertIsNotNone(r2.directive)

        # the Commission should have received a penalty of 15 points
        self.assertEqual(commission.influence_points, 85)

        # finally, d1 should now be processed
        self.assertTrue(d1.processed)

        # DONE WITH FIRST TEST BATCH
        # reset mock game
        mock_game.minutes_passed.return_value = 150

        # submit a Directive and add it to next round
        directive = Directive.create_random_mutation(r1.directive)
        directive.submit()
        r3.directive = directive
        r3.save()

        # test function for the second time
        handle_deadlines()

        r1.refresh_from_db()
        r2.refresh_from_db()
        r3.refresh_from_db()
        d1.refresh_from_db()
        d2.refresh_from_db()
        commission.refresh_from_db()

        # r1 and r2 should be inactive, r3 should be active
        self.assertFalse(r1.active)
        self.assertFalse(r2.active)
        self.assertTrue(r3.active)

        # now r3 should have a directive
        self.assertIsNotNone(r3.directive)

        # the Commission should now have received a reward of 10 poitns
        self.assertEqual(commission.influence_points, 95)

        # d2 should now be processed
        self.assertTrue(d2.processed)


class StartRoundTestCase(TestCase):
    """
    Tests related to `game.tasks.start_round()`, which starts a new `Round`
    """
    pass
