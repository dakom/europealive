""" Tests for Game module """
import csv
import os
from datetime import datetime, timedelta
from math import ceil
from unittest.mock import Mock, patch

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.utils import timezone
from django.utils.timezone import make_aware

from core.models import (AttacheAdvisor, Commission, CommissionStatic,
                         Directive, LobbyOrganization, LobbyOrganizationStatic,
                         MediaAdvisor, PendingStudent, PoliticalAdvisor,
                         Secretary, Student, Team, TechnicalAdvisor)
from events.models import DeadlineEvent
from game.models import (AdvisorMemo, CalendarEntry, Country, CountryStatic,
                         FinalTeamVote, Game, MemoDeadline, MyStaffRoleContent,
                         MyStaffTime, ParliamentGroup, ParliamentGroupStatic,
                         Room, Round, VoteResult)
from inf_points.models import DirectiveInvestment
from meta.models import GameConfiguration
from news.models import MediaHouse, MediaHouseStatic


class GetCurrentGameTestCase(TestCase):
    """
    Tests related to class function `Game.get_current_game()`
    """

    def test_returns_none_when_no_game_is_created(self):
        """
        `None` should be returned when there is no `Game` object in db
        """
        self.assertIsNone(Game.get_current_game())

    def test_returns_game_if_game_is_created(self):
        """
        A game object should be returned, no matter if it has started or not
        """
        # create a game that has not yet started
        game = Game.objects.create(
            configuration=GameConfiguration.objects.create(),
            game_started=False
        )

        self.assertEqual(Game.get_current_game(), game)

        game.game_started = True
        game.save()

        self.assertEqual(Game.get_current_game(), game)


class AssignInitialStudentsJobsTestCase(TestCase):
    """
    Tests for Game.assign_initial_students_jobs()
    """
    @patch('game.models.all_models.Game.get_next_job_template')
    def test_creates_students_from_job_templates(
            self, mock_get_next_job_temp
    ):
        """
        Test that the function calls `Game.get_next_job_template` and
        `StudentJobTemplate.create_any_student` for each `PendingStudent`
        object
        """

        mock_job_template = Mock()
        mock_get_next_job_temp.return_value = mock_job_template

        # filepath to test data
        test_data = os.path.join(
            settings.BASE_DIR,
            'game/tests/test_student_data.csv'
        )

        # create the pending students
        with open(test_data, 'r') as f:
            reader = csv.reader(f)
            for i, line in enumerate(reader):
                if i >= 100:
                    break
                username = line[2].lower() + str(i)
                PendingStudent.objects.create(
                    first_name=line[1],
                    last_name=line[2],
                    username=username,
                    password='whatever'
                )

        # create an empty game
        gamestart = timezone.now()
        game = Game.objects.create(
            starting_time=gamestart,
            last_job_ordering=0,
            configuration=GameConfiguration.objects.create()
        )

        students_to_create = PendingStudent.objects.count()
        game.assign_initial_students_jobs(
            PendingStudent.objects.all()
        )

        self.assertEqual(
            mock_get_next_job_temp.call_count,
            students_to_create
        )

        for student in PendingStudent.objects.all():
            mock_job_template.create_any_student.assert_any_call(
                student.user,
                delete_pending_student=True
            )


class GetNextJobTemplateTestCase(TestCase):
    """
    Tests related to function `Game.get_next_job_template`
    """

    @patch("meta.models.GameConfiguration.get_next_job_in_conf")
    def test_first_pass_is_true_if_job_ordering_smaller_than_conf_order(
            self, mock_get_next_job
    ):
        """
        Test that `Game.job_ordering_first_pass` is `True` as long as the
        returned `JobInConfiguration` has a larger `order` attribute than
        `Game.last_job_ordering`
        """
        # create an empty game
        gamestart = timezone.now()
        game = Game.objects.create(
            starting_time=gamestart,
            last_job_ordering=9,
            configuration=GameConfiguration.objects.create()
        )

        mock_get_next_job.return_value = Mock(order=10, job=None)

        _ = game.get_next_job_template()

        self.assertTrue(game.job_ordering_first_pass)
        self.assertEqual(10, game.last_job_ordering)

    @patch("meta.models.GameConfiguration.get_next_job_in_conf")
    def test_first_pass_is_false_if_job_ordering_bigger_than_conf_order(
            self, mock_get_next_job
    ):
        """
        Test that `Game.job_ordering_first_pass` is set to `False` when the
        return `JobInConfiguration` has a lower `order` attribute than
        `Game.last_job_ordering` (which means we have finished the first pass
        and a are cycling through the jobs again)
        """
        # create an empty game
        gamestart = timezone.now()
        game = Game.objects.create(
            starting_time=gamestart,
            last_job_ordering=11,
            configuration=GameConfiguration.objects.create()
        )

        mock_get_next_job.return_value = Mock(order=1, job=None)

        _ = game.get_next_job_template()

        self.assertFalse(game.job_ordering_first_pass)
        self.assertEqual(1, game.last_job_ordering)


class RoundTestCase(TestCase):
    """
    Tests for Round model
    """
    def setUp(self):
        """
        Set up a game with 4 rounds
        """
        super().setUp()

        start_time = timezone.make_aware(timezone.datetime(1991, 12, 27, 9))
        Game.objects.create(
            name='TestGame',
            starting_time=start_time,
            last_job_ordering=0,
            configuration=GameConfiguration.objects.create()
        )
        ct_com = ContentType.objects.get_for_model(Commission)
        ct_dir = ContentType.objects.get_for_model(Directive)
        ct_sec = ContentType.objects.get_for_model(Secretary)
        ct_ipi = ContentType.objects.get_for_model(DirectiveInvestment)

        d1 = DeadlineEvent.objects.create(
            offset=105,
            actor=ct_com,
            action=ct_dir,
        )
        d2 = DeadlineEvent.objects.create(
            offset=195,
            actor=ct_com,
            action=ct_dir,
        )
        d3 = DeadlineEvent.objects.create(
            offset=255,
            actor=ct_com,
            action=ct_dir,
        )

        ip1 = DeadlineEvent.objects.create(
            offset=45,
            actor=ct_sec,
            action=ct_ipi,
        )
        ip2 = DeadlineEvent.objects.create(
            offset=115,
            actor=ct_sec,
            action=ct_ipi,
        )
        ip3 = DeadlineEvent.objects.create(
            offset=200,
            actor=ct_sec,
            action=ct_ipi,
        )

        self.round1 = Round.objects.create(
            round_end=d1,
            active=True,
            order=1,
            ip_deadline=ip1,
        )
        self.round2 = Round.objects.create(
            round_end=d2,
            active=False,
            order=2,
            ip_deadline=ip2,
        )
        self.round3 = Round.objects.create(
            round_end=d3,
            active=False,
            order=3,
            ip_deadline=ip3,
        )
        self.round4 = Round.objects.create(
            active=False,
            order=4,
            ip_deadline=None,
        )

    def test_check_string_rep(self):
        """Testing string representation"""
        self.assertEqual("Round 1", str(self.round1))

    def test_get_previous_round_returns_immediate_predecessor(self):
        """
        Test that round 1 precedes round 2 and that no round precedes first
        round
        """
        self.assertEqual(self.round2.get_previous_round(), self.round1)
        self.assertEqual(self.round3.get_previous_round(), self.round2)
        self.assertEqual(self.round4.get_previous_round(), self.round3)
        self.assertIsNone(self.round1.get_previous_round())

    def test_get_following_round(self):
        """
        Testing that round 2 follow round 1 and that no round follows last round
        """
        self.assertEqual(self.round1.get_following_round(), self.round2)
        self.assertEqual(self.round2.get_following_round(), self.round3)
        self.assertEqual(self.round3.get_following_round(), self.round4)
        self.assertIsNone(self.round4.get_following_round())

    def test_length_gives_difference_in_current_and_next_deadline_offset(self):
        """Test that `Round.length` returns the length in minutes and that
        this is the different between the offset of `round_end` deadline of the
        current Round and the next Round."""
        len1 = 105
        len2 = 90
        len3 = 60

        self.assertEqual(self.round1.length, len1)
        self.assertEqual(self.round2.length, len2)
        self.assertEqual(self.round3.length, len3)

    def test_length_of_final_round_is_999(self):
        """Test that `Round.length` called on the final Round returns a large
        number (999)"""
        self.assertTrue(self.round4.is_final) # sanity
        self.assertEqual(self.round4.length, 999)

    def test_get_start_time_round(self):
        """Test that `Round.get_start_time` calculates the correct start time"""
        start_time = Game.get_current_game().starting_time

        # minutes passing during each round
        delta1 = timedelta(minutes=105)
        delta2 = timedelta(minutes=90) + delta1
        delta3 = timedelta(minutes=60) + delta2

        self.assertEqual(start_time, self.round1.get_start_time())
        self.assertEqual(start_time + delta1, self.round2.get_start_time())
        self.assertEqual(start_time + delta2, self.round3.get_start_time())
        self.assertEqual(start_time + delta3, self.round4.get_start_time())

    def test_ip_investment_time(self):
        start_time = Game.get_current_game().starting_time

        # minutes passing during each round
        delta1 = timedelta(minutes=105)
        delta2 = timedelta(minutes=90) + delta1
        delta3 = timedelta(minutes=60) + delta2

        ip_min1 = timedelta(minutes=45)
        ip_min2 = timedelta(minutes=10)
        ip_min3 = timedelta(minutes=5)
        ip_min4 = timedelta(minutes=0)

        self.assertEqual(
            self.round1.start_time + ip_min1,
            self.round1.ip_investment_time
        )
        self.assertEqual(
            self.round2.start_time + ip_min2,
            self.round2.ip_investment_time
        )
        self.assertEqual(
            self.round3.start_time + ip_min3,
            self.round3.ip_investment_time
        )
        self.assertEqual(
            self.round4.start_time + ip_min4,
            self.round4.ip_investment_time
        )

    def test_is_final(self):
        self.assertFalse(self.round1.is_final)
        self.assertFalse(self.round2.is_final)
        self.assertFalse(self.round3.is_final)
        self.assertTrue(self.round4.is_final)


class CountryStaticTestCase(TestCase):
    """
    Tests for CountryStatic model
    """

    def test_total_population_count(self):
        """
        Test that class method `CountryStatic.total_population_count` returns
        a correct count of the population count of all countries
        """
        for population in [10, 100, 1000]:
            CountryStatic.objects.create(
                population=population,
                starting_ip=100
            )
        self.assertEqual(1110, CountryStatic.total_population_count())

    def test_voting_weight(self):
        """
        Test that voting weight is correctly calculated
        """
        country = CountryStatic.objects.create(
            starting_ip=100,
            population=50
        )
        CountryStatic.objects.create(
            starting_ip=100,
            population=50
        )
        self.assertEqual(.5, country.voting_weight)


class ParliamentVotePercentagesTestCase(TestCase):
    """
    Tests related to function `FinalTeamVote.parliament_vote_percentages`
    """

    def test_if_no_votes_return_pair_of_zeros(self):
        """
        If no votes have been cast, the function should (0, 0)
        """
        res = FinalTeamVote.parliament_vote_percentages()
        self.assertEqual(res, (0,0))

    def test_if_no_yes_votes_return_pair_of_0_and_100(self):
        """
        If no yes votes, should return (0, 100)
        """
        # create a random group
        team = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=10,
                starting_ip=100
            )
        )

        # vote no
        FinalTeamVote.objects.create(by=team, option='no')

        res = FinalTeamVote.parliament_vote_percentages()
        self.assertEqual(res, (0, 100))

    def test_if_all_yes_votes_return_pair_of_100_and_0(self):
        """
        If all have votes yes, return (100, 0)
        """
        # create a random group
        team = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=10,
                starting_ip=100
            )
        )

        # vote yes
        FinalTeamVote.objects.create(by=team, option='yes')

        res = FinalTeamVote.parliament_vote_percentages()
        self.assertEqual(res, (100, 0))

    def test_if_half_vote_yes_return_pair_of_50_and_50(self):
        """
        If half of the total member count have voted yes, return (50, 50)
        """
        # create a couple of groups
        mc25 = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=25,
                starting_ip=100
            )
        )
        mc50 = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=50,
                starting_ip=100
            )
        )
        mc75 = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=75,
                starting_ip=100
            )
        )

        # vote yes, yes and no
        FinalTeamVote.objects.create(by=mc25, option='yes')
        FinalTeamVote.objects.create(by=mc50, option='yes')
        FinalTeamVote.objects.create(by=mc75, option='no')

        res = FinalTeamVote.parliament_vote_percentages()
        self.assertEqual(res, (50, 50))


    def test_groups_that_has_not_voted_is_not_included_in_the_count(self):
        """
        If GroupA and GroupB has voted, but GroupC hasn't, then only the
        member counts of GroupA and GroupB should affect the results
        """
        # create a couple of groups
        groupA = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=25,
                starting_ip=100
            )
        )
        groupB = ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=50,
                starting_ip=100
            )
        )
        ParliamentGroup.objects.create(
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                member_count=100,
                starting_ip=100
            )
        )

        # 33% vote not, 66% vote yes
        FinalTeamVote.objects.create(by=groupA, option='no')
        FinalTeamVote.objects.create(by=groupB, option='yes')

        yes, no = FinalTeamVote.parliament_vote_percentages()
        self.assertAlmostEqual(yes, 2 / 3 * 100)
        self.assertAlmostEqual(no, 1 / 3 * 100)


class ParliamentResultTestCase(TestCase):
    """
    Tests related to class method `FinalTeamVote.parliament_result`
    """

    def test_vote_is_passed_by_simple_majority(self):
        """
        Test that if more than 50% votes in favour, then the vote is passed
        """
        target = 'game.models.FinalTeamVote.parliament_vote_percentages'
        with patch(target) as mock_vote:
            mock_vote.return_value = (50.01, 49.99)
            res = FinalTeamVote.parliament_result()

        self.assertEqual(res, VoteResult.PASSED)

    def test_vote_is_passed_if_evenly_split(self):
        """
        If exactly 50% votes in favour (and the other 50% vote agains), then
        the vote is passed
        """
        target = 'game.models.FinalTeamVote.parliament_vote_percentages'
        with patch(target) as mock_vote:
            mock_vote.return_value = (50., 50.)
            res = FinalTeamVote.parliament_result()

        self.assertEqual(res, VoteResult.PASSED)

    def test_vote_is_rejected_if_less_than_50_is_in_favour(self):
        """
        If less 50% votes in favour the vote is rejected
        """
        target = 'game.models.FinalTeamVote.parliament_vote_percentages'
        with patch(target) as mock_vote:
            mock_vote.return_value = (49.99, 50.01)
            res = FinalTeamVote.parliament_result()

        self.assertEqual(res, VoteResult.REJECTED)

    def test_vote_is_undecided_if_vote_percentages_are_zero(self):
        """
        If `FinalTeamVote.parliament_vote_percentages` returns (0,0) then the
        vote is undecided
        """
        target = 'game.models.FinalTeamVote.parliament_vote_percentages'
        with patch(target) as mock_vote:
            mock_vote.return_value = (0, 0)
            res = FinalTeamVote.parliament_result()

        self.assertEqual(res, VoteResult.UNDECIDED)


class CouncilVotePercentagesTestCase(TestCase):
    """
    Tests related to class method `FinalTeamVote.council_vote_percentages`
    """

    def test_if_no_votes_return_0_0_0(self):
        """
        If no votes, return (0, 0, 0)
        """
        res = FinalTeamVote.council_vote_percentages()
        self.assertEqual(res, (0,0,0))

    def test_if_all_yes_return_100_0_0(self):
        """
        If all yes, return (100, 0, 0)
        """
        team = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                population=7500,
                starting_ip=100
            )
        )

        FinalTeamVote.objects.create(by=team, option='yes')

        res = FinalTeamVote.council_vote_percentages()
        self.assertEqual(res, (100, 0, 0))

    def test_if_all_no_return_0_100_0(self):
        """
        If all no, return (0, 100, 0)
        """
        team = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                population=7500,
                starting_ip=100
            )
        )

        FinalTeamVote.objects.create(by=team, option='no')

        res = FinalTeamVote.council_vote_percentages()
        self.assertEqual(res, (0, 100, 0))

    def test_if_all_abstain_return_0_0_100(self):
        """
        If all abstains, return (0, 0, 100)
        """
        team = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                population=7500,
                starting_ip=100
            )
        )

        FinalTeamVote.objects.create(by=team, option='abstain')

        res = FinalTeamVote.council_vote_percentages()
        self.assertEqual(res, (0, 0, 100))


class CouncilResulTestCase(TestCase):
    """
    Tests related to class method `FinalTeamVote.council_result`
    """
    pass


class DirectivePassedTestCase(TestCase):
    """
    Tests related to class method `FinalTeamVote.directive_passed`
    """

    @patch('game.models.FinalTeamVote.council_result')
    @patch('game.models.FinalTeamVote.parliament_result')
    def test_return_true_if_both_chambers_passed(
        self, mock_parliament, mock_council
    ):
        """
        If the directive is passed in both the parliament and the council,
        return `True`
        """
        mock_parliament.return_value = VoteResult.PASSED
        mock_council.return_value = VoteResult.PASSED
        res = FinalTeamVote.directive_passed()
        self.assertTrue(res)

    @patch('game.models.FinalTeamVote.council_result')
    @patch('game.models.FinalTeamVote.parliament_result')
    def test_return_false_if_parliament_rejected_it(
        self, mock_parliament, mock_council
    ):
        """
        If the directive is rejected in the parliament then return False even
        if the council passed it
        """
        mock_parliament.return_value = VoteResult.REJECTED
        mock_council.return_value = VoteResult.PASSED
        res = FinalTeamVote.directive_passed()
        self.assertFalse(res)

    @patch('game.models.FinalTeamVote.council_result')
    @patch('game.models.FinalTeamVote.parliament_result')
    def test_return_false_if_council_rejected_it(
        self, mock_parliament, mock_council
    ):
        """
        If the directive is rejected in the council then return `False` even
        if the parliament passed it
        """
        mock_parliament.return_value = VoteResult.PASSED
        mock_council.return_value = VoteResult.REJECTED
        res = FinalTeamVote.directive_passed()
        self.assertFalse(res)

    @patch('game.models.FinalTeamVote.council_result')
    @patch('game.models.FinalTeamVote.parliament_result')
    def test_return_false_if_both_chambers_rejected_it(
        self, mock_parliament, mock_council
    ):
        """
        If the directive is rejected in both the council and the parliament,
        then return `False`
        """
        mock_parliament.return_value = VoteResult.REJECTED
        mock_council.return_value = VoteResult.REJECTED
        res = FinalTeamVote.directive_passed()
        self.assertFalse(res)


class AdvisorMemoTestCase(TestCase):

    def test_submitted_by_returns_advisor_subtype(self):
        team = Commission.objects.create(
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        tech_memo = AdvisorMemo.objects.create(
            writer=TechnicalAdvisor.objects.create(
                username='Technical',
                team=team
            )
        )

        poli_memo = AdvisorMemo.objects.create(
            writer=PoliticalAdvisor.objects.create(
                username='Political',
                team=team
            )
        )
        atta_memo = AdvisorMemo.objects.create(
            writer=AttacheAdvisor.objects.create(
                username='Attache',
                team=team
            )
        )
        medi_memo = AdvisorMemo.objects.create(
            writer=MediaAdvisor.objects.create(
                username='Media',
                team=team
            )
        )

        tech_memo.refresh_from_db()
        poli_memo.refresh_from_db()
        atta_memo.refresh_from_db()
        medi_memo.refresh_from_db()

        self.assertIsInstance(tech_memo.submitted_by(), TechnicalAdvisor)
        self.assertIsInstance(poli_memo.submitted_by(), PoliticalAdvisor)
        self.assertIsInstance(atta_memo.submitted_by(), AttacheAdvisor)
        self.assertIsInstance(medi_memo.submitted_by(), MediaAdvisor)


class MyStaffTimeTestCase(TestCase):
    """
    Tests for the class `game.MyStaffTime`
    """

    def setUp(self):
        """
        Create a `MyStaffTime` object to test with
        """
        mst = MyStaffTime.objects.create(
            role_content=MyStaffRoleContent.objects.create(
                name='test',
                headline='test',
                subline='test'
            ),
            description='test',
        )

    def test_time_text_returns_offset_string_if_no_event_display_time(self):
        """
        If a `MyStaff` object is associated with an event with no display time
        (eg. if no game is in DB yet), then it should just return a string that
        gives the offset
        """
        offset = 42
        exp = f'Offset {offset}'

        mst = MyStaffTime.objects.first()
        attr = { 'display_time': None, 'offset': offset, 'duration': 0 }
        with patch('game.models.MyStaffTime.event') as mock_event:
            mock_event.configure_mock(**attr)
            self.assertEqual(mst.time_text, exp)

    def test_time_text_returns_formatted_datetime_if_event_has_display_time(
        self
    ):
        """
        If the event associated with the object has a display time (but a
        duration of 0!), then `time_text` should return a string formatted
        datetime
        """

        # only hour and minutes count
        display_time = timezone.datetime(1991, 12, 27, 9, 15)
        exp = '09:15'

        mst = MyStaffTime.objects.first()
        attr = { 'display_time': display_time, 'duration': 0 }
        with patch('game.models.MyStaffTime.event') as mock_event:
            mock_event.configure_mock(**attr)
            self.assertEqual(mst.time_text, exp)

    def test_time_text_returns_duration_if_event_has_display_time_and_duration(
        self
    ):
        """
        If the associated event has a display time and a duration, then
        `time_text` should return a string formatting the start and end time
        """

        # only hour and minutes count
        display_time = timezone.datetime(1991, 12, 27, 9, 15)
        exp = '09:15-09:45'

        mst = MyStaffTime.objects.first()
        attr = { 'display_time': display_time, 'duration': 30 }
        with patch('game.models.MyStaffTime.event') as mock_event:
            mock_event.configure_mock(**attr)
            self.assertEqual(mst.time_text, exp)


class AllocateRoomsTestCase(TestCase):
    """
    Tests for class method `Room.allocate_rooms`
    """

    def setUp(self):
        """
        Create teams that need rooms!
        """
        for c in ['A-land', 'B-land', 'C-land', 'D-land', 'E-land', 'F-land']:
            Country.objects.create(
                name=c,
                influence_points=100,
                static=CountryStatic.objects.create(
                    starting_ip=100,
                    population=10
                )
            )

        for p in ['Red Group', 'Green Group', 'Blue Group', 'Orange Group']:
            ParliamentGroup.objects.create(
                name=p,
                influence_points=100,
                static=ParliamentGroupStatic.objects.create(
                    starting_ip=100,
                    member_count=25
                )
            )

        for l in ['InCrypted Lagune']:
            LobbyOrganization.objects.create(
                name=l,
                influence_points=100,
                static=LobbyOrganizationStatic.objects.create(
                    starting_ip=100
                )
            )

        for m in ['Business Now', 'Scoop Dog Magazine']:
            MediaHouse.objects.create(
                name=m,
                influence_points=100,
                static=MediaHouseStatic.objects.create(
                    starting_ip=100
                )
            )

        Commission.objects.create(
            name='The Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

    def test_mediahouses_and_commission_are_put_in_common_room(self):
        """
        The Commission and the MediaHouses should just end up in the common
        room
        """
        # lets create a common room
        myroom = Room.objects.create(kind=Room.COMMON_ROOM)

        # now - for sports - lets also create some class rooms
        for _ in range(3):
            Room.objects.create(kind=Room.CLASS_ROOM)

        # then we'll call the allocation function
        Room.allocate_rooms()

        # now we damn test this shit!
        self.assertEqual(
            myroom,
            Commission.objects.get().room
        )

        for team in MediaHouse.objects.all():
            self.assertEqual(myroom, team.room)

    def test_all_teams_get_a_room(self):
        """
        Test that you get a room, he gets a room, she gets a room, EVERYBODY
        GETS A ROOM!
        """
        # lets create a common room
        Room.objects.create(kind=Room.COMMON_ROOM)

        # now - for sports - lets also create some class rooms
        for _ in range(3):
            Room.objects.create(kind=Room.CLASS_ROOM)

        # then we'll call the allocation function
        Room.allocate_rooms()

        for team in Team.objects.all():
            self.assertIsNotNone(team.room)

    def test_teams_are_distributed_almost_evenly(self):
        """
        The room allocation algorithm should allow for a maximum difference of
        1 in the number of teams allocated to (class) rooms
        """
        num_teams = Team.objects.count() - MediaHouse.objects.count() - 1
        Room.objects.create(kind=Room.COMMON_ROOM)

        # this test assumes the number of non-common-room teams are 11
        self.assertEqual(num_teams, 11)

        # lets have 4 class rooms
        for _ in range(4):
            Room.objects.create(kind=Room.CLASS_ROOM)

        # allocate shit
        Room.allocate_rooms()

        max_teams_count = 0
        min_teams_count = 100
        for room in Room.objects.filter(kind=Room.CLASS_ROOM):
            teams_count = room.teams.count()
            if teams_count > max_teams_count:
                max_teams_count = teams_count

            if teams_count < min_teams_count:
                min_teams_count = teams_count

        self.assertEqual(max_teams_count - min_teams_count, 1)

    def test_team_types_are_distributed(self):
        """
        For any team type (eg. Country), there should be as few of the same
        type as possible in any room
        """
        # we'll create 4 class rooms this time
        num_rooms = 4

        # get expected max of team type
        exp_country = ceil(Country.objects.count() / num_rooms)
        exp_group = ceil(ParliamentGroup.objects.count() / num_rooms)
        exp_lobby = ceil(LobbyOrganization.objects.count() / num_rooms)

        # room creation
        Room.objects.create(kind=Room.COMMON_ROOM)
        for _ in range(num_rooms):
            Room.objects.create(kind=Room.CLASS_ROOM)

        # allocate it
        Room.allocate_rooms()

        # count and spend way to much code and time on it but who cares
        # we are just writing stupid tests
        max_country, max_group, max_lobby = 0, 0, 0
        for room in Room.objects.filter(kind=Room.CLASS_ROOM):
            num_country, num_group, num_lobby = 0, 0, 0
            for team in room.teams.all():
                team = team.get_subclass()
                if isinstance(team, Country):
                    num_country += 1
                elif isinstance(team, ParliamentGroup):
                    num_group += 1
                elif isinstance(team, LobbyOrganization):
                    num_lobby += 1
                else:
                    break

            if num_country > max_country:
                max_country = num_country
            if num_group > max_group:
                max_group = num_group
            if num_lobby > max_lobby:
                max_lobby = num_lobby

        self.assertEqual(exp_country, max_country)
        self.assertEqual(exp_group, max_group)
        self.assertEqual(exp_lobby, max_lobby)


class RoomStringMethodTestCase(TestCase):
    """
    Test that `Room.__str__` works so we don't get admin issues
    """

    def test_everything_at_once(self):
        """
        A lazy test that just test a couple of things and calls it a day
        """
        room1 = Room.objects.create(kind=Room.COMMON_ROOM)
        pk = room1.pk
        self.assertTrue(room1.__str__(), f'Common room {pk}')

        room2 = Room.objects.create(kind=Room.CLASS_ROOM)
        pk = room2.pk
        self.assertTrue(room2.__str__(), f'Class room {pk}')
