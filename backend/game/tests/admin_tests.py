from django.test import TestCase
from django.urls import reverse

from core.models import Commission, CommissionStatic, User
from game.models import Country, CountryStatic, Room


class RoomAdminTestCase(TestCase):
    """
    Test of the admin views governed by `RoomAdmin`
    """

    def setUp(self):
        """
        Create a user with admin rights
        """
        self.user = User.objects.create_superuser(
            email='what@ever.com',
            username='whatever',
            password='whatever'
        )


    def test_changelist_loads_normally_when_no_rooms_exist(self):
        """
        Test that the changelist view is loaded properly, even if no `Room` is
        displayed in the view
        """
        self.client.force_login(self.user)
        url = reverse('admin:game_room_changelist')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

    def test_changelist_loads_normally_when_rooms_have_no_name_nor_teams(self):
        """
        Test that the changelist view is loaded properly, if rooms are created
        but don't have names nor teams allocated
        """
        self.client.force_login(self.user)
        Room.objects.create(kind=Room.COMMON_ROOM)
        for _ in range(3):
            Room.objects.create(kind=Room.CLASS_ROOM)

        res = self.client.get(reverse('admin:game_room_changelist'))
        self.assertEqual(res.status_code, 200)

    def test_changelist_loads_normally_when_rooms_are_not_allocated(self):
        """
        Test that the changelist view is loaded properly, if rooms are created,
        named but not yet allocated to any teams
        """
        self.client.force_login(self.user)
        Room.objects.create(kind=Room.COMMON_ROOM, name='Aula')
        for name in ['R1','R2','R3']:
            Room.objects.create(kind=Room.CLASS_ROOM, name=name)

        res = self.client.get(reverse('admin:game_room_changelist'))
        self.assertEqual(res.status_code, 200)

    def test_changelist_loads_normally_when_rooms_are_complete(self):
        """
        Test that the changelist view is loaded properly, if rooms are created
        and named and allocated
        """
        Commission.objects.create(
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )
        for _ in range(6):
            Country.objects.create(
                influence_points=100,
                static=CountryStatic.objects.create(
                    starting_ip=100,
                    population=100
                )
            )

        self.client.force_login(self.user)
        Room.objects.create(kind=Room.COMMON_ROOM, name='Aula')
        for name in ['R1','R2','R3']:
            Room.objects.create(kind=Room.CLASS_ROOM, name=name)

        Room.allocate_rooms()

        res = self.client.get(reverse('admin:game_room_changelist'))
        self.assertEqual(res.status_code, 200)
