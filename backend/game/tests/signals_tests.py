"""
Test signal receivers for `game` app
"""
from unittest.mock import call, patch

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.utils import timezone

from events.models import BaseEvent, DeadlineEvent, MeetingEvent
from game.models import Game, Room
from meta.models import GameConfiguration


class SetDisplayTimeOnSaveTestCase(TestCase):
    """
    Test that the receiver function on `pre_save` works
    """

    @patch('game.signals.BaseEvent.set_display_time')
    @patch('game.signals.Game.get_current_game')
    def test_receiver_called_on_subtypes(
        self, mock_get_current_game, mock_set_display_time
    ):
        """
        Test that the receiver function is called when creating instances of
        `DeadlineEvent` and `MeetingEvent`
        """
        gamestart = timezone.datetime(1991, 12, 27, 9)
        mock_get_current_game.return_value.starting_time = gamestart

        # any contenttype will do here
        ct = ContentType.objects.get_for_model(BaseEvent)

        event1 = DeadlineEvent(offset=10, actor=ct, action=ct)
        event1.save()
        mock_set_display_time.assert_called_once_with(gamestart, save=False)
        mock_get_current_game.assert_called_once()

        event2 = MeetingEvent(offset=10)
        event2.save()
        self.assertEqual(mock_set_display_time.call_count, 2)
        self.assertEqual(mock_get_current_game.call_count, 2)

    @patch('game.signals.BaseEvent.set_display_time')
    @patch('game.signals.Game.get_current_game')
    def test_set_display_time_not_called_when_saving_existing_model(
        self, mock_get_current_game, mock_set_display_time
    ):
        """
        Test that the receiver function does not call the events own
        `set_display_time()` function when saving instances that
        already exists
        """
        event1 = MeetingEvent.objects.create(offset=10)

        # these will have been called once while creating the event
        # so they should be reset
        mock_get_current_game.reset_mock()
        mock_set_display_time.reset_mock()

        event1.name = 'Dont call me maybe'
        event1.save()

        # now check that they aren't called again
        mock_get_current_game.assert_not_called()
        mock_set_display_time.assert_not_called()

    @patch('game.signals.BaseEvent.set_display_time')
    @patch('game.signals.Game.get_current_game')
    def test_set_display_time_not_called_when_no_game_exists(
        self, mock_get_current_game, mock_set_display_time
    ):
        """
        Test that the receiver function does not call the events own
        `set_display_time()` function when `Game.get_current_game` returns
        `None`
        """
        mock_get_current_game.return_value = None

        event1 = MeetingEvent(offset=10)
        event1.name = 'Dont call me maybe'
        event1.save()

        mock_get_current_game.assert_called_once()
        mock_set_display_time.assert_not_called()


class RememberStartingTimeGameTestCase(TestCase):
    """
    Test pre_init receiver on `Game`
    """

    def test_old_starting_time_gets_set(self):
        """
        When initialising a `Game` object, it should have an attribute set
        to its current starting time
        """
        gamestart = timezone.make_aware(
            timezone.datetime(1991, 12, 27, 9, 0)
        )

        Game.objects.create(
            configuration=GameConfiguration.objects.create(),
            starting_time=gamestart,
            last_job_ordering=0
        )

        game = Game.get_current_game()
        self.assertTrue(hasattr(game, 'old_starting_time'))
        self.assertEqual(game.old_starting_time, gamestart)


class UpdateEventsOnGamePostSaveTestCase(TestCase):
    """
    Test post_save receiver on `Game`
    """

    @patch('game.signals.BaseEvent.set_display_time')
    def test_new_starting_time_triggers_call_to_set_display_time(
        self, mock_set_display_time
    ):
        """
        Test that when `Game.starting_time` is changed, then all events will
        have their `.set_display_time` called with the new gamestart
        """
        # just create three random events
        # NOTE: creating these before creating a Game ensures that they wont
        # have their display_time set by the post_save signal on events
        BaseEvent.objects.create(offset=10)
        BaseEvent.objects.create(offset=20)
        BaseEvent.objects.create(offset=30)

        gamestart = timezone.make_aware(
            timezone.datetime(1991, 12, 27, 9, 0)
        )
        Game.objects.create(
            configuration=GameConfiguration.objects.create(),
            starting_time=gamestart,
            last_job_ordering=0
        )

        game = Game.get_current_game()
        new_gamestart = gamestart + timezone.timedelta(minutes=30)
        game.starting_time = new_gamestart
        game.save()

        self.assertEqual(mock_set_display_time.call_count, 3)
        mock_set_display_time.assert_has_calls([
            call(new_gamestart),
            call(new_gamestart),
            call(new_gamestart)
        ])


    @patch('game.signals.BaseEvent.set_display_time')
    def test_set_display_time_not_called_when_starting_time_same(
        self, mock_set_display_time
    ):
        """
        Test that `BaseEvent.set_display_time` is not called if
        `Game.starting_time` is not changed in save
        """
        # just create three random events
        BaseEvent.objects.create(offset=10)
        BaseEvent.objects.create(offset=10)
        BaseEvent.objects.create(offset=10)

        gamestart = timezone.make_aware(
            timezone.datetime(1991, 12, 27, 9, 0)
        )
        Game.objects.create(
            configuration=GameConfiguration.objects.create(),
            starting_time=gamestart,
            last_job_ordering=0
        )

        game = Game.get_current_game()
        game.name = 'Spartacus'
        game.save()

        self.assertEqual(mock_set_display_time.call_count, 0)


class UpdateRoomNameOnPostSaveTestCase(TestCase):
    """
    Test of the signal `game.signals.update_room_name_on_post_save`
    """

    def test_room_name_on_meetingevent_changed(self):
        """
        Test that the room_name attribute of a meetingevent is updated when
        a new room is assigned (and save() is called on that room)
        """
        event = MeetingEvent.objects.create(
            name='test_name',
            offset=10
        )

        room1 = Room.objects.create(name='room1')
        room2 = Room.objects.create(name='room2')

        room1.meetings.add(event)
        event.room_name = room1.name
        event.save()

        # now lets test

        room2.meetings.add(event)
        room2.save()

        event.refresh_from_db()

        self.assertNotIn(event, room1.meetings.all())
        self.assertEqual(event.room_name, room2.name)

    def test_succeeds_if_event_has_no_prior_room(self):
        """
        Test that the signal works even if the meetingevent had no prior room
        assigned
        """
        event = MeetingEvent.objects.create(
            name='test_name',
            offset=10
        )
        room1 = Room.objects.create(name='room1')

        # now lets test

        room1.meetings.add(event)
        room1.save()

        event.refresh_from_db()
        self.assertEqual(event.room_name, room1.name)

    def test_succeeds_if_event_somehow_has_multiple_rooms(self):
        """
        This should not really be necessary, but for good measure, test that
        if multiple rooms points to the same meetingevent, they should all
        lose that relation on a save after a new room is chosen
        """
        event = MeetingEvent.objects.create(
            name='test_name',
            offset=10
        )

        room1 = Room.objects.create(name='room1')
        room2 = Room.objects.create(name='room2')
        room3 = Room.objects.create(name='room3')

        room1.meetings.add(event)
        room2.meetings.add(event)

        # sanity check
        self.assertIn(event, room1.meetings.all())
        self.assertIn(event, room2.meetings.all())

        # do actual testing
        room3.meetings.add(event)
        room3.save()

        event.refresh_from_db()

        self.assertNotIn(event, room1.meetings.all())
        self.assertNotIn(event, room2.meetings.all())

        self.assertEqual(event.room_name, room3.name)
