"""
Test cases for all classes in `game`
"""
from game.tests.complete_tests import EntireGameTestCase
from game.tests.general_tests import (AdvisorMemoTestCase,
                                      AllocateRoomsTestCase,
                                      AssignInitialStudentsJobsTestCase,
                                      CouncilVotePercentagesTestCase,
                                      CountryStaticTestCase,
                                      DirectivePassedTestCase,
                                      GetNextJobTemplateTestCase,
                                      MyStaffTimeTestCase,
                                      ParliamentResultTestCase,
                                      ParliamentVotePercentagesTestCase,
                                      RoomStringMethodTestCase, RoundTestCase)
from game.tests.schema_tests import (CalendarEntriesQueryTestCase,
                                     FinalVoteMutationTestCase,
                                     UpdateMemoMutationTestCase)
from game.tests.signals_tests import (RememberStartingTimeGameTestCase,
                                      SetDisplayTimeOnSaveTestCase,
                                      UpdateEventsOnGamePostSaveTestCase,
                                      UpdateRoomNameOnPostSaveTestCase)
from game.tests.tasks_tests import (AwardSpecialSuccessCriteriaTestCase,
                                    HandleDeadlinesTestCase,
                                    StartGameTaskTestCase)
