"""
Test entire game
"""
import logging
import random
from io import StringIO
from unittest import skip
from unittest.mock import patch

from django.contrib.contenttypes.models import ContentType
from django.core import management
from django.test import TestCase
from django.utils import timezone

from core.management.commands.runtasks import Command
from core.models import (ArticleSelection, AttacheAdvisor, Chief, Commission,
                         CommissionStatic, Directive, DirectiveArticle,
                         LobbyOrganization, LobbyOrganizationStatic,
                         MediaAdvisor, Minister, PoliticalAdvisor, Secretary,
                         SpokesPerson, Strategist, Team, TeamPriority,
                         TechnicalAdvisor)
from events.models import BaseEvent, DeadlineEvent, DeadlineTask
from game.models import (AdvisorMemo, Country, CountryStatic, FinalTeamVote,
                         Game, ParliamentGroup, ParliamentGroupStatic, Round)
from game.tasks import handle_deadlines
from inf_points.models import DirectiveInvestment, PointTransaction
from meta.models import GameConfiguration


@skip("something is wrong with loading contenttypes for fixture")
class EntireGameTestCase(TestCase):
    """
    A test class for testing a complete run of the game
    """
    fixtures = ['fixture_events.json']

    def setUp(self):
        """
        Create the game, the rounds, the teams, the players and load the
        fixture with the correct events
        """
        ContentType.objects.clear_cache()
        logging.disable(logging.CRITICAL)

        gamestart = timezone.datetime(1991, 12, 27, 9)
        gamestart = timezone.make_aware(gamestart)
        Game.objects.create(
            starting_time=gamestart,
            teams_created=True,
            game_started=True,
            last_job_ordering=0,
            configuration=GameConfiguration.objects.create()
        )

        for event in BaseEvent.objects.all():
            event.set_display_time(gamestart)

        # create rounds
        ip_deadlines = DeadlineEvent.objects.filter(
            name='IP deadline'
        ).order_by('offset')
        round_ends = DeadlineEvent.objects.filter(
            name__contains='The Commission sends'
        ).order_by('offset')

        Round.objects.create(
            active=True,
            order=1,
            round_end=round_ends.first(),
            ip_deadline=ip_deadlines.first(),
            directive=Directive.create_first_directive()
        )

        Round.objects.create(
            active=False,
            order=2,
            round_end=round_ends[1],
            ip_deadline=ip_deadlines[1],
        )

        Round.objects.create(
            active=False,
            order=3,
            round_end=round_ends[2],
            ip_deadline=ip_deadlines[2]
        )

        Round.objects.create(
            active=False,
            order=4
        )

        # create Teams and Students
        commission = Commission.objects.create(
            name='European Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        Strategist.objects.create(
            username=f'commissioner',
            password='whatever',
            team=commission
        )

        for num in [6, 8]:
            TeamPriority.objects.create(
                team=commission,
                article=DirectiveArticle.objects.get(number=num)
            )

        lobby = LobbyOrganization.objects.create(
            name='InCrypted Lagune',
            influence_points=100,
            static=LobbyOrganizationStatic.objects.create(
                starting_ip=100
            )
        )

        Strategist.objects.create(
            username='Chief Lobbyist',
            password='whatever',
            team=lobby
        )

        for num in [3, 4]:
            TeamPriority.objects.create(
                team=lobby,
                article=DirectiveArticle.objects.get(number=num)
            )

        for name, pop, arts in [('A-land', 50, (1,3)), ('B-land', 100, (2,5))]:
            country = Country.objects.create(
                name=name,
                influence_points=100,
                static=CountryStatic.objects.create(
                    population=pop,
                    starting_ip=100
                )
            )

            Minister.objects.create(
                username=f'minister_{country.name}',
                password='whatever',
                team=country
            )

            for num in arts:
                TeamPriority.objects.create(
                    team=country,
                    article=DirectiveArticle.objects.get(number=num)
                )

        for name, pop, arts in [('Red Group', 7, (5,7)), ('Green Group', 14, (2,8))]:
            group = ParliamentGroup.objects.create(
                name=name,
                influence_points=100,
                static=ParliamentGroupStatic.objects.create(
                    member_count=pop,
                    starting_ip=100
                )
            )

            SpokesPerson.objects.create(
                username=f'spokesperson_{group.name}',
                password='whatever',
                team=group,
            )

            for num in arts:
                TeamPriority.objects.create(
                    team=group,
                    article=DirectiveArticle.objects.get(number=num)
                )

        # common student jobs
        for team in Team.objects.all():
            Secretary.objects.create(
                username=f'secretary_{team.name}',
                password='whatever',
                team=team
            )

            Chief.objects.create(
                username=f'chief_{team.name}',
                password='whatever',
                team=team
            )

            TechnicalAdvisor.objects.create(
                username=f'tech1_{team.name}',
                password='whatever',
                team=team
            )
            TechnicalAdvisor.objects.create(
                username=f'tech2_{team.name}',
                password='whatever',
                team=team
            )

            PoliticalAdvisor.objects.create(
                username=f'pol_adv_{team.name}',
                password='whatever',
                team=team
            )

        # finally, create all tasks
        DeadlineEvent.create_all_tasks()

    def tearDown(self):
        """
        Re-enable logging just to be nice
        """
        logging.disable(logging.NOTSET)

    def test_all(self):
        """
        Keep track of how much time is gone using variable `minutes_passed` and
        attempt to test all deadlines and pointtransactions
        """

        game = Game.get_current_game()
        gamestart = game.starting_time

        runtasks = Command()
        out = StringIO()

        # test that all Teams still have 100 points before first deadline
        trans_before = PointTransaction.objects.count()
        d1 = DeadlineEvent.objects.order_by('offset').first()

        # first deadline should be the IP deadline
        self.assertEqual(d1.name, 'IP deadline')

        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = d1.offset - 1
            management.call_command('runtasks', stdout=out)

        for team in Team.objects.all():
            self.assertEqual(team.influence_points, 100)

        self.assertEqual(PointTransaction.objects.count(), trans_before)

        # lets have some time pass
        minutes_passed = d1.offset

        # lets have all team, except the lobbyorg, invest IP
        directive = Round.get_current_round().directive
        lobby = LobbyOrganization.objects.first()
        for priority in TeamPriority.objects.exclude(team=lobby):
            team = priority.team
            investment = DirectiveInvestment(
                by=team,
                priority=priority,
                amount=min(60, team.influence_points)
            )
            investment.directive = directive
            investment.save()

            with patch('events.models.models.timezone.now') as mock_now:
                curr_time = gamestart + timezone.timedelta(minutes=minutes_passed)
                mock_now.return_value = curr_time
                investment.submit()

        # check that no future tasks have been completed
        for task in DeadlineTask.objects.filter(deadline__offset__gt=minutes_passed):
            self.assertFalse(task.completed())

        # now, lets handle the current deadline
        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = d1.offset
            management.call_command('runtasks', stdout=out)

        # ensure the deadline is marked as processed
        d1.refresh_from_db()
        self.assertTrue(d1.processed)

        # lobby should have lost 2 points for not investing IP
        lobby.refresh_from_db()
        self.assertEqual(lobby.influence_points, 98)

        # all other teams should have 2 points, which were rewarded to them
        # after investing the other 100 they had
        for team in Team.objects.exclude(pk=lobby.pk):
            self.assertEqual(team.influence_points, 2)

        # there should be a PointTransaction for each TeamPriority among the
        # non-lobby Teams aswell as for each task associated with d1
        exp = TeamPriority.objects.exclude(
            team=lobby
        ).count() + d1.tasks.count()
        self.assertEqual(PointTransaction.objects.count(), exp)

        ### Next step

        # now we expect the next 4 deadlines to relate to advisors
        d2s = DeadlineEvent.objects.filter(
            processed=False
        ).order_by('offset')[:4]

        get_for_model = ContentType.objects.get_for_model
        adv_ctypes = list(map(
            get_for_model,
            [TechnicalAdvisor, PoliticalAdvisor, MediaAdvisor, AttacheAdvisor]
        ))
        for d in d2s:
            self.assertIn(d.actor, adv_ctypes)

        # since this setup only considers TechnicalAdvisors and
        # PoliticalAdvisors, lets get deadlines for them
        d2a = [d for d in d2s if d.actor == get_for_model(PoliticalAdvisor)][0]
        d2b = [d for d in d2s if d.actor == get_for_model(TechnicalAdvisor)][0]

        # lets ensure we know which is which
        self.assertTrue(d2a.offset < d2b.offset)
        self.assertEqual(
            d2a.actor, get_for_model(PoliticalAdvisor)
        )
        self.assertEqual(
            d2b.actor, get_for_model(TechnicalAdvisor)
        )

        # we let time pass
        minutes_passed = d2a.offset - 1

        # this time, lets have B-land be our villain
        b_country = Country.objects.get(name='B-land')

        # before we do anything else, lets register the state of the game
        points_before = {
            team.pk: team.influence_points for team in Team.objects.all()
        }
        trans_before = PointTransaction.objects.count()

        # now, advisors from all other team submit
        for adv in PoliticalAdvisor.objects.exclude(team=b_country):
            memo = AdvisorMemo.objects.create(
                subject='Something clever',
                content='Do this or that',
                writer=adv
            )
            with patch('events.models.models.timezone.now') as mock_now:
                curr_time = gamestart + timezone.timedelta(minutes=minutes_passed)
                mock_now.return_value = curr_time
                memo.submit()

            # now the first task for this advisor should be completed (on time)
            task = adv.tasks.first()
            self.assertTrue(task.completed())
            self.assertTrue(task.completed_on_time())

        # lets call `handle_deadlines()` before incrementing the time (nothing
        # should happen)
        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = minutes_passed
            management.call_command('runtasks', stdout=out)

        for team in Team.objects.all():
            self.assertEqual(points_before[team.pk], team.influence_points)

        self.assertEqual(trans_before, PointTransaction.objects.count())

        for d in d2s:
            self.assertFalse(d.processed)

        # now, lets increment the time and call `handle_deadlines()` again
        minutes_passed = d2a.offset

        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = minutes_passed
            management.call_command('runtasks', stdout=out)

        # all teams, except B-land, should have received 2 points
        for team in Team.objects.exclude(pk=b_country.pk):
            self.assertEqual(team.influence_points, points_before[team.pk] + 2)

        # b_country should have lost 2 points
        b_country.refresh_from_db()
        self.assertEqual(
            b_country.influence_points,
            points_before[b_country.pk] - 2
        )

        # and there should be a new PointTransaction for each team
        self.assertEqual(
            PointTransaction.objects.count(),
            trans_before + Team.objects.count()
        )

        # next up, the TechnicalAdvisors must submit memos. We start by
        # incrementing and saving the state
        minutes_passed = d2b.offset - 1
        points_before = {
            team.pk: team.influence_points for team in Team.objects.all()
        }
        trans_before = PointTransaction.objects.count()

        # There are two on each team (in this setup), so lets have one from
        # b_country miss the deadline
        lazy_advisor = TechnicalAdvisor.objects.filter(team=b_country).first()
        for adv in TechnicalAdvisor.objects.exclude(pk=lazy_advisor.pk):
            memo = AdvisorMemo.objects.create(
                subject='Something clever',
                content='Do this or that',
                writer=adv
            )
            with patch('events.models.models.timezone.now') as mock_now:
                curr_time = gamestart + timezone.timedelta(minutes=minutes_passed)
                mock_now.return_value = curr_time
                memo.submit()

            # now the first task for this advisor should be completed (on time)
            task = adv.tasks.first()
            self.assertTrue(task.completed())
            self.assertTrue(task.completed_on_time())

        # increment time and call `handle_deadlines()`
        minutes_passed = d2b.offset

        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = minutes_passed
            management.call_command('runtasks', stdout=out)

        # two transactions for each team should have been made
        self.assertEqual(
            PointTransaction.objects.count(),
            trans_before + (Team.objects.count() * 2)
        )

        # all other teams than B-land should have gotten 2 * 1 points
        for team in Team.objects.exclude(pk=b_country.pk):
            self.assertEqual(team.influence_points, points_before[team.pk] + 2)

        # B-land should have the same amount of points (+1 and -1)
        b_country.refresh_from_db()
        self.assertEqual(
            b_country.influence_points,
            points_before[b_country.pk]
        )

        # lets also make sure, that our 4 deadlines are now marked as processed
        # (even them with no advisors in the game)
        for d in d2s:
            d.refresh_from_db()
            self.assertTrue(d.processed)


        ### Next step

        # now we expect the Round to end with the Commission publishing a new
        # directive
        d3 = DeadlineEvent.objects.filter(
            processed=False
        ).order_by('offset').first()

        self.assertEqual(
            d3.name,
            'The Commission sends out the first Directive draft'
        )

        curr_round = Round.get_current_round()
        self.assertEqual(curr_round.order, 1)
        self.assertTrue(curr_round.active)

        # increment time and save state
        minutes_passed = d3.offset - 1
        points_before = {
            team.pk: team.influence_points for team in Team.objects.all()
        }
        trans_before = PointTransaction.objects.count()

        # we can create and submit the Directive before making article
        # selections (in game, selections would be done before, but it does
        # not matter here)
        with patch('events.models.models.timezone.now') as mock_now:
            curr_time = gamestart + timezone.timedelta(minutes=minutes_passed)
            mock_now.return_value = curr_time
            new_directive = Directive.objects.create()
            new_directive.submit()

        # we'll just generate a random article selection (8 integers between 1
        # and 5)
        selections = [random.randint(1,6) for _ in range(8)]
        for i, selection in enumerate(selections):
            article = i + 1
            ArticleSelection.objects.create(
                article=DirectiveArticle.objects.get(number=article),
                items_selected=selection,
                directive=new_directive
            )

        # we also need to manually add the directive to the second round
        # (in game, this is done when submitting via `core/schema.py`)
        next_round = Round.objects.get(order=2)
        next_round.directive = new_directive
        next_round.save()

        # now, lets calculate the expected points each team should receive
        expected_points = { team.pk: 0 for team in Team.objects.all() }
        investments = DirectiveInvestment.objects.filter(
            _directive=curr_round.directive
        )

        for investment in investments:
            i = investment.priority.article.number - 1
            # return between 0% and 80% of the investment
            reward = investment.amount * (1 + (0.2 * (selections[i] - 1)))
            expected_points[investment.by.pk] += int(reward)

        # Commission should also be rewarded for submitting on time
        commission = Commission.objects.first()
        expected_points[commission.pk] += 2

        # before we increment time, lets make sure that `handle_deadlines()`
        # wont give or subtract points and that round hasn't changed
        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = minutes_passed
            management.call_command('runtasks', stdout=out)

        curr_round = Round.get_current_round()
        self.assertEqual(curr_round.order, 1)
        self.assertTrue(curr_round.active)

        for team in Team.objects.all():
            self.assertEqual(points_before[team.pk], team.influence_points)

        # now, increment time and call `handle_deadlines()` again
        minutes_passed = d3.offset
        with patch('game.tasks.Game.minutes_passed') as mock_min_passed:
            mock_min_passed.return_value = minutes_passed
            management.call_command('runtasks', stdout=out)

        # the round should now have changed
        curr_round.refresh_from_db()
        self.assertFalse(curr_round.active)
        self.assertTrue(curr_round.round_end.processed)

        next_round = Round.get_current_round()
        self.assertTrue(next_round.active)
        self.assertEqual(next_round.order, 2)

        # check that teams have received the correct points
        for team in Team.objects.all():
            self.assertEqual(
                team.influence_points,
                points_before[team.pk] + expected_points[team.pk]
            )

        # lets move on
        curr_round = next_round

        ### NEXT STEP ###
