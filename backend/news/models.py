""" Models for News stories written by students """
from django.db import models
from core.models import Student, Team, TeamStaticBase


class MediaHouse(Team):
    """ A Media House team comprised of users playing as journalists. """
    static = models.OneToOneField("MediaHouseStatic", on_delete=models.PROTECT)

    def __str__(self):
        return f'Media House {self.name}'

    class Meta:
        verbose_name = "media house"
        verbose_name_plural = "media houses"


class MediaHouseStatic(TeamStaticBase):
    """ Static information about a Media House """
    team_class = MediaHouse

    editorial_line = models.CharField(max_length=500)
    countries = models.CharField(max_length=500)

    class Meta:
        verbose_name = "media house static"
        verbose_name_plural = "media house statics"


class Journalist(Student):
    """ A student whose job it is to write news """
    mediahouse = models.ForeignKey(MediaHouse, on_delete=models.PROTECT)
    is_editor = models.BooleanField(default=False)

    class Meta:
        verbose_name = "journalist"
        verbose_name_plural = "journalists"


class NewsItem(models.Model):
    """ Base class for news telegrams and articles """
    writer = models.ForeignKey(Journalist, on_delete=models.CASCADE)

    headline = models.CharField(max_length=50)

    # If null, item is not published.
    publishing_time = models.DateTimeField(
        blank=True, null=True, help_text="automatically set when published")

    # Should always be false if publishing_time is set.
    is_draft = models.BooleanField(default=True)

    is_breaking_news = models.BooleanField(
        default=False,
        help_text="The newest breaking news from a Media House is highlighted to readers")

    def __str__(self):
        if self.is_draft:
            return f'Article draft by {self.writer}'
        return f'{self.writer}: {self.headline}'

    class Meta:
        abstract = True


class NewsTelegram(NewsItem):
    """ A short-form news item """
    body_text = models.TextField(max_length=500)

    kind = "telegram"

    def to_news_item(self):
        """ Used for views generic over news items """
        self.telegram_id = self.id # pylint: disable=attribute-defined-outside-init
        self.__class__ = NewsItem
        return self


class NewsArticle(NewsItem):
    """ An article publised by a journalist from a media house """
    lead_text = models.TextField(max_length=300)

    body_text = models.TextField(max_length=2000)

    is_opinion_piece = models.BooleanField(
        default=False, help_text="Opinion pieces express the views of the journalist")

    kind = "article"

    class Meta:
        verbose_name = "news article"
        verbose_name_plural = "news articles"

    def to_news_item(self):
        """ Used for views generic over news items """
        self.article_id = self.id # pylint: disable=attribute-defined-outside-init
        self.__class__ = NewsItem
        return self


class NewsReaction(models.Model):
    """
    A reaction from a student on a piece of news.
    """
    is_positive = models.BooleanField(default=True)
    by = models.ForeignKey(Student, on_delete=models.CASCADE)

    # Only one of these may be set!
    telegram = models.ForeignKey(NewsTelegram, null=True, blank=True, on_delete=models.CASCADE)
    article = models.ForeignKey(NewsArticle, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return f'{"Positive" if self.is_positive else "Negative"} reaction from {self.by.full_name}'

    class Meta:
        verbose_name = "news reaction"
        verbose_name_plural = "news reactions"
        unique_together = ("by", "article", "telegram")
