""" News GraphQL schema """
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
from itertools import chain

import graphene
from django.utils import timezone
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import DjangoObjectType

from core.shared_schema import StudentType, TeamTypeMixin, gql_strict_list
from inf_points.models import PointTransaction
from news.forms import NewsArticleForm
from news.models import (Journalist, MediaHouse, MediaHouseStatic, NewsArticle,
                         NewsItem, NewsReaction, NewsTelegram)


class MediaHouseStaticType(DjangoObjectType):
    class Meta:
        model = MediaHouseStatic


class MediaHouseType(DjangoObjectType, TeamTypeMixin):
    starting_room = graphene.String(required=True)

    class Meta:
        model = MediaHouse

    def resolve_starting_room(self, _info):
        return self.starting_room


class JournalistType(DjangoObjectType):
    student = graphene.Field(StudentType, required=True)
    class Meta:
        model = Journalist
        only_fields = ["id", "team", "student", "mediahouse"]


class NewsArticleType(DjangoObjectType):
    class Meta:
        model = NewsArticle


class NewsArticleMutation(DjangoModelFormMutation):
    newsArticle = graphene.Field(NewsArticleType)
    class Meta:
        form_class = NewsArticleForm

    @classmethod
    def perform_mutate(cls, form, info):
        form.instance.writer_id = info.context.user.student.journalist.id
        if not form.instance.is_draft:
            # Temporary shortcut since there is no editor
            form.instance.publishing_time = timezone.now()
        return super().perform_mutate(form, info)


class NewsReactionMutation(graphene.Mutation):
    did_react = graphene.Field(graphene.Boolean, required=True)
    class Arguments:
        article_id = graphene.ID(required=True)
        is_positive = graphene.Boolean(required=True)


    def mutate(self, info, article_id, is_positive):
        article = NewsArticle.objects.get(id=article_id)

        try:
            existing_reaction = NewsReaction.objects.get(
                article=article,
                by=info.context.user.student,
            )
            return NewsReactionMutation(did_react=False)
        except NewsReaction.DoesNotExist:
            pass

        reaction = NewsReaction(
            article=article,
            by=info.context.user.student,
            is_positive=is_positive,
        )
        reaction.save()

        if is_positive:
            PointTransaction.send(
                description="Positiv reaktion på \"%s\"" % article.headline,
                amount=5,
                recipient=article.writer.team,
            )
        else:
            PointTransaction.send(
                description="Negativ reaktion på \"%s\"" % article.headline,
                amount=-1,
                recipient=article.writer.team,
            )
        return NewsReactionMutation(did_react=True)



class NewsTelegramType(DjangoObjectType):
    class Meta:
        model = NewsTelegram


class NewsItemKind(graphene.Enum):
    ARTICLE = 1
    TELEGRAM = 2


class NewsItemType(DjangoObjectType):
    class Meta:
        model = NewsItem

    lead_text = graphene.String()
    body_text = graphene.String(required=True)
    article_id = graphene.ID()
    telegram_id = graphene.ID()


class NewsReactionType(DjangoObjectType):
    news_item = graphene.Field(NewsItemType, required=True)
    class Meta:
        model = NewsReaction
        exclude_fields = ("article", "telegram")

    def resolve_news_item(self, _info):
        # pylint: disable=no-member
        if self.article is not None:
            return self.article.to_news_item()
        return self.telegram.to_news_item()



class Query:
    article = graphene.Field(NewsArticleType, id=graphene.Int(), required=True)
    my_articles = gql_strict_list(NewsArticleType)
    my_news_items = gql_strict_list(NewsItemType)
    news_items = gql_strict_list(NewsItemType)
    news_item = graphene.Field(NewsItemType, item_id=graphene.ID(),
                               kind=NewsItemKind(), required=True)
    mediahouses = gql_strict_list(MediaHouseType)
    my_news_reaction = graphene.Field(NewsReactionType, article_id=graphene.ID())

    def resolve_article(self, _info, **kwargs):
        article_id = kwargs.get('id')
        return NewsArticle.objects.get(pk=article_id)

    def resolve_my_articles(self, info):
        student = info.context.user.student
        if hasattr(student, "journalist"):
            return NewsArticle.objects.filter(writer_id=student.journalist.id)
        return []

    def resolve_my_news_items(self, info):
        """ Retrieves a journalist's articles and telegrams. Downcasts them to
        NewsItems to be able to return together. Sets article_id or telegram_id
        depending on original type. """
        student = info.context.user.student
        if hasattr(student, "journalist"):
            articles = NewsArticle.objects.filter(writer_id=student.journalist.id)
            telegrams = NewsTelegram.objects.filter(writer_id=student.journalist.id)
            return [item.to_news_item() for item in chain(articles, telegrams)]
        return []

    def resolve_published_articles(self, _info):
        return NewsArticle.objects.filter(publishing_time__isnull=False)

    def resolve_news_items(self, _info):
        articles = NewsArticle.objects.filter(
            publishing_time__isnull=False
        ).order_by('-publishing_time')
        telegrams = NewsTelegram.objects.filter(publishing_time__isnull=False)
        return [item.to_news_item() for item in chain(articles, telegrams)]

    def resolve_news_item(self, _info, item_id, kind):
        item = None
        if kind == NewsItemKind.ARTICLE:
            item = NewsArticle.objects.get(id=item_id)
        elif kind == NewsItemKind.TELEGRAM:
            item = NewsTelegram.objects.get(id=item_id)
        else:
            raise NotImplementedError("Unrecognized News item type")
        return item.to_news_item()

    def resolve_mediahouses(self, _info):
        mediahouses = MediaHouse.objects.all()
        return mediahouses if mediahouses else []

    def resolve_my_news_reaction(self, info, article_id):
        student = info.context.user.student
        reaction = NewsReaction.objects.filter(
            by=student,
            article_id=article_id,
        )
        try:
            return reaction[0]
        except IndexError:
            return None


class Mutation:
    update_article = NewsArticleMutation.Field()
    react_to_news = NewsReactionMutation.Field()
