""" Api views for news stories """
from django.db.models import TextField
from rest_framework import permissions, response, serializers, views, viewsets

from news import models


class MediaHouseSerializer(serializers.ModelSerializer):
    """ Serializes media houses """
    class Meta:
        model = models.MediaHouse
        exclude = []


# class NewsItemProxy(models.NewsItem):
#     """ This proxy sets abstract=False to allow DRF serializers to work with NewsItems.
#     Should only be used to serialize the subclasses, not for database management. """
#     body_text = TextField()
#     class Meta(models.NewsItem.Meta):
#         abstract = False
#         managed = False


# class NewsItemSerializer(serializers.ModelSerializer):
#     """ Serializes both articles and telegrams. Slight hack because normally
#     a ModelSerializer cannot accept an abstract model. """
#     mediahouse_logo = serializers.CharField(source="writer.team.mediahouse.logo.url", read_only=True)
#     kind = serializers.CharField(read_only=True)

#     class Meta:
#         model = NewsItemProxy
#         fields = ["id", "headline", "body_text", "mediahouse_logo", "kind", "is_breaking_news"]


class NewsArticleSerializer(serializers.ModelSerializer):
    """ Article serializer """
    logo = serializers.CharField(source="writer.team.mediahouse.logo.url", read_only=True)

    class Meta:
        model = models.NewsArticle
        exclude = ["writer"]


class NewsTelegramSerializer(serializers.ModelSerializer):
    """ Telegram serializer """
    logo = serializers.CharField(source="writer.team.mediahouse.logo.url", read_only=True)

    class Meta:
        model = models.NewsTelegram
        exclude = ["writer"]


# class NewsfeedView(views.APIView):
#     """ Returns all kinds of NewsItems sorted by publishing time """
#     permission_classes = (permissions.IsAuthenticated,)
#     reason_name = "newsfeed"

#     # Action, queryset and get_serializer specified for use by Reason Generator.
#     action = "list"
#     queryset = NewsItemProxy.objects.all()

#     def get_serializer(self): # pylint: disable=no-self-use
#         """ Returns serializer for introspection by Reason Generator """
#         return NewsItemSerializer()

#     def get(self, request):
#         """ The view function called by DRF """
#         news_items = []
#         for model in [models.NewsArticle, models.NewsTelegram]:
#             queryset = model.objects.filter(publishing_time__isnull=False)\
#                 .order_by("-publishing_time")
#             items = NewsItemSerializer(queryset, many=True).data
#             for item in items:
#                 item["body_text"] = item["body_text"][:100]
#             news_items.extend(items)

#         return response.Response(news_items)


class NewsArticleViewSet(viewsets.ReadOnlyModelViewSet):
    """ Views for long news items """
    queryset = models.NewsArticle.objects.all()
    serializer_class = NewsArticleSerializer


class NewsTelegramViewSet(viewsets.ReadOnlyModelViewSet):
    """ Views for short news items """
    queryset = models.NewsTelegram.objects.all()
    serializer_class = NewsTelegramSerializer


class MediaHouseViewSet(viewsets.ModelViewSet):
    """ Media House Views """
    queryset = models.MediaHouse.objects.all()
    serializer_class = MediaHouseSerializer
