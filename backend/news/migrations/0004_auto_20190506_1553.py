# Generated by Django 2.2 on 2019-05-06 13:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20190506_1343'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newsarticle',
            name='publisher',
        ),
        migrations.RemoveField(
            model_name='newstelegram',
            name='publisher',
        ),
        migrations.AddField(
            model_name='newsarticle',
            name='writer',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='news.Journalist'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='newstelegram',
            name='writer',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='news.Journalist'),
            preserve_default=False,
        ),
    ]
