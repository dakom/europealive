# Generated by Django 2.2 on 2019-04-23 15:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NewsItemProxy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=50)),
                ('publishing_time', models.DateTimeField(blank=True, help_text='automatically set when published', null=True)),
                ('is_breaking_news', models.BooleanField(default=False, help_text='The newest breaking news from a Media House is highlighted to readers')),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MediaHouse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('logo', models.FileField(help_text='.svg file if possible, else image.', upload_to='media_house')),
            ],
        ),
        migrations.CreateModel(
            name='NewsTelegram',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=50)),
                ('publishing_time', models.DateTimeField(blank=True, help_text='automatically set when published', null=True)),
                ('is_breaking_news', models.BooleanField(default=False, help_text='The newest breaking news from a Media House is highlighted to readers')),
                ('body_text', models.TextField(max_length=500)),
                ('publisher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='news.MediaHouse', verbose_name='publisher')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NewsArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=50)),
                ('publishing_time', models.DateTimeField(blank=True, help_text='automatically set when published', null=True)),
                ('is_breaking_news', models.BooleanField(default=False, help_text='The newest breaking news from a Media House is highlighted to readers')),
                ('lead_text', models.TextField(max_length=300)),
                ('body_text', models.TextField(max_length=2000)),
                ('is_opinion_piece', models.BooleanField(default=False, help_text='Opinion pieces express the views of the journalist')),
                ('publisher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='news.MediaHouse', verbose_name='publisher')),
            ],
            options={
                'verbose_name_plural': 'news articles',
                'verbose_name': 'news article',
            },
        ),
    ]
