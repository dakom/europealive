# Generated by Django 2.2 on 2019-05-06 11:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_auto_20190426_1646'),
        ('news', '0002_auto_20190506_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mediahouse',
            name='id',
        ),
        migrations.AddField(
            model_name='mediahouse',
            name='team_ptr',
            field=models.OneToOneField(auto_created=True, default=1, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='core.Team'),
            preserve_default=False,
        ),
    ]
