# Generated by Django 2.2 on 2019-05-06 11:35

import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_auto_20190426_1646'),
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Journalist',
            fields=[
                ('student_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='core.Student')),
            ],
            options={
                'verbose_name': 'journalist',
                'verbose_name_plural': 'journalists',
            },
            bases=('core.student',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterModelOptions(
            name='mediahouse',
            options={'verbose_name': 'media house', 'verbose_name_plural': 'media houses'},
        ),
        migrations.RemoveField(
            model_name='mediahouse',
            name='name',
        ),
    ]
