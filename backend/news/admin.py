""" Admin for news models """
from django.contrib import admin

from core.admin import StudentAdminMixin

from .models import (Journalist, MediaHouse, MediaHouseStatic, NewsArticle,
                     NewsReaction, NewsTelegram)

#pylint: disable=missing-docstring

admin.site.register(NewsReaction)

@admin.register(Journalist)
class JournalistAdmin(StudentAdminMixin, admin.ModelAdmin):
    fieldsets = StudentAdminMixin.fieldsets + (
        ('Journalist', {
            'fields': ('mediahouse', 'is_editor')
        }),
    )

    add_fieldsets = StudentAdminMixin.add_fieldsets + (
        ('Journalist', {
            'fields': ('mediahouse', 'is_editor')
        }),
    )


@admin.register(MediaHouseStatic)
class MediaHouseStaticAdmin(admin.ModelAdmin):
    exclude = ('starting_priorities',)

@admin.register(MediaHouse)
class MediaHouseAdmin(admin.ModelAdmin):
    pass


@admin.register(NewsArticle)
class NewsArticleAdmin(admin.ModelAdmin):
    pass


@admin.register(NewsTelegram)
class NewsTelegramAdmin(admin.ModelAdmin):
    pass
