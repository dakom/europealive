from django.test import TestCase

from news.models import Journalist, MediaHouse, MediaHouseStatic


class JournalistTestCase(TestCase):
    """
    TestCase for the Journalist class
    """
    fixtures = ['base_setup.json']

    def setUp(self):
        MediaHouse.objects.create(
            name='Test_mediahouse',
            special_success_criteria='',
            special_success_criteria_value=1,
            video_embed_url_raw='',
            influence_points=100,
            static=MediaHouseStatic.objects.first()
        )

    def test_create_journalist(self):
        journalist = Journalist.objects.create(
            email='test@test.dk',
            first_name='Test_firstname',
            last_name='Test_lastname',
            team=MediaHouse.objects.first(),
            job_description_raw='Write articles',
            mediahouse=MediaHouse.objects.first()
        )
        self.assertIsNotNone(journalist)
        self.assertIsNotNone(journalist.pk)

    def test_delete_journalist(self):
        journalist = Journalist.objects.create(
            email='test@test.dk',
            first_name='Test_firstname',
            last_name='Test_lastname',
            team=MediaHouse.objects.first(),
            job_description_raw='Write articles',
            mediahouse=MediaHouse.objects.first()
        )
        num_journalists = Journalist.objects.count()

        journalist.delete()
        self.assertEqual(num_journalists - 1, Journalist.objects.count())
