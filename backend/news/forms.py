""" News forms """
from django.forms import ModelForm

from news.models import NewsArticle



class NewsArticleForm(ModelForm):
    """ Form for Graphene mutations """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Disable some checks for draft news items
        self.fields["headline"].required = False
        self.fields["lead_text"].required = False
        self.fields["body_text"].required = False

    class Meta:
        model = NewsArticle
        fields = ["headline", "is_breaking_news", "body_text", "lead_text",
                  "is_opinion_piece", "is_draft"]
