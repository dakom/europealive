""" App """
# pylint: disable=import-outside-toplevel
import vinaigrette
from django.apps import AppConfig


class NewsConfig(AppConfig):
    """ News Config """
    name = 'news'

    def ready(self):
        from .models import MediaHouseStatic
        vinaigrette.register(
            MediaHouseStatic,
            [
                'name',
                'special_success_criteria',
                'video_embed_url',
                'editorial_line',
                'countries'
            ]
        )
