"""
Here we define Signals provided by this app
"""
from django.dispatch import Signal

event_happened = Signal(providing_args=['event'])
