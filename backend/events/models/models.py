"""
Event models
"""
from datetime import datetime, timedelta

from django.apps import apps
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone

from events.signal_defs import event_happened


def allowed_content_types(ms, d={}):
    d['id__in'] = [
        ct.id for ct in ContentType.objects.get_for_models(
            *[apps.get_model(m) for m in ms]
        ).values()
    ]
    return d


def allowed_meeting_attendees():
    """
    Return a list of model names if defined in settings.MEETING_ATTENDEES
    """
    try:
        return allowed_content_types(settings.MEETING_ATTENDEES)
    except AttributeError:
        return None


def allowed_deadline_actors():
    """
    Return a dictionary of allowed contenttypes for DeadlineEvent.actor
    """
    try:
        return allowed_content_types(settings.DEADLINE_ACTORS)
    except AttributeError:
        return None


def allowed_deadline_actions():
    """
    Return a dictionary of allowed actions if defined in settings
    """
    try:
        return allowed_content_types(settings.DEADLINE_ACTIONS)
    except AttributeError:
        return None


class BaseEvent(models.Model):
    """
    Base class for all events
    """

    name = models.CharField(
        help_text='Name of this event',
        max_length=500,
    )
    player_description = models.TextField(
        help_text='Description of event for the player',
        max_length=500,
        blank=True,
        null=True
    )
    facilitator_description = models.TextField(
        help_text='Verbose description of event for the facilitator',
        max_length=750,
        blank=True,
        null=True
    )
    offset = models.IntegerField(
        help_text='Minutes after game start at which the event should occur'
    )
    duration = models.IntegerField(
        default=0,
        help_text='Specify how long this event takes',
    )
    display_in_calendar = models.BooleanField(
        help_text='Show this event in the calendar sidebar',
        default=False,
        null=True
    )
    display_in_extended_calendar = models.BooleanField(
        help_text='Show this event on extended calendar page',
        default=True,
        null=True
    )
    _display_time = models.DateTimeField(
        help_text='Time at which the event actually occurs',
        editable=False,
        null=True
    )

    class Meta:
        ordering = ['offset', 'duration']

    def __str__(self):
        return f'{self.name} ({self.offset})'

    @property
    def minute_offset(self):
        """
        Gives the remaining offset in minutes when all whole hours have been
        removed (ie. a number between 0-59)
        """
        return self.offset % 60

    @property
    def hour_offset(self):
        """
        Gives the offset in hours, discounting reamining minutes
        """
        return int((self.offset - self.minute_offset) / 60)

    @property
    def display_time(self):
        """
        Returns the actual time this event will occur (relative to game start).
        """
        if self._display_time is None:
            return None
        return self._display_time.astimezone(timezone.get_current_timezone())

    def set_display_time(self, gamestart, save=True):
        """
        Calculates the display time baed on the gamestart and the offset.
        """
        if timezone.is_naive(gamestart):
            gamestart = timezone.make_aware(gamestart)

        self._display_time = gamestart + timedelta(minutes=self.offset)
        if save:
            self.save()


class MeetingEvent(BaseEvent):
    """
    Event handling meetings
    """

    room_name = models.CharField(
        help_text='The room in which the meeting takes place',
        max_length=50,
        null=True,
        blank=True
    )

    agenda = models.TextField(
        help_text='Agenda of the meeting',
        max_length=500,
        null=True,
        blank=True
    )

    participants = models.ManyToManyField(
        ContentType,
        related_name='meetings',
        help_text='Who should participate in the meeting?',
        limit_choices_to=allowed_meeting_attendees
    )


class DeadlineEvent(BaseEvent):
    """
    Event handling deadlines
    """

    actor = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        related_name='deadlines',
        help_text='Which actor (Team or Student) must meet the deadline?',
        limit_choices_to=allowed_deadline_actors
    )
    action = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        related_name='submission_deadlines',
        help_text='What action should be performed before the deadline?',
        limit_choices_to=allowed_deadline_actions
    )

    reward = models.IntegerField(
        default=0,
        help_text='Points awarded if deadline is met'
    )

    penalty = models.IntegerField(
        default=0,
        help_text='Points deducted if deadline is not met'
    )

    processed = models.BooleanField(
        default=False,
        help_text='Mark if this deadline has been handled yet'
    )

    def submit(self, actor, submittable):
        """
        Submit `submittable` to the _first_ uncompleted `DeadlineTask` related
        to this `DeadlineEvent` and the given `actor`.
        """
        submit_cls = self.action.model_class()
        if not isinstance(submittable, submit_cls):
            raise ValueError(f'{submittable} is not of type {submit_cls}')

        actor_cls = self.actor.model_class()
        if not isinstance(actor, actor_cls):
            raise ValueError(f'Actor {actor} is not of type {actor_cls}')

        task = actor.tasks.filter(
            deadline=self,
            _submittable__isnull=True
        ).first()

        # TODO: HANDLE THIS!
        if task is None:
            return False

        task.submit(submittable)
        task.save()

        return True

    def deadline_time(self):
        """
        Returns the time at which this deadline occurs
        """
        if self.display_time is None:
            raise ValueError(f'Deadline time for {self} has not been set')
        return self.display_time

    def create_task(self, actor):
        """
        Takes an 'actor' (which must be of the same type as the actor defined
        in self.actor) and creates a task for that actor
        """
        actor_cls = self.actor.model_class()
        if not isinstance(actor, actor_cls):
            raise ValueError(f'{actor} is not of type {actor_cls}')

        task = DeadlineTask.objects.create(
            actor_object=actor, deadline=self
        )
        return task

    @classmethod
    def create_all_tasks(cls):
        """
        Goes through all `DeadlineEvent`s and creates a `DeadlineTask` for each
        actor associated with the given deadline
        """
        for deadline in cls.objects.all():
            for actor in deadline.actor.get_all_objects_for_this_type():
                deadline.create_task(actor)

    def trigger(self):
        """
        Trigger event and send signal
        """
        if self.processed:
            return

        self.processed = True
        self.save()
        event_happened.send(sender=self.__class__, event=self)


class Submittable(models.Model):
    """
    Supertype which models needs to inherit from, if they are to be used with
    deadlines
    """
    is_submitted = models.BooleanField(default=False)

    def submitted_by(self):
        """
        Most be overridden by subclasses
        """
        raise ValueError('This function must be overridden by subclasses')

    def submit(self, deadline=None, submit_to_processed=False):
        """
        Submit this item to the next unprocessed `DeadlineEvent` (unless
        `deadline` is specified in which case, that will be used). Has no
        effect if it is already submitted or if no upcoming, unprocessed
        `DeadlineEvent`s exist. If `submit_to_processed` is `True`, then the
        function will also try and submit to processed deadlines.
        """
        if self.is_submitted:
            return False

        actor = self.submitted_by()
        if deadline is None:
            deadline = DeadlineEvent.objects.filter(
                actor=ContentType.objects.get_for_model(actor),
                action=ContentType.objects.get_for_model(self),
                processed=submit_to_processed
            ).order_by('offset').first()

        if deadline is not None:
            success = deadline.submit(actor, self)
            if success:
                self.is_submitted = True
                self.save(auto_submit=False)

            return success

        else:
            return False

    def save(self, auto_submit=False, *args, **kwargs):
        """
        If `auto_submit=True`, calling save will result in this submittable
        being submitted to its associated DeadlineEvent (if one exists)
        """
        # pylint: disable=keyword-arg-before-vararg
        super(Submittable, self).save(*args, **kwargs)

        if auto_submit:
            self.submit()


class DeadlineTask(models.Model):

    # generic relatin to actor (note that actors will have an actual
    # GenericRelation to a DeadlineTask if inheriting from DeadlineActorMixin
    actor_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    actor_id = models.PositiveIntegerField()
    actor_object = GenericForeignKey('actor_type', 'actor_id')

    # the deadline to which this task attains
    deadline = models.ForeignKey(
        DeadlineEvent,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='tasks',
        help_text='Set a deadline for when this task must be submitted',
    )

    completed_at = models.DateTimeField(
        null=True,
        blank=True,
        help_text='The time at which this task was completed'
    )

    # the content to be submitted
    _submittable = models.ForeignKey(
        Submittable,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text='The content that gets submitted'
    )

    # store the contenttype of the actual model of the submitted content
    _submittable_ctype = models.ForeignKey(
        ContentType,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='submittable_contenttype'
    )

    def get_submittable(self, subclass=True):
        """
        Returns the actual model instance of the submitted item (unless
        `subclass=False` in which case the parent `Submittable` is returned)
        """
        if self._submittable is None:
            return None

        if not subclass:
            return self._submittable

        return self._submittable_ctype.get_object_for_this_type(
            id=self._submittable.id
        )

    def get_actor(self):
        """
        Return the actor object instance associated with this task
        """
        return self.actor_object

    def submit(self, submittable):
        """
        Set `Submittable` so that the actual model instance (the subclass) can
        be retrieved
        """
        self._submittable = submittable
        self._submittable_ctype = ContentType.objects.get_for_model(submittable)
        self.completed_at = timezone.now()
        self.save()

    def completed(self):
        """
        Returns `True` if something has been submitted for this task
        """
        return self._submittable is not None

    def completed_on_time(self):
        """
        Returns `True` if this task was completed before the deadline. Otherwise
        returns `False` (also if task is not yet completed)
        """
        if self.completed():
            return self.completed_at <= self.deadline.deadline_time()
