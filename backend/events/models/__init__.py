"""
Import models to make them accessible via events.models
"""
from .models import (BaseEvent, DeadlineEvent, DeadlineTask, MeetingEvent,
                     Submittable)
from .models_for_testing import (TestActorTypeBar, TestActorTypeFoo,
                                 TestSubmittableTypeBar,
                                 TestSubmittableTypeFoo)
