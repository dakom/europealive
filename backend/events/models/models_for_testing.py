"""
Fake models needed for testing GenericRelations
"""
from django.db import models

from events.mixins import DeadlineActorMixin
from events.models import Submittable


class TestActorTypeFoo(DeadlineActorMixin):
    """
    Fake model representing an actor of type Foo
    """


class TestActorTypeBar(DeadlineActorMixin):
    """
    Fake model representing an actor of type Bar
    """


class TestSubmittableTypeFoo(Submittable):
    """
    Fake model representing a submittable of type Foo. `submitted_by` returns
    a TestActorTypeFoo, which is arbitrarily chosen, and can be null.
    """
    by = models.ForeignKey(
        TestActorTypeFoo,
        on_delete=models.SET_NULL,
        null=True
    )

    def submitted_by(self):
        return self.by


class TestSubmittableTypeBar(Submittable):
    """
    Fake model representing a submittable of type Bar. `submitted_by` returns
    a TestActorTypeBar, which is arbitrarily chosen, and can be null.
    """
    by = models.ForeignKey(
        TestActorTypeBar,
        on_delete=models.SET_NULL,
        null=True
    )

    def submitted_by(self):
        return self.by
