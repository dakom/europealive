# Generated by Django 2.2 on 2021-11-25 13:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_testactortypebar_testactortypefoo_testsubmittabletypebar_testsubmittabletypefoo'),
    ]

    operations = [
        migrations.AddField(
            model_name='testsubmittabletypebar',
            name='by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='events.TestActorTypeBar'),
        ),
        migrations.AddField(
            model_name='testsubmittabletypefoo',
            name='by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='events.TestActorTypeFoo'),
        ),
    ]
