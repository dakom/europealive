"""
Mixins are - here - abstract models that should be 'mixed in' with regular
classes to achieve some specialized functionality
"""

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models


class DeadlineActorMixin(models.Model):
    """
    Inherit from `DeadlineActorMixin` to fully utilize the functionality of
    `DeadlineEvents`
    """
    tasks = GenericRelation(
        'events.DeadlineTask',
        content_type_field='actor_type',
        object_id_field='actor_id',
        related_query_name='%(app_label)s_%(class)s_actor'
    )

    class Meta:
        abstract = True
