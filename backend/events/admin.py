"""
Admin module for `events` app
"""
# pylint: disable=unnecessary-pass
from django.contrib import admin  # pylint: disable=unused-import

from events.models import DeadlineEvent, MeetingEvent


def display_time_offset(obj):
    """
    Format offset as 'hour:minutes' with zeros padded to minutes
    """
    return f'{obj.hour_offset}:{obj.minute_offset:02}'


class BaseEventAdmin(admin.ModelAdmin):
    """
    Base Admin class for all event types. Adds a `display_time_offset` function
    that returns the offset as an easy-to-read string like `%H:%M`
    """
    list_display = [display_time_offset, 'name']
    list_display_links = ['name']


@admin.register(MeetingEvent)
class MeetingEventAdmin(BaseEventAdmin):
    """
    Admin class for `MeetingEvent`
    """
    pass


@admin.register(DeadlineEvent)
class DeadlineEventAdmin(BaseEventAdmin):
    """
    Admin class for `DeadlineEvent`
    """
    pass
