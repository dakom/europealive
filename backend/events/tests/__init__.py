"""
Import test cases for `events` app
"""

from events.tests.test_baseevent import BaseEventsTestCase
from events.tests.test_deadlineevent import (CreateTaskTestCase,
                                             DeadlineEventTestCase,
                                             SubmitTestCase)
from events.tests.test_deadlinetask import DeadlineTaskTestCase
from events.tests.test_signals import (TriggerTestCase,
                                       UpdateDisplayTimeOnOffsetChangeTestCase)
from events.tests.test_submittable import SubmittableTestCase
