# pylint: disable=line-too-long,protected-access
"""
Tests for DeadlineTask
"""
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.utils import timezone

from events.models import (DeadlineEvent, DeadlineTask, Submittable,
                           TestActorTypeFoo, TestSubmittableTypeFoo)


class DeadlineTaskTestCase(TestCase):
    """
    Test general functionality of DeadlineTask
    """

    def test_submit(self):
        """
        Test that calling submit with a subclass of `Submittable` sets both
        `DeadlineTask._submittable` and `DeadlineTask._submittable_ctype`
        """
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ContentType.objects.get_for_model(TestActorTypeFoo),
            action=ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        )
        actor = TestActorTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor
        )

        # submittable to submit
        submittable = TestSubmittableTypeFoo.objects.create()

        time_before = timezone.now()

        # function to test
        task.submit(submittable)

        time_after = timezone.now()

        task.refresh_from_db()

        ctype = ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        parent = Submittable.objects.get(id=submittable.id)
        self.assertEqual(task._submittable, parent)
        self.assertEqual(task._submittable_ctype, ctype)
        self.assertGreater(task.completed_at, time_before)
        self.assertLess(task.completed_at, time_after)

    def test_submit_with_non_submittable_type_fails(self):
        """
        Test that calling submit with a subclass of `Submittable` sets both
        `DeadlineTask._submittable` and `DeadlineTask._submittable_ctype`
        """
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ContentType.objects.get_for_model(TestActorTypeFoo),
            action=ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        )
        actor = TestActorTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor
        )

        # submittable to submit
        not_submittable = TestActorTypeFoo.objects.create()

        # function to test
        self.assertRaises(ValueError, task.submit, not_submittable)

    def test_get_submittable_returns_actual_model_instance(self):
        """
        Test that `DeadlineEvent.get_submittable` returns the child model
        instead of the parent `Submittable`
        """
        ctypes = ContentType.objects.get_for_models(
            TestActorTypeFoo,
            TestSubmittableTypeFoo
        )
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ctypes[TestActorTypeFoo],
            action=ctypes[TestSubmittableTypeFoo]
        )
        actor = TestActorTypeFoo.objects.create()
        submittable = TestSubmittableTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
            _submittable=submittable,
            _submittable_ctype=ctypes[TestSubmittableTypeFoo]
        )

        # if we don't refresh from db, then the _submittable field on our
        # local task variable will be referencing the child submittable
        task.refresh_from_db()

        # function to test
        res = task.get_submittable()

        self.assertEqual(res, submittable)
        self.assertIsInstance(res, TestSubmittableTypeFoo)

    def test_get_submittable_returns_parent_if_subclass_false_given_as_arg(self):
        """
        Test that `DeadlineEvent.get_submittable` returns the parent
        `Submittable` if `subclass=False` is given as argument
        """
        ctypes = ContentType.objects.get_for_models(
            TestActorTypeFoo,
            TestSubmittableTypeFoo
        )
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ctypes[TestActorTypeFoo],
            action=ctypes[TestSubmittableTypeFoo]
        )
        actor = TestActorTypeFoo.objects.create()
        submittable = TestSubmittableTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
            _submittable=submittable,
            _submittable_ctype=ctypes[TestSubmittableTypeFoo]
        )

        # if we don't refresh from db, then the _submittable field on our
        # local task variable will be referencing the child submittable
        task.refresh_from_db()

        res = task.get_submittable(subclass=False)

        self.assertNotEqual(res, submittable)
        self.assertEqual(res, Submittable.objects.get(id=res.id))
        self.assertFalse(isinstance(res, TestSubmittableTypeFoo))

    def test_get_submittable_returns_none_if_nothing_is_submitted(self):
        """
        Test that `DeadlineEvent.get_submittable` returns `None` if nothing
        has been submitted yet
        """
        ctypes = ContentType.objects.get_for_models(
            TestActorTypeFoo,
            TestSubmittableTypeFoo
        )
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ctypes[TestActorTypeFoo],
            action=ctypes[TestSubmittableTypeFoo]
        )
        actor = TestActorTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
        )

        res = task.get_submittable()

        self.assertIsNone(res)

    def test_completed_returns_true_if_submittable_is_not_none(self):
        """
        Test that calling `completed` returns `True` if `DeadlineTask._submittable` is not None
        """
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ContentType.objects.get_for_model(TestActorTypeFoo),
            action=ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        )
        actor = TestActorTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
            _submittable=TestSubmittableTypeFoo.objects.create()
        )
        task.refresh_from_db()
        self.assertTrue(task.completed())

    def test_completed_returns_true_if_submit_has_been_called_successfully(self):
        """
        Test that calling `completed` returns `True` if `DeadlineTask.submit` has been called without errors
        """
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ContentType.objects.get_for_model(TestActorTypeFoo),
            action=ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        )
        actor = TestActorTypeFoo.objects.create()
        submittable = TestSubmittableTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
        )
        task.submit(submittable)
        task.refresh_from_db()
        self.assertTrue(task.completed())

    def test_completed_returns_false_if_nothing_is_submitted(self):
        """
        Test that calling `completed` returns `False` if `DeadlineTask._submittable` is None
        """
        deadline = DeadlineEvent.objects.create(
            name='whatever',
            offset=10,
            actor=ContentType.objects.get_for_model(TestActorTypeFoo),
            action=ContentType.objects.get_for_model(TestSubmittableTypeFoo)
        )
        actor = TestActorTypeFoo.objects.create()
        task = DeadlineTask.objects.create(
            deadline=deadline,
            actor_object=actor,
        )
        self.assertFalse(task.completed())
