"""
Tests for DeadlineEvent class
"""
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from events.models import DeadlineEvent, DeadlineTask
from events.models.models_for_testing import (TestActorTypeBar,
                                              TestActorTypeFoo,
                                              TestSubmittableTypeFoo)


class DeadlineEventTestCase(TestCase):
    """
    Tests for the DeadlineEvent class
    """

    def test_deadlineevent_create(self):
        """
        Simply test that a DeadlineEvent can be created as expected
        """

        deadline = DeadlineEvent.objects.create(
            name='test_deadline',
            offset=30,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        self.assertIsInstance(deadline, DeadlineEvent)

    def test_create_task_given_actor_of_correct_type_creates_task(self):
        """
        If `create_task(actor)` is given an `actor` corresponding to the type
        referenced in `deadline.actor`, the function should create a new
        DeadlineTask for that actor
        """

        num_tasks_before = DeadlineTask.objects.count()
        deadline = DeadlineEvent.objects.create(
            name='test_deadline',
            offset=30,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        actor = TestActorTypeFoo.objects.create()
        task = deadline.create_task(actor)
        self.assertEqual(DeadlineTask.objects.count(), num_tasks_before + 1)
        self.assertEqual(task.actor_object, actor)

    def test_create_task_given_actor_of_incorrect_type_raises_error(self):
        """
        If `create_task(actor)` is given an `actor` not corresponding to the
        type referenced in `deadline.actor`, the function should raise a
        ValueError
        """

        num_tasks_before = DeadlineTask.objects.count()
        deadline = DeadlineEvent.objects.create(
            name='test_deadline',
            offset=30,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        actor = TestActorTypeBar.objects.create()
        self.assertRaises(ValueError, deadline.create_task, actor)
        self.assertEqual(DeadlineTask.objects.count(), num_tasks_before)

    def test_create_all_tasks_creates_tasks_for_students(self):
        """
        We create a bunch of objects and associate DeadlineEvents with them to
        test, that DeadlineEvent.create_all_tasks() creates a task for each
        actor and each deadline
        """

        for i in range(12):
            if i % 2 == 0:
                TestActorTypeFoo.objects.create()
            elif i % 3 == 0:
                TestActorTypeBar.objects.create()

        # actor FOO must submit FOO after 50
        DeadlineEvent.objects.create(
            name='Deadline 1 for TestActorTypeFoo',
            offset=50,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        # actor FOO must submit BAR after 150
        DeadlineEvent.objects.create(
            name='Deadline 2 for TestActorTypeFoo',
            offset=150,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypebar')
        )

        # actor BAR must submit BAR after 90
        DeadlineEvent.objects.create(
            name='Deadline 1 for TestActorTypeBar',
            offset=90,
            actor=ContentType.objects.get(model='testactortypebar'),
            action=ContentType.objects.get(model='testsubmittabletypebar')
        )

        # actor BAR must submit FOO after 200
        DeadlineEvent.objects.create(
            name='Deadline 2 for TestActorTypeBar',
            offset=200,
            actor=ContentType.objects.get(model='testactortypebar'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        # actor BAR must submit FOO after 200 (two identical deadlines)
        DeadlineEvent.objects.create(
            name='Deadline 3 for TestActorTypeBar',
            offset=200,
            actor=ContentType.objects.get(model='testactortypebar'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

        # this is the function we are testing
        DeadlineEvent.create_all_tasks()

        # first test that the total number of created tasks is correct
        actor_foo_count = TestActorTypeFoo.objects.count()
        actor_bar_count = TestActorTypeBar.objects.count()

        exp_total = (actor_foo_count * 2) + (actor_bar_count * 3)
        self.assertEqual(DeadlineTask.objects.count(), exp_total)

        # test that for each actor, the appropriate amount of
        # tasks has been created
        for actor_foo in TestActorTypeFoo.objects.all():
            self.assertEqual(actor_foo.tasks.count(), 2)

        for actor_bar in TestActorTypeBar.objects.all():
            self.assertEqual(actor_bar.tasks.count(), 3)


class CreateTaskTestCase(TestCase):
    """
    Tests for `DeadlineEvent.create_task`
    """

    def test_new_task_is_saved_and_returned(self):
        """
        Test that `DeadlineEvent.create_task` creates and returns a new task
        """
        # track num of tasks before
        tasks_before = DeadlineTask.objects.count()

        # create deadline and actor
        deadline = DeadlineEvent.objects.create(
            name='Test deadline',
            offset=100,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )
        actor = TestActorTypeFoo.objects.create()

        # function to test
        task = deadline.create_task(actor)

        self.assertIsInstance(task, DeadlineTask)
        self.assertEqual(task, DeadlineTask.objects.first())
        self.assertEqual(DeadlineTask.objects.count(), tasks_before + 1)
        self.assertEqual(task.actor_object, actor)
        self.assertIsNone(task.get_submittable())
        self.assertEqual(task.deadline, deadline)


class SubmitTestCase(TestCase):
    """
    Test `DeadlineEvent.submit(actor, submittable)`
    """

    def test_task_is_updated_after_successfull_submit(self):
        """
        Test that when `DeadlineEvent.submit` is called successfully, the
        related `DeadlineTask` is updated appropriately
        """
        deadline = DeadlineEvent.objects.create(
            name='Test deadline',
            offset=100,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )
        actor = TestActorTypeFoo.objects.create()
        task = deadline.create_task(actor)

        submittable = TestSubmittableTypeFoo.objects.create(by=actor)

        # this is the function we are testing
        deadline.submit(actor, submittable)

        task.refresh_from_db()
        self.assertEqual(task.get_submittable(), submittable)
        self.assertTrue(task.completed())

    def test_multiple_tasks_can_be_submitted_in_sequence(self):
        """
        Test the case where one actor has multiple tasks associated with
        the same deadline and submits these one at a time
        """
        deadline = DeadlineEvent.objects.create(
            name='Test deadline',
            offset=100,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )
        actor = TestActorTypeFoo.objects.create()

        # create multiple tasks for same actor and deadline
        task1 = deadline.create_task(actor)
        task2 = deadline.create_task(actor)
        task3 = deadline.create_task(actor)

        # the actor creates her submittables
        submittable1 = TestSubmittableTypeFoo.objects.create(by=actor)
        submittable2 = TestSubmittableTypeFoo.objects.create(by=actor)
        submittable3 = TestSubmittableTypeFoo.objects.create(by=actor)

        # submit that shit!
        deadline.submit(actor, submittable1)
        deadline.submit(actor, submittable2)
        deadline.submit(actor, submittable3)

        # update the task variables
        task1.refresh_from_db()
        task2.refresh_from_db()
        task3.refresh_from_db()

        # strictly speaking, `submittable1` does not need to be associated with
        # with `task1`, but this check is merely to ensure, that every task
        # was given a different submittable
        self.assertEqual(task1.get_submittable(), submittable1)
        self.assertEqual(task2.get_submittable(), submittable2)
        self.assertEqual(task3.get_submittable(), submittable3)
