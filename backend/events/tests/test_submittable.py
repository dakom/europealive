from unittest.mock import patch

from django.apps import apps
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from events.models import (DeadlineEvent, DeadlineTask, Submittable,
                           TestActorTypeBar, TestSubmittableTypeBar)


class SubmittableTestCase(TestCase):
    """
    Tests for the Submittable class
    """

    def get_submittable_subclasses(self):
        """
        Helper method for getting all subclasses of `Submittable` (in any
        installed app)
        """
        # pylint: disable=no-self-use, invalid-name

        return [
            m for m in apps.get_models() if 'submittable_ptr_id' in m.__dict__
        ]

    def test_all_subclasses_implements_submitted_by(self):
        """
        All subclasses of `Submittable` must have an implementation of
        `submitted_by()` (this is necessary for the custom `save()` method
        on `Submittable` to work). This test tests that the function on the
        subclass model is different from the one on `Submittable`, which just
        raises an Error
        """
        models = self.get_submittable_subclasses()
        for m in models:
            self.assertNotEqual(
                m.submitted_by,
                Submittable.submitted_by,
                msg=f'{m.__name__} does not override `Submittable.submitted_by()`'
            )

    def test_submit_sets_is_submitted_and_associated_task_returns_completed(self):
        """
        If `Submittable.submit` is called, then `Submittable.is_submitted` should be `True`, and `DeadlineTask.completed` should return `True`
        """
        actor = TestActorTypeBar.objects.create()
        DeadlineEvent.objects.create(
            name='Test deadline',
            offset=100,
            actor=ContentType.objects.get_for_model(TestActorTypeBar),
            action=ContentType.objects.get_for_model(TestSubmittableTypeBar)
        )
        DeadlineEvent.create_all_tasks()

        submittable = TestSubmittableTypeBar(by=actor)
        submittable.save(auto_submit=True)

        self.assertTrue(submittable.is_submitted)

        task = actor.tasks.get()
        self.assertTrue(task.completed())


    def test_submit_only_submits_to_first_unprocessed_deadline(self):
        """
        `Submittable.submit` should submit the
        submittable to the first unprocessed `DeadlineEvent`, sorted by
        `DeadlineEvent.offset`
        """

        actor = TestActorTypeBar.objects.create()
        d1 = DeadlineEvent.objects.create(
            name='Already processed',
            offset=50,
            actor=ContentType.objects.get_for_model(TestActorTypeBar),
            action=ContentType.objects.get_for_model(TestSubmittableTypeBar),
            processed=True
        )
        d2 = DeadlineEvent.objects.create(
            name='Target deadline',
            offset=100,
            actor=ContentType.objects.get_for_model(TestActorTypeBar),
            action=ContentType.objects.get_for_model(TestSubmittableTypeBar),
        )
        d3 = DeadlineEvent.objects.create(
            name='Not next deadline',
            offset=150,
            actor=ContentType.objects.get_for_model(TestActorTypeBar),
            action=ContentType.objects.get_for_model(TestSubmittableTypeBar),
        )
        DeadlineEvent.create_all_tasks()

        submittable = TestSubmittableTypeBar.objects.create(by=actor)
        submittable.submit()

        self.assertTrue(submittable.is_submitted)

        self.assertFalse(d1.tasks.first().completed())
        self.assertTrue(d2.tasks.first().completed())
        self.assertFalse(d3.tasks.first().completed())

    def test_submit_has_no_effect_if_submittable_was_already_submitted(self):
        """
        `Submittable.submit` should just return without doing anything if its
        `Submittable.is_submitted` is `True`
        """
        actor = TestActorTypeBar.objects.create()
        DeadlineEvent.objects.create(
            name='Test deadline',
            offset=100,
            actor=ContentType.objects.get_for_model(TestActorTypeBar),
            action=ContentType.objects.get_for_model(TestSubmittableTypeBar)
        )
        DeadlineEvent.create_all_tasks()

        submittable = TestSubmittableTypeBar.objects.create(by=actor)
        submittable.submit()

        # sanity check
        self.assertTrue(submittable.is_submitted)

        # store the time at which the task was completed
        task = actor.tasks.get()
        completed_at = task.completed_at

        # sanity check
        self.assertIsNotNone(completed_at)

        # we pretend to make some changes to the model
        submittable.foo = 'bar'
        with patch('events.models.Submittable.submitted_by') as mock_sub_by:
            submittable.save(auto_submit=True)
            mock_sub_by.assert_not_called()

        task.refresh_from_db()
        self.assertEqual(task.completed_at, completed_at)


    def test_gets_submitted_if_auto_submit_is_passed_to_save(self):
        """
        If `auto_submit=True` is passed to `Submittable.save` the `submit`
        function on `Submittable` should be called once
        """
        actor = TestActorTypeBar.objects.create()
        submittable = TestSubmittableTypeBar(by=actor)

        with patch('events.models.Submittable.submit') as mock_submit:
            submittable.save(auto_submit=True)
            mock_submit.assert_called_once()
