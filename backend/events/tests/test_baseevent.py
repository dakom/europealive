"""
Tests for the events app

TODO: Remove dependency on other apps
"""
from datetime import datetime
from unittest.mock import patch

from django.test import TestCase
from django.utils import timezone

from events.models import BaseEvent


class BaseEventsTestCase(TestCase):
    """
    Tests for the BaseEvent class
    """

    def test_set_display_time_sets_hidden_field(self):
        """
        set_display_time(gamestart) should set _display_time according to
        gamestart + offset, and then this value should be accessible through
        the property display_time
        """
        gamestart = timezone.make_aware(datetime(2021, 1, 1, 9))
        event = BaseEvent.objects.create(
            name='test event',
            offset=90,
        )
        event.set_display_time(gamestart)
        event.refresh_from_db()

        # pylint: disable-next=protected-access
        self.assertEqual(event._display_time, event.display_time)
        self.assertIsNotNone(event.display_time)

        exp_time = timezone.make_aware(datetime(2021, 1, 1, 10, 30))
        self.assertEqual(event.display_time, exp_time)

    @patch('events.models.models.timezone')
    def test_set_display_time_makes_gamestart_arg_tz_aware(
            self, mock_timezone
    ):
        """
        If a naive datetime object is passed to `set_display_time(gamestart)`,
        this should be converted to an aware datetime object
        """
        # pylint: disable=unnecessary-lambda
        gamestart = datetime(2021, 1, 1, 9)
        gamestart_aware = timezone.make_aware(gamestart)

        mock_timezone.is_naive.return_value = True
        mock_timezone.make_aware.return_value = gamestart_aware
        mock_timezone.get_current_timezone.return_value = \
            timezone.get_current_timezone()

        # sanity check
        self.assertTrue(timezone.is_naive(gamestart))

        event = BaseEvent.objects.create(
            name='test event',
            offset=90,
        )

        event.set_display_time(gamestart)
        event.refresh_from_db()

        mock_timezone.is_naive.assert_called_once_with(gamestart)
        mock_timezone.make_aware.assert_called_once_with(gamestart)
        self.assertTrue(timezone.is_aware(event.display_time))
