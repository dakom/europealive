"""
Test signals defined in `events.signal_defs` and recievers in `events.signals`
"""
from django.contrib.contenttypes.models import ContentType
from django.db.utils import IntegrityError
from django.test import TestCase
from django.utils import timezone

from events.models import BaseEvent, DeadlineEvent
from events.signal_defs import event_happened


class TriggerTestCase(TestCase):
    """
    TestCase to test the function `DeadlineEvent.trigger` which should send the
    `event_happened` signal
    """

    def setUp(self):
        """
        Create a deadline to work with
        """
        self.return_deadline = None
        self.signal_sent = False

        self.deadline = DeadlineEvent.objects.create(
            name='test_deadline',
            offset=30,
            actor=ContentType.objects.get(model='testactortypefoo'),
            action=ContentType.objects.get(model='testsubmittabletypefoo')
        )

    def test_event_happened_signal_is_sent_with_self_as_arg(self):
        """
        Test that a reciever connected to the `event_happened` signal is called
        when the `.trigger()` function of an event is called (because
        `trigger()` fires the event)
        """
        self.return_deadline = None

        def handler(sender, event, **kwargs):
            # pylint: disable=unused-argument
            self.return_deadline = event

        event_happened.connect(handler)
        self.deadline.trigger()

        self.assertEqual(self.return_deadline, self.deadline)
        self.assertTrue(self.return_deadline.processed)

        event_happened.disconnect(handler)

    def test_event_happened_not_sent_if_event_already_processed(self):
        """
        Test that the signal is not fired if `trigger()` is called on an event
        that has already been processed
        """
        self.signal_sent = False
        self.deadline.processed = True
        self.deadline.save()

        def handler(sender, event, **kwargs):
            # pylint: disable=unused-argument
            self.signal_sent = True

        event_happened.connect(handler)
        self.deadline.trigger()

        self.assertFalse(self.signal_sent)

        event_happened.disconnect(handler)


class UpdateDisplayTimeOnOffsetChangeTestCase(TestCase):
    """
    Test signal reciever `events.signals.update_display_time_on_offset_change`
    """

    def test_display_time_is_updated_when_offset_changes(self):
        """
        Test that a Signal is fired to update `DeadlineEvent.display_time` if
        its offset is changed
        """
        event = BaseEvent.objects.create(offset=10)

        gamestart = timezone.datetime(1991, 12, 27, 9, 0)
        event.set_display_time(gamestart)

        event.offset = 15
        event.save()

        event.refresh_from_db()
        self.assertEqual(
            event.display_time,
            timezone.make_aware(timezone.datetime(1991, 12, 27, 9, 15))
        )

    def test_returns_silently_if_object_had_no_prior_display_time(self):
        """
        Test that if the event didn't have display_time set prior to the change,
        then nothing will happen and the reciever function returns silently
        """
        event = BaseEvent.objects.create(offset=10)
        self.assertIsNone(event.display_time)

        event.offset = event.offset * 2
        event.save()

        event.refresh_from_db()
        self.assertIsNone(event.display_time)

    def test_returns_silently_if_offset_set_to_none(self):
        """
        Test that if offset is being set to None, then that will not cause an
        exception to be raised in the signal but rather at the save() level
        """
        event = BaseEvent.objects.create(offset=10)
        gamestart = timezone.datetime(1991, 12, 27, 9, 0)
        event.set_display_time(gamestart)

        event.offset = None

        # the error should occur in the save method, rather than in the signal
        self.assertRaises(IntegrityError, event.save)
