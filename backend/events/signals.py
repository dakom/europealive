"""
Define signals for `events` app
"""
from datetime import timedelta

from django.db.models.signals import pre_save

from events.models import BaseEvent, DeadlineEvent, MeetingEvent


def update_display_time_on_offset_change(sender, instance, **kwargs):
    """
    When offset is changed, update display_time if it has been set before
    """
    try:
        obj = sender.objects.get(pk=instance.pk)

    # object is only just being created, nothing to change
    except sender.DoesNotExist:
        return

    # display_time hasn't been set yet
    if obj.display_time is None:
        return

    # offset hasn't changed
    if obj.offset == instance.offset:
        return

    # this will result in an error when we get to the actual `save()`
    if obj.offset is None or instance.offset is None:
        return

    # calculate gamestart and set for instance to be updated
    gamestart = obj.display_time - timedelta(minutes=obj.offset)
    instance.set_display_time(gamestart, save=False)


classes_to_connect = [BaseEvent, DeadlineEvent, MeetingEvent]
for cls in classes_to_connect:
    pre_save.connect(
        update_display_time_on_offset_change,
        sender=cls,
        dispatch_uid='upd_dpt_pre_save' + cls.__name__
    )
