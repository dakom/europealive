# Event module for Communico Edu-LARP games


## Open questions

- Should `actor` be a combination of a job and a team?
- Who should be the actor for a `DirectiveInvestment`? Either the Team (though
    that would require multiple `DeadlineEvent`s) or just the `Secretary`
    (though that would require changing the function `submitted_by()` on
    `DeadlineEvent`)?
- Should events even work on `ContentType`s or should they point directly at a
    role or job (though that would require multiple events and cause troubles
    with the calendar)?
