""" App """
# pylint: disable=import-outside-toplevel
import vinaigrette
from django.apps import AppConfig


class EventsConfig(AppConfig):
    """
    Configuration class
    """
    name = 'events'

    def ready(self):
        import events.signals
        import events.signal_defs
        from .models import BaseEvent, DeadlineEvent, MeetingEvent
        vinaigrette.register(
            BaseEvent, [
                'name', 'player_description', 'facilitator_description'
            ]
        )
        vinaigrette.register(
            DeadlineEvent, [
                'name', 'player_description', 'facilitator_description'
            ]
        )
        vinaigrette.register(
            MeetingEvent, [
                'name', 'player_description', 'facilitator_description'
            ]
        )
        vinaigrette.register(
            MeetingEvent, ['agenda']
        )
