from events.models import Submittable


def create_submittable():
    """
    Add this as keyword argument `default` to migration files when subclassing
    `Submittable` from an existing model to deal with not-null constraints.
    That is, when going from `class Foo(models.Model)` to `class
    Foo(Submittable)`, run `python manage.py makemigrations`, select option 1
    and set any value as default. Then, before migrating, open the new migration
    file and change

    ```
        operations = [
            ...,
            migrations.AddField(
                model_name='Foo',
                name='submittable_ptr_id',
                field=models.OneToOneField(..., default=0, ...),
                ...
            ),
            ...,
        ]
    ```

    to

    ```
        operations = [
            ...,
            migrations.AddField(
                model_name='Foo',
                name='submittable_ptr_id',
                field=models.OneToOneField(..., default=events.utils.create_submittable, ...),
                ...
            ),
            ...,
        ]
    ```
    """
    submittable = Submittable(is_submitted=False)
    submittable.save(auto_submit=False)
    return submittable.id
