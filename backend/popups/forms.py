from django.forms import ModelForm

from .models import PopupMessage


class PopupMessageForm(ModelForm):
    """ Form used by Graphene Mutation """
    class Meta:
        model = PopupMessage
        fields = ["title", "message"]
