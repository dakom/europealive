""" Popups apps """
from django.apps import AppConfig
import vinaigrette

class PopupsConfig(AppConfig):
    """ Popups config """
    name = 'popups'

    def ready(self):
        from .models import (
            PopupMessage
        )

        vinaigrette.register(PopupMessage, ['title', 'message'])
