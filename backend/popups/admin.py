""" Popup admin """
from django import forms
from django.apps import apps
from django.contrib import admin
from django.db.utils import ProgrammingError
from django.utils.timezone import get_current_timezone

from core.models import CommissionStatic, LobbyOrganizationStatic
from game.models import CountryStatic, ParliamentGroupStatic
from news.models import MediaHouseStatic

from .models import PopupMessage


def team_choices(as_dict=False):
    choices = [("all", "All")]
    try:
        for cls in [CountryStatic, ParliamentGroupStatic,
                    LobbyOrganizationStatic, CommissionStatic, MediaHouseStatic]:
            teams = cls.objects.all()
            choices += [
                ("%s$%i" % (type(t).__name__, t.id), t.name) for t in teams
            ]
    except ProgrammingError:
        # Database hasn't been migrated yet
        pass

    return dict(choices) if as_dict else choices

def role_choices(as_dict=False):
    choices = [
        ("all", "All"),
        ("Journalist", "Journalist"),
        ("Secretary", "Secretary"),
        ("Chief", "Chief"),
        ("Strategist", "Strategist"),
        ("Attache Advisor", "Attache Advisor"),
        ("Media Advisor", "Media Advisor"),
        ("Political Advisor", "Political Advisor"),
        ("Technical Advisor", "Technical Advisor"),
    ]
    return dict(choices) if as_dict else choices


class PopupMessageAdminForm(forms.ModelForm):
    """ Special form to allow for choosing recipients """
    team_recipients = forms.ChoiceField(choices=team_choices())
    role_recipients = forms.ChoiceField(choices=role_choices())

    class Meta:
        model = PopupMessage
        exclude = ('recipients',)

        widgets = {
            'recipients': forms.HiddenInput()
        }

    def save(self, commit=True):
        """ Sets the recipient field on the saved instance by combining the
        values of team_recipients and role_recipients """
        team = self.cleaned_data['team_recipients']
        role = self.cleaned_data['role_recipients']
        instance = super(PopupMessageAdminForm, self).save(commit=False)
        instance.recipients = team + '#' + role
        if commit:
            instance.save()
        return instance

    def __init__(self, *args, **kwargs):
        """ If the form is working on an existing instance, the initial values
        of team_recipients and role_recipients are set from intance.recipients """
        instance = kwargs.get('instance')
        initial = kwargs.get('initial', {})
        if instance is not None:
            recipients = instance.recipients.split('#')
            role = recipients[-1]
            initial['team_recipients'] = recipients[0]
            initial['role_recipients'] = recipients[-1]

        kwargs['initial'] = initial
        super().__init__(*args, **kwargs)


@admin.register(PopupMessage)
class PopupMessageAdmin(admin.ModelAdmin):
    """ Set special form for popup messages """

    def time(self, obj):
        tz = get_current_timezone()
        return obj.time.astimezone(tz).strftime('%H:%M:%S')
    time.admin_order_field = 'minute_offset'

    # two functions for showing the recipients in admin overview
    # note the renaming to avoid confusion with the actual fields

    def display_team_recipients(self, obj):
        team = obj.recipients.split('#')[0]
        return team_choices(as_dict=True)[team]
    display_team_recipients.short_description = 'Team recipients'

    def display_role_recipients(self, obj):
        role = obj.recipients.split('#')[1]
        return role_choices(as_dict=True)[role]
    display_role_recipients.short_description = 'Role recipients'


    form = PopupMessageAdminForm

    ordering = ('minute_offset',)
    list_per_page = 25
    fieldsets = (
        ('General', {
            'fields': ('title', 'message', ('send_time', 'minute_offset'),
                       'sender', 'important'),
        }),
        ('Recipients', {
            'fields': ('team_recipients', 'role_recipients'),
        }),
    )
    list_display = (
        'title',
        'time',
        'display_team_recipients',
        'display_role_recipients'
    )
