from datetime import datetime

from django.test import RequestFactory, TestCase
from django.utils.timezone import make_aware
from graphene.test import Client

from core.models import Commission, CommissionStatic
from game.models import Game
from meta.models import GameConfiguration
from news.models import Journalist, MediaHouse, MediaHouseStatic
from popups.models import PopupMessage
from schema import schema


def execute_test_client_api_query(
        api_query, user=None, variable_values=None, **kwargs):
    """
    Returns the results of executing a graphQL query using the graphene test
    client.  This is a helper method for our tests

    Source: https://stackoverflow.com/a/47762174/4969373
    """
    request_factory = RequestFactory()

    context_value = request_factory.get('/api/')
    context_value.user = user
    client = Client(schema)
    executed = client.execute(
        api_query,
        context_value=context_value,
        variable_values=variable_values,
        **kwargs
    )
    return executed


class ResolveMyPopupsTestCase(TestCase):
    """
    Test the function that fetches the appropriate PopupMessages for a given
    user at a given time
    """

    def setUp(self):
        config = GameConfiguration.objects.create()
        start_time = datetime(2020, 12, 16, 11, 21, 0, 0)

        game = Game.objects.create(
            name='Test game',
            starting_time=make_aware(start_time),
            configuration=config,
            last_job_ordering=0
        )
        game.create_teams()
        self.game = game

        MediaHouse.objects.create(
            influence_points=100,
            static=MediaHouseStatic.objects.create(starting_ip=100)
        )

    def test_journalist_gets_popup_for_all_for_team_and_for_job(self):
        """
        Test that journalist gets popups for journalists, for their mediahouse
        and for all players. Also check, that a journalist does not get popups
        for commission and for secrataries
        """

        # create a media house and a journalist working there
        media_house = MediaHouse.objects.create(
            influence_points=100,
            static=MediaHouseStatic.objects.create(starting_ip=100)
        )

        journalist = Journalist.objects.create(
            email='test@test.dk',
            first_name='Test-fornavn',
            last_name='Test-efternavn',
            team=media_house,
            mediahouse=media_house,
            job_description_raw=''
        )

        # create a popup for all working at the media house
        static_name = type(media_house.static).__name__
        static_id = media_house.static.id
        for_mediahouse = 'For a MediaHouse'
        PopupMessage.objects.create(
            title=for_mediahouse,
            message=for_mediahouse,
            recipients=f'{static_name}${static_id}#all',
            minute_offset=10
        )

        # create a popup for all journalists
        for_journalists = 'For journalists'
        PopupMessage.objects.create(
            title=for_journalists,
            message=for_journalists,
            recipients='all#Journalist',
            minute_offset=10
        )

        # create a popup for all players
        for_all = 'For all'
        PopupMessage.objects.create(
            title=for_all,
            message=for_all,
            recipients='all#all',
            minute_offset=10
        )

        # create a popup for only secretaries
        for_secretaries = 'For secretaries'
        PopupMessage.objects.create(
            title=for_secretaries,
            message=for_secretaries,
            recipients='all#Secretary',
            minute_offset=10
        )

        # create a commission and a popup for the commission
        commission_static = Commission.objects.create(
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        ).static

        static_name = type(commission_static).__name__
        static_id = commission_static.id
        for_commission = 'For Commision'
        PopupMessage.objects.create(
            title=for_commission,
            message=for_commission,
            recipients=f'{static_name}${static_id}#all',
            minute_offset=10
        )


        # query to be executed
        query = '''
    query($since: DateTime) {
        myPopups(since: $since) {
            id
            message
            title
            important
            sender {
              fullName
            }
            time
        }
    }
  '''
        res = execute_test_client_api_query(
            query, user=journalist, variable_values={'since': None}
        )
        popups = res['data']['myPopups']

        # test that three popups are returned
        self.assertEqual(len(popups), 3)

        # we want to check the titles of the popups
        titles = [p['title'] for p in popups]

        # no popups should be for secretaries or commission
        self.assertNotIn(for_secretaries, titles)
        self.assertNotIn(for_commission, titles)

        # the popups for journalists, the mediahouse and all should be there
        self.assertIn(for_all, titles)
        self.assertIn(for_journalists, titles)
        self.assertIn(for_mediahouse, titles)
