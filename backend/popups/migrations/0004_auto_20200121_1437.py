# Generated by Django 2.2 on 2020-01-21 13:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('popups', '0003_popupmessage_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='popupmessage',
            name='recipients',
            field=models.CharField(help_text="Code to decide which users should receive a message.                   Examples: 'all', 'ParliamentGroup', 'Team$5'", max_length=70),
        ),
        migrations.AlterField(
            model_name='popupmessage',
            name='title',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
