# Generated by Django 2.2 on 2020-01-29 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('popups', '0005_popupmessage_important'),
    ]

    operations = [
        migrations.AddField(
            model_name='popupmessage',
            name='minute_offset',
            field=models.PositiveSmallIntegerField(blank=True, help_text='Minutes after game start offset.', null=True),
        ),
        migrations.AlterField(
            model_name='popupmessage',
            name='send_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
