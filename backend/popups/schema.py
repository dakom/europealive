""" Popups GraphQL schema """
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
import graphene
from django.db.models import Q
from django.utils import timezone
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import DjangoObjectType

from core.models import Team
from core.shared_schema import gql_strict_list, query_or_empty_list
from game.models import Game

from . import forms
from .models import PopupMessage


class PopupMessageType(DjangoObjectType):
    time = graphene.types.datetime.DateTime(required=True)

    class Meta:
        model = PopupMessage
        only_fields = ["id", "title", "message", "sender", "important", "recipients", "time"]


class PopupMessageMutation(DjangoModelFormMutation):
    popupMessage = graphene.Field(PopupMessageType)

    class Meta:
        form_class = forms.PopupMessageForm

    @classmethod
    def perform_mutate(cls, form, info):
        form.instance.important = True
        form.instance.sender = info.context.user
        form.instance.send_time = timezone.now()
        form.instance.recipients = "all#all"
        return super().perform_mutate(form, info)


class Query:
    my_popups = graphene.Field(gql_strict_list(PopupMessageType),
                               since=graphene.types.datetime.DateTime())
    popups_sent_by_me = gql_strict_list(PopupMessageType)

    def resolve_my_popups(self, info, **kwargs):
        """
        Returns popups relevant to user.
        If "since" is given, only return popups sent since that time. Otherwise send all.
        """

        # Determine which recipient categories the user belongs to
        recipient_categories = ["all#all"]

        if not info.context.user.is_anonymous:
            user = info.context.user

            if hasattr(user, "student"):
                t = Team.objects.filter(id=user.student.team.id).select_subclasses()[0]
                team = '{}${}'.format(type(t.static).__name__, t.static.id)
                role = user.student.job

                recipient_categories.append(team + '#' + role)
                recipient_categories.append('all' + '#' + role)
                recipient_categories.append(team + '#' + 'all')

        # Determine which popups should be shown at this time
        minutes = lambda timedelta: timedelta.total_seconds() / 60
        now = timezone.now()
        game = Game.get_current_game()

        messages = PopupMessage.objects.filter(
            Q(send_time__isnull=False)
            | Q(minute_offset__lt=minutes(now - game.starting_time)),
            recipients__in=recipient_categories,
        ).order_by("-send_time")

        if "since" in kwargs:
            since = kwargs["since"]
            messages = messages.filter(
                Q(send_time__gt=since)
                | Q(minute_offset__gt=minutes(since - game.starting_time))
            )

        return query_or_empty_list(messages)

    def resolve_popups_sent_by_me(self, info):
        user = info.context.user
        return query_or_empty_list(user.popupmessage_set.all().order_by("-send_time"))

class Mutation:
    update_popup_message = PopupMessageMutation.Field()
