""" Popup models """
from datetime import datetime, timedelta

from django.core.exceptions import ValidationError
from django.db import models

from core.models import User
from game.models import Game


class PopupMessage(models.Model):
    """
    A popup message sent automatically to users. May have a sender, or be sent by the system.
    A user will receive all messages which have a 'recipient' value they fall under, which may be
    a category or a specific team or user id.
    A message may be scheduled to be sent by setting minute_offset, or may be sent instantly
    by setting send_time.
    """

    title = models.CharField(blank=True, max_length=50)
    message = models.TextField()
    sender = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    important = models.BooleanField(
        default=False,
        help_text="Important messages are visually emphasized.",
    )
    recipients = models.CharField(
        max_length=70,
        help_text="Code to decide which users should receive a message.\
                   Examples: 'all', 'ParliamentGroupStatic$3'",
    )

    # EXACTLY ONE of send_time and minute_offset must be set.
    send_time = models.DateTimeField(null=True, blank=True)
    minute_offset = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        help_text="Minutes after game start offset."
    )

    def __str__(self):
        return self.message[:15]

    @property
    def time(self):
        """ Abstracts over send_time and minute_offset, always returning a datetime. """
        if self.send_time is not None:
            return self.send_time
        game = Game.get_current_game()
        if game is None:
            # no Game in database, use arbitrary default starting_time
            game_start = datetime(2021, 10, 1, 9, 0)
        else:
            game_start = Game.get_current_game().starting_time
        entry_time = game_start + timedelta(minutes=self.minute_offset)
        return entry_time

    def save(self, *args, **kwargs): #pylint: disable=arguments-differ
        """ This override ensures that exactly one of send_time and minute_offset is set """
        if sum([self.send_time is None, self.minute_offset is None]) != 1:
            raise ValidationError("Either send_time or minute_offset must be set")
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = "popup message"
        verbose_name_plural = "popup messages"
