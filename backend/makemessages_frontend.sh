#! /bin/bash
# Crowdin delivers no-wrapped files, so we mirror here to avoid messy merges
./manage.py makemessages --no-wrap -d djangojs --symlinks
