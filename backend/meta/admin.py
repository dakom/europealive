# pylint: disable=missing-docstring
from django.contrib import admin
from adminsortable2.admin import SortableInlineAdminMixin

from .models import (
    GameConfiguration, StudentJobTemplate, JobInConfiguration,
)

admin.site.register(StudentJobTemplate)


class JobInConfigurationInline(SortableInlineAdminMixin,
                               admin.TabularInline):
    model = JobInConfiguration
    readonly_fields = ("job_",)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Optimize queryset. Related models are needed to show names in list
        """
        if db_field.name == "job":
            kwargs["queryset"] = StudentJobTemplate.objects.select_related(
                "country", "parliament_group", "media_house", "commission",
                "lobby_organization")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        """ Optimize query to show readonly job name """
        qs = super().get_queryset(request)
        qs = qs.select_related(
            "job", "job__country", "job__parliament_group", "job__media_house",
            "job__commission", "job__lobby_organization")
        return qs

    def job_(self, instance): # pylint: disable=no-self-use
        """ Repeat job to allow ctrl-f search in browser """
        return str(instance.job)


@admin.register(GameConfiguration)
class GameConfigurationAdmin(admin.ModelAdmin):
    inlines = (JobInConfigurationInline,)

    actions = ["make_copy"]

    def make_copy(self, request, queryset):
        for game_config in queryset:
            job_in_configurations = game_config.jobinconfiguration_set.all()

            # Get new config id
            game_config.pk = None
            game_config.name += " - COPY"
            game_config.save()

            # Copy all job configurations as well
            for jic in job_in_configurations:
                jic.configuration = game_config
                jic.pk = None
                jic.save()
