""" App """
from django.apps import AppConfig
import vinaigrette


class MetaConfig(AppConfig):
    """ Meta config """
    name = 'meta'

    def ready(self):
        from .models import StudentJobTemplate
        vinaigrette.register(StudentJobTemplate, ['job_description', 'job_title'])
