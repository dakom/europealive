""" Meta models are used to set up game, but not changed during game """
from django.core.exceptions import ValidationError
from django.db import models
from enumfields import Enum, EnumIntegerField

from core.models import (Advisor, AttacheAdvisor, Chief, Commission,
                         CommissionStatic, LobbyOrganization,
                         LobbyOrganizationStatic, MediaAdvisor,
                         PoliticalAdvisor, Secretary, Strategist, Student,
                         TechnicalAdvisor)
from game.models import (Country, CountryStatic, ParliamentGroup,
                         ParliamentGroupStatic)
from news.models import Journalist, MediaHouse, MediaHouseStatic


class Job(Enum):
    """ Jobs which a student can be assigned """
    Secretary = 0
    Strategist = 1
    Chief = 2
    TechnicalAdvisor = 3
    PoliticalAdvisor = 4
    AttacheAdvisor = 5
    MediaAdvisor = 6

    NewsEditor = 7
    Journalist = 8

    Minister = 9
    SpokesPerson = 10


class GameConfiguration(models.Model):
    """
    A configuration specifies which student jobs are available in the game
    """
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=200, blank=True)
    max_students = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "game configuration"
        verbose_name_plural = "game configurations"

    def get_required_teams(self):
        """
        Determines which teams are needed in the game for the selected jobs.
        If no StudentJobTemplate references a TeamStatic, that team will not be created.
        """
        job_in_configurations = self.jobinconfiguration_set.all()
        job_template_ids = job_in_configurations.values_list("job", flat=True)
        job_templates = StudentJobTemplate.objects.filter(id__in=job_template_ids).distinct()

        # Use set to remove duplicates
        return set(t.get_team_static() for t in job_templates)

    def get_next_job_in_conf(self, last_order, first_pass):
        """
        Get the job in the configuration with order immediately
        following given last_order.
        If first_pass is false, only consider repeatable jobs.
        """
        if first_pass:
            jobs = self.jobinconfiguration_set.all()
        else:
            jobs = self.jobinconfiguration_set.filter(repeatable=True)

        try:
            return jobs.filter(order__gt=last_order)[0]
        except IndexError:
            # We have reached the end of jobs, restart from top.
            return self.get_next_job_in_conf(0, first_pass=False)


class StudentJobTemplate(models.Model):
    """
    Template for assigning a job to a student.
    Primarily used at game start
    """
    job = EnumIntegerField(Job)
    job_title = models.CharField(max_length=50, blank=True, null=True)
    job_description = models.TextField()

    # EXACTLY ONE TEAM STATIC MUST BE NON-NULL!!
    country = models.ForeignKey(CountryStatic, blank=True, null=True,
                                on_delete=models.SET_NULL)
    parliament_group = models.ForeignKey(
        ParliamentGroupStatic, blank=True, null=True,
        on_delete=models.SET_NULL
    )
    media_house = models.ForeignKey(
        MediaHouseStatic, blank=True, null=True, on_delete=models.SET_NULL
    )
    commission = models.ForeignKey(
        CommissionStatic, blank=True, null=True, on_delete=models.SET_NULL
    )
    lobby_organization = models.ForeignKey(
        LobbyOrganizationStatic, blank=True, null=True,
        on_delete=models.SET_NULL
    )

    @property
    def team_fields(self):
        """ Return the possible foreign key fields to teams """
        return [self.country, self.parliament_group, self.media_house,
                self.commission, self.lobby_organization]

    def clean(self):
        """ Check that exactly one team is set """
        number_of_teams = sum(t is not None for t in self.team_fields)
        if number_of_teams > 1:
            raise ValidationError(
                "A Student cannot be on more than one team"
            )
        if number_of_teams == 0:
            raise ValidationError("A Student must be on a team")

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        """
        Call clean manually to ensure exactly one team is set even when
        save is called directly.
        """
        self.clean()
        return super().save(*args, **kwargs)

    def get_team_static(self):
        """ Returns the one team static field which has been set """
        return next(t for t in self.team_fields if t is not None)

    def get_team_from_game(self):
        """
        Returns the team associated with selected team static.
        Only works when teams have been created!
        """
        if self.country is not None:
            return Country.objects.get(static=self.country).team_ptr
        if self.parliament_group is not None:
            return ParliamentGroup.objects.get(
                static=self.parliament_group
            ).team_ptr
        if self.media_house is not None:
            return MediaHouse.objects.get(
                static=self.media_house
            ).team_ptr
        if self.commission is not None:
            return Commission.objects.get(static=self.commission).team_ptr
        if self.lobby_organization is not None:
            return LobbyOrganization.objects.get(
                static=self.lobby_organization
            ).team_ptr

        raise RuntimeError("Job Template does not specify team")

    def __str__(self):
        return "%s in %s" % (str(self.job), str(self.get_team_static()))

    class Meta:
        verbose_name = "student job template"
        verbose_name_plural = "student job templates"

    def create_any_student(self, user, delete_pending_student=False):
        """
        Create the subclass of student specified by self.job.
        If specified, deletes associated PendingStudent of user.

        WARNING: Django does not support instantiating subclass from existing
        superclass model (https://code.djangoproject.com/ticket/7623).
        This hacks around it user __dict__.update.
        """
        team = self.get_team_from_game()
        substudent = None
        shared_args = {
            "user_ptr_id":user.id,
            "team": team,
            "job_description_raw": self.untranslated('job_description'),
            "job_title_raw": self.untranslated('job_title'),
        }

        if self.job == Job.Secretary:
            substudent = Secretary(**shared_args)

        elif self.job in [Job.Strategist, Job.Minister, Job.SpokesPerson]:
            substudent = Strategist(**shared_args)

        elif self.job == Job.Chief:
            substudent = Chief(**shared_args)

        elif self.job == Job.TechnicalAdvisor:
            substudent = TechnicalAdvisor(**shared_args)

        elif self.job == Job.PoliticalAdvisor:
            substudent = PoliticalAdvisor(**shared_args)

        elif self.job == Job.AttacheAdvisor:
            substudent = AttacheAdvisor(**shared_args)

        elif self.job == Job.MediaAdvisor:
            substudent = MediaAdvisor(**shared_args)

        elif self.job == Job.NewsEditor:
            substudent = Journalist(mediahouse=team.mediahouse,
                                    is_editor=True, **shared_args)

        elif self.job == Job.Journalist:
            substudent = Journalist(mediahouse=team.mediahouse,
                                    is_editor=False, **shared_args)
        else:
            raise NotImplementedError("Job template not recognized", self.job)

        substudent.__dict__.update(user.__dict__)
        substudent.save()


        if delete_pending_student:
            user.pendingstudent.delete(keep_parents=True)

        return substudent


class JobInConfiguration(models.Model):
    """
    A job included in a game configuration.
    Like a many-to-many relation with options.
    """
    configuration = models.ForeignKey(GameConfiguration, on_delete=models.CASCADE)
    job = models.ForeignKey(StudentJobTemplate, on_delete=models.PROTECT)
    repeatable = models.BooleanField(default=False)
    order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = "job in configuration"
        verbose_name_plural = "jobs in configurations"
        ordering = ["order"]
