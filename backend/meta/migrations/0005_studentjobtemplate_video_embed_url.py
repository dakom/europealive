# Generated by Django 2.2 on 2019-10-07 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meta', '0004_auto_20191004_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentjobtemplate',
            name='video_embed_url',
            field=models.URLField(default='www.example.com', help_text='Link to an embeddable video, for example                 https://player.vimeo.com/video/12345789', max_length=500),
            preserve_default=False,
        ),
    ]
