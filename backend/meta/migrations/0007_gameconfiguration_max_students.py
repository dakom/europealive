# Generated by Django 2.2 on 2022-06-22 09:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meta', '0006_remove_studentjobtemplate_video_embed_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='gameconfiguration',
            name='max_students',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
