# Generated by Django 2.2 on 2019-10-03 12:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meta', '0002_auto_20190930_1641'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentjobtemplate',
            name='job_description',
            field=models.TextField(default='Job description missing'),
            preserve_default=False,
        ),
    ]
