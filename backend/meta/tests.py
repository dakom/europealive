"""
Tests related to the `meta` app
"""

from django.test import TestCase

from game.models import CountryStatic
from meta.models import (GameConfiguration, Job, JobInConfiguration,
                         StudentJobTemplate)


class GetNextJobInConfTestCase(TestCase):
    """
    Tests related to function `GameConfiguration.get_next_job_in_conf`
    """

    def setUp(self):
        """
        Create a `GameConfiguration` object and add two jobs. The tests
        currently expects these two specific jobs being created
        """

        self.config = GameConfiguration.objects.create()

        # create non-repeatable job
        JobInConfiguration.objects.create(
            configuration=self.config,
            repeatable=False,
            order=1,
            job=StudentJobTemplate.objects.create(
                job=Job.Secretary,
                country=CountryStatic.objects.create(
                    starting_ip=100,
                    population=10
                )
            )
        )

        # create repeatable job
        JobInConfiguration.objects.create(
            configuration=self.config,
            repeatable=True,
            order=2,
            job=StudentJobTemplate.objects.create(
                job=Job.Secretary,
                country=CountryStatic.objects.create(
                    starting_ip=100,
                    population=10
                )
            )
        )

    def test_when_first_pass_true_non_repeatable_job_may_be_returned(self):
        """
        Test that when we are going through the jobs in the configuration for
        the first time (ie. `first_pass=True`), non-repeatable jobs can be
        returned
        """

        exp_job = JobInConfiguration.objects.get(repeatable=False)

        # test with `first_pass=True`
        returned_job = self.config.get_next_job_in_conf(0, True)

        self.assertFalse(returned_job.repeatable)
        self.assertEqual(returned_job.id, exp_job.id)

    def test_when_first_pass_false_non_repeatable_job_not_returned(self):
        """
        Test that when we are going through the jobs in the configuration for
        another time (ie. `first_pass=False`), only repeatable jobs can be
        returned
        """
        exp_job = JobInConfiguration.objects.get(repeatable=True)

        # test what happens when `first_pass=False`
        returned_job = self.config.get_next_job_in_conf(1, False)

        self.assertTrue(returned_job.repeatable)
        self.assertEqual(returned_job.id, exp_job.id)

    def test_after_last_job_order_function_starts_from_0_with_first_pass_false(self):
        """
        Test that when we the function is called with `last_order` larger
        than the highest ordered job, then the function will 'wrap around' and
        return the lowest numbered repeatable job instead (shoul ideally
        be tested with @patch and checked that `get_next_job_in_conf` is
        called once with arguments (0, False))
        """
        # create last repeatable job
        JobInConfiguration.objects.create(
            configuration=self.config,
            repeatable=True,
            order=3,
            job=StudentJobTemplate.objects.create(
                job=Job.Secretary,
                country=CountryStatic.objects.create(
                    starting_ip=100,
                    population=10
                )
            )
        )

        # get lowest numbered repeatable job
        exp_job = JobInConfiguration.objects.filter(
            repeatable=True
        ).order_by('order').first()

        # we set `last_order=3` which is the highest ordered job
        returned_job = self.config.get_next_job_in_conf(3, True)

        self.assertTrue(returned_job.repeatable)
        self.assertEqual(returned_job.id, exp_job.id)
