#pylint: disable=missing-docstring

import time

import digitalocean

MASTER_DOMAIN = "europeatwork.org"
GAME_MASTER_TAG = "game_master"

# Add stuff like allowed hosts, RUN_GAME_SERVER_LAUNCHER etc
# Or we could just overwrite the cronfiles on the new machines so the task isnt run

CLOUD_CONFIG = """#cloud-config

write_files:
  - path: /var/www/europealive/backend/core/settings/slave_env.py
    owner: www-data:www-data
    permissions: '0644'
    content: |
      # NOTE: The existence of this file also determines which cronjobs run!
      ALLOWED_HOSTS = ["{domain}"]
      RUN_GAME_SERVER_LAUNCHER = False
      IS_MASTER_SERVER = False
      GAME_LAUNCHER_ID = {game_launcher_id}
"""


def snapshot_master(token, droplet_name):
    manager = digitalocean.Manager(token=token)

    game_master = manager.get_all_droplets(tag_name=GAME_MASTER_TAG).pop()
    time.sleep(5)
    return game_master.take_snapshot(droplet_name, return_dict=False)


def create_droplet(token, snapshot_id, name, slug, size, game_launcher_id):
    manager = digitalocean.Manager(token=token)
    keys = manager.get_all_sshkeys()

    variables = {
        "domain": ".".join([slug, MASTER_DOMAIN]),
        "game_launcher_id": game_launcher_id,
    }

    droplet = digitalocean.Droplet(
        token=token,
        name=name,
        region='fra1',  # Frankfurt
        image=snapshot_id,
        size_slug=size,  # Needs to be the the same size as the original or bigger
        ssh_keys=keys,
        backups=False,
        user_data=CLOUD_CONFIG.format(**variables),
        tags=["europa-game"],
    )

    droplet.create()
    action_id = droplet.action_ids.pop()
    action = droplet.get_action(action_id)
    return action


def resize_droplet(token, droplet_id, size):
    manager = digitalocean.Manager(token=token)
    droplet = manager.get_droplet(droplet_id=droplet_id)
    return droplet.resize(size, return_dict=False)


def droplet_power_on(token, droplet_id):
    manager = digitalocean.Manager(token=token)
    droplet = manager.get_droplet(droplet_id=droplet_id)
    return droplet.power_on(return_dict=False)


def get_snapshot(token, droplet_name):
    manager = digitalocean.Manager(token=token)

    for snapshot in manager.get_all_snapshots():
        if snapshot.name == droplet_name:
            return snapshot
    return None


def create_subdomain(token, slug, droplet_id):
    domain = digitalocean.Domain(token=token, name=MASTER_DOMAIN)
    manager = digitalocean.Manager(token=token)
    droplet = manager.get_droplet(droplet_id)

    record = domain.create_new_domain_record(
        type="A",
        name=slug,
        data=droplet.ip_address,
    )
    return record


def delete_snapshots(token, droplet_name):
    manager = digitalocean.Manager(token=token)
    for snap in manager.get_all_snapshots():
        if snap.name == droplet_name:
            snap.destroy()


def delete_droplet(token, droplet_id):
    manager = digitalocean.Manager(token=token)
    droplet = manager.get_droplet(droplet_id)
    return droplet.destroy()


def delete_subdomain(token, slug):
    domain = digitalocean.Domain(token=token, name=MASTER_DOMAIN)
    records = domain.get_records()

    for record in records:
        if record.name == slug:
            record.destroy()
