"""
Custom Permission classes
"""
from rest_framework import permissions


class IsAuthenticatedAndOwner(permissions.BasePermission):
    """
    Custom Permission class to only allow owners to access objects
    """

    # pylint: disable=unused-argument
    def has_object_permission(self, request, view, obj):
        """
        check for ownership
        """
        try:
            return obj.owner.id == request.user.id
        except AttributeError:
            return True
