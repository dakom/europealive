"""
Define views for handling GameLaunchers
"""
from rest_framework import exceptions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from gamelauncher.models import APIUser, GameLauncher
from gamelauncher.permissions import IsAuthenticatedAndOwner
from gamelauncher.serializers import GameLauncherSerializer


class GameLauncherViewSet(viewsets.ModelViewSet):
    """
    Basic ViewSet for manipulating and retrieving GameLaunchers
    """
    queryset = GameLauncher.objects.all()
    serializer_class = GameLauncherSerializer
    permission_classes = [IsAuthenticatedAndOwner]

    def get_queryset(self):
        """
        Return only users own gamelaunchers
        """
        user = self.request.user
        if not user.is_authenticated:
            raise exceptions.NotAuthenticated

        return GameLauncher.objects.filter(owner=user)

    @action(detail=False, methods=['get'])
    def check_slug(self, request):
        """
        Check that a given slug is available
        """
        slug = request.GET.get('slug')
        gamelauncher_id = request.GET.get('gamelauncher_id')
        if GameLauncher.objects.filter(
            slug=slug,
            deleted=False
        ).exclude(id=gamelauncher_id).exists():
            return Response({ 'available': False })
        else:
            return Response({ 'available': True })

    def perform_create(self, serializer):
        """
        Set owner when creating new GameLauncher
        """
        serializer.save(owner=self.apiUser)

    @property
    def apiUser(self):
        """
        Shorthand for APIUser.objects.get(id=self.request.user.id)
        """
        return APIUser.objects.get(id=self.request.user.id)
