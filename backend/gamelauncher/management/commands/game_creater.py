"""
Command to be run when a Game has to be created
"""
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from core.models import Facilitator
from events.models import BaseEvent
from game.models import Game
from gamelauncher.models import GameLauncher


class Command(BaseCommand): #pylint: disable=missing-docstring
    help = 'Run the command that to make sure a game exists on the game server. '\
           'Should never be run on master, since it must never have a game in db.'

    def print(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def manage_facilitators(self, game_launcher):
        # make Facilitators able to access the backend
        try:
            group = Group.objects.get(name='Spilleder')
            facilitators = Facilitator.objects.filter(
                partner_organization=game_launcher.owner
            )
            for facilitator in facilitators:
                group.user_set.add(facilitator)
                facilitator.is_staff = True
                facilitator.save()

        except Group.DoesNotExist:
            pass

        # delete Facilitators from other organizations
        Facilitator.objects.exclude(
            partner_organization=game_launcher.owner
        ).delete()

    def handle(self, *args, **options):
        assert not settings.IS_MASTER_SERVER

        if not Game.objects.all().exists():
            game_launcher = GameLauncher.objects.get(
                id=settings.GAME_LAUNCHER_ID
            )

            self.manage_facilitators(game_launcher)

            game = Game.objects.create(
                name=game_launcher.slug,
                starting_time=game_launcher.gamestart_at,
                configuration=game_launcher.gameconfig,
                last_job_ordering=0,
            )

            game.create_teams()

            for event in BaseEvent.objects.all():
                event.set_display_time(game.starting_time)
