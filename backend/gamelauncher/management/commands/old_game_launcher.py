import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from gamelauncher.api import (
    snapshot_master, get_snapshot, create_droplet,
    create_subdomain
)
from gamelauncher.models import GameLauncher


class Command(BaseCommand):
    help = "Run the command that launcher and deletes game droplets"

    def print(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def handle(self, *args, **options):
        is_master_server = getattr(settings, "IS_MASTER_SERVER", False)

        if not is_master_server:
            self.stdout.write(self.style.ERROR(
                "Not running this command unless IS_MASTER_SERVER = True"))
            return

        now = timezone.now()

        games_to_create = GameLauncher.objects.filter(
            create_at__gte=now, launch_completed=False
        ).values_list("pk", flat=True)

        token = os.environ.get("DIGITALOCEAN_ACCESS_TOKEN")

        def get_game_or_none(primary_key):
            try:
                return GameLauncher.objects.select_for_update(
                    skip_locked=True).get(pk=primary_key)
            except GameLauncher.DoesNotExist:
                return None

        for gid in games_to_create:
            self.stdout.write("%s checking game id %s" % (str(timezone.now()), gid))

            with transaction.atomic():

                game = get_game_or_none(gid)

                if game is not None and not game.snapshot_created:
                    self.print("Creating snapshot of game_master server")

                    snapshot_action = snapshot_master(token, game.droplet_name)
                    snapshot_action.wait(20)

                    game.snapshot_created = True
                    game.save()

            with transaction.atomic():

                game = get_game_or_none(gid)

                if game is not None and not game.droplet_created:
                    self.print("Creating droplet from snapshot")

                    snapshot = get_snapshot(token, game.droplet_name)
                    create_action = create_droplet(
                        token, snapshot.id, game.droplet_name,
                        game.slug, game.server_size, game.id
                    )
                    create_action.wait(20)

                    game.droplet_id = create_action.resource_id
                    game.droplet_created = True
                    game.save()

            with transaction.atomic():
                game = get_game_or_none(gid)

                if game is not None and not game.dns_created:
                    self.print("Setting DNS")

                    create_subdomain(token, game.slug, game.droplet_id)

                    game.dns_created = True
                    game.save()

            with transaction.atomic():
                game = get_game_or_none(gid)

                if game is not None and game.all_done():
                    game.launch_completed = True
                    game.save()
            self.print("Done")
