"""
Command for creating and upscaling droplets
"""
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from gamelauncher.api import (create_droplet, create_subdomain,
                              droplet_power_on, get_snapshot, resize_droplet,
                              snapshot_master)
from gamelauncher.models import GameLauncher


class Command(BaseCommand):
    """ Manage gamelaunchers """

    help = "Run the command that launches and upscales game droplets"

    def create_droplets(self, token):
        """
        Create droplets from gamelaunchers with create_at today in a thread
        safe manner
        """

        # select_for_update locks all rows until the end of the transaction
        gamelaunchers = GameLauncher.objects.select_for_update(
            # skip_locked makes sure, that we skip any instances with a lock
            skip_locked=True
        ).filter(
            create_at__lte=timezone.now(), launch_completed=False
        )

        # initiate transaction and ensure atomicity to avoid race conditions
        # and multiple calls to the api
        with transaction.atomic():

            for launcher in gamelaunchers:

                # start by taking a snapshot of the master server
                if not launcher.snapshot_created:
                    action = snapshot_master(token, launcher.droplet_name)
                    action.wait(20)

                    launcher.snapshot_created = True
                    launcher.save()

                # then create a droplet from that snapshot and store its id
                if not launcher.droplet_created:
                    snapshot = get_snapshot(token, launcher.droplet_name)
                    action = create_droplet(
                        token, snapshot.id, launcher.droplet_name,
                        launcher.slug, launcher.server_size, launcher.id
                    )
                    action.wait(20)

                    launcher.droplet_id = action.resource_id
                    launcher.droplet_created = True
                    launcher.save()

                # create subdomain (eg. `slug.europeatwork.org`) for new droplet
                if not launcher.dns_created:
                    create_subdomain(token, launcher.slug, launcher.droplet_id)

                    launcher.dns_created = True
                    launcher.save()

                # register that all is well and done
                if launcher.all_done():
                    launcher.launch_completed = True
                    launcher.save()

    def upscale_droplets(self, token):
        """
        Thread-safe function for upscaling droplets if their game begins today.

        TODO: Add support for manually specifying the new droplet size. Note,
        that it must have a disk size that is equal to or greater than that
        before (https://slugs.do-api.dev/)
        """
        #pylint: disable=no-member

        # select_for_update locks all rows until the end of the transaction
        gamelaunchers = GameLauncher.objects.select_for_update(
            # skip_locked makes sure, that we skip any instances with a lock
            skip_locked=True
        ).filter(
            upscaled=False,
            launch_completed=True,
            deleted=False,
            gamestart_at__date=timezone.now().date()
        )

        # initiate transaction and ensure atomicity to avoid race conditions
        # and multiple calls to the api
        with transaction.atomic():

            for launcher in gamelaunchers:

                # maybe add support for a specific new droplet size
                droplet_id = launcher.droplet_id
                size = GameLauncher.UPSCALE_SIZE

                # do the resizing and wait for completion
                action = resize_droplet(token, droplet_id, size)
                action.wait()

                launcher.server_size = size
                launcher.upscaled = True
                launcher.save()

                # droplet must be turned on again after resize
                action = droplet_power_on(token, droplet_id)
                action.wait()

    def print(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def handle(self, *args, **kwargs):

        is_master_server = getattr(settings, "IS_MASTER_SERVER", False)
        if not is_master_server:
            self.stdout.write(
                self.style.ERROR(
                    "Not running this command unless IS_MASTER_SERVER = True"
                )
            )
            return

        token = os.environ.get("DIGITALOCEAN_ACCESS_TOKEN")
        self.create_droplets(token)
        self.upscale_droplets(token)
