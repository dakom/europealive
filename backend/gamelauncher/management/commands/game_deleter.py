import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from gamelauncher.api import (
    delete_snapshots, delete_droplet, delete_subdomain
)
from gamelauncher.models import GameLauncher


class Command(BaseCommand):
    help = 'Run the command that deletes game droplets'

    def print(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def handle(self, *args, **options):
        # is_game_server = os.path.isfile("/game_server")
        is_master_server = getattr(settings, "IS_MASTER_SERVER", False)

        if not is_master_server:
            self.stdout.write(self.style.ERROR(
                "Not running this command unless IS_MASTER_SERVER = True"))
            return

        now = timezone.now()

        games_to_delete = GameLauncher.objects.filter(
            delete_at__lte=now.date(), launch_completed=True,
            deleted=False,
        )
        token = os.environ.get('DIGITALOCEAN_ACCESS_TOKEN')

        for game in games_to_delete:

            if game.snapshot_created:
                self.print("Deleting snapshot for game %s" % game.slug)
                delete_snapshots(token, game.droplet_name)
                game.snapshot_created = False
                game.save()

            if game.droplet_created:
                self.print("Deleting droplet")
                delete_droplet(token, game.droplet_id)
                game.droplet_created = False
                game.save()

            if game.dns_created:
                self.print("Deleting DNS")
                delete_subdomain(token, game.slug)
                game.dns_created = False
                game.save()

            if not game.all_done():
                game.deleted = True
                game.save()
