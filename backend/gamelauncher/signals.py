# pylint: disable=unused-argument
"""
Signals for gamelauncher app
"""
from django.db.models.signals import post_init, post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from rest_framework.authtoken.models import Token

from gamelauncher.models import APIUser, GameLauncher
from meta.models import GameConfiguration


@receiver(post_save, sender=APIUser)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Create an API token when a new APIUser is created
    """
    if created:
        Token.objects.create(user=instance)

@receiver(pre_save, sender=GameLauncher)
def update_gameconfig_before_save(sender, instance, **kwargs):
    """
    Set GameConfiguration based on num_students
    """
    if not hasattr(instance, 'gameconfig'):
        config = GameConfiguration.objects.filter(
            max_students__gte=instance.num_students
        ).order_by('max_students').first()
        instance.gameconfig = config
