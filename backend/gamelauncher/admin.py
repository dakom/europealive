#pylint: disable=missing-docstring
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from gamelauncher.models import (
    GameLauncher, APIUser
)

@admin.register(APIUser)
class APIUserAdmin(UserAdmin):
    def token(self, obj):
        if hasattr(obj, 'auth_token'):
            return obj.auth_token
        return ''

    fieldsets = (
        ('User', { 'fields': ('username', 'password') }),
        ('API', { 'fields': ('token',) }),
    )

    readonly_fields = ('token', )


@admin.register(GameLauncher)
class GameLaunchAdmin(admin.ModelAdmin):
    """ Admin view of Round model """
    list_display = [
        "slug", "create_at", "droplet_name",
        "snapshot_created", "droplet_created", "dns_created",
        "delete_at", "launch_completed", "deleted"
    ]

    def get_readonly_fields(self, request, obj=None):
        """
        If snapshot has been created, the server creation process has started. At this point,
        fields are marked read-only as changes would possibly not be effective.
        """
        if obj and obj.snapshot_created:
            return ["slug", "create_at", "gamestart_at", "gameconfig", "server_size"]
        return []
