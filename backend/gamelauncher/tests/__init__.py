""" Test module for gamelauncher app """
from gamelauncher.tests.test_commands import (CreateDropletsTestCase,
                                              GameCreaterTestCase,
                                              RaceCondTestCase,
                                              ResizeDropletsTestCase)
from gamelauncher.tests.test_signals import UpdateGameconfigBeforeSaveTestCase
