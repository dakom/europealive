from django.test import TestCase
from django.utils import timezone

from gamelauncher.models import GameLauncher
from meta.models import GameConfiguration


class UpdateGameconfigBeforeSaveTestCase(TestCase):
    """
    Tests of `gamelauncher.signals.update_gameconfig_before_save`
    """
    def setUp(self):
        student_counts = [None, 100, 20, None, 65]
        for max_students in student_counts:
            GameConfiguration.objects.create(
                name=f'config {max_students}',
                max_students=max_students
            )


    def test_gameconfiguration_is_added_after_create(self):
        """
        The signal should add a gameconfiguration
        """

        today = timezone.now().date()
        launcher = GameLauncher(
            create_at=today + timezone.timedelta(days=10),
            gamestart_at=timezone.now() + timezone.timedelta(days=15),
            delete_at=today + timezone.timedelta(days=17),
            num_students=20
        )
        launcher.save()

        self.assertIsNotNone(launcher.gameconfig)

    def test_gameconfigurations_with_max_students_none_are_ignored(self):
        """
        Test that GameConfigurations with None in max_students are ignored
        """
        today = timezone.now().date()
        launcher = GameLauncher(
            create_at=today + timezone.timedelta(days=10),
            gamestart_at=timezone.now() + timezone.timedelta(days=15),
            delete_at=today + timezone.timedelta(days=17),
            num_students=0
        )
        launcher.save()

        self.assertIsNotNone(launcher.gameconfig)
        self.assertIsNotNone(launcher.gameconfig.max_students)

    def test_gameconfigurations_with_smallest_allowed_max_students_are_selected(self):
        """
        Test that GameConfigurations with smallest max_students are chosen
        """
        today = timezone.now().date()
        launcher = GameLauncher(
            create_at=today + timezone.timedelta(days=10),
            gamestart_at=timezone.now() + timezone.timedelta(days=15),
            delete_at=today + timezone.timedelta(days=17),
            num_students=50
        )
        launcher.save()

        self.assertEqual(launcher.gameconfig.max_students, 65)

    def test_gameconfig_is_not_updated_if_create_at_is_today(self):
        """
        Title says it all
        """
        today = timezone.now().date()
        launcher = GameLauncher(
            create_at=today,
            gamestart_at=timezone.now() + timezone.timedelta(days=15),
            delete_at=today + timezone.timedelta(days=17),
            gameconfig=GameConfiguration.objects.get(max_students=100),
            num_students=70
        )
        launcher.save()

        launcher.num_students = 60
        launcher.save()
        launcher.refresh_from_db()

        self.assertEqual(launcher.gameconfig.max_students, 100)
