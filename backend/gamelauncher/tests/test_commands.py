import concurrent.futures
import logging
import time as timing
from datetime import date, datetime, time, timedelta
from io import StringIO
from unittest.mock import Mock, call, patch

from django.contrib.auth.models import Group
from django.core.management import call_command
from django.db import connections
from django.db.models import signals
from django.test import TestCase, TransactionTestCase, override_settings
from django.utils import timezone

from core.models import Facilitator
from events.models import BaseEvent
from game.models import Game
from gamelauncher.management.commands.game_creater import \
    Command as game_creater_command
from gamelauncher.management.commands.game_launcher import Command
from gamelauncher.models import APIUser, GameLauncher
from gamelauncher.signals import update_gameconfig_before_save
from meta.models import GameConfiguration


@override_settings(GAME_LAUNCHER_ID=42)
class GameCreaterTestCase(TestCase):
    """
    Test of `manage.py game_creater`
    """

    PATH = 'gamelauncher.management.commands.game_creater'

    def setUp(self):
        """
        Disable logging
        """
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        """
        Re-enable logging
        """
        logging.disable(logging.NOTSET)

    @override_settings(IS_MASTER_SERVER=True)
    def test_assertion_error_raised_if_on_master_server(self):
        """
        Test that this function will never run if
        `settings.IS_MASTER_SERVER=True`
        """
        self.assertRaises(AssertionError, call_command, 'game_creater')

    @override_settings(IS_MASTER_SERVER=False)
    @patch(f'{PATH}.GameLauncher.objects.get')
    @patch('gamelauncher.management.commands.game_creater.Command.manage_facilitators')
    def test_gamelauncher_is_chosen_from_id_in_settings(
        self, mock_manage_facilitators, mock_get_gamelauncher
    ):
        """
        Test that a gamelauncher with ID as specified in settings is used
        """
        # we use make_aware here to not get annoying RuntimeWarning
        gamestart = timezone.make_aware(
            timezone.datetime(1991, 12, 27, 9)
        )
        config = GameConfiguration.objects.create()
        mock_get_gamelauncher.return_value = Mock(**{
            'slug': 'test-game',
            'gamestart_at': gamestart,
            'gameconfig': config
        })

        call_command('game_creater')

        mock_get_gamelauncher.assert_called_once_with(id=42)
        self.assertEqual(Game.objects.count(), 1)

        game = Game.objects.get()
        self.assertEqual(game.name, 'test-game')
        self.assertEqual(game.starting_time, gamestart)
        self.assertEqual(game.configuration, config)
        self.assertEqual(game.last_job_ordering, 0)

    @override_settings(IS_MASTER_SERVER=False)
    @patch('gamelauncher.management.commands.game_creater.Command.manage_facilitators')
    @patch(f'{PATH}.BaseEvent.set_display_time')
    @patch(f'{PATH}.GameLauncher.objects.get')
    @patch(f'{PATH}.Game.objects.create')
    def test_create_teams_and_set_display_time_is_called(
        self, mock_game_create, mock_gamelauncher_get,
        mock_set_display_time, mock_manage_facilitators
    ):
        """
        When a Game is created, `Game.create_teams()` and
        `BaseEvent.set_display_time(gamestart)` should be called (the last for
        all events)
        """
        gamestart = timezone.datetime(1991, 12, 27, 9)
        mock_game = Mock(starting_time=gamestart)
        mock_game_create.return_value = mock_game

        # create some events
        for name, offset in [('a', 18), ('b', 7), ('c', 10)]:
            BaseEvent.objects.create(name=name, offset=offset)

        call_command('game_creater')

        mock_game.create_teams.assert_called_once()
        mock_set_display_time.assert_has_calls([
            call(gamestart), call(gamestart), call(gamestart)
        ])


class ManageFacilitatorsTestCase(TestCase):
    """
    Test the function `Command.manage_facilitators`
    """
    def setUp(self):
        """
        Create two partner organizations and four Facilitators
        """
        Group.objects.create(name='Spilleder')
        config = GameConfiguration.objects.create(
            name='default',
            max_students=250
        )

        create_at = date(2021, 10, 10)
        gamestart_at = timezone.make_aware(datetime(2021, 10, 10, 10))
        delete_at = date(2021, 10, 20)

        partners = [ 'partner1', 'partner2' ]
        users = [ f'user{i}' for i in range(1,5) ]
        for partner in partners:
            new_partner = APIUser.objects.create_user(
                username=partner,
                password='whatever'
            )
            GameLauncher.objects.create(
                create_at=create_at,
                gamestart_at=gamestart_at,
                delete_at=delete_at,
                num_students=120,
                gameconfig=config,
                owner=new_partner
            )
            for _ in range(2):
                Facilitator.objects.create_user(
                    partner_organization=new_partner,
                    username=users.pop(),
                    password='whatever'
                )

    def test_partner_facilitators_gets_group_and_admin_status(self):
        """
        Test that the facilitators from the relevant GameLauncher is given staff
        status and added to the Spilleder group. And at the same time, test
        that facilitators from other partners are deleted
        """
        launcher = GameLauncher.objects.first()
        command = game_creater_command()
        command.manage_facilitators(launcher)

        for facilitator in Facilitator.objects.filter(
            partner_organization=launcher.owner
        ):
            self.assertTrue(facilitator.is_staff)
            self.assertTrue(
                facilitator.groups.filter(name='Spilleder').exists()
            )

        other_partner_facilitators = Facilitator.objects.exclude(
            partner_organization=launcher.owner
        )
        self.assertFalse(other_partner_facilitators.exists())

    def test_that_external_users_are_still_deleted_if_group_does_not_exist(self):
        """
        The function should just delete the external facilitators if the
        Spilleder group for some reason does not exist
        """
        Group.objects.get(name='Spilleder').delete()

        launcher = GameLauncher.objects.first()
        command = game_creater_command()
        command.manage_facilitators(launcher)

        for facilitator in Facilitator.objects.filter(
            partner_organization=launcher.owner
        ):
            self.assertFalse(facilitator.is_staff)
            self.assertFalse(
                facilitator.groups.filter(name='Spilleder').exists()
            )

        other_partner_facilitators = Facilitator.objects.exclude(
            partner_organization=launcher.owner
        )
        self.assertFalse(other_partner_facilitators.exists())

    @override_settings(IS_MASTER_SERVER=False)
    @patch('gamelauncher.management.commands.game_creater.Command.manage_facilitators')
    def test_function_is_called_when_not_master_server(self, mock_func):
        launcher = GameLauncher.objects.last()
        GAME_LAUNCHER_ID = launcher.id
        with override_settings(GAME_LAUNCHER_ID=GAME_LAUNCHER_ID):
            call_command('game_creater')

        mock_func.assert_called_once_with(launcher)


@patch("gamelauncher.management.commands.game_launcher.create_subdomain")
@patch("gamelauncher.management.commands.game_launcher.create_droplet")
@patch("gamelauncher.management.commands.game_launcher.snapshot_master")
@patch("gamelauncher.management.commands.game_launcher.get_snapshot")
class CreateDropletsTestCase(TestCase):

    def setUp(self):
        """
        Disable signal that update gameconfig (not necessary here)
        """
        signals.pre_save.disconnect(
            update_gameconfig_before_save,
            sender=GameLauncher
        )

    def tearDown(self):
        """
        Enable the signals again after tests
        """
        signals.pre_save.connect(
            update_gameconfig_before_save,
            sender=GameLauncher
        )

    def test_api_functions_called_on_ready_game_launchers(
        self, mock_get_snapshot, mock_snapshot_master, mock_create_droplet, mock_create_subdomain
    ):

        # empty config for creating GameLaunchers
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        # two GameLaunchers for creationg (one today, one yesterday)
        create1 = GameLauncher.objects.create(
            slug='create1',
            create_at=timezone.now(),
            gamestart_at=timezone.now() + timedelta(days=1),
            delete_at=timezone.now() + timedelta(days=2),
            gameconfig=config,
            num_students=0
        )
        create2 = GameLauncher.objects.create(
            slug='create2',
            create_at=timezone.now() - timedelta(days=1),
            gamestart_at=timezone.now() + timedelta(days=1),
            delete_at=timezone.now() + timedelta(days=2),
            gameconfig=config,
            num_students=0
        )

        # define mocks
        mock_action = Mock(wait=Mock(return_value=True))
        mock_get_snapshot.return_value = Mock(id=42)
        mock_snapshot_master.return_value = Mock(wait=Mock(return_value=True))
        mock_create_droplet.return_value = Mock(
            wait=Mock(return_value=True),
            resource_id=42
        )
        mock_create_subdomain.return_value = Mock(wait=Mock(return_value=True))

        # run function to test
        cmd = Command()
        cmd.create_droplets('token')

        # refresh GameLaunchers
        create1.refresh_from_db()
        create2.refresh_from_db()

        # assert that fields have been updated correctly
        self.assertEqual(create1.droplet_id, '42')
        self.assertTrue(create1.snapshot_created)
        self.assertTrue(create1.droplet_created)
        self.assertTrue(create1.dns_created)
        self.assertTrue(create1.launch_completed)
        self.assertFalse(create1.upscaled)

        self.assertEqual(create2.droplet_id, '42')
        self.assertTrue(create2.snapshot_created)
        self.assertTrue(create2.droplet_created)
        self.assertTrue(create2.dns_created)
        self.assertTrue(create2.launch_completed)
        self.assertFalse(create2.upscaled)

        # define expected calls to mock functions
        snap_calls = [
            call('token', create1.droplet_name),
            call('token', create2.droplet_name)
        ]

        create_droplet_calls = [
            call(
                'token', 42, create1.droplet_name,
                create1.slug, create1.server_size, create1.id
            ),
            call(
                'token', 42, create2.droplet_name,
                create2.slug, create2.server_size, create2.id
            )
        ]

        subdomain_calls = [
            call('token', create1.slug, create1.droplet_id),
            call('token', create2.slug, create2.droplet_id),
        ]

        # assert mock functions have been called correctly
        mock_snapshot_master.has_calls(snap_calls)
        self.assertEqual(mock_snapshot_master.call_count, 2)

        mock_get_snapshot.has_calls(snap_calls)
        self.assertEqual(mock_get_snapshot.call_count, 2)

        mock_create_droplet.has_calls(create_droplet_calls)
        self.assertEqual(mock_create_droplet.call_count, 2)

        mock_create_subdomain.has_calls(subdomain_calls)
        self.assertEqual(mock_create_subdomain.call_count, 2)

    def test_launcher_with_launch_completed_is_skipped(
        self, mock_get_snapshot, mock_snapshot_master, mock_create_droplet, mock_create_subdomain
    ):
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        launcher = GameLauncher.objects.create(
            slug='skip2',
            create_at=timezone.now() - timedelta(days=1),
            gamestart_at=timezone.now() + timedelta(days=2),
            delete_at=timezone.now() + timedelta(days=3),
            gameconfig=config,
            num_students=0,
            droplet_id='99',
            snapshot_created=True,
            droplet_created=True,
            dns_created=True,
            launch_completed=True
        )

        mock_action = Mock(wait=Mock(return_value=True))
        mock_get_snapshot.return_value = Mock(id=42)

        mock_snapshot_master.return_value = Mock(wait=Mock(return_value=True))
        mock_create_droplet.return_value = Mock(
            wait=Mock(return_value=True),
            resource_id=42
        )
        mock_create_subdomain.return_value = Mock(wait=Mock(return_value=True))

        cmd = Command()
        cmd.create_droplets('token')

        launcher.refresh_from_db()

        self.assertEqual(launcher.droplet_id, '99')
        self.assertTrue(launcher.snapshot_created)
        self.assertTrue(launcher.droplet_created)
        self.assertTrue(launcher.dns_created)
        self.assertTrue(launcher.launch_completed)
        self.assertFalse(launcher.upscaled)

        mock_get_snapshot.assert_not_called()
        mock_snapshot_master.assert_not_called()
        mock_create_droplet.assert_not_called()
        mock_create_subdomain.assert_not_called()

    def test_launcher_with_create_at_in_future_is_skipped(
        self, mock_get_snapshot, mock_snapshot_master, mock_create_droplet, mock_create_subdomain
    ):
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        launcher = GameLauncher.objects.create(
            slug='skip1',
            create_at=timezone.now() + timedelta(days=1),
            gamestart_at=timezone.now() + timedelta(days=2),
            delete_at=timezone.now() + timedelta(days=3),
            gameconfig=config,
            num_students=0,
        )

        mock_action = Mock(wait=Mock(return_value=True))
        mock_get_snapshot.return_value = Mock(id=42)

        mock_snapshot_master.return_value = Mock(wait=Mock(return_value=True))
        mock_create_droplet.return_value = Mock(
            wait=Mock(return_value=True),
            resource_id=42
        )
        mock_create_subdomain.return_value = Mock(wait=Mock(return_value=True))


        cmd = Command()
        cmd.create_droplets('token')

        launcher.refresh_from_db()

        self.assertEqual(launcher.droplet_id, '')
        self.assertFalse(launcher.snapshot_created)
        self.assertFalse(launcher.droplet_created)
        self.assertFalse(launcher.dns_created)
        self.assertFalse(launcher.launch_completed)
        self.assertFalse(launcher.upscaled)

        mock_get_snapshot.assert_not_called()
        mock_snapshot_master.assert_not_called()
        mock_create_droplet.assert_not_called()
        mock_create_subdomain.assert_not_called()


@patch("gamelauncher.management.commands.game_launcher.resize_droplet")
@patch("gamelauncher.management.commands.game_launcher.droplet_power_on")
class ResizeDropletsTestCase(TestCase):

    def test_launcher_with_gamestart_today_gets_upscaled(
        self, mock_power_on, mock_resize_droplet
    ):

        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        gamestart = timezone.make_aware(timezone.datetime.combine(
            timezone.now().date(), time(9)
        ))

        launcher = GameLauncher.objects.create(
            slug='launcher1',
            create_at=timezone.now() - timedelta(days=1),
            gamestart_at=gamestart,
            delete_at=timezone.now() + timedelta(days=1),
            gameconfig=config,
            num_students=0,
            droplet_id='42',
            snapshot_created=True,
            droplet_created=True,
            dns_created=True,
            launch_completed=True
        )

        self.assertFalse(launcher.upscaled)

        cmd = Command()
        cmd.upscale_droplets('token')

        launcher.refresh_from_db()

        self.assertTrue(launcher.upscaled)
        self.assertEqual(launcher.server_size, GameLauncher.UPSCALE_SIZE)

        mock_resize_droplet.assert_called_once_with(
            'token', '42', GameLauncher.UPSCALE_SIZE
        )
        mock_power_on.assert_called_once_with(
            'token', '42'
        )

    def test_launcher_with_gamestart_in_future_is_skipped(
        self, mock_power_on, mock_resize_droplet
    ):

        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        gamestart = timezone.make_aware(timezone.datetime.combine(
            timezone.now().date(), time(9)
        ))

        launcher = GameLauncher.objects.create(
            slug='launcher1',
            create_at=timezone.now() - timedelta(days=1),
            gamestart_at=gamestart + timedelta(days=1),
            delete_at=timezone.now() + timedelta(days=2),
            gameconfig=config,
            droplet_id='42',
            num_students=0,
            snapshot_created=True,
            droplet_created=True,
            dns_created=True,
            launch_completed=True
        )

        self.assertFalse(launcher.upscaled)

        cmd = Command()
        cmd.upscale_droplets('token')

        launcher.refresh_from_db()

        self.assertFalse(launcher.upscaled)
        self.assertEqual(launcher.server_size, GameLauncher.BASIC)

        mock_resize_droplet.assert_not_called()
        mock_power_on.assert_not_called()

    def test_launcher_with_upscaled_true_is_skipped(
        self, mock_power_on, mock_resize_droplet
    ):

        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        gamestart = timezone.make_aware(timezone.datetime.combine(
            timezone.now().date(), time(9)
        ))

        launcher = GameLauncher.objects.create(
            slug='launcher1',
            create_at=timezone.now() - timedelta(days=1),
            gamestart_at=gamestart,
            delete_at=timezone.now() + timedelta(days=2),
            gameconfig=config,
            num_students=0,
            server_size=GameLauncher.UPSCALE_SIZE,
            droplet_id='42',
            snapshot_created=True,
            droplet_created=True,
            dns_created=True,
            launch_completed=True,
            upscaled=True
        )

        cmd = Command()
        cmd.upscale_droplets('token')

        launcher.refresh_from_db()

        self.assertEqual(launcher.server_size, GameLauncher.UPSCALE_SIZE)

        mock_resize_droplet.assert_not_called()
        mock_power_on.assert_not_called()


@patch("gamelauncher.management.commands.game_launcher.Command.upscale_droplets")
@patch("gamelauncher.management.commands.game_launcher.Command.create_droplets")
class GameLauncherCommandTestCase(TestCase):
    def call_command(self, *args, **kwargs):
        out = StringIO()
        call_command(
            'game_launcher',
            *args,
            stdout=out,
            stderr=StringIO,
            **kwargs
        )
        return out.getvalue()

    @override_settings(IS_MASTER_SERVER=False)
    def test_command_is_not_run_if_not_on_master_server(
        self, mock_create_droplets, mock_upscale_droplets
    ):
        """ test that the gamelauncher command is only ever run on
        master server """
        # use --no-color to avoid styling
        out = self.call_command('--no-color')
        self.assertEqual(
            out,
            "Not running this command unless IS_MASTER_SERVER = True\n"
        )

        mock_create_droplets.assert_not_called()
        mock_upscale_droplets.assert_not_called()

    @override_settings(IS_MASTER_SERVER=True)
    @patch("os.environ.get")
    def test_create_and_upscale_droplets_using_token_from_env_on_master_server(
        self, mock_env_get, mock_create_droplets, mock_upscale_droplets
    ):
        """ test that both create_droplets and upscale_droplets are just called
        once each when invoking the command """
        mock_env_get.return_value = 'token'
        self.call_command()

        # os.environ.get is also called a couple of times by Django itself,
        # so we just look for one lookup of DIGITALOCEAN_ACCESS_TOKEN
        mock_env_get.assert_called_with('DIGITALOCEAN_ACCESS_TOKEN')
        mock_create_droplets.assert_called_once_with('token')
        mock_upscale_droplets.assert_called_once_with('token')


class RaceCondTestCase(TransactionTestCase):
    """
    Special testcase to test for race conditions on the game_launcher command.
    The tests uses a number of non-test functions that mocks some behaviour of
    the digitalocean api calls and, more importantly, uses time.sleep(1) to
    introduce some lag. Then, a ThreadPoolExecutor calls the command in 5
    concurrent threads after which we test for the expected results.
    """

    def setUp(self):
        """
        These counters are used to track that the model gets updated as expected
        """
        self.snapshot_counter = 0
        self.resource_id = 10

    def call_command(self, *args, **kwargs):
        """
        A wrappoer around `call_command()` that returns the output to stdout
        """
        out = StringIO()
        call_command(
            'game_launcher',
            *args,
            stdout=out,
            stderr=StringIO,
            **kwargs
        )
        return out.getvalue()

    def on_done(self, _):
        """
        Closes all db connections in the current thread. Should be called when
        a thread terminates (via .add_done_callback())
        """
        connections.close_all()

    def sleep(self, *args, seconds=0.1):
        """
        Wrapper arount time.sleep(1). Note that `time` is imported as `timing`
        so as to avoid naming conflicts with the python time object.
        """
        timing.sleep(seconds)
        return True

    def snapshot_master_side_effect(self, *args):
        """
        snapshot_master should return an action with a `wait()` function
        """
        self.sleep()
        return Mock(wait=Mock(side_effect=self.sleep))

    def get_snapshot_side_effect(self, *args):
        """
        get_snapshot should return a snapshot with an id. We create the ids by
        incrementing `self.snapshot_counter` on each call.
        """
        self.sleep()
        self.snapshot_counter += 1
        return Mock(id=self.snapshot_counter)

    def create_droplet_side_effect(self, *args):
        """
        create_droplet should return an action with a resource_id. We create
        the ids by incrementing `self.resource_id` on each call.
        """
        self.sleep()
        self.resource_id += 1
        return Mock(
            resource_id=self.resource_id,
            side_effect=self.sleep
        )

    def create_subdomain_side_effect(self, *args):
        """
        create_subdomain need not return anything
        """
        self.sleep()
        return True

    def resize_droplet_side_effect(self, *args):
        """
        resize_droplet should return an action with a function `wait()`
        """
        self.sleep()
        return Mock(wait=Mock(side_effect=self.sleep))

    def droplet_power_on_side_effect(self, *args):
        """
        droplet_power_on should return an action with a function `wait()`
        """
        self.sleep()
        return Mock(wait=Mock(side_effect=self.sleep))

    @patch("gamelauncher.management.commands.game_launcher.create_subdomain")
    @patch("gamelauncher.management.commands.game_launcher.create_droplet")
    @patch("gamelauncher.management.commands.game_launcher.snapshot_master")
    @patch("gamelauncher.management.commands.game_launcher.get_snapshot")
    def test_race_conditions_on_create_droplets(
        self, mock_get_snapshot, mock_snapshot_master,
        mock_create_droplet, mock_create_subdomain
    ):
        """
        Create 3 gamelaunchers and call the game_launcher command in 5 threads
        concurrently. Then test that the droplet_ids have been set correctly
        and that all the digitalocean api calls have each only been called 3
        times (with the expected parameters)
        """

        # empty config for creating GameLaunchers
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        # common arguments for all the test gamelaunchers
        kwargs = {
            'create_at': timezone.now(),
            'gamestart_at': timezone.now() + timedelta(days=1),
            'delete_at': timezone.now() + timedelta(days=2),
            'gameconfig': config,
            'num_students': 0,
        }

        # create 3 gamelaunchers
        launcher1 = GameLauncher.objects.create(
            slug='launcher1',
            **kwargs
        )
        launcher2 = GameLauncher.objects.create(
            slug='launcher2',
            **kwargs
        )
        launcher3 = GameLauncher.objects.create(
            slug='launcher3',
            **kwargs
        )

        # define mocks
        mock_snapshot_master.side_effect = self.snapshot_master_side_effect
        mock_get_snapshot.side_effect = self.get_snapshot_side_effect
        mock_create_droplet.side_effect = self.create_droplet_side_effect
        mock_create_subdomain.side_effect = self.create_subdomain_side_effect


        # run command in 5 threads
        cmd = Command()
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for _ in range(5):
                future = executor.submit(cmd.create_droplets, 'token')
                future.add_done_callback(self.on_done)

        # refresh GameLaunchers
        launcher1.refresh_from_db()
        launcher2.refresh_from_db()
        launcher3.refresh_from_db()

        # assert that fields have been updated correctly
        self.assertEqual(launcher1.droplet_id, '11')
        self.assertEqual(launcher2.droplet_id, '12')
        self.assertEqual(launcher3.droplet_id, '13')

        # define expected calls to mock functions
        snap_calls = [
            call('token', launcher1.droplet_name),
            call('token', launcher2.droplet_name),
            call('token', launcher3.droplet_name),
        ]

        create_droplet_calls = [
            call(
                'token', 1, launcher1.droplet_name,
                launcher1.slug, launcher1.server_size, launcher1.id
            ),
            call(
                'token', 2, launcher2.droplet_name,
                launcher2.slug, launcher2.server_size, launcher2.id
            ),
            call(
                'token', 3, launcher3.droplet_name,
                launcher3.slug, launcher3.server_size, launcher3.id
            ),
        ]

        subdomain_calls = [
            call('token', launcher1.slug, '11'),
            call('token', launcher2.slug, '12'),
            call('token', launcher3.slug, '13'),
        ]

        # assert mock functions have been called correctly
        mock_snapshot_master.has_calls(snap_calls)
        self.assertEqual(mock_snapshot_master.call_count, 3)

        mock_get_snapshot.has_calls(snap_calls)
        self.assertEqual(mock_get_snapshot.call_count, 3)

        mock_create_droplet.has_calls(create_droplet_calls)
        self.assertEqual(mock_create_droplet.call_count, 3)

        mock_create_subdomain.has_calls(subdomain_calls)
        self.assertEqual(mock_create_subdomain.call_count, 3)

    @patch("gamelauncher.management.commands.game_launcher.droplet_power_on")
    @patch("gamelauncher.management.commands.game_launcher.resize_droplet")
    def test_race_conditions_on_upscale_droplets(
        self, mock_resize_droplet, mock_droplet_power_on
    ):
        """
        Create 3 gamelaunchers and call the game_launcher command in 5 threads
        concurrently. Then test that the sizes have been set correctly, that
        upscaled is set to True for all the gamelaunchers, and that all the
        digitalocean api calls have each only been called 3 times (with the
        expected parameters)
        """

        # empty config for creating GameLaunchers
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        # common arguments for all the test gamelaunchers
        kwargs = {
            'create_at': timezone.now() - timedelta(days=1),
            'gamestart_at': timezone.now(),
            'delete_at': timezone.now() + timedelta(days=2),
            'gameconfig': config,
            'launch_completed': True,
            'num_students': 0,
        }

        # create 3 gamelaunchers
        launcher1 = GameLauncher.objects.create(
            slug='launcher1',
            droplet_id=1,
            **kwargs
        )
        launcher2 = GameLauncher.objects.create(
            slug='launcher2',
            droplet_id=2,
            **kwargs
        )
        launcher3 = GameLauncher.objects.create(
            slug='launcher3',
            droplet_id=3,
            **kwargs
        )

        # define mocks
        mock_resize_droplet.side_effect = self.resize_droplet_side_effect
        mock_droplet_power_on.side_effect = self.droplet_power_on_side_effect

        # run command in 5 threads
        threads = 5
        cmd = Command()
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for _ in range(threads):
                future = executor.submit(cmd.upscale_droplets, 'token')
                future.add_done_callback(self.on_done)

        # list of expected calls to populate
        resize_calls = []
        power_on_calls = []

        # for each launcher...
        for launcher in [launcher1, launcher2, launcher3]:
            # ...get a fresh instance from db
            launcher.refresh_from_db()

            # ...assert new size and upscaling
            self.assertEqual(launcher.server_size, GameLauncher.UPSCALE_SIZE)
            self.assertTrue(launcher.upscaled)

            # ...populate expected calls
            resize_calls.append(
                call('token', launcher.droplet_id, launcher.server_size)
            )
            power_on_calls.append(call('token', launcher.droplet_id))

        # assert that all our mocked api calls were treated as expected
        mock_resize_droplet.has_calls(resize_calls)
        self.assertEqual(mock_resize_droplet.call_count, 3)

        mock_droplet_power_on.has_calls(power_on_calls)
        self.assertEqual(mock_droplet_power_on.call_count, 3)


    @override_settings(IS_MASTER_SERVER=True)
    @patch("gamelauncher.management.commands.game_launcher.os")
    @patch("gamelauncher.management.commands.game_launcher.create_subdomain")
    @patch("gamelauncher.management.commands.game_launcher.create_droplet")
    @patch("gamelauncher.management.commands.game_launcher.snapshot_master")
    @patch("gamelauncher.management.commands.game_launcher.get_snapshot")
    @patch("gamelauncher.management.commands.game_launcher.droplet_power_on")
    @patch("gamelauncher.management.commands.game_launcher.resize_droplet")
    def test_launcher_with_create_at_and_gamestart_same_day(
        self, mock_resize_droplet, mock_droplet_power_on, mock_get_snapshot,
        mock_snapshot_master, mock_create_droplet, mock_create_subdomain,
        mock_os
    ):
        """
        Test that if a GameLauncher has the same create_at date as its gamestart
        date, then multiple calls to the game_launcher command wont begin
        resizing before the launch is completed
        """
        # empty config for creating GameLaunchers
        config = GameConfiguration.objects.create(
            name='test_config', description='test'
        )

        # common arguments for all the test gamelaunchers
        kwargs = {
            'create_at': timezone.now().date(),
            'gamestart_at': timezone.now(),
            'delete_at': timezone.now() + timedelta(days=2),
            'gameconfig': config,
            'num_students': 0,
        }

        # create gamelauncher
        launcher1 = GameLauncher.objects.create(
            slug='launcher1',
            **kwargs
        )

        # define mocks
        mock_snapshot_master.side_effect = self.snapshot_master_side_effect
        mock_get_snapshot.side_effect = self.get_snapshot_side_effect
        mock_create_droplet.side_effect = self.create_droplet_side_effect
        mock_create_subdomain.side_effect = self.create_subdomain_side_effect
        mock_resize_droplet.side_effect = self.resize_droplet_side_effect
        mock_droplet_power_on.side_effect = self.droplet_power_on_side_effect

        # we mock the whole thing here to test how many calls to os.environ.get
        # without actually globally mocking this pretty important functionality
        mock_os.environ = Mock(get=Mock(return_value='token'))
        mock_env_get = mock_os.environ.get

        # run command in 5 threads
        threads = 5
        cmd = Command()
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for _ in range(threads):
                future = executor.submit(self.call_command)
                future.add_done_callback(self.on_done)

        mock_snapshot_master.assert_called_once()
        mock_get_snapshot.assert_called_once()
        mock_create_droplet.assert_called_once()
        mock_create_subdomain.assert_called_once()
        mock_resize_droplet.assert_called_once()
        mock_droplet_power_on.assert_called_once()

        launcher1.refresh_from_db()
        self.assertEqual(mock_env_get.call_count, threads)
        self.assertTrue(launcher1.launch_completed)
        self.assertTrue(launcher1.upscaled)
