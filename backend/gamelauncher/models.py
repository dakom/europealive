"""Models related to creating new game servers"""
import time

from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class APIUser(User):
    """
    Class for handling Users allowed to acces the API
    """

    class Meta:
        verbose_name = 'APIUser'
        verbose_name_plural = 'APIUsers'


class GameLauncher(models.Model):
    """
    Model to track the state of game servers on digital ocean
    """

    # the two possible server configs
    BASIC = "s-1vcpu-2gb"
    UPSCALE_SIZE = "c-8"

    slug = models.SlugField(
        max_length=40,
        help_text="The first part of the address. For example, \
            choosing slug to be 'testgame', the address will be \
            'testgame.europeatwork.org."
    )

    create_at = models.DateField(
        help_text="Date at which the platform becomes available to teachers."
    )
    gamestart_at = models.DateTimeField(
        help_text="Time at which students are assigned their job."
    )
    delete_at = models.DateField(
        help_text="Date at which the game shuts down, and all data is deleted."
    )
    num_students = models.PositiveIntegerField(
        help_text="The expected number of students in the game "
        "(used to chose an appropriate GameConfiguration)"
    )

    gameconfig = models.ForeignKey(
        'meta.GameConfiguration',
        on_delete=models.CASCADE,
        blank=True,
        help_text="Determines which teams are available, and which jobs are assigned",
    )

    server_size = models.CharField(
        max_length=20,
        default=BASIC,
        editable=False,
    )

    owner = models.ForeignKey(
        APIUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    droplet_name = models.CharField(max_length=100, editable=False)
    droplet_id = models.CharField(max_length=30, editable=False)

    snapshot_created = models.BooleanField(default=False)
    droplet_created = models.BooleanField(default=False)
    dns_created = models.BooleanField(default=False)

    launch_completed = models.BooleanField(default=False)

    upscaled = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    # pylint: disable=arguments-differ
    def save(self, *args, **kwargs):
        if not self.droplet_name:
            time_stamp = int(time.time())
            self.droplet_name = f'europa-{self.slug}-{time_stamp}'
        super().save(*args, **kwargs)

    def all_done(self):
        "Utility function to make sure we're done"
        return all(
            [self.snapshot_created, self.droplet_created, self.dns_created]
        )
