# Generated by Django 2.2 on 2020-02-03 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamelauncher', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamelauncher',
            name='launch_completed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='gamelauncher',
            name='droplet_name',
            field=models.CharField(editable=False, max_length=100),
        ),
    ]
