"""
Serializers for gamelauncher API
"""
from rest_framework import serializers

from gamelauncher.models import GameLauncher


class GameLauncherSerializer(serializers.ModelSerializer):
    """
    Serializer handling GameLaunchers
    """
    class Meta:
        model = GameLauncher
        fields = [
            'id',
            'slug',
            'create_at',
            'gamestart_at',
            'delete_at',
            'num_students'
        ]
