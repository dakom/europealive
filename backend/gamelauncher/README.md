# App for handling of creating and deleting game servers

## Usage
Create a `GameLauncher` object in the backend to have the app launching a new
gameserver at a specified time (`GameLauncher.create_at`). By default, a
new `GameLauncher` will create a very small server (see links to
server sizes via the link at the bottom), which currently is a 25GB disk with
2GB RAM, 1 CPU core and a monthly price of 10\$. On the day of the game
(`GameLauncher.gamestart_at`) the server will be upscaled to a CPU-Optimized
server with a disk of 100GB, 16GB of RAM and 8 CPU cores.

The server is deleted again when we reach `GameLauncher.delete_at`.

### Useful links

- [Droplet sizes and API names](https://slugs.do-api.dev/) at DigitalOcean
