#pylint: disable=missing-docstring

from django.apps import AppConfig


class GamelauncherConfig(AppConfig):
    """ GameLauncher config """
    name = 'gamelauncher'

    def ready(self):
        """ import signals """
        # pylint: disable=import-outside-toplevel,unused-import
        import gamelauncher.signals
