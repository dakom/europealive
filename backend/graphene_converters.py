""" Custom converters to bridge our types and Graphene """
# WARNING: DO NOT IMPORT MODELS HERE
# This module must be importable before apps have loaded, and thus cannot reference
# them, even transitively.
# This is needed because the converters must be registered before Graphene traverses
# the models.
import graphene
import django
from django.contrib.postgres.fields import ArrayField
from graphene_django.converter import convert_django_field
from graphene_django.forms.converter import convert_form_field
from graphene_django.registry import get_global_registry

import enumfields


## DJANGO >MODEL< FIELD CONVERTERS ##

# Add custom conversion to tie enumfields and graphene together.
# Must be done BEFORE importing schemas
@convert_django_field.register(enumfields.EnumIntegerField)
def convert_enuminteger_to_enum(field, registry=None):
    """ Convert EnumIntegerFields to Graphene Enums instead of default Ints """
    enum = graphene.Enum.from_enum(field.enum)
    converted = enum(description=field.help_text, required=not field.null)
    if registry is not None:
        registry.register_converted_field(field, converted)
    return converted


@convert_django_field.register(ArrayField)
def convert_postgres_array_to_list(field, registry=None):
    """ Override default converter to fix this bug:
    https://github.com/graphql-python/graphene-django/issues/536
    Remove this code when fix is upstreamed.
    """
    inner_type = convert_django_field(field.base_field)
    if not isinstance(inner_type, (graphene.List, graphene.NonNull)):
        inner_type = (graphene.NonNull(type(inner_type))
                      if inner_type.kwargs['required'] else type(inner_type))
    return graphene.List(inner_type, description=field.help_text, required=not field.null)


## DJANGO >FORM< FIELD CONVERTERS ##


@convert_form_field.register(enumfields.forms.EnumChoiceField)
def convert_form_field_to_enum(field):
    """ NOTE: This does not work with vanilla EnumChoiceField.
    Requires .type attribute to be set.
    See core.forms.mk_enuminteger_graphene_field
    """
    return field.type


@convert_form_field.register(django.contrib.postgres.forms.SplitArrayField)
def convert_form_field_to_list(field):
    """ This converter is missing from Graphene_django. """
    inner_type = convert_form_field(field.base_field)
    if not isinstance(inner_type, (graphene.List, graphene.NonNull)):
        inner_type = (graphene.NonNull(type(inner_type))
                      if inner_type.kwargs['required'] else type(inner_type))
    return graphene.List(inner_type, description=field.help_text, required=field.required)
