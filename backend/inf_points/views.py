""" Influence points views """
#pylint: disable=too-many-ancestors
from rest_framework import viewsets, generics, exceptions
from rest_framework.permissions import IsAuthenticated
from django.db import transaction

from . import models, serializers


class PointsAssignedViewSet(viewsets.ModelViewSet):
    """ Influence points awarded views """

    queryset = models.PointsAssigned.objects.all()
    serializer_class = serializers.PointsAssignedSerializer

    def get_serializer_class(self):
        if self.action == 'update':
            return serializers.PointsAssignedUpdateSerializer
        return self.serializer_class


class PointTransactionViewSet(generics.CreateAPIView):
    """ Transfer of influence points to a team. Updates recipient and sender's
    influence points, and saves a PointTransaction as a log """

    def perform_create(self, serializer):
        # Is this the correct place to put transaction, or does the serializer
        # perform db queries earlier?
        with transaction.atomic():
            sending_team = self.request.user.student.team
            receiving_team = serializer.validated_data["recipient"]
            amount = serializer.validated_data["amount"]
            serializer.validated_data["sender"] = sending_team
            if len(serializer.validated_data['description']) == 0:
                serializer.validated_data["description"] = (
                    "Transferred by %s" % self.request.user.student.full_name)

            if sending_team.influence_points >= amount:
                serializer.save()
                sending_team.influence_points -= amount
                sending_team.save()
                receiving_team.influence_points += amount
                receiving_team.save()
            else:
                raise exceptions.ValidationError(
                    "Amount cannot exceed team's current influence points")

    queryset = models.PointTransaction.objects.all()
    serializer_class = serializers.NewPointTransactionSerializer
    permission_classes = [IsAuthenticated]
