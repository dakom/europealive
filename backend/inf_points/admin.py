""" Influence points admin """
from django.contrib import admin

from inf_points.forms import DirectiveInvestmentAdminSubmitForm
from inf_points.models import (DirectiveInvestment, PointsAssigned,
                               PointTransaction)

# Register your models here.
admin.site.register(PointsAssigned)

@admin.register(DirectiveInvestment)
class DirectiveInvestmentAdmin(admin.ModelAdmin):
    """ Admin for DirectiveInvestment. Uses gameround to help indicate when
    investment was made """
    def gameround(self, obj):
        return obj.directive.round_set.first()

    gameround.short_description = 'Round'

    fieldsets = (
        (None, {'fields': (
            'by', 'priority', '_directive', 'amount', 'is_submitted'
        )}),
        ('Submit for deadline', {
            'classes': ('collapse',),
            'fields': ('submit', 'deadline')
        }),
    )

    list_display = ['by', 'priority', 'amount', 'gameround']
    form = DirectiveInvestmentAdminSubmitForm

    def save_model(self, request, obj, form, change):
        """
        Override to submit `DirectiveInvestment` if specified in form
        """
        super().save_model(request, obj, form, change)
        if form.cleaned_data['submit']:
            obj.submit(deadline=form.cleaned_data['deadline'])


@admin.register(PointTransaction)
class PointTransactionAdmin(admin.ModelAdmin):
    """ Admin for PointTransactions (thank you very much pylint!!) """
    list_display = ('recipient', 'sender', 'amount', 'description', 'time_sent')
