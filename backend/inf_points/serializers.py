""" Influence points serializers """
from django.db import transaction

from . import models
from rest_framework import serializers


class PointsAssignedSerializer(serializers.ModelSerializer):
    """ PointsAssigned serializer """
    class Meta:
        model = models.PointsAssigned
        fields = ("id", "team", "round", "article", "amount")

    def save(self, *args, **kwargs):
        with transaction.atomic():
            assigned = super().save(*args, **kwargs)
            team = assigned.team
            new_amount = self.validated_data["amount"]
            delta = new_amount - assigned.amount
            team.influence_points -= delta

            if team.influence_points < 0:
                raise serializers.ValidationError("Assigned more points than available")
            if new_amount < 0:
                raise serializers.ValidationError("negative points not allowed")
            team.save()


class PointsAssignedUpdateSerializer(PointsAssignedSerializer):
    """
    PointsAssigned serializer for update operations, where
    key fields should be read only.
    """

    class Meta(PointsAssignedSerializer.Meta):
        read_only_fields = ("team", "round", "article")


class NewPointTransactionSerializer(serializers.ModelSerializer):
    """ PointTransaction serializer """

    class Meta:
        model = models.PointTransaction
        fields = ("amount", "recipient", "description")
