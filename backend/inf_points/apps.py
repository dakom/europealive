from django.apps import AppConfig


class InfPointsConfig(AppConfig):
    name = 'inf_points'
