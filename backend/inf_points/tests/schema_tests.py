"""
Test GraphQL calls defined in `inf_points.schema`
"""
import json

from django.contrib.contenttypes.models import ContentType
from graphene_django.utils.testing import GraphQLTestCase

from core.models import (Commission, CommissionStatic, Directive,
                         DirectiveArticle, DirectiveItem, Secretary,
                         TeamPriority)
from events.models import DeadlineEvent
from game.models import Round
from inf_points.models import DirectiveInvestment


class DirectiveInvestmentMutationTestCase(GraphQLTestCase):
    """
    Test functionality related to the class `DirectiveInvestmentMutation`
    """

    GRAPHQL_URL = '/graphql'

    def setUp(self):
        """
        Create a Commission and some Rounds and Directive-stuff
        """
        team = Commission.objects.create(
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )

        user = Secretary.objects.create(
            team=team
        )
        self.client.force_login(user)

        for i in range(1, 9):
            art = DirectiveArticle.objects.create(number=i)
            for j in range(1, 6):
                DirectiveItem.objects.create(number=j, article=art)

        for art_num in [2, 4, 6]:
            TeamPriority.objects.create(
                team=team,
                article=DirectiveArticle.objects.get(number=art_num)
            )

        Round.objects.create(
            active=True,
            order=1,
            directive=Directive.create_first_directive()
        )
        Round.objects.create(
            active=False,
            order=2,
            directive=None
        )
        Round.objects.create(
            active=False,
            order=3,
            directive=None
        )
        Round.objects.create(
            active=False,
            order=4,
            directive=None,
        )

    def test_investment_is_submitted_when_created(self):
        """
        When performing the mutation, the new `DirectiveInvestment` should be
        submitted and its associated task marked as completed
        """
        # for some reason, this has to be called, or else the contenttype for
        # DirectiveInvestment and NewsReaction (??) are mixed up
        ContentType.objects.clear_cache()

        deadline = DeadlineEvent.objects.create(
            offset=100,
            actor=ContentType.objects.get_for_model(Secretary),
            action=ContentType.objects.get_for_model(DirectiveInvestment)
        )
        user = Secretary.objects.first()
        task = deadline.create_task(user)

        priority = user.team.priorities.first()

        variables = { 'priority': priority.id, 'amount': 30 }
        res = self.query('''
        mutation($priority: ID!, $amount: Int!) {
            makeDirectiveInvestment(input: {
                priority: $priority,
                amount: $amount,
            }) {
                directiveInvestment { id }
            }
        }
        ''', variables=variables
        )

        self.assertResponseNoErrors(res)
        content = json.loads(res.content)
        data = content['data']['makeDirectiveInvestment']
        inv_id = int(data['directiveInvestment']['id'])

        task.refresh_from_db()
        self.assertTrue(task.completed())
        self.assertIsNotNone(task.get_submittable())
        self.assertEqual(
            task.get_submittable(),
            DirectiveInvestment.objects.get(id=inv_id)
        )
