"""
Test for `inf_points/forms.py`
"""
from unittest.mock import patch

from django.db.models.query import EmptyQuerySet
from django.db.utils import ProgrammingError
from django.test import TestCase

from inf_points.forms import queryset_for_deadlines


class QuerysetForDeadlinesTestCase(TestCase):
    """
    Test `queryset_for_deadlines()`
    """

    @patch('inf_points.forms.DeadlineEvent.objects.filter')
    def test_empty_queryset_returned_when_programmingerror(
        self, mock_filter
    ):
        """
        When the queryset in the function is executed during app initialization
        (but before migrations, ie. in a new environment or in the test
        environment on BitBucket), a ProgrammingError is raised. Test that
        we handle that situation correctly.
        """
        mock_filter.side_effect = ProgrammingError()
        qs = queryset_for_deadlines()
        self.assertIsInstance(qs, EmptyQuerySet)
