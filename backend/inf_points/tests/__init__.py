"""
Import test cases for `inf_points`
"""
from inf_points.tests.form_tests import QuerysetForDeadlinesTestCase
from inf_points.tests.schema_tests import DirectiveInvestmentMutationTestCase
