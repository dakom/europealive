""" Models for influence points """
from django.db import models, transaction
from django.db.models import F

from core.models import (Directive, DirectiveArticle, Secretary, Team,
                         TeamPriority)
from events.models import DeadlineTask, Submittable


class PointsAssigned(models.Model):
    """
    Influence points assigned from team to directive.
    May be edited while given round is active
    """
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    round = models.ForeignKey("game.Round", on_delete=models.CASCADE)
    article = models.ForeignKey(DirectiveArticle, on_delete=models.CASCADE)
    amount = models.IntegerField(verbose_name="number of points")

    class Meta:
        verbose_name = "points given"
        verbose_name_plural = "points given"
        unique_together = ("team", "round", "article")


class PointTransaction(models.Model):
    """ A log entry of a transaction of influence points. """
    description = models.CharField(max_length=500)
    amount = models.SmallIntegerField()
    time_sent = models.DateTimeField(auto_now_add=True)

    recipient = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name="ingoing_transaction"
    )

    # If null, the points were sent by the game, not a specific team.
    sender = models.ForeignKey(
        Team,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="outgoing_transactions"
    )

    # associate a task with this transaction if wanted
    task = models.ForeignKey(
        DeadlineTask,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text='Task which this transaction is related to'
    )

    def __str__(self):
        return f'Transaction {self.description[:15]}'

    class Meta:
        verbose_name = "influence point transaction"
        verbose_name_plural = "influence point transactions"

    @classmethod
    def send(cls, description, amount, recipient, sender=None, task=None):
        """
        Automatically updates teams' influence points, and creates log entry
        """
        trans = cls.objects.create(
            description=description,
            amount=amount,
            recipient=recipient,
            sender=sender,
            task=task
        )

        recipient.influence_points = F('influence_points') + amount
        recipient.save()
        recipient.refresh_from_db()

        if sender is not None:
            sender.influence_points = F('influence_points') - amount
            sender.save()

        return trans


class DirectiveInvestment(Submittable):
    """ An investment by a team in one of its prioritized directive article """
    by = models.ForeignKey(Team, on_delete=models.CASCADE)
    priority = models.ForeignKey(TeamPriority, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField()
    _directive = models.ForeignKey(Directive, on_delete=models.CASCADE)
    transaction = models.ForeignKey(
        PointTransaction,
        null=True,
        on_delete=models.PROTECT
    )

    @property
    def directive(self):
        return self._directive

    @directive.setter
    def directive(self, val):
        self._directive = val

    def submitted_by(self):
        """
        Return the Secretary of the Team that has submitted this investment
        (overrides `Submittable.submitted_by`)

        NOTE: Assumes only 1 Secretary per Team!!
        """
        return Secretary.objects.get(team=self.by)

    def save(self, *args, **kwargs):

        # Check integrity
        if self.priority not in TeamPriority.objects.filter(team=self.by):
            raise ValueError("Can only invest in team's prioritized articles")

        if self.transaction is None:
            if self.amount > self.by.influence_points:
                raise ValueError("Cannot invest more than team currently has")

            num = self.priority.article.number
            trans = PointTransaction.send(
                description= f'Investment in Directive Article {num}',
                amount=-self.amount,
                recipient=self.by,
                sender=None
            )
            self.transaction = trans

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "directive investment"
        verbose_name_plural = "directive investments"
        unique_together = ("by", "priority", "_directive")
        ordering = ["priority__article__number"]
