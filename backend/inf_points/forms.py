""" Forms for inf_points models, intended for use by Graphene not Django rendering. """
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.db.utils import ProgrammingError
from django.utils.translation import gettext as _

from events.models import DeadlineEvent, DeadlineTask
from inf_points.models import DirectiveInvestment


class DirectiveInvestmentForm(forms.ModelForm):
    """ Form used by Graphene mutation """
    class Meta:
        model = DirectiveInvestment
        fields = ["priority", "amount"]


def queryset_for_deadlines():
    """
    Wrapper function for the `queryset` argument provided for the `deadline`
    field on `DirectiveInvestmentAdminSubmitForm`, so that we can catch the
    cases where the database has not yet been initialised (for example, when
    running tests on BitBucket or migrating for the first time).
    """
    try:
        return DeadlineEvent.objects.filter(
            action=ContentType.objects.get_for_model(
                DirectiveInvestment
            )
        )
    except ProgrammingError:
        return DeadlineEvent.objects.none()


class DirectiveInvestmentAdminSubmitForm(forms.ModelForm):
    """ Form that allows submitting via the admin site """
    submit = forms.BooleanField(
        required=False,
        help_text=_(
            'Check this box, if the DirectiveInvestment should be ' \
            'submitted for the selected deadline.'
        )
    )
    deadline = forms.ModelChoiceField(
        queryset=queryset_for_deadlines(),
        required=False,
        help_text=_(
            'Deadline for which the investment should be or has been submitted'
        )
    )

    def __init__(self, *args, **kwargs):
        """
        If the form works on an instance that has already been submitted,
        then load the deadline and provide as initial data. Also disable the
        submit fields to avoid double submissions
        """
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.is_submitted:
            task = DeadlineTask.objects.filter(
                _submittable=self.instance
            ).first()

            if task is not None:
                self.initial['deadline'] = task.deadline
                self.fields['submit'].disabled = True
                self.fields['deadline'].disabled = True

    def clean(self):
        """
        Raise ValidationError if `submit` is `True` but no `deadline` has been
        given
        """
        cleaned_data = super().clean()
        submit = cleaned_data.get('submit')
        deadline = cleaned_data.get('deadline')

        if submit is True and deadline is None:
            raise forms.ValidationError(
                _("Must specify deadline when trying to submit"),
                code='invalid'
            )

    def save(self, commit=True):
        """
        The actual submitting is done in the ModelAdmin.save_model function
        rather than in this save function. That is because, when the ModelAdmin
        calls this save, it passes `commit=False` and thus the investment
        is not saved here and therefore cannot be submitted. Annoying.
        """
        return super().save(commit=commit)

    class Meta:
        model = DirectiveInvestment
        exclude = ('transaction',)
