# Generated by Django 2.2 on 2019-10-29 16:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inf_points', '0006_directiveinvestment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='directiveinvestment',
            options={'ordering': ['priority__article__number'], 'verbose_name': 'directive investment', 'verbose_name_plural': 'directive investments'},
        ),
    ]
