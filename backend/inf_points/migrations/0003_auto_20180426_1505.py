# Generated by Django 2.0.2 on 2018-04-26 13:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inf_points', '0002_auto_20180426_1500'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pointsassigned',
            old_name='points',
            new_name='amount',
        ),
    ]
