# Generated by Django 2.0.2 on 2018-04-26 13:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_directivearticle_short_description'),
        ('game', '0002_auto_20180425_1643'),
        ('inf_points', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PointsAssigned',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(verbose_name='antal point')),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.DirectiveArticle')),
                ('round', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.Round')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Team')),
            ],
            options={
                'verbose_name': 'point givet',
                'verbose_name_plural': 'point givet',
            },
        ),
        migrations.AlterUniqueTogether(
            name='pointsawarded',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='pointsawarded',
            name='round',
        ),
        migrations.RemoveField(
            model_name='pointsawarded',
            name='team',
        ),
        migrations.DeleteModel(
            name='PointsAwarded',
        ),
        migrations.AlterUniqueTogether(
            name='pointsassigned',
            unique_together={('team', 'round')},
        ),
    ]
