""" Influence points GraphQL schema """
import graphene
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
from django.db.models import Q
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import DjangoObjectType

from core.models import Directive
from core.shared_schema import gql_strict_list
from game.models import Round
from inf_points import forms
from inf_points.models import DirectiveInvestment, PointTransaction


class PointTransactionType(DjangoObjectType):
    class Meta:
        model = PointTransaction


class DirectiveInvestmentType(DjangoObjectType):
    class Meta:
        model = DirectiveInvestment


class DirectiveInvestmentMutation(DjangoModelFormMutation):
    directiveInvestment = graphene.Field(DirectiveInvestmentType)

    class Meta:
        form_class = forms.DirectiveInvestmentForm

    @classmethod
    def perform_mutate(cls, form, info):
        # Investment is always on behalf of own team
        form.instance.by = info.context.user.student.team
        # Investment is always tied to current round/directive
        form.instance._directive_id = Round.get_current_round().directive_id
        try:
            # If investment has already been made, update existing investment
            existing_investment = DirectiveInvestment.objects.get(
                by=form.instance.by,
                _directive_id=form.instance._directive_id,
                priority=form.instance.priority
            )
            form.instance.id = existing_investment.id
            if existing_investment.amount == form.instance.amount:
                # Investment has not changed, nothing to do
                return existing_investment
        except DirectiveInvestment.DoesNotExist:
            # If investment has not been made, we can save new without setting id
            pass
        mutation = super().perform_mutate(form, info)
        mutation.directiveInvestment.submit()
        return mutation


class MyTeamIpLog(graphene.ObjectType):
    """ Sends transactions along with current balance
    to allow client to calculate previous balances """
    transactions = gql_strict_list(PointTransactionType)
    current_balance = graphene.Int(required=True)


class Query:
    my_team_ip_log = graphene.Field(MyTeamIpLog, required=True)
    my_current_investments = gql_strict_list(DirectiveInvestmentType)

    def resolve_my_team_ip_log(self, info):
        my_team = info.context.user.student.team
        transactions = PointTransaction.objects.filter(
            Q(recipient=my_team) | Q(sender=my_team)
        ).order_by("-time_sent")
        return {
            "transactions": transactions,
            "current_balance": my_team.influence_points,
        }

    def resolve_my_current_investments(self, info):

        return DirectiveInvestment.objects.filter(
            _directive=Round.get_current_round().directive,
            by=info.context.user.student.team,
        )

class Mutation:
    make_directive_investment = DirectiveInvestmentMutation.Field()
