""" Collects GraphQL schemas from apps """
import graphene
from graphene_django.debug import DjangoDebug

import news.schema
import core.schema
import inf_points.schema
import game.schema
import popups.schema

#pylint: disable=too-few-public-methods


class RootQuery(news.schema.Query, core.schema.Query, inf_points.schema.Query,
                game.schema.Query, popups.schema.Query, graphene.ObjectType):
    """ Should inherit from all apps' query types """
    debug = graphene.Field(DjangoDebug, name='_debug')


class RootMutation(
        news.schema.Mutation, core.schema.Mutation, inf_points.schema.Mutation,
        game.schema.Mutation, popups.schema.Mutation, graphene.ObjectType):
    """ Should inherit from all apps' mutation types """


schema = graphene.Schema(query=RootQuery, mutation=RootMutation) # pylint: disable=invalid-name
