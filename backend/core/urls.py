"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import logout_then_login
from django.core import management
from django.urls import path
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog
from graphene_django.views import GraphQLView
from rest_framework import routers

import core.views as coreviews
import game.views as gameviews
import gamelauncher.views as gamelauncher_views
import inf_points.views as inf_points_views
import news.views as newsviews

# Routers provide an easy way of automatically determining the URL conf.
ROUTER = routers.DefaultRouter()
ROUTER.register(r'users', coreviews.UserViewSet)
ROUTER.register(r'ip_recipient_teams', coreviews.IPRecipientViewSet)
ROUTER.register(r'rounds', gameviews.RoundViewSet)
ROUTER.register(r'games', gameviews.GameViewSet)
ROUTER.register(r'directive_template', coreviews.DirectiveTemplateViewSet)
ROUTER.register(r'directive', coreviews.DirectiveViewSet)
ROUTER.register(r'points_awarded', inf_points_views.PointsAssignedViewSet)
# ROUTER.register(r'point_transaction', inf_points_views.PointTransactionViewSet)

ROUTER.register(r'media_houses', newsviews.MediaHouseViewSet)
ROUTER.register(r'news_articles', newsviews.NewsArticleViewSet)
ROUTER.register(r'news_telegrams', newsviews.NewsTelegramViewSet)

ROUTER.register(
    r'gamelaunchers',
    gamelauncher_views.GameLauncherViewSet
)

DATA_URLS = [
    url(r'^', include(ROUTER.urls)),
    # url(r'newsfeed/$', newsviews.NewsfeedView.as_view(), name="newsfeed"),
    url(r'^current_round/$', gameviews.CurrentRoundView.as_view(), name='get_current_round'),
    url(r'^point_transaction/$', inf_points_views.PointTransactionViewSet.as_view(),
        name='make_point_transaction'),
]

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='main-spa', permanent=False)),
    path('i18n/', include('django.conf.urls.i18n')),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='js-i18n-catalog'),
    path('job_template_test/<int:count>', gameviews.test_show_job_templates),
    url(r'^data/', include(DATA_URLS)),
    url(r'^graphql$', GraphQLView.as_view(graphiql=True)),
    url(r'^logout/$', logout_then_login, name='logout'),
    path('admin/', admin.site.urls),
    path("admin/", include('loginas.urls')),
    url(
        r'admin/allocate-rooms/',
        gameviews.AllocateRooms.as_view(),
        name='allocate_rooms'
    ),
    url(r'api-auth/', include("rest_framework.urls")),
    url(r'^app/', coreviews.serve_app_debug, name="main-spa"),
] + static("appstatic/", document_root="../frontend/build") + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    management.call_command('graphql_schema', indent=2)

    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
