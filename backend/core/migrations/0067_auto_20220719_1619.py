# Generated by Django 2.2 on 2022-07-19 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0066_auto_20220406_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='commissionstatic',
            name='new_special_success_criteria',
            field=models.TextField(max_length=2000, null=True),
        ),
        migrations.AddField(
            model_name='lobbyorganizationstatic',
            name='new_special_success_criteria',
            field=models.TextField(max_length=2000, null=True),
        ),
    ]
