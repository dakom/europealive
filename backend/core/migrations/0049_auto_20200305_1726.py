# Generated by Django 2.2 on 2020-03-05 16:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_auto_20200304_2213'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='special_success_criteria_value',
            field=models.CharField(choices=[('article 1', 'Article 1'), ('article 2', 'Article 2'), ('article 3', 'Article 3'), ('article 4', 'Article 4'), ('article 5', 'Article 5'), ('article 6', 'Article 6'), ('article 7', 'Article 7'), ('article 8', 'Article 8'), ('commission special', 'Commission special')], default='article 1', help_text='Technical resolution of success criteria', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='team',
            name='special_success_criteria',
            field=models.CharField(help_text='description of success criteria', max_length=1000),
        ),
    ]
