# Generated by Django 2.2 on 2020-03-04 21:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0046_auto_20200304_2158'),
    ]

    operations = [
        migrations.RenameField(
            model_name='team',
            old_name='video_embed_url',
            new_name='video_embed_url_raw',
        ),
    ]
