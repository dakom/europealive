# Generated by Django 2.2 on 2019-08-09 15:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20190809_1715'),
    ]

    operations = [
        migrations.CreateModel(
            name='TeamPriority',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.DirectiveArticle')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Team')),
            ],
            options={
                'verbose_name': 'team priority',
                'verbose_name_plural': 'team priorities',
            },
        ),
        migrations.AddField(
            model_name='team',
            name='priorities',
            field=models.ManyToManyField(through='core.TeamPriority', to='core.DirectiveArticle'),
        ),
    ]
