# Generated by Django 2.2 on 2021-12-14 13:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0059_auto_20211206_1840'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attacheadvisor',
            options={'verbose_name': 'attache advisors'},
        ),
        migrations.AlterModelOptions(
            name='mediaadvisor',
            options={'verbose_name': 'media advisors'},
        ),
        migrations.AlterModelOptions(
            name='politicaladvisor',
            options={'verbose_name': 'political advisors'},
        ),
        migrations.AlterModelOptions(
            name='technicaladvisor',
            options={'verbose_name': 'technical advisors'},
        ),
    ]
