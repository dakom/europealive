# Generated by Django 2.2 on 2021-12-06 17:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0038_auto_20211122_0921'),
        ('authtoken', '0002_auto_20160226_1747'),
        ('admin', '0003_logentry_add_action_flag_choices'),
        ('auth', '0011_update_proxy_permissions'),
        ('news', '0019_mediahousestatic_staff_page'),
        ('popups', '0007_auto_20200304_1425'),
        ('core', '0058_attacheadvisor_mediaadvisor_politcaladvisor_technicaladvisor'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PolitcalAdvisor',
            new_name='PoliticalAdvisor',
        ),
    ]
