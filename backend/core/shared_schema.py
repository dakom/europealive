""" Core GraphQL shared type definitions to be importable without cycles """
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
import graphene
import graphene_django
from graphene_django.types import DjangoObjectType

from core.models import (Advisor, ArticleSelection, Chief, Commission,
                         Directive, DirectiveArticle, DirectiveIndication,
                         DirectiveItem, LobbyOrganization, PendingStudent,
                         Secretary, Strategist, Student, Teacher, Team,
                         TeamPriority, User)


def gql_strict_list(subtype):
    """ Utility function for creating GraphQL type non-null
    list of non-null values. Very useful, since null values are often better
    represented as empty lists. """
    return graphene.NonNull(graphene.List(graphene.NonNull(subtype)))


def query_or_empty_list(query):
    return query if query else []


class UserType(DjangoObjectType):
    class Meta:
        model = User
    full_name = graphene.String(required=True)
    def resolve_full_name(self, _info):
        return self.full_name


class PendingStudentType(DjangoObjectType):
    user = graphene.Field(UserType, required=True)
    class Meta:
        model = PendingStudent


class StudentType(DjangoObjectType):
    user = graphene.Field(UserType, required=True)
    full_name = graphene.String(required=True)
    job_description = graphene.String(required=True)
    job_title = graphene.String(required=True)
    job_name = graphene.String(required=True)

    def resolve_job_name(self, _info):
        return self.job

    def resolve_job_title(self, _info):
        return self.job_title
    
    def resolve_full_name(self, _info):
        return self.full_name

    class Meta:
        model = Student


class TeacherType(DjangoObjectType):
    user = graphene.Field(UserType, required=True)
    class Meta:
        model = Teacher


class StudentTypeMixin:
    """ Adds student field to student subtypes """
    student = graphene.Field(StudentType, required=True)
    full_name = graphene.String(required=True)

    def resolve_full_name(self, _info):
        return self.full_name


class DirectiveArticleType(DjangoObjectType):
    class Meta:
        model = DirectiveArticle


class SecretaryType(DjangoObjectType, StudentTypeMixin):
    class Meta:
        model = Secretary


class AdvisorType(DjangoObjectType, StudentTypeMixin):
    class Meta:
        model = Advisor


class ChiefType(DjangoObjectType, StudentTypeMixin):
    class Meta:
        model = Chief


class StrategistType(DjangoObjectType, StudentTypeMixin):
    class Meta:
        model = Strategist


class DirectiveItemType(DjangoObjectType):
    class Meta:
        model = DirectiveItem


class ArticleSelectionType(DjangoObjectType):
    class Meta:
        model = ArticleSelection


class DirectiveType(DjangoObjectType):
    class Meta:
        model = Directive
        only_fields = ("id", "selections")


class DirectiveIndicationType(DjangoObjectType):
    """
    GraphQL type for `DirectiveIndication`
    """
    directive = graphene.Field(DirectiveType, required=True)

    class Meta:
        model = DirectiveIndication

    def resolve_directive(self, _info):
        """
        Invoke property `DirectiveIndication.directive` to get associated
        `Directive`
        """
        return self.directive


class TeamPriorityType(DjangoObjectType):
    class Meta:
        model = TeamPriority


class TeamKind(graphene.Enum):
    Commission = 1
    ParliamentGroup = 2
    Country = 3
    LobbyOrganization = 4
    MediaHouse = 5


class TeamType(DjangoObjectType):
    name = graphene.String(required=True)
    priorities = gql_strict_list(TeamPriorityType)
    students = gql_strict_list(StudentType)
    kind = TeamKind(required=True)
    video_embed_url = graphene.String(required=True)
    special_success_criteria = graphene.String(required=True)
    starting_room = graphene.String(required=True)

    class Meta:
        model = Team

    def resolve_name(self, _info):
        return self.get_subclass().static.name

    def resolve_special_success_criteria(self, _info):
        return self.get_subclass().static.special_success_criteria

    def resolve_students(self, _info):
        return self.students.all() # pylint: disable=no-member

    def resolve_priorities(self, _info):
        return self.priorities.all() # pylint: disable=no-member

    def resolve_logo(self, _info):
        return self.logo.url # pylint: disable=no-member

    def resolve_kind(self, _info):
        if hasattr(self, "commission"):
            return TeamKind.Commission
        if hasattr(self, "parliamentgroup"):
            return TeamKind.ParliamentGroup
        if hasattr(self, "country"):
            return TeamKind.Country
        if hasattr(self, "lobbyorganization"):
            return TeamKind.LobbyOrganization
        if hasattr(self, "mediahouse"):
            return TeamKind.MediaHouse

        raise ValueError("Team kind not recognized")

    def resolve_starting_room(self, _info):
        return self.starting_room


class TeamTypeMixin:
    """ Inherited by team subclass types """
    team = graphene.Field(TeamType, required=True)
    def resolve_team(self, _info):
        return self.team_ptr

    def resolve_logo(self, _info):
        return self.logo.url # pylint: disable=no-member


class LobbyOrganizationType(DjangoObjectType, TeamTypeMixin):
    class Meta:
        model = LobbyOrganization


class CommissionType(DjangoObjectType, TeamTypeMixin):
    class Meta:
        model = Commission
