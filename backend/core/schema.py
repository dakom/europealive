""" Core GraphQL Queries and mutations """
import graphene
# pylint: disable=missing-docstring, too-few-public-methods, no-self-use
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password
from django.utils import timezone
from django.utils.translation import gettext as _
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.forms.types import ErrorType

from core import forms
from core.models import (ArticleSelection, Commission, Directive,
                         DirectiveArticle, DirectiveIndication, Indication,
                         LobbyOrganization, Team)
from core.shared_schema import (CommissionType, DirectiveArticleType,
                                DirectiveIndicationType, DirectiveType,
                                LobbyOrganizationType, TeamType, UserType,
                                gql_strict_list, query_or_empty_list)
from game.models import Game, Round


class DirectiveIndicationSumType(graphene.ObjectType):
    more = gql_strict_list(graphene.Int)
    less = gql_strict_list(graphene.Int)
    ok = gql_strict_list(graphene.Int)


class DirectiveIndicationMutation(DjangoModelFormMutation):
    directiveIndication = graphene.Field(DirectiveIndicationType)

    class Meta:
        form_class = forms.DirectiveIndicationForm

    @classmethod
    def perform_mutate(cls, form, info):
        form.instance.by = info.context.user.student.team
        mutation = super().perform_mutate(form, info)
        form.instance.submit()
        return mutation


class CreateStudentMutation(DjangoModelFormMutation):
    # pendingStudent = graphene.Field(PendingStudentType)
    user = graphene.Field(UserType)

    class Meta:
        form_class = forms.PendingStudentForm

    @classmethod
    def perform_mutate(cls, form, info):
        # Hash and salt password
        form.instance.password = make_password(form.instance.password)

        # Set appropriate permissions
        form.instance.is_staff = False
        form.instance.is_superuser = False
        form.instance.is_active = True

        super().perform_mutate(form, info)
        login(info.context, form.instance) # form.instance.id is set by perform_mutate
        user = form.instance.user

        game = Game.get_current_game()
        if game.game_started:
            # Slightly ugly, since we create a PendingStudent to delete it
            job_template = game.get_next_job_template()
            job_template.create_any_student(user, delete_pending_student=True)

        return CreateStudentMutation(
            user=form.instance.user,
            pendingStudent=form.instance,
        )


class CreateTeacherMutation(DjangoModelFormMutation):
    user = graphene.Field(UserType)

    class Meta:
        form_class = forms.TeacherForm

    @classmethod
    def perform_mutate(cls, form, info):
        # Hash and salt password
        form.instance.password = make_password(form.instance.password)

        # Set appropriate permissions
        form.instance.is_staff = False
        form.instance.is_superuser = False
        form.instance.is_active = True

        super().perform_mutate(form, info)
        login(info.context, form.instance) # form.instance.id is set by perform_mutate

        return CreateTeacherMutation(
            user=form.instance.user,
            teacher=form.instance,
        )


# class UpdateStartingRoomMutation(graphene.Mutation):
#     did_update = graphene.Field(graphene.Boolean, required=True)

#     class Arguments:
#         starting_room = graphene.String(required=True)
#         team_id = graphene.ID(required=True)

#     def mutate(self, _info, starting_room, team_id):
#         team = Team.objects.get(id=team_id)
#         team.starting_room = starting_room
#         team.save()

#         return UpdateStartingRoomMutation(True)


class PublishDirectiveMutation(graphene.Mutation):
    did_publish = graphene.Field(graphene.Boolean)
    errors = graphene.List(ErrorType)

    class Arguments:
        items = gql_strict_list(graphene.Int)
        round_id = graphene.ID(required=True)

    def mutate(self, _info, items, round_id):
        next_round = Round.objects.get(id=round_id)
        if next_round.id != int(round_id):
            return PublishDirectiveMutation(
                errors=[ErrorType(
                    field="round_id",
                    messages=[_("Can only publish directive for next round")]
                )]
            )
        if next_round.directive is not None:
            return PublishDirectiveMutation(
                errors=[ErrorType(
                    field="round_id",
                    messages=[_("You have already published a directive for this round!")]
                )]
            )
        new_directive = Directive.objects.create()
        for article, selected in zip(DirectiveArticle.objects.all(), items):
            ArticleSelection.objects.create(
                article=article,
                items_selected=selected,
                directive=new_directive,
            )
        next_round.directive = new_directive
        next_round.save()
        new_directive.submit()
        return PublishDirectiveMutation(did_publish=True)


class Query:
    my_team = graphene.Field(TeamType, required=True)
    team = graphene.Field(TeamType, required=True, team_id=graphene.Int())
    all_teams = gql_strict_list(TeamType)
    all_negotiating_teams = gql_strict_list(TeamType)
    directive_articles = gql_strict_list(DirectiveArticleType)
    directive_indication = graphene.Field(DirectiveIndicationType, id=graphene.Int(), required=True)
    directive_indications = gql_strict_list(DirectiveIndicationType)
    directive_indications_sum = graphene.Field(DirectiveIndicationSumType)
    my_indication_for = graphene.Field(DirectiveIndicationType, directive_id=graphene.ID())
    lobby_organizations = gql_strict_list(LobbyOrganizationType)
    commission = graphene.Field(CommissionType, required=True)
    directive = graphene.Field(DirectiveType, directive_id=graphene.ID(), required=True)
    login = graphene.Field(graphene.Boolean, required=True, username=graphene.String(),
                           password=graphene.String())

    def resolve_directive_articles(self, _info):
        return DirectiveArticle.objects.all().order_by('number')

    def resolve_my_team(self, info):
        return info.context.user.student.team

    def resolve_team(self, _info, team_id):
        return Team.objects.get(id=team_id)

    def resolve_all_teams(self, _info):
        return query_or_empty_list(Team.objects.all().order_by("-influence_points"))

    def resolve_all_negotiating_teams(self, _info):
        return query_or_empty_list(Team.objects.filter(mediahouse__isnull=True).order_by("-influence_points"))

    def resolve_directive_indication(self, info, id):
        return DirectiveIndication.objects.get(id=id)

    def resolve_directive_indications(self, info):
        directive = Round.get_current_round().directive
        indications = DirectiveIndication.objects.filter(
            target_directive=directive
        ).order_by('by__name')
        countries = [i for i in indications if hasattr(i.by, 'country')]
        groups = [i for i in indications if hasattr(i.by, 'parliamentgroup')]
        indications = countries + groups
        return indications if indications else []

    def resolve_directive_indications_sum(self, info):
        directive = Round.get_current_round().directive
        indication_sets = DirectiveIndication.objects.filter(
            target_directive=directive
        )
        more, less, ok = [0]*8, [0]*8, [0]*8
        for ind_set in indication_sets:
            for i, indication in enumerate(ind_set.indications):
                if indication == Indication.MORE:
                    more[i] += 1
                elif indication == Indication.LESS:
                    less[i] += 1
                else:
                    ok[i] += 1
        return {
            'more': more,
            'less': less,
            'ok': ok
        }

    def resolve_my_indication_for(self, info):
        """ Returns user's team's indication for currently active directive.
        None if team has not yet indicated. """
        if hasattr(info.context.user, "teacher"):
            return None

        team = info.context.user.student.team
        directive = Round.get_current_round().directive
        try:
            return DirectiveIndication.objects.get(
                target_directive=directive,
                by=team
            )
        except DirectiveIndication.DoesNotExist:
            return None

    def resolve_lobby_organizations(self, _info):
        orgs = LobbyOrganization.objects.all()
        return orgs if orgs else []

    def resolve_commission(self, _info):
        return Commission.objects.all()[0]

    def resolve_directive(self, _info):
        return Round.get_current_round().directive

    def resolve_login(self, info, username, password):
        user = authenticate(username=username, password=password)
        if user is not None:
            login(info.context, user) # #This causes a cookie to be set on response
            return True
        return False


class Mutation:
    make_directive_indication = DirectiveIndicationMutation.Field()
    create_student = CreateStudentMutation.Field()
    create_teacher = CreateTeacherMutation.Field()
    # update_starting_room = UpdateStartingRoomMutation.Field()
    publish_directive = PublishDirectiveMutation.Field(required=True)
