""" Run periodic tasks """
import logging

from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from game.models import FinalTeamVote, Game, Round
from game.tasks import (award_special_success_criteria_points,
                        handle_deadlines, start_game_task)


class Command(BaseCommand):
    help = "Run periodically to execute due tasks"

    def handle(self, *args, **options):
        """
        Call functions that handle timed events in the game
        """
        logger = logging.getLogger("core.runtasks")
        now = timezone.now()
        logger.info("%s Executing command runtasks", str(now))

        # get all relevant tasks (tuples of function plus arguments)
        tasks = []

        # get current game and return, if none exist yet
        game = Game.get_current_game()
        if game is None:
            return

        # get start_game_task if game is not yet started
        if game is not None and not game.game_started:
            if game.starting_time <= now:
                tasks.append((start_game_task, []))

        # always call handle_deadlines
        tasks.append((handle_deadlines, []))

        # if the special task haven't run and everyone has voted, give bonus
        gameround = Round.get_current_round()
        if not gameround.special_success_criteria_task_ran \
                and FinalTeamVote.voting_complete():
            tasks.append(
                (award_special_success_criteria_points, [gameround])
            )

        # run tasks one at a time
        for (func, args) in tasks:
            logger.info("runtasks executing: %s", str(func))
            try:
                with transaction.atomic():
                    func(*args)
            except Exception: # pylint: disable=broad-except
                logger.error(
                    "Task failed to execute",
                    exc_info=True,
                    extra={
                        "time": now,
                        "func": func,
                    }
                )
