""" Generate Reason from the command line. Used in deployment. """
from django.core.management.base import BaseCommand
from reason_generator.generator import ReasonGenerator

class Command(BaseCommand):
    """ Generate Reason code """
    help = "Generates Reason code to interact with Django API"

    def handle(self, *args, **options):
        ReasonGenerator("Test reason", "generated_reason").generate()
        self.stdout.write(self.style.SUCCESS("Successfully generated Reason API code"))
