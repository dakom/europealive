""" Core views """
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import viewsets

from core import serializers
from core.models import Directive, DirectiveArticle, Team, User


class UserViewSet(viewsets.ModelViewSet):
    """ Base user views. Will be changed with new user subclasses. """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class IPRecipientViewSet(viewsets.ModelViewSet):
    """ Base team views. Will be changed with new user subclasses. """
    queryset = Team.objects.exclude(mediahouse__isnull=False)
    serializer_class = serializers.TeamSerializer


class DirectiveTemplateViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Return all articles in database, which together represent the
    directive template
    """
    queryset = DirectiveArticle.objects.all()
    serializer_class = serializers.DirectiveArticleSerializer


class DirectiveViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Return all articles in database, which together represent the
    directive template
    """
    queryset = Directive.objects.all()
    serializer_class = serializers.DirectiveSerializer

@ensure_csrf_cookie
def serve_app_debug(request):
    """ Serves the app html. """

    return render(request, "core/app_base.html")
