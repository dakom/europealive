"""
Import all test cases for `core`
"""
from core.tests.commands_tests import RuntasksTestCase
from core.tests.schema_tests import (DirectiveIndicationMutationTestCase,
                                     PublishDirectiveMutationTestCase,
                                     ResolveCreateStudentTestCase,
                                     ResolveDirectiveIndicationsTestCase,
                                     ResolveLoginTestCase)
