"""
Test functionality in `core.schema`
"""
import json
import random
from unittest.mock import Mock, patch

from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from graphene_django.utils.testing import GraphQLTestCase

from core.models import (Commission, CommissionStatic, Directive,
                         DirectiveArticle, DirectiveIndication, DirectiveItem,
                         PendingStudent, Secretary, Student, Team, User)
from events.models import DeadlineEvent
from game.models import (Country, CountryStatic, Game, ParliamentGroup,
                         ParliamentGroupStatic, Round)
from meta.models import GameConfiguration


class PublishDirectiveMutationTestCase(GraphQLTestCase):
    """
    Tests functionality related to publishing a `Directive` via
    `game.schema.PublishDirectiveMutation`
    """

    GRAPHQL_URL = '/graphql'

    def setUp(self):
        """
        Create some Rounds and Directive-stuff
        """
        for i in range(1, 9):
            art = DirectiveArticle.objects.create(number=i)
            for j in range(1, 6):
                DirectiveItem.objects.create(number=j, article=art)


        Round.objects.create(
            active=True,
            order=1,
            directive=Directive.create_first_directive()
        )
        Round.objects.create(
            active=False,
            order=2,
            directive=None
        )
        Round.objects.create(
            active=False,
            order=3,
            directive=None
        )
        Round.objects.create(
            active=False,
            order=4,
            directive=None,
        )

    @patch('core.schema.Directive.submit')
    def test_directive_can_be_published(self, mock_submit):
        """
        Test that a valid query creates and publishes a new directive and calls
        `Directive.submit()` (which is further tested in next test)
        """
        curr_round = Round.get_current_round()
        next_round = curr_round.get_following_round()
        count_before = Directive.objects.count()
        items = [1, 2, 3, 4, 5, 1, 2, 3]

        res = self.query(
            '''
                mutation($items: [Int!]!, $roundId: ID!) {
                    publishDirective(items: $items, roundId: $roundId) {
                        didPublish
                        errors {
                            field
                            messages
                        }
                    }
                }
            ''',
            variables = {
                'items': items,
                'roundId': next_round.id
            }
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        data = content['data']['publishDirective']

        self.assertTrue(data['didPublish'])
        self.assertIsNone(data['errors'])

        next_round.refresh_from_db()

        self.assertEqual(Directive.objects.count(), count_before + 1)
        self.assertIsNotNone(next_round.directive)
        self.assertNotEqual(curr_round.directive, next_round.directive)

        selections = next_round.directive.selections.all()
        for i, item in enumerate(items):
            self.assertTrue(
                selections.get(article__number=i+1, items_selected=item)
            )

        # for good measure, test that the submit function was called
        mock_submit.assert_called_once()

    def test_directive_is_submitted_when_published(self):
        """
        When publishing a `Directive`, it should be submitted and thus the
        corresponding `DeadlineTask` should be marked as completed
        """
        team = Commission.objects.create(
            name='Commission',
            influence_points=100,
            static=CommissionStatic.objects.create(starting_ip=100)
        )
        deadline = DeadlineEvent.objects.create(
            offset=100,
            actor=ContentType.objects.get_for_model(team),
            action=ContentType.objects.get_for_model(Directive)
        )
        task = deadline.create_task(team)

        curr_round = Round.get_current_round()
        next_round = curr_round.get_following_round()
        items = [1, 2, 3, 4, 5, 1, 2, 3]

        res = self.query(
            '''
                mutation($items: [Int!]!, $roundId: ID!) {
                    publishDirective(items: $items, roundId: $roundId) {
                        didPublish
                        errors {
                            field
                            messages
                        }
                    }
                }
            ''',
            variables = {
                'items': items,
                'roundId': next_round.id
            }
        )

        self.assertResponseNoErrors(res)

        task.refresh_from_db()
        next_round.refresh_from_db()

        self.assertTrue(task.completed())
        self.assertEqual(task.get_submittable(), next_round.directive)
        self.assertTrue(next_round.directive.is_submitted)


class ResolveCreateStudentTestCase(GraphQLTestCase):
    """
    Tests related to `CreateStudentMutation` class in `core.schema`
    """
    GRAPHQL_URL = '/graphql'

    def setUp(self):
        """
        Setup a basic game for each test (probably only necessary in one test)
        """
        config = GameConfiguration.objects.create(name='TestConfig')
        start_time = timezone.datetime(2020, 12, 16, 11, 21, 0, 0)

        game = Game.objects.create(
            name='Test game',
            starting_time=timezone.make_aware(start_time),
            configuration=config,
            last_job_ordering=0
        )
        game.create_teams()

    def test_create_pending_student_before_gamestart_valid(self):
        """
        Test that the createStudent mutation, if called before gamestart, will
        create a new PendingStudent
        """
        count_before = PendingStudent.objects.count()

        res = self.query(
            '''
            mutation(
                $firstName: String!,
                $lastName: String!,
                $username: String!,
                $password: String!
                ) {
                  createStudent(
                    input:{
                        firstName: $firstName,
                        lastName: $lastName,
                        username: $username,
                        password: $password
                    }
                  ) {
                      user {
                        id
                      }
                      errors {
                        field
                        messages
                      }
                    }
                }
            ''',
            variables = {
                'firstName': 'Malthe',
                'lastName': 'Nielsen',
                'username': 'maltheAwesomeType',
                'password': 'hejJegErEtKodeord'
            }
        )

        self.assertResponseNoErrors(res)

        self.assertEqual(PendingStudent.objects.count(), count_before + 1)

        content = json.loads(res.content)
        query = content['data']['createStudent']
        user = PendingStudent.objects.get(id=int(query['user']['id']))

        self.assertEqual(user.first_name, 'Malthe')
        self.assertEqual(user.last_name, 'Nielsen')
        self.assertEqual(user.username, 'maltheAwesomeType')

    def test_create_pending_student_gives_error_when_username_exists(self):
        """
        Test that the createStudent mutation cant create another user with the
        same username as someone that already exists
        """

        PendingStudent.objects.create_user(
            first_name='Mads',
            last_name='Petersen',
            username='maltheAwesomeType',
            password='whatever'
        )

        count_before = PendingStudent.objects.count()

        res = self.query(
            '''
            mutation(
                $firstName: String!,
                $lastName: String!,
                $username: String!,
                $password: String!
                ) {
                  createStudent(
                    input:{
                        firstName: $firstName,
                        lastName: $lastName,
                        username: $username,
                        password: $password
                    }
                  ) {
                      user {
                        id
                      }
                      errors {
                        field
                        messages
                      }
                    }
                }
            ''',
            variables = {
                'firstName': 'Malthe',
                'lastName': 'Nielsen',
                'username': 'maltheAwesomeType',
                'password': 'hejJegErEtKodeord'
            }
        )

        self.assertResponseNoErrors(res)
        self.assertEqual(PendingStudent.objects.count(), count_before)

        content = json.loads(res.content)
        query = content['data']['createStudent']
        self.assertIsNone(query['user'])

        self.assertEqual(len(query['errors']), 1)
        self.assertEqual(query['errors'][0]['field'], 'username')
        self.assertEqual(
            query['errors'][0]['messages'],
            ['User with this Username already exists.']
        )

    @patch('core.schema.Game.get_next_job_template')
    def test_create_student_when_game_is_started(
            self, mock_get_next
    ):
        """
        Test that the createStudent mutation works even after the game has
        started, by creating a PendingStudent, fetching the next job template
        and creating a student from this (deleting the PendingStudent)
        """
        game = Game.get_current_game()
        game.game_started = True
        game.save()

        mock_job_template = Mock()
        mock_get_next.return_value = mock_job_template

        res = self.query(
            '''
            mutation(
                $firstName: String!,
                $lastName: String!,
                $username: String!,
                $password: String!
                ) {
                  createStudent(
                    input:{
                        firstName: $firstName,
                        lastName: $lastName,
                        username: $username,
                        password: $password
                    }
                  ) {
                      user {
                        id
                      }
                      errors {
                        field
                        messages
                      }
                    }
                }
            ''',
            variables = {
                'firstName': 'Malthe',
                'lastName': 'Nielsen',
                'username': 'maltheAwesomeType',
                'password': 'hejJegErEtKodeord'
            }
        )
        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        query = content['data']['createStudent']
        user = User.objects.get(id=int(query['user']['id']))

        mock_get_next.assert_called_once()
        mock_job_template.create_any_student.asser_called_once_with(
            user, delete_pending_student=True
        )

        self.assertEqual(user.first_name, 'Malthe')
        self.assertEqual(user.last_name, 'Nielsen')
        self.assertEqual(user.username, 'maltheAwesomeType')


class ResolveLoginTestCase(GraphQLTestCase):
    """
    Test the GraphQL login query
    """
    GRAPHQL_URL = '/graphql'

    def test_login_valid(self):
        """
        Test that the login query works for correct values of `username` and
        `password`
        """
        PendingStudent.objects.create_user(
            first_name='Malthe',
            last_name='Nielsen',
            username='maltheAwesomeType',
            password='whatever'
        )

        res = self.query(
            '''
            query($username:String!, $password:String!) {
                login(username:$username, password:$password)
            }
            ''',
            variables =  {
                'username': 'maltheAwesomeType',
                'password': 'whatever'
            }
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        self.assertTrue(content['data']['login'])

    def test_login_invalid(self):
        """
        Test that login fails if wrong password is used
        """
        PendingStudent.objects.create_user(
            first_name='Malthe',
            last_name='Nielsen',
            username='maltheAwesomeType',
            password='whatever'
        )

        res = self.query(
            '''
            query($username:String!, $password:String!) {
                login(username:$username, password:$password)
            }
            ''',
            variables = {
                'username': 'maltheAwesomeType',
                'password': 'wrong'
            }
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        self.assertFalse(content['data']['login'])


class DirectiveIndicationMutationTestCase(GraphQLTestCase):
    """
    Test `DirectiveIndicationMutation` in `core/schema.py`
    """

    GRAPHQL_URL = '/graphql'

    query_str = """
        mutation($indications: [Indication!]!, $directive: ID!) {
            makeDirectiveIndication(
                input: {
                    targetDirective: $directive,
                    indications: $indications
                }
            ) {
                directiveIndication {
                    id
                    indications
                }
                errors {
                    field
                    messages
                }
            }
        }
    """

    def setUp(self):
        """
        Create the necessary `DirectiveArticle`s
        """
        for i in range(1, 9):
            art = DirectiveArticle.objects.create(number=i)
            for j in range(1, 6):
                DirectiveItem.objects.create(number=j, article=art)


        Round.objects.create(
            active=True,
            order=1,
            directive=Directive.create_first_directive()
        )
        Round.objects.create(
            active=False,
            order=2,
            directive=None
        )
        team = Country.objects.create(
            influence_points=100,
            static=CountryStatic.objects.create(
                starting_ip=100,
                population=10
            )
        )
        user = Secretary.objects.create_user(
            username='someDude',
            password='whatever',
            team=team
        )
        self.client.force_login(user)

    @patch('core.schema.DirectiveIndication.submit')
    def test_valid_query_creates_new_DirectiveIndication_object(
            self, mock_submit
    ):
        """
        A valid query should create a new `DirectiveIndication` and return this
        """
        indications_before = DirectiveIndication.objects.count()
        directive = Round.get_current_round().directive

        # arbitrary selection
        selections = [
            'MORE', 'LESS', 'MORE', 'OK', 'LESS', 'MORE', 'OK', 'MORE'
        ]

        res = self.query(
            self.query_str,
            variables = {
                'directive': directive.id,
                'indications': selections
            }
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        data = content['data']['makeDirectiveIndication']

        self.assertEqual(
            DirectiveIndication.objects.count(),
            indications_before + 1
        )

        indication = DirectiveIndication.objects.last()
        self.assertEqual(indication.id, int(data['directiveIndication']['id']))
        self.assertEqual(selections, data['directiveIndication']['indications'])

        # patched to make stuff work, so lets just check it (more in next test)
        mock_submit.assert_called_once()

    def test_DirectiveIndication_is_submitted(self):
        """
        As part of the mutation, the indication should be submitted
        """
        indications_before = DirectiveIndication.objects.count()
        directive = Round.get_current_round().directive

        deadline = DeadlineEvent.objects.create(
            offset=100,
            actor=ContentType.objects.get_for_model(Country),
            action=ContentType.objects.get_for_model(DirectiveIndication)
        )
        task = deadline.create_task(Country.objects.first())

        # arbitrary selection
        selections = [
            'MORE', 'LESS', 'MORE', 'OK', 'LESS', 'MORE', 'OK', 'MORE'
        ]

        res = self.query(
            self.query_str,
            variables = {
                'directive': directive.id,
                'indications': selections
            }
        )

        self.assertResponseNoErrors(res)

        content = json.loads(res.content)
        data = content['data']['makeDirectiveIndication']

        task.refresh_from_db()
        indication = DirectiveIndication.objects.get(
            id=int(data['directiveIndication']['id'])
        )

        self.assertTrue(indication.is_submitted)
        self.assertTrue(task.completed())

    @patch('core.schema.DirectiveIndication.submit')
    def test_indicating_to_same_directive_twice_gives_error(
            self, mock_submit
    ):
        """
        It should not be allowed to indicate to the same Directive twice
        """
        indications_before = DirectiveIndication.objects.count()
        directive = Round.get_current_round().directive

        # arbitrary selection
        selections = [
            'MORE', 'LESS', 'MORE', 'OK', 'LESS', 'MORE', 'OK', 'MORE'
        ]

        res = self.query(
            self.query_str,
            variables = {
                'directive': directive.id,
                'indications': selections
            }
        )

        self.assertResponseNoErrors(res)

        # check submit was called and then reset mock
        mock_submit.assert_called_once()
        mock_submit.reset_mock()

        selections[0] = 'LESS'
        # send it again with new selection
        res = self.query(
            self.query_str,
            variables = {
                'directive': directive.id,
                'indications': selections
            }
        )

        content = json.loads(res.content)
        self.assertIn(
            'duplicate key value violates unique constraint',
            content['errors'][0]['message']
        )

        # this time, submit should not have been called
        mock_submit.assert_not_called()


class ResolveDirectiveIndicationsTestCase(GraphQLTestCase):
    """
    Test `core.schema.Query.resolve_directive_indications`
    """

    GRAPHQL_URL = '/graphql'

    QUERY_STR = """
        query {
            directiveIndications {
                indications
                by {
                    name
                }
            }
        }
    """

    def setUp(self):
        """
        Create a basic setup with a round, a directive and threee teams
        """
        for i in range(1, 9):
            art = DirectiveArticle.objects.create(number=i)
            for j in range(1, 6):
                DirectiveItem.objects.create(number=j, article=art)


        directive = Directive.create_first_directive()
        Round.objects.create(
            active=True,
            order=1,
            directive=directive
        )

        Country.objects.create(
            name='A-land',
            influence_points=100,
            static=CountryStatic.objects.create(
                name='A-land',
                population=10,
                starting_ip=100
            )
        )
        Country.objects.create(
            name='B-land',
            influence_points=100,
            static=CountryStatic.objects.create(
                name='B-land',
                population=10,
                starting_ip=100
            )
        )
        ParliamentGroup.objects.create(
            name='Red Group',
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                name='Red Group',
                member_count=10,
                starting_ip=100
            )
        )
        ParliamentGroup.objects.create(
            name='Blue Group',
            influence_points=100,
            static=ParliamentGroupStatic.objects.create(
                name='Blue Group',
                member_count=10,
                starting_ip=100
            )
        )

        for team in Team.objects.exclude(name='Blue Group'):
            indications = [random.randint(0, 2) for _ in range(8)]
            DirectiveIndication.objects.create(
                by=team,
                indications=indications,
                target_directive=directive
            )

    def test_query_resolves_and_returns_indications_list(self):
        """
        Test that a simple query will fetch a list of indications for the
        current directive
        """
        res = self.query(self.QUERY_STR)

        # assert no errors
        self.assertResponseNoErrors(res)

        content = json.loads(res.content)

        # define a list of all team names to check against
        names = ['A-land', 'B-land', 'Red Group', 'Blue Group']

        for obj in content['data']['directiveIndications']:
            # indications list should have length 8
            self.assertEqual(len(obj['indications']), 8)

            # the team name should be in our defined list
            self.assertIn(obj['by']['name'], names)

            # remove the name from the list
            names.remove(obj['by']['name'])

        # only blue group should be left as they didn't indicate
        self.assertEqual(names, ['Blue Group'])
