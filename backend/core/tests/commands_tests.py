"""
Tests related to commands for `core`
"""
# pylint: disable=too-many-arguments, unused-argument, no-self-use
import logging
from datetime import timedelta
from unittest.mock import Mock, patch

from django.core import management
from django.test import TestCase
from django.utils import timezone

CMD_PATH = 'core.management.commands.runtasks'

@patch(f'{CMD_PATH}.award_special_success_criteria_points')
@patch(f'{CMD_PATH}.handle_deadlines')
@patch(f'{CMD_PATH}.start_game_task')
@patch(f'{CMD_PATH}.FinalTeamVote.voting_complete')
@patch(f'{CMD_PATH}.Round.get_current_round')
@patch(f'{CMD_PATH}.Game.get_current_game')
class RuntasksTestCase(TestCase):
    """
    Test the `runtasks` command, that ensures timed events are executed in due
    time
    """

    def setUp(self):
        """
        The command send output to pythons `logging` module, which we want to
        disable during testing
        """
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        """
        Re-enable logging when testing is done
        """
        logging.disable(logging.NOTSET)

    def test_no_tasks_are_executed_if_game_is_None(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If game is `None`, (eg. when `runtasks` is called the first time on the
        slave server before another script has had time to create a game) then
        no tasks should be executed
        """
        mock_get_game.return_value = None

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()
        mock_handle_deadlines.assert_not_called()
        mock_award.assert_not_called()

    def test_start_game_called_if_starting_time_is_now_and_not_yet_started(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If game is not None, starting time is now and the game has not yet been
        started, then `runtasks` should call the `start_game_task` task
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now(),
            game_started=False
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_called_once()

    def test_start_game_called_if_past_starting_time_and_not_yet_started(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If game is not None, the game has not yet been started and the starting
        time is in the past, the `start_game_task` task should run
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now() - timedelta(minutes=1),
            game_started=False
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_called_once()

    def test_start_game_not_called_if_game_is_none(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If `Game.get_current_game()` returns `None`, then `runtasks` should
        terminate without running the `start_game_task` task (and without
        crashing)
        """
        mock_get_game.return_value = None

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()

    def test_start_game_not_called_if_game_started(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If the game is already started, the `start_game_task` task should not
        run (no matter the starting time)
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now() - timedelta(minutes=1),
            game_started=True
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()

        # try again a minute later
        mock_get_game.return_value = Mock(
            starting_time=timezone.now(),
            game_started=True
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()

        # and again another minute later
        mock_get_game.return_value = Mock(
            starting_time=timezone.now() + timedelta(minutes=1),
            game_started=True
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()

    def test_start_game_not_called_if_starting_time_in_future(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If the starting time is in the future, the `start_game_task` task should
        not run
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now() + timedelta(minutes=1),
            game_started=False
        )

        management.call_command('runtasks')

        mock_start_game_task.assert_not_called()

    def test_award_called_if_voting_complete_task_not_ran(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If voting is completed and the task
        `award_special_success_criteria_points` havent yet ran, then it should
        run
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now(),
            game_started=True
        )

        mock_get_round.return_value = Mock(
            special_success_criteria_task_ran=False
        )
        mock_vote_complete.return_value = True

        management.call_command('runtasks', verbosity=0)

        mock_award.assert_called_once()

    def test_award_not_called_if_final_round_and_not_voting_complete(
        self, mock_get_game, mock_get_round, mock_vote_complete,
        mock_start_game_task, mock_handle_deadlines, mock_award
    ):
        """
        If voting is not completed, then `award_special_success_criteria_points`
        should not run, even if it is the final round
        """
        mock_get_game.return_value = Mock(
            starting_time=timezone.now(),
            game_started=True
        )

        mock_get_round.return_value = Mock(is_final=True)
        mock_vote_complete.return_value = False

        management.call_command('runtasks', verbosity=0)

        mock_award.assert_not_called()
