"""
This import adds and overrides some Graphene converters.
This is done in __init__ to ensure that no app Graphene schemas are loaded before,
as this is essentially advanced monkeypatching.
"""
import graphene_converters # pylint: disable=unused-import


default_app_config = 'core.apps.CoreConfig'
