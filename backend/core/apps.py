""" App """
from django.apps import AppConfig
import vinaigrette


class CoreConfig(AppConfig):
    """ Core config """
    name = 'core'

    def ready(self):
        from .models import (
            DirectiveArticle, DirectiveItem, LobbyOrganizationStatic,
            CommissionStatic,
        )
        vinaigrette.register(DirectiveArticle, ['description', 'short_description'])
        vinaigrette.register(DirectiveItem, ['text'])

        vinaigrette.register(LobbyOrganizationStatic, ['name', 'special_success_criteria',
                                                       'video_embed_url'])
        vinaigrette.register(CommissionStatic, ['name', 'special_success_criteria',
                                                'video_embed_url'])
