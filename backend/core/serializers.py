""" Core serializers """
from rest_framework import serializers

from core.models import (
    User, Team, Student, DirectiveArticle, DirectiveItem, Directive,
    ArticleSelection,
)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """ Base user serializer. """
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'is_staff')


class StudentSerializer(serializers.ModelSerializer):
    """ Student serializer. """
    class Meta:
        model = Student
        fields = ('id', 'team', 'first_name', 'last_name', 'email')


class TeamSerializer(serializers.ModelSerializer):
    """ Team serializer """
    students = StudentSerializer(many=True)

    class Meta:
        model = Team
        fields = ('id', 'name', 'students', 'influence_points', 'starting_room', 'logo')


class DirectiveArticleSerializer(serializers.ModelSerializer):
    """ Serializer of a directive template article """

    class ItemSerializer(serializers.ModelSerializer):
        """ Item serializer """
        class Meta:
            model = DirectiveItem
            fields = ('id', 'number', 'text')

    items = ItemSerializer(many=True)

    class Meta:
        model = DirectiveArticle
        fields = ('id', 'number', 'description', 'items', 'short_description')


class DirectiveSerializer(serializers.ModelSerializer):
    """ Directive serializer """

    class SelectionSerializer(serializers.ModelSerializer):
        """ Article selection serializer providing only the basic """
        class Meta:
            model = ArticleSelection
            fields = ("id", "article", "items_selected")

    selections = SelectionSerializer(many=True)

    class Meta:
        model = Directive
        fields = ('id', 'selections')
        depth = 1
