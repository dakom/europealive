""" Core admin """
# pylint: disable=missing-docstring
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.timezone import get_current_timezone

from .models import (Advisor, ArticleSelection, Chief, Commission,
                     CommissionStatic, Directive, DirectiveArticle,
                     DirectiveIndication, DirectiveItem, Facilitator,
                     LobbyOrganization, LobbyOrganizationStatic,
                     PendingStudent, Secretary, Strategist, Student, Teacher,
                     Team, TeamPriority, User)

# admin.site.register(PendingStudent)
admin.site.register(LobbyOrganization)
admin.site.register(LobbyOrganizationStatic)
admin.site.register(Commission)
admin.site.register(CommissionStatic)
admin.site.register(TeamPriority)


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    change_form_template = 'loginas/change_form.html'
    list_display = ('username', 'first_name', 'last_name')
    search_fields = ['username', 'first_name', 'last_name']


class StudentAdminMixin(UserAdmin):
    fieldsets = (
        ('User', { 'fields': ('username', 'password') }),
        ('Personal info', { 'fields': ('first_name', 'last_name', 'email' ) }),
        ('Miscalleneous', {
            'classes': ('collapse',),
            'fields': (
                'is_active', 'is_staff', 'is_superuser',
                'date_joined', 'last_login'
            )
        }),
        ('Student', {
            'fields': ('team', 'job_title_raw','job_description_raw')
        }),
    )

    add_fieldsets = UserAdmin.add_fieldsets + (
        ('Student', {
            'fields': ('team', 'job_title_raw', 'job_description_raw')
        }),
    )

    change_form_template = 'loginas/change_form.html'
    list_display = ('username', 'first_name', 'last_name', 'team', 'job')
    search_fields = ['first_name', 'last_name']
    list_filter = ('team', )


@admin.register(PendingStudent)
class PendingStudentAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name')


@admin.register(Student)
class StudentAdmin(StudentAdminMixin):
    pass


@admin.register(Teacher)
class TeacherAdmin(CustomUserAdmin):
    pass


@admin.register(Facilitator)
class FacilitatorAdmin(CustomUserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        ('Partner', {
            'fields': ('partner_organization',)
        }),
    )

    add_fieldsets = UserAdmin.add_fieldsets + (
        ('Partner', {
            'fields': ('partner_organization',)
        }),
    )


class TeamPriorityInline(admin.StackedInline):
    model = TeamPriority
    extra = 1
    max_num = 3


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    inlines = [TeamPriorityInline]


class DirectiveItemAdmin(admin.StackedInline):
    model = DirectiveItem
    extra = 0
    min_num = 5


@admin.register(DirectiveArticle)
class DirectiveArticleAdmin(admin.ModelAdmin):
    inlines = [DirectiveItemAdmin]


class ArticleSelectionAdmin(admin.StackedInline):
    model = ArticleSelection
    extra = 0
    min_num = 8


@admin.register(Directive)
class DirectiveAdmin(admin.ModelAdmin):
    def gameround(self, obj):
        return obj.round_set.first()

    list_display = ( '__str__', 'creation_time', 'gameround' )
    inlines = [ArticleSelectionAdmin]
    readonly_fields = ['creation_time']


@admin.register(DirectiveIndication)
class DirectiveIndicationAdmin(admin.ModelAdmin):
    pass


@admin.register(Secretary)
class SecretaryAdmin(StudentAdminMixin):
    pass


@admin.register(Strategist)
class StrategistAdmin(StudentAdminMixin):
    pass


@admin.register(Chief)
class ChiefAdmin(StudentAdminMixin):
    pass


@admin.register(Advisor)
class AdvisorAdmin(StudentAdminMixin, admin.ModelAdmin):
    fieldsets = StudentAdminMixin.fieldsets + (
        ('Advisor', {
            'fields': ('kind',)
        }),
    )
