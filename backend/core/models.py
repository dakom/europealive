""" Models shared by most or all of project """
import random
from abc import abstractmethod

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from enumfields import Enum, EnumIntegerField
from model_utils.managers import InheritanceManager

from events.mixins import DeadlineActorMixin
from events.models import Submittable

SUCCESS_CRITERIA_CHOICES = [
    ("article 1", "Article 1"),
    ("article 2", "Article 2"),
    ("article 3", "Article 3"),
    ("article 4", "Article 4"),
    ("article 5", "Article 5"),
    ("article 6", "Article 6"),
    ("article 7", "Article 7"),
    ("article 8", "Article 8"),
    ("commission special", "Commission special"),
    ("none", "None"),
]

class User(AbstractUser):
    """
    Base user class inherited from by all users
    """
    # Override AbstractUser fields to make important fields mandatory
    username = models.CharField(_('username'), unique=True, max_length=35)
    email = models.EmailField(_('email address'), null=True)
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    # End of overrides

    @property
    def full_name(self):
        """ Concatenates first and last name """
        return f'{self.first_name} {self.last_name}'


class TeamStaticBase(models.Model):
    """
    Contains the information about a team which does not change between games.
    Inherited by all team subclass statics.
    """
    name = models.CharField(max_length=200, verbose_name="navn")
    starting_ip = models.IntegerField()

    special_success_criteria = models.TextField(
        null=True,
        max_length=2000
    )

    special_success_criteria_value = models.CharField(
        max_length=50, choices=SUCCESS_CRITERIA_CHOICES,
        help_text="Technical resolution of success criteria")
    video_embed_url = models.URLField(
        max_length=500,
        help_text="Link to an embeddable video, for example \
                https://player.vimeo.com/video/12345789"
    )
    starting_priorities = models.ManyToManyField("core.DirectiveArticle")
    staff_page = models.ForeignKey(
        'game.MyStaffPage',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text='Overview page for the Chief of Staff'
    )
    logo = models.FileField(upload_to="team", help_text=".svg file if possible, else image.")

    class Meta:
        verbose_name = "team static"
        verbose_name = "team statics"
        abstract = True

    def __str__(self):
        return self.name

    def get_team_fields(self):
        """ Returns the fields needed to initiate a team from static info. """
        return {
            "name": self.name,
            "influence_points": self.starting_ip,
            "special_success_criteria": self.special_success_criteria,
            "special_success_criteria_value": self.special_success_criteria_value,
            "video_embed_url_raw": self.video_embed_url,
            "logo": self.logo,
            "static": self,
        }

    @property
    @abstractmethod
    def team_class(self):
        """ Should return team class, ie ParliamentGroup for ParliamentGroupStatic. """

    def create_team(self):
        """
        This method creates a team from the static information.
        Is called at start of first round.
        All TeamStaticBase subclasses must define get_team_class or override
        this method.
        """
        cls = self.team_class
        new_team = cls(**self.get_team_fields())
        new_team.save()
        for priority in self.starting_priorities.all():
            TeamPriority.objects.create(
                team=new_team,
                article=priority,
            )


class Team(DeadlineActorMixin):
    """ A team of participants competing together """
    objects = InheritanceManager()

    name = models.CharField(max_length=200, verbose_name="navn")
    room = models.ForeignKey(
        'game.Room',
        on_delete=models.SET_NULL,
        related_name='teams',
        null=True,
        blank=True
    )
    special_success_criteria = models.CharField(
        max_length=2000,
        help_text="description of success criteria"
    )
    special_success_criteria_value = models.CharField(
        max_length=50,
        choices=SUCCESS_CRITERIA_CHOICES,
        help_text="Technical resolution of success criteria"
    )
    video_embed_url_raw = models.URLField(
        max_length=500,
        help_text="Untranslated link to an embeddable video, for example \
                https://player.vimeo.com/video/12345789"
    )

    # This field is DE-NORMALIZED for easier querying. Should be calculatable
    # from starting points and the PointTransaction log.
    influence_points = models.IntegerField(
        verbose_name="influence points"
    )

    logo = models.FileField(
        upload_to="team",
        help_text=".svg file if possible, else image."
    )

    def __str__(self):
        return self.name

    def get_subclass(self):
        """ Return the team type instance (Country, ParliamentGroup, etc.) of
        this team. Returns None if it has no type """
        types = [
            'commission',
            'country',
            'parliamentgroup',
            'lobbyorganization',
            'mediahouse'
        ]
        for t in types:
            if hasattr(self, t):
                return getattr(self, t)
        return None

    class Meta:
        verbose_name = "team"
        verbose_name_plural = "teams"

    @property
    def video_embed_url(self):
        """ Gets correct video url for chosen language """
        return _(self.video_embed_url_raw)

    @property
    def starting_room(self):
        """
        String rep of team room
        """
        if self.room is None:
            return '-'
        return self.room.name


class PendingStudent(User):
    """
    A student who has not yet been assigned a job.
    Usually a student signs up before game start, and is assigned a job
    upon game start.
    """
    def __str__(self):
        return "PendingStudent " + str(self.username)

    @property
    def user(self):
        """ Get underlying user object """
        return User.objects.get(username=self.username)

    class Meta:
        verbose_name = "pending student"
        verbose_name_plural = "pending students"


class UserInheritanceManager(InheritanceManager, BaseUserManager):
    """ Adds user manager functionality to the inheritance manager """

    def create_user(self, username, password=None, *args, **kwargs):
        """
        Override `create_user` so it can be called from any student object
        """
        user = self.model(username=username, *args, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user


class Student(User, DeadlineActorMixin):
    """
    A user competing in the game.
    A Student's username should always be equal to its email.
    """
    objects = UserInheritanceManager()

    team = models.ForeignKey(
        Team,
        verbose_name="team",
        related_name="students",
        on_delete=models.CASCADE,
    )
    
    job_description_raw = models.TextField(help_text="Untranslated job description")
    job_title_raw = models.CharField(help_text="Untranslated job title ", max_length=50)
    
    def __str__(self):
        return "Student " + self.full_name

    @property
    def user(self):
        """ Get underlying user object """
        return User.objects.get(username=self.username)

    class Meta:
        verbose_name = "student"
        verbose_name_plural = "students"

    @property
    def job_description(self):
        """
        Translate the job description.
        The msgid is in languages files already because it is marked on the
        StudentJobTemplate the student was created from.
        """
        return _(self.job_description_raw)
    
    @property
    def job_title(self):
        """
        Translate the job title.
        The msgid is in languages files already because it is marked on the
        StudentJobTemplate the student was created from.
        """
        return _(self.job_title_raw)

    @property
    def job(self):
        """
        Return the student job function
        """
        if hasattr(self, 'advisor'):
            return f'{self.advisor.kind.capitalize()} Advisor'

        for job in ['Chief', 'Journalist', 'Secretary', 'Strategist']:
            if hasattr(self, job.lower()):
                return job

        return 'No job found'


class Teacher(User):
    """
    A Teacher supervising the game.
    A teacher's username should always be equal to its email.
    """

    def __str__(self):
        return "Teacher " + self.full_name

    @property
    def user(self):
        """ Get underlying user object """
        return User.objects.get(username=self.username)

    class Meta:
        verbose_name = "teacher"
        verbose_name_plural = "teachers"


class Facilitator(Teacher):
    """
    An admin user that has access to certain parts of the backend in order to
    be able to facilitate the game. Is associated to a Partner organization via
    the APIUser relationsship so that only Facilitators involved in a given game
    gets access to that specific instance.
    """
    partner_organization = models.ForeignKey(
        'gamelauncher.APIUser',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return 'Facilitator ' + self.full_name

    class Meta:
        verbose_name = 'facilitator'
        verbose_name_plural = 'facilitators'


class DirectiveArticle(models.Model):
    """ An article of the directive template """
    number = models.SmallIntegerField(unique=True, verbose_name="nummer")
    description = models.TextField(verbose_name="beskrivelse")
    short_description = models.CharField(max_length=500, verbose_name="kort beskrivelse")

    def __str__(self):
        return f'Article {self.number}: {self.short_description}'

    class Meta:
        verbose_name = "directive article"
        verbose_name_plural = "directive articles"
        ordering = ["number"]


class DirectiveItem(models.Model):
    """ One item of an article """
    text = models.TextField(verbose_name="tekst")
    number = models.SmallIntegerField(verbose_name="nummer")
    article = models.ForeignKey(DirectiveArticle, on_delete=models.CASCADE, related_name="items")

    def __str__(self):
        return f'DirectiveItem {self.article.number}.{self.number}'

    class Meta:
        verbose_name = "directive item"
        verbose_name_plural = "directive items"
        unique_together = ("number", "article")
        ordering = ["article__number", "number"]


class Directive(Submittable):
    """
    A proposed directive modeled as a number of chosen items per article.
    Should have an ArticleSelection for each article.
    """
    creation_time = models.DateTimeField(
        auto_now_add=True,
        help_text="automatically set when created"
    )

    def __str__(self):
        r = self.round_set.first()
        if r is None:
            return 'Directive (no round)'

        if r.order == 1:
            return 'Initial Directive proposal'
        elif r.order == 2:
            return 'First Directive draft'
        elif r.order == 3:
            return 'Second Directive draft'
        elif r.is_final:
            return 'Final Directive'
        else:
            return f'Directive {r.order}'

    def submitted_by(self):
        """
        A Directive is always submitted by the Commission.
        Implemented according to specification in `Submittable`.
        """
        return Commission.objects.first()

    @classmethod
    def create_random_mutation(cls, old_directive):
        """
        Returns a new directive based on given old, in which each article is
        randomly increased, decreased or stays the same with equal probability.
        """
        directive = cls()
        directive.save()
        for old_selection in old_directive.selections.all():

            items_selected = old_selection.items_selected
            selection_options = [items_selected - 1, items_selected,
                                 items_selected + 1]
            items_selected = selection_options[random.randrange(3)]
            items_selected = max(min(items_selected, 8), 1)

            selection = ArticleSelection(
                article=old_selection.article,
                items_selected=items_selected,
                directive=directive,
            )
            selection.save()

        directive.save()
        return directive

    @classmethod
    def create_first_directive(cls):
        """
        Create and return an initial directive draft, where item 2 is selected for all articles.
        """
        directive = cls()
        directive.save()

        for article in DirectiveArticle.objects.all():
            ArticleSelection.objects.create(
                article=article,
                items_selected=2,
                directive=directive,
            )
        return directive

    class Meta:
        verbose_name = "directive"
        verbose_name_plural = "directives"


class TeamPriority(models.Model):
    """ All teams have three priority articles which they can invest IP in """
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="priorities")
    article = models.ForeignKey(DirectiveArticle, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "team priority"
        verbose_name_plural = "team priorities"
        ordering = ["article__number"]

    def __str__(self):
        return f'{self.article} ({self.team.name})'


class ArticleSelection(models.Model):
    """ A directive consists of a selection of a single Directive Item for
    each of the DirectiveArticles. """
    article = models.ForeignKey(DirectiveArticle, on_delete=models.PROTECT)
    items_selected = models.SmallIntegerField(
        verbose_name="antal punkter", validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ])
    directive = models.ForeignKey(Directive, related_name="selections", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.article.number}:{self.items_selected} ({self.directive})'

    class Meta:
        verbose_name = "article selection"
        verbose_name_plural = "article selections"
        unique_together = ("directive", "article")
        ordering = ["article__number"]


class Indication(Enum):
    """ The possible wishes a team can indicate on a directive article """
    LESS = 0
    OK = 1
    MORE = 2


class DirectiveIndication(Submittable):
    """ An indication for each article in a directive """
    by = models.ForeignKey(Team, on_delete=models.CASCADE)
    indications = ArrayField(EnumIntegerField(Indication), size=8)
    target_directive = models.ForeignKey(Directive, on_delete=models.CASCADE)

    @property
    def directive(self):
        return self.target_directive

    @directive.setter
    def directive(self, directive):
        self.target_directive = directive

    def submitted_by(self):
        return self.by.get_subclass()

    def __str__(self):
        return f'Indication by {self.by} for {self.directive}'

    class Meta:
        verbose_name = "directive indication"
        verbose_name_plural = "directive indications"
        unique_together = ("by", "target_directive")


class LobbyOrganization(Team):
    """ A Team of students playing as lobbyists """
    static = models.OneToOneField("LobbyOrganizationStatic",
                                  on_delete=models.PROTECT)
    def __str__(self):
        return f'Lobby Organization {self.name}'

    class Meta:
        verbose_name = "lobby organization"
        verbose_name_plural = "lobby organizations"


class LobbyOrganizationStatic(TeamStaticBase):
    """ Static information about a lobbyist team """
    team_class = LobbyOrganization

    class Meta:
        verbose_name = "lobby organization static"
        verbose_name_plural = "lobby organization statics"


class Commission(Team):
    """ The team playing as the European Commission.
    Only one should exist in a game. """
    static = models.OneToOneField("CommissionStatic",
                                  on_delete=models.PROTECT)

    @classmethod
    def get_commission(cls):
        """ There can only be one commission, so simply returns first """
        return cls.objects.first()

    def __str__(self):
        return "The European Commission"

    class Meta:
        verbose_name = "european commission"
        verbose_name_plural = "european commissions"


class CommissionStatic(TeamStaticBase):
    """
    Static information about commission.
    This model is a little artificial, since there is always one commission.
    We have it anyway for it to be homogenous with other team models.
    """
    team_class = Commission

    class Meta:
        verbose_name = "commission static"
        verbose_name_plural = "commission statics"


class Secretary(Student):
    """ A student who is a secretary of their team """
    class Meta:
        verbose_name = "secretary"
        verbose_name_plural = "secretaries"


KIND_CHOICES = (
    ('technical', 'Technical'),
    ('political', 'Political'),
    ('attache', 'Attaché'),
    ('media', 'Media'),
)

class Advisor(Student):
    """ A student who is any kind of advisor in their team """
    # kind = EnumIntegerField(AdvisorKind)
    kind = models.CharField(max_length=100, choices=KIND_CHOICES)

    def get_subtype(self):
        return getattr(self, f'{self.kind}advisor')

    class Meta:
        verbose_name = "advisor"
        verbose_name_plural = "advisors"


class TechnicalAdvisor(Advisor):
    """
    Subclassing to have every type of role to be its own class
    """

    def save(self, *args, **kwargs):
        if not self.pk:
            self.kind = 'technical'
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'technical advisor'
        verbose_name = 'technical advisors'


class PoliticalAdvisor(Advisor):
    """
    Subclassing to have every type of role to be its own class
    """

    def save(self, *args, **kwargs):
        if not self.pk:
            self.kind = 'political'
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'political advisor'
        verbose_name = 'political advisors'


class AttacheAdvisor(Advisor):
    """
    Subclassing to have every type of role to be its own class
    """

    def save(self, *args, **kwargs):
        if not self.pk:
            self.kind = 'attache'
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'attache advisor'
        verbose_name = 'attache advisors'


class MediaAdvisor(Advisor):
    """
    Subclassing to have every type of role to be its own class
    """

    def save(self, *args, **kwargs):
        if not self.pk:
            self.kind = 'media'
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'media advisor'
        verbose_name = 'media advisors'


class Chief(Student):
    """ A student who is a chief of staff of their team """
    class Meta:
        verbose_name = "chief"
        verbose_name_plural = "chiefs"


class Strategist(Student):
    """ A student who is a Minister or similar strategy role in their team """
    class Meta:
        verbose_name = "strategist"
        verbose_name_plural = "strategists"


class Minister(Strategist):
    """
    A Strategist for a Country. Enables events directly minded at ministers
    """
    class Meta:
        verbose_name = 'minister'
        verbose_name_plural = 'ministers'


class SpokesPerson(Strategist):
    """
    A Strategist for a ParliamentGroup. Enables events directly minded at
    spokespersons
    """
    class Meta:
        verbose_name = 'spokesperson'
        verbose_name_plural = 'spokespersons'
