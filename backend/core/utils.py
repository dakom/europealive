
def custom_loginas(request, target_user):
    """
    custom validation function for `loginas' functionality, that allows
    members of the Spilleder group to login as other users (except superusers)
    """
    user = request.user
    if user.is_superuser:
        return True

    if user.groups.filter(name='Spilleder').exists() and \
            not target_user.is_superuser:
        return True

    return False
