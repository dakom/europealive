""" Forms fore core models, intended for use by Graphene not Django rendering. """
from django.contrib.postgres.forms import SplitArrayField, SplitArrayWidget
from django.forms import ModelForm
from enumfields import EnumIntegerField

from core.models import (DirectiveIndication, Indication, PendingStudent,
                         Teacher)
from core.shared_schema import DirectiveIndicationType


class SplitArrayGrapheneWidget(SplitArrayWidget):
    """ Convert POST data into normal format before calling sub-widget """
    def value_from_datadict(self, data, files, name):
        for index, value in enumerate(data[name]):
            data['%s_%s' % (name, index)] = value
        return super().value_from_datadict(data, files, name)


class SplitArrayGrapheneField(SplitArrayField):
    """ Set default widget to Graphene converter """
    def __init__(self, base_field, size, *args, **kwargs):
        kwargs["widget"] = SplitArrayGrapheneWidget(widget=base_field.widget, size=size)
        super().__init__(base_field, size, *args, **kwargs)


def mk_enuminteger_graphene_field(enum, graphene_type):
    """ Wraps EnumIntegerField formfield creation.
    Graphene cannot handle deduplicating Enum types during form building,
    so we need direct reference to type here. """
    field = EnumIntegerField(enum).formfield()
    field.type = graphene_type
    return field


class DirectiveIndicationForm(ModelForm):
    """ Form used by Graphene mutation """
    indications = SplitArrayGrapheneField(
        mk_enuminteger_graphene_field(
            Indication,
            # .of_type unwrap Graphene NonNull and List
            DirectiveIndicationType._meta.fields["indications"].type.of_type.of_type
        ),
        size=8
    )

    class Meta:
        model = DirectiveIndication
        fields = ["indications", "target_directive"]


class PendingStudentForm(ModelForm):
    """ Form used by Graphene mutation """
    class Meta:
        model = PendingStudent
        fields = ["first_name", "last_name", "username", "password"]


class TeacherForm(ModelForm):
    """ Form used by Graphene mutation """
    class Meta:
        model = Teacher
        fields = ["first_name", "last_name", "username", "password"]
